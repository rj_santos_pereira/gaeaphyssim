#version 330 core

layout(location = 0) in vec2 v_vert_pos;
layout(location = 1) in vec2 v_vert_uv;

out vec2 f_vert_uv;

void main()
{
	gl_Position = vec4(v_vert_pos, 0.0, 1.0);

	f_vert_uv = v_vert_uv;
}
