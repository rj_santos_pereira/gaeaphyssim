#version 330 core

in vec2 f_vert_uv;

uniform sampler2D u_tex_sampler;
uniform vec3 u_color = vec3(1.0);

layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 tex_color = texture(u_tex_sampler, f_vert_uv);
    
    if (tex_color.rgb == vec3(0.0))
    {
        discard;
    }

    vec4 mix_color = tex_color * vec4(u_color, 1.0);

    frag_color = mix_color;
} 
