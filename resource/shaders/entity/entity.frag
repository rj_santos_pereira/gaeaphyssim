#version 330 core

in vec3 f_vert_pos;
in vec3 f_vert_norm;
in vec2 f_vert_uv;

in vec4 f_color;

uniform sampler2D u_tex_sampler;

uniform vec3 u_view_pos = vec3(0.0);
uniform vec3 u_light_pos = vec3(0.0);

uniform vec3 u_ambient_color = vec3(1.0);
uniform vec3 u_diffuse_color = vec3(0.0);
uniform vec3 u_specular_color = vec3(0.0);

// This should be changed per body
const float c_shininess = 32;

layout(location = 0) out vec4 frag_color;

void main()
{
    // We're simply using this to set the mesh visibility;
    // this allows us to adjust visibility per instance
    if (f_color.a == 0.0)
    {
        discard;
    }

    // Do a linear interpolation between the color and the texture,
    // using the texture's alpha to determine the final frag color
    vec4 tex_color = texture(u_tex_sampler, f_vert_uv);
    vec3 mix_color = mix(f_color.rgb, tex_color.rgb, tex_color.a);

    // Calculate light direction
    vec3 light_dir = normalize(f_vert_pos - u_light_pos);

    // Calculate diffuse lighting
    float diffuse_factor = max(dot(f_vert_norm, -light_dir), 0.0);
    vec3 diffuse_color = diffuse_factor * u_diffuse_color;

    // Calculate view direction
    vec3 view_dir = normalize(f_vert_pos - u_view_pos);

    // Calculate reflection direction
    vec3 reflect_dir = reflect(light_dir, f_vert_norm);

    // Calculate specular lighting
    float specular_factor = pow(max(dot(reflect_dir, -view_dir), 0.0), c_shininess);
    vec3 specular_color = specular_factor * u_specular_color;

    frag_color = vec4((u_ambient_color + diffuse_color + specular_color) * mix_color, 1.0);
} 
