#version 330 core

// Per-vertex
layout(location = 0) in vec3 v_vert_pos;
layout(location = 1) in vec3 v_vert_norm;
layout(location = 2) in vec2 v_vert_uv;

// Per-instance
layout(location = 3) in vec4 v_color;
layout(location = 4) in mat4 v_model_mat;
layout(location = 8) in mat4 v_normal_mat;

uniform mat4 u_view_proj_mat;

out vec3 f_vert_pos;
out vec3 f_vert_norm;
out vec2 f_vert_uv;

out vec4 f_color;

void main()
{
	vec4 vert_local_pos = vec4(v_vert_pos, 1.0);
	vec4 vert_world_pos = v_model_mat * vert_local_pos;

	vec4 vert_local_norm = vec4(v_vert_norm, 0.0);
	vec4 vert_world_norm = v_normal_mat * vert_local_norm;

	gl_Position = u_view_proj_mat * vert_world_pos;

	f_vert_pos = vert_world_pos.xyz;
	f_vert_norm = vert_world_norm.xyz;
	f_vert_uv = v_vert_uv;

	f_color = v_color;
}
