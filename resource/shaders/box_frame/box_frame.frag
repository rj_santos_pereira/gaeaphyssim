#version 330 core

in vec4 f_color;

layout(location = 0) out vec4 frag_color;

void main()
{
    // We're simply using this to set the mesh visibility;
    // this allows us to adjust visibility per instance
    if (f_color.a == 0.0)
    {
        discard;
    }

    frag_color = f_color;
} 
