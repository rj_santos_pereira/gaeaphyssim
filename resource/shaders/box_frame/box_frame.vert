#version 330 core

// Per-vertex
layout(location = 0) in vec3 v_vert_pos;

// Per-instance
layout(location = 1) in vec4 v_color;
layout(location = 2) in mat4 v_model_mat;

uniform mat4 u_view_proj_mat;

out vec4 f_color;

void main()
{
	gl_Position = u_view_proj_mat * v_model_mat * vec4(v_vert_pos, 1.0);

	f_color = v_color;
}
