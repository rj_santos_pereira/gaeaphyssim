#version 330 core

uniform vec3 u_light_color = vec3(1.0);

layout(location = 0) out vec4 frag_color;

void main()
{
    frag_color = vec4(u_light_color, 1.0);
} 
