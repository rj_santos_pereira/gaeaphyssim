#version 330 core

layout(location = 0) in vec3 v_vert_pos;

uniform mat4 u_model_view_proj_mat;

void main()
{
	gl_Position = u_model_view_proj_mat * vec4(v_vert_pos, 1.0);
}
