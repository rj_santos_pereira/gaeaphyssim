#include "library/math/polyhedron.hpp"

int test_polyhedron()
{
	gaea::polyhedron poly_1{ {
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, -0.5f, +0.5f }, gaea::vector{ -0.5f, +0.5f, +0.5f }, 0, 5 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, +0.5f, +0.5f }, gaea::vector{ -0.5f, +0.5f, -0.5f }, 0, 4 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, +0.5f, -0.5f }, gaea::vector{ -0.5f, -0.5f, -0.5f }, 0, 2 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, -0.5f, -0.5f }, gaea::vector{ -0.5f, -0.5f, +0.5f }, 0, 1 },

			gaea::polyhedron_edge{ gaea::vector{ +0.5f, -0.5f, +0.5f }, gaea::vector{ -0.5f, -0.5f, +0.5f }, 1, 5 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, -0.5f, +0.5f }, gaea::vector{ -0.5f, -0.5f, -0.5f }, 1, 0 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, -0.5f, -0.5f }, gaea::vector{ +0.5f, -0.5f, -0.5f }, 1, 2 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, -0.5f, -0.5f }, gaea::vector{ +0.5f, -0.5f, +0.5f }, 1, 3 },

			gaea::polyhedron_edge{ gaea::vector{ -0.5f, -0.5f, -0.5f }, gaea::vector{ -0.5f, +0.5f, -0.5f }, 2, 0 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, +0.5f, -0.5f }, gaea::vector{ +0.5f, +0.5f, -0.5f }, 2, 4 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, +0.5f, -0.5f }, gaea::vector{ +0.5f, -0.5f, -0.5f }, 2, 3 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, -0.5f, -0.5f }, gaea::vector{ -0.5f, -0.5f, -0.5f }, 2, 1 },

			gaea::polyhedron_edge{ gaea::vector{ +0.5f, +0.5f, +0.5f }, gaea::vector{ +0.5f, -0.5f, +0.5f }, 3, 5 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, -0.5f, +0.5f }, gaea::vector{ +0.5f, -0.5f, -0.5f }, 3, 1 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, -0.5f, -0.5f }, gaea::vector{ +0.5f, +0.5f, -0.5f }, 3, 2 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, +0.5f, -0.5f }, gaea::vector{ +0.5f, +0.5f, +0.5f }, 3, 4 },

			gaea::polyhedron_edge{ gaea::vector{ -0.5f, +0.5f, +0.5f }, gaea::vector{ +0.5f, +0.5f, +0.5f }, 4, 5 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, +0.5f, +0.5f }, gaea::vector{ +0.5f, +0.5f, -0.5f }, 4, 3 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, +0.5f, -0.5f }, gaea::vector{ -0.5f, +0.5f, -0.5f }, 4, 2 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, +0.5f, -0.5f }, gaea::vector{ -0.5f, +0.5f, +0.5f }, 4, 0 },

			gaea::polyhedron_edge{ gaea::vector{ +0.5f, -0.5f, +0.5f }, gaea::vector{ +0.5f, +0.5f, +0.5f }, 5, 3 },
			gaea::polyhedron_edge{ gaea::vector{ +0.5f, +0.5f, +0.5f }, gaea::vector{ -0.5f, +0.5f, +0.5f }, 5, 4 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, +0.5f, +0.5f }, gaea::vector{ -0.5f, -0.5f, +0.5f }, 5, 0 },
			gaea::polyhedron_edge{ gaea::vector{ -0.5f, -0.5f, +0.5f }, gaea::vector{ +0.5f, -0.5f, +0.5f }, 5, 1 },
		} };
	
	return 0;
}