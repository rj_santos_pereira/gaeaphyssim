#include "core/core_sycl.hpp"
#include "library/math/generic_math.hpp"

#include "system/device_manager.hpp"
#include "system/random_engine.hpp"

// https://www.cs.jhu.edu/~misha/ReadingSeminar/Papers/Sengupta07.pdf
// http://www.heterogeneouscompute.org/wordpress/wp-content/uploads/2011/06/RadixSort.pdf
// https://www.eecis.udel.edu/~xli/publications/huang2009empirically.pdf
// https://groups.google.com/g/thrust-users/c/BoLsxO6b4FY

template <class T>
struct kernel_t_bitonic_sort;

template <typename T, typename Alloc>
void t_bitonic_sort(sycl::queue q, sycl::buffer<T, 1, Alloc> buf, size_t vectorSize) {
	int numStages = 0;
	// 2^numStages should be equal to length
	// i.e number of times you halve the lenght to get 1 should be numStages
	for (std::size_t tmp = vectorSize; tmp > 1; tmp >>= 1) {
		++numStages;
	}
	sycl::range<1> r{ vectorSize / 2 };
	for (int stage = 0; stage < numStages; ++stage) {
		// Every stage has stage + 1 passes
		for (int passOfStage = 0; passOfStage < stage + 1; ++passOfStage) {
			auto f = [=](sycl::handler& h) mutable {
				auto a = buf.template get_access<sycl::access::mode::read_write>(h);
				h.parallel_for<kernel_t_bitonic_sort<T>>(
					cl::sycl::range<1>{r},
					[a, stage, passOfStage](sycl::item<1> it) {
						int sortIncreasing = 1;
						sycl::id<1> id = it.get_id();
						int threadId = id.get(0);

						int pairDistance = 1 << (stage - passOfStage);
						int blockWidth = 2 * pairDistance;

						int leftId = (threadId % pairDistance) +
							(threadId / pairDistance) * blockWidth;
						int rightId = leftId + pairDistance;

						T leftElement = a[leftId];
						T rightElement = a[rightId];

						int sameDirectionBlockWidth = 1 << stage;

						if ((threadId / sameDirectionBlockWidth) % 2 == 1) {
							sortIncreasing = 1 - sortIncreasing;
						}

						T greater;
						T lesser;

						if (leftElement > rightElement) {
							greater = leftElement;
							lesser = rightElement;
						}
						else {
							greater = rightElement;
							lesser = leftElement;
						}

						a[leftId] = sortIncreasing ? lesser : greater;
						a[rightId] = sortIncreasing ? greater : lesser;
					});
			};  // command group functor
			q.submit(f);
		}  // passStage
	}    // stage
}  // bitonic_sort

template <class Type>
struct kernel_bitonic_sort;

template <class Type, class CompType = std::less<Type>>
gaea::array<sycl::event> bitonic_sort(sycl::queue& command_queue, sycl::buffer<Type>& data_buffer, std::size_t data_size = 0, CompType comp_func = CompType())
{
	gaea::array<sycl::event> sort_event_array;

	if (data_size == 0)
	{
		data_size = data_buffer.get_range().size();
	}

	if (gaea::math::mod_power_two(data_size, data_size) != 0)
	{
		GAEA_CRASH("Data size must be a power of two!");
	}

	std::size_t num_stages = std::size_t(std::log2(data_size));

	sort_event_array.reserve(num_stages * (num_stages + 1) >> 1);

	sycl::range<1> num_work_items{ data_size >> 1 };

	for (std::size_t stage = 0; stage < num_stages; ++stage)
	{
		for (std::size_t substage = 0; substage < stage + 1; ++substage)
		{
			std::size_t pair_distance_power = stage - substage;
			std::size_t pair_distance = 1llu << pair_distance_power;
			
			std::size_t block_size = pair_distance << 1;

			sort_event_array.push_back(command_queue.submit(
				[&](sycl::handler& command_group)
				{
					auto data_accessor = data_buffer.template get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

					command_group.parallel_for<kernel_bitonic_sort<Type>>(
						sycl::range<1>{ num_work_items },
						[=](sycl::item<1> work_item)
						{
							std::size_t work_item_idx = work_item.get_linear_id();

							// Same as (work_item_idx / pair_distance) * block_size + (work_item_idx % pair_distance)
							// but takes advantage of the fact that pair_distance is a power of two
							std::size_t left_idx = (work_item_idx >> pair_distance_power) * block_size + (work_item_idx & (pair_distance - 1));
							std::size_t right_idx = left_idx + pair_distance;

							Type left_elem = data_accessor[left_idx];
							Type right_elem = data_accessor[right_idx];

							bool is_reversed = (work_item_idx >> stage) & 1;
							bool is_sorted = comp_func(left_elem, right_elem);

							bool should_swap = is_reversed == is_sorted; // XNOR

							data_accessor[left_idx] = should_swap ? right_elem : left_elem;
							data_accessor[right_idx] = should_swap ? left_elem : right_elem;
						}
					);
				}
			));
		}
	}

	return sort_event_array;
}

int test_bitonic_sort()
{
	gaea::random_engine rand_eng;

	gaea::device_manager device_manager;
	gaea::device device;

	device_manager.select_device(device, sycl::info::device_type::gpu);

	std::cout << device.name() << std::endl;

	sycl::queue command_queue{
		device,
		[](sycl::exception_list const& ex_list)
		{
			for (std::exception_ptr const& ex_ptr : ex_list)
			{
				try
				{
					std::rethrow_exception(ex_ptr);
				}
				catch (sycl::exception const& ex)
				{
					std::cout << "Caught async SYCL exception:\n"
							  << ex.what() << "\n";
				}
			}
		},
		sycl::property_list{ sycl::property::queue::enable_profiling {} }
	};

	gaea::array<float32> elements(1024 * 1024);

	sycl::buffer<float32> last_elements_buffer{ elements.size() };

	constexpr std::size_t num_iterations = 100;
	
	for (std::size_t n = 0; n <= num_iterations; ++n)
	{
		sycl::buffer<float32> elements_buffer{ elements.size() };
		
		if (n < num_iterations)
		{
			std::generate(elements.begin(), elements.end(),
				[&rand_eng]() -> float32
				{
#if 0
					float32 retval;
					do
					{
						int32 value = rand_eng.generate_int();
						retval = reinterpret_cast<float32 const&>(value);
					} while (retval != retval);
					return retval;
#else
					return float32(rand_eng.generate_int());
#endif
				});

			std::memcpy(elements_buffer.get_access<sycl::access::mode::write>().get_pointer(), elements.data(), elements.size() * sizeof(float32));
			bitonic_sort(command_queue, elements_buffer, elements_buffer.get_range().size(), std::greater<float32>());
		}
		
		if (n > 0)
		{
			std::cout << "n=" << n << "\n";
#if 0
			std::cout << "Before sort:\n";
			std::for_each(elements.begin(), elements.begin() + 128,
				[](float32 const& elem)
				{
					std::cout << elem << " ";
				});
			std::cout << "\n";
#endif
			
			std::memcpy(elements.data(), last_elements_buffer.get_access<sycl::access::mode::read>().get_pointer(), elements.size() * sizeof(float32));

			bool is_sorted = std::is_sorted(elements.begin(), elements.end(), std::greater<float32>());

#if 0
			std::cout << "After sort:\n";
			std::for_each(elements.begin(), elements.begin() + 128,
				[](float32 const& elem)
				{
					std::cout << elem << " ";
				});
			std::cout << "\n";
#endif

			std::cout << "Is sorted: " << (is_sorted ? "yes" : "no") << std::endl;

			if (!is_sorted)
				__debugbreak();
		}

		last_elements_buffer = elements_buffer;
	}

	return 0;
}