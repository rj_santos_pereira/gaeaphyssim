#include "library/math/matrix.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

int test_mat_quat_vec()
{
    gaea::vector vx{ 1.0, 0.0, 0.0 };

    gaea::quaternion qz1{ std::cos(gaea::pi_f / 4.0f), 0.0, 0.0, std::sin(gaea::pi_f / 4.0f) };
    gaea::quaternion qz2 = gaea::quaternion::from_euler(gaea::vector{ 0.0, 0.0, 90.0 });

    gaea::vector rvx1 = qz1 * vx;
    gaea::vector rvx2 = qz2 * vx;

    gaea::vector vy{ 0.0, 1.0, 0.0 };

    gaea::quaternion qx1{ std::cos(gaea::pi_f / 4.0f), std::sin(gaea::pi_f / 4.0f), 0.0, 0.0 };
    gaea::quaternion qx2 = gaea::quaternion::from_euler(gaea::vector{ 90.0, 0.0, 0.0 });

    gaea::vector rvy1 = qx1 * vy;
    gaea::vector rvy2 = qx2 * vy;

    gaea::vector vz{ 0.0, 0.0, 1.0 };

    gaea::quaternion qy1{ std::cos(gaea::pi_f / 4.0f), 0.0, std::sin(gaea::pi_f / 4.0f), 0.0 };
    gaea::quaternion qy2 = gaea::quaternion::from_euler(gaea::vector{ 0.0, 90.0, 0.0 });

    gaea::vector rvz1 = qy1 * vz;
    gaea::vector rvz2 = qy2 * vz;

    gaea::matrix3 m1{ {
        +0.21f, +5.00f, +3.01f,
        +4.80f, +7.35f, -4.60f,
        +5.87f, -2.31f, +8.18f
    } };

    gaea::matrix3 m1i = m1.inverse();
    float32 m1d = m1.determinant();

    gaea::quaternion q1 = gaea::quaternion::from_euler(gaea::vector{ 35.f, -15.f, -50.f });
    gaea::matrix3 qm1 = q1.to_matrix();

    gaea::vector v1{ 5.f, 2.f, 1.5f };

    gaea::vector r1 = q1 * v1;
    gaea::vector r2 = qm1 * v1;

	return 0;
}
