#include "core/core_sycl.hpp"

#include "system/device_manager.hpp"
#include "system/random_engine.hpp"
#include "library/math/vector.hpp"

class kernel_non_fundamental_types;

int test_non_fundamental_types()
{
    gaea::random_engine rand_eng;

    gaea::device_manager device_manager;
    gaea::device device;

    device_manager.select_device(device, sycl::info::device_type::gpu);

    std::cout << device.name() << std::endl;

    sycl::queue command_queue{
        device,
        [](sycl::exception_list const& ex_list)
        {
            for (std::exception_ptr const& ex_ptr : ex_list)
            {
                try
                {
                    std::rethrow_exception(ex_ptr);
                }
                catch (sycl::exception const& ex)
                {
                    std::cout << "Caught async SYCL exception:\n"
                              << ex.what() << "\n";
                }
            }
        },
        sycl::property_list{ sycl::property::queue::enable_profiling{} }
    };

    gaea::fixed_array<gaea::vector, 8> data;
    gaea::fixed_array<gaea::vector, 8> data_copy;

    std::for_each(data.begin(), data.end(),
        [](auto& elem)
        {
            elem = gaea::vector{ 0.f };
        });

    std::for_each(data_copy.begin(), data_copy.end(),
        [](auto& elem)
        {
            elem = gaea::vector{ 0.f };
        });

    sycl::buffer<gaea::vector> input_buffer;
    sycl::buffer<gaea::vector> output_next_buffer{ 8 };
    sycl::buffer<gaea::vector> output_curr_buffer{ 8 };
    sycl::buffer<gaea::vector> output_prev_1_buffer;

    // ReSharper disable once CppEntityAssignedButNoRead
    sycl::buffer<gaea::vector> output_prev_2_buffer;

    {
        auto output_curr_accessor = output_curr_buffer.get_access<sycl::access::mode::write>();
        std::memcpy(output_curr_accessor.get_pointer(), data.data(), data.size() * sizeof(gaea::vector));
    }

    for (std::size_t n = 0; n < 1000; ++n)
    {
        gaea::fixed_array<gaea::vector, 8> input_data;
        std::for_each(input_data.begin(), input_data.end(), 
            [&rand_eng](auto& elem)
            {
	            elem = gaea::vector{
	            	float32(rand_eng.generate_int(0, 9)),
	            	float32(rand_eng.generate_int(0, 9)),
	            	float32(rand_eng.generate_int(0, 9))
	            };
            });

        std::transform(input_data.begin(), input_data.end(), data_copy.begin(), data_copy.begin(),
            [](auto const& elem1, auto const& elem2) { return elem2 + elem1; });

        {
            input_buffer = sycl::buffer<gaea::vector>{ 8 };

            auto input_accessor = input_buffer.get_access<sycl::access::mode::write>();
            std::memcpy(input_accessor.get_pointer(), input_data.data(), input_data.size() * sizeof(gaea::vector));
        }

        float32 kernel_time;
        {
            gaea::scope_stopwatch kernel_stopwatch{ kernel_time };

            command_queue.submit(
                [&](sycl::handler& command_group)
                {
                    auto input_accessor = input_buffer.get_access<sycl::access::mode::read>(command_group);
                    auto output_curr_accessor = output_curr_buffer.get_access<sycl::access::mode::read_write>(command_group);
                    auto output_next_accessor = output_next_buffer.get_access<sycl::access::mode::discard_write>(command_group);

                    auto input_copy = sycl::accessor<gaea::vector, 1, sycl::access::mode::read_write, sycl::access::target::local>(input_buffer.get_range(), command_group);

                    //sycl::stream devout{ 1024, 128, command_group };

                    command_group.parallel_for<kernel_non_fundamental_types>(
                        sycl::nd_range<1>{ 8, 8 },
                        [=](sycl::nd_item<1> item)
                        {
                            std::size_t id = item.get_global_linear_id();

                            if (id < 8)
                            {
                                std::size_t inverse_id = 7 - id;

                                input_copy[id] = input_accessor[id];

                                item.barrier(sycl::access::fence_space::local_space);
                            	
                                output_next_accessor[inverse_id] = output_curr_accessor[inverse_id] += input_copy[inverse_id];
                            }
                        }
                    );
                }
            );

            if (output_prev_1_buffer.get_range().size() > 0)
            {
                auto output_accessor = output_prev_1_buffer.get_access<sycl::access::mode::read>();
                std::memcpy(data.data(), output_accessor.get_pointer(), data.size() * sizeof(gaea::vector));
            }

            // Keeping this buffer one extra frame may seem unnecessary,
            // since we are only waiting one frame before reading back,
            // but apparently this improves performance by about ~500-1000us...
            // Not sure why, could be some cost related to memory deallocation,
            // but either way, it's relatively painless to keep this buffer alive for a bit longer,
            // considering the benefits.
            output_prev_2_buffer = output_prev_1_buffer;
            output_prev_1_buffer = output_curr_buffer;
            output_curr_buffer = output_next_buffer;
            output_next_buffer = sycl::buffer<gaea::vector>{ 8 };
        }

        std::cout << "Elements:";
        std::for_each(data.begin(), data.end(), [&cout = std::cout](auto const& elem) { cout << " " << elem.to_string(); });
        std::cout << "\n";

        std::cout
            << "Kernel time: " << kernel_time << "s\n" << std::endl;
    }

    {
        auto output_accessor = output_curr_buffer.get_access<sycl::access::mode::read>();
        std::memcpy(data.data(), output_accessor.get_pointer(), data.size() * sizeof(gaea::vector));
    }

    try
    {
        command_queue.wait_and_throw();
    }
    catch (sycl::exception const& ex)
    {
        std::cout << "Caught sync SYCL exception:\n"
            << ex.what() << "\n";
    }

    std::cout << "GPU Elements:";
    std::for_each(data.begin(), data.end(), [&cout = std::cout](auto const& elem) { cout << " " << elem.to_string(); });
    std::cout << "\n";
    std::cout << "CPU elements:";
    std::for_each(data_copy.begin(), data_copy.end(), [&cout = std::cout](auto const& elem) { cout << " " << elem.to_string(); });
    std::cout << "\n" << std::endl;

    return 0;
}