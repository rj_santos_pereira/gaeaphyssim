#include "core/core_sycl.hpp"
#include "library/math/generic_math.hpp"

#include "system/device_manager.hpp"
#include "system/random_engine.hpp"

// Kernel declarations

template <class Type>
struct kernel_interleaving_divergent_parallel_reduce;

template <class Type>
std::vector<sycl::event> interleaving_divergent_parallel_reduce(sycl::queue command_queue, sycl::buffer<Type, 1> element_buffer, sycl::buffer<Type, 1> result_buffer)
{
	std::vector<sycl::event> events_array;
	
	std::size_t num_elements = element_buffer.get_count();
	std::size_t work_group_size = command_queue.get_device().get_info<sycl::info::device::max_work_group_size>();

	while (num_elements > 1)
	{
		std::size_t num_work_groups = std::size_t(std::ceil(float32(num_elements) / float32(work_group_size)));
		std::size_t num_work_group_elements = num_work_groups > 1 ? work_group_size : num_elements;

		events_array.push_back(command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto g_element_accessor = element_buffer.template get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto g_result_accessor = result_buffer.template get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				
				auto l_element_accessor = sycl::accessor<Type, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_elements, command_group);

				command_group.parallel_for<kernel_interleaving_divergent_parallel_reduce<Type>>(
					sycl::nd_range<1>{ num_work_groups * num_work_group_elements, num_work_group_elements },
					[=](sycl::nd_item<1> work_item)
					{
						std::size_t g_elem_idx = work_item.get_group_linear_id();
						std::size_t l_elem_idx = work_item.get_local_linear_id();

						std::size_t elem_idx = g_elem_idx * num_work_group_elements + l_elem_idx;

						if (elem_idx < num_elements)
						{
							l_element_accessor[l_elem_idx] = g_element_accessor[elem_idx];
						}

						work_item.barrier(sycl::access::fence_space::local_space);

						for (std::size_t stride = 1; stride < num_work_group_elements; stride <<= 1)
						{
							std::size_t stride_elem_idx = l_elem_idx + stride;

							if (l_elem_idx % (stride << 1) == 0 && stride_elem_idx < num_work_group_elements)
							{
								l_element_accessor[l_elem_idx] += l_element_accessor[stride_elem_idx];
							}

							work_item.barrier(sycl::access::fence_space::local_space);
						}

						if (l_elem_idx == 0)
						{
							g_element_accessor[g_elem_idx] = l_element_accessor[0];
							if (g_elem_idx == 0)
							{
								g_result_accessor[0] = l_element_accessor[0];
							}
						}
					}
				);
			}
		));

		num_elements = num_work_groups;
	}

	return events_array;
}

template <class Type>
struct kernel_interleaving_convergent_parallel_reduce;

template <class Type>
struct kernel_interleaving_convergent_parallel_reduce_copy;

template <class Type>
std::vector<sycl::event> interleaving_convergent_parallel_reduce(sycl::queue& command_queue, sycl::buffer<Type> element_buffer, sycl::buffer<Type> result_buffer)
{
	std::vector<sycl::event> events_array;
	
	std::size_t num_elements = element_buffer.get_count();
	std::size_t work_group_size = command_queue.get_device().get_info<sycl::info::device::max_work_group_size>();

	sycl::buffer<Type> copy_buffer{ num_elements };

	command_queue.submit(
		[&](sycl::handler& command_group)
		{
			auto gl_element_accessor = element_buffer.template get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
			auto gl_copy_accessor = copy_buffer.template get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);

			command_group.parallel_for<kernel_interleaving_convergent_parallel_reduce_copy<Type>>(
				sycl::range<1>{ num_elements },
				[=](sycl::item<1> work_item)
				{
					std::size_t elem_idx = work_item.get_linear_id();

					gl_copy_accessor[elem_idx].store(gl_element_accessor[elem_idx]);
				}
			);
		}
	);
	
	while (num_elements > 1)
	{
		std::size_t num_work_groups = std::size_t(std::ceil(float32(num_elements) / float32(work_group_size)));
		std::size_t num_work_group_elements = num_work_groups > 1 ? work_group_size : num_elements;

		events_array.push_back(command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto gl_copy_accessor = copy_buffer.template get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);
				auto gl_result_accessor = result_buffer.template get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				
				auto l_element_accessor = sycl::accessor<Type, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_elements, command_group);

				command_group.parallel_for<kernel_interleaving_convergent_parallel_reduce<Type>>(
					sycl::nd_range<1>{ num_work_groups * num_work_group_elements, num_work_group_elements },
					[=](sycl::nd_item<1> work_item)
					{
						std::size_t g_elem_idx = work_item.get_group_linear_id();
						std::size_t l_elem_idx = work_item.get_local_linear_id();

						std::size_t elem_idx = g_elem_idx * num_work_group_elements + l_elem_idx;

						if (elem_idx < num_elements)
						{
							l_element_accessor[l_elem_idx] = gl_copy_accessor[elem_idx].load();
						}

						work_item.barrier(sycl::access::fence_space::local_space);

						for (std::size_t stride = 1; stride < num_work_group_elements; stride <<= 1)
						{
							std::size_t reduce_elem_idx = l_elem_idx * (stride << 1);
							std::size_t stride_elem_idx = reduce_elem_idx + stride;

							if (stride_elem_idx < num_work_group_elements)
							{
								l_element_accessor[reduce_elem_idx] += l_element_accessor[stride_elem_idx];
							}

							work_item.barrier(sycl::access::fence_space::local_space);
						}

						if (l_elem_idx == 0)
						{
							gl_copy_accessor[g_elem_idx].store(l_element_accessor[0]);
							
							if (g_elem_idx == 0)
							{
								gl_result_accessor[0] = l_element_accessor[0];
							}
						}
					}
				);
			}
		));

		num_elements = num_work_groups;
	}

	return events_array;
}

// Not good for non-squares TODO FIXME
template <class Type>
struct kernel_sequential_parallel_reduce;

template <class Type>
std::vector<sycl::event> sequential_parallel_reduce(sycl::queue command_queue, sycl::buffer<Type, 1> element_buffer, sycl::buffer<Type, 1> result_buffer)
{
	std::vector<sycl::event> events_array;
	
	std::size_t num_elements = element_buffer.get_count();
	std::size_t work_group_size = command_queue.get_device().get_info<sycl::info::device::max_work_group_size>();

	while (num_elements > 1)
	{
		std::size_t num_work_groups = std::size_t(std::ceil(float32(num_elements) / float32(work_group_size)));
		std::size_t num_work_group_elements = num_work_groups > 1 ? work_group_size : num_elements;

		events_array.push_back(command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto g_element_accessor = element_buffer.template get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto g_result_accessor = result_buffer.template get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				
				auto l_element_accessor = sycl::accessor<Type, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_elements, command_group);

				command_group.parallel_for<kernel_sequential_parallel_reduce<Type>>(
					sycl::nd_range<1>{ num_work_groups * num_work_group_elements, num_work_group_elements },
					[=](sycl::nd_item<1> work_item)
					{
						std::size_t g_elem_idx = work_item.get_group_linear_id();
						std::size_t l_elem_idx = work_item.get_local_linear_id();

						std::size_t elem_idx = g_elem_idx * num_work_group_elements + l_elem_idx;

						if (elem_idx < num_elements)
						{
							l_element_accessor[l_elem_idx] = g_element_accessor[elem_idx];
						}

						work_item.barrier(sycl::access::fence_space::local_space);

						for (std::size_t stride = num_work_group_elements >> 1; stride > 0; stride >>= 1)
						{
							if (l_elem_idx < stride)
							{
								l_element_accessor[l_elem_idx] += l_element_accessor[l_elem_idx + stride];
							}

							work_item.barrier(sycl::access::fence_space::local_space);
						}

						if (l_elem_idx == 0)
						{
							g_element_accessor[g_elem_idx] = l_element_accessor[0];
							if (g_elem_idx == 0)
							{
								g_result_accessor[0] = l_element_accessor[0];
							}
						}
					}
				);
			}
		));

		num_elements = num_work_groups;
	}

	return events_array;
}

// Not good TODO FIXME
template <class Type>
struct kernel_half_sequential_parallel_reduce;

template <class Type>
std::vector<sycl::event> half_sequential_parallel_reduce(sycl::queue command_queue, sycl::buffer<Type, 1> element_buffer, sycl::buffer<Type, 1> result_buffer)
{
	std::vector<sycl::event> events_array;
	
	std::size_t num_elements = element_buffer.get_count();
	std::size_t work_group_size = command_queue.get_device().get_info<sycl::info::device::max_work_group_size>();

	while (num_elements > 1)
	{
		num_elements >>= 1;
		
		std::size_t num_work_groups = std::size_t(std::ceil(float32(num_elements) / float32(work_group_size)));
		std::size_t num_work_group_elements = num_work_groups > 1 ? work_group_size : num_elements;

		events_array.push_back(command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto g_element_accessor = element_buffer.template get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto g_result_accessor = result_buffer.template get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				
				auto l_element_accessor = sycl::accessor<Type, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_elements, command_group);

				command_group.parallel_for<kernel_half_sequential_parallel_reduce<Type>>(
					sycl::nd_range<1>{ num_work_groups * num_work_group_elements, num_work_group_elements },
					[=](sycl::nd_item<1> work_item)
					{
						std::size_t num_total_elements = num_elements << 1;
						
						std::size_t g_elem_idx = work_item.get_group_linear_id();
						std::size_t l_elem_idx = work_item.get_local_linear_id();

						std::size_t elem_idx = g_elem_idx * num_work_group_elements + l_elem_idx;

						if (elem_idx + num_work_group_elements < num_total_elements)
						{
							l_element_accessor[l_elem_idx] = g_element_accessor[elem_idx] + g_element_accessor[elem_idx + num_work_group_elements];
						}

						work_item.barrier(sycl::access::fence_space::local_space);

						for (std::size_t stride = num_work_group_elements >> 1; stride > 0; stride >>= 1)
						{
							if (l_elem_idx + stride < num_work_group_elements)
							{
								l_element_accessor[l_elem_idx] += l_element_accessor[l_elem_idx + stride];
							}

							work_item.barrier(sycl::access::fence_space::local_space);
						}

						if (l_elem_idx == 0)
						{
							g_element_accessor[g_elem_idx] = l_element_accessor[0];
							if (g_elem_idx == 0)
							{
								g_result_accessor[0] = l_element_accessor[0];
							}
						}
					}
				);
			}
		));

		num_elements = num_work_groups;
	}

	return events_array;
}

int test_parallel_reduce()
{
	gaea::random_engine random_engine;

	gaea::device_manager device_manager;
	gaea::device device;

	device_manager.select_device(device, sycl::info::device_type::gpu);

	std::cout << device.name() << std::endl;

	sycl::queue command_queue{
		device,
		[](sycl::exception_list const& except_list)
		{
			for (std::exception_ptr const& except_ptr : except_list)
			{
				try
				{
					std::rethrow_exception(except_ptr);
				}
				catch (sycl::exception const& except)
				{
					std::cout << "Caught async SYCL exception:\n"
							  << except.what() << "\n";
				}
			}
		},
		sycl::property_list{ sycl::property::queue::enable_profiling{} }
	};

	// TEST PARALLEL_REDUCTION

	std::size_t array_size = std::size_t(1) * 1025;// *128;

	std::cout << "Generating arrays with " << array_size << " elements...\n";

	auto gen_start = stdchr::high_resolution_clock::now();

	std::vector<uint32> cpu_arr(array_size);
	std::generate(cpu_arr.begin(), cpu_arr.end(), [&random_engine] { return uint32(random_engine.generate_int(0, 9)); });
	std::vector<uint32> gpu_arr = cpu_arr;

	auto gen_finish = stdchr::high_resolution_clock::now();

	std::cout << "Done!\n";
	std::cout << "Time elapsed: " << stdchr::duration<double>(gen_finish - gen_start).count() << "s\n\n";

	std::cout << "Running CPU parallel-reduce...\n";

	uint32 cpu_result = 0;
	auto cpu_start = stdchr::high_resolution_clock::now();

	{
		cpu_result = std::reduce(std::execution::par_unseq, cpu_arr.begin(), cpu_arr.end(), uint32(0), std::plus<uint32>());
	}

	auto cpu_finish = stdchr::high_resolution_clock::now();

	std::cout << "Done!\nSum: " << cpu_result << "\n";
	std::cout << "Time elapsed: " << stdchr::duration<double>(cpu_finish - cpu_start).count() << "s\n\n";

	std::cout << "Running GPU parallel-reduce...\n";

	uint32 gpu_result = 0;
	int64 gpu_command_elapsed = 0;
	stdchr::high_resolution_clock::time_point gpu_start, gpu_finish;

	std::vector<sycl::event> command_events;

	sycl::buffer<uint32> element_buffer{ gaea::math::ceil_power_two(array_size) };
	sycl::buffer<uint32> result_buffer{ 1 };
	
	std::memcpy(
		element_buffer.get_access<sycl::access::mode::discard_write>().get_pointer(),
		gpu_arr.data(),
		gpu_arr.size() * sizeof(uint32)
	);

	gpu_start = stdchr::high_resolution_clock::now();
	
	{
		command_events = interleaving_convergent_parallel_reduce(command_queue, element_buffer, result_buffer);

		command_queue.wait();
	}

	std::memcpy(
		gpu_arr.data(),
		element_buffer.get_access<sycl::access::mode::read>().get_pointer(),
		gpu_arr.size() * sizeof(uint32)
	);

	gpu_finish = stdchr::high_resolution_clock::now();

	gpu_result = *result_buffer.get_access<sycl::access::mode::read>().get_pointer();

	std::for_each(command_events.begin(), command_events.end(),
		[&gpu_command_elapsed](sycl::event const& command_event)
		{
			auto event_start = command_event.get_profiling_info<sycl::info::event_profiling::command_start>();
			auto event_end = command_event.get_profiling_info<sycl::info::event_profiling::command_end>();

			std::cout << "Event start:\t" << event_start << "ns\nEvent end:\t" << event_end << "ns\n";

			gpu_command_elapsed += event_end - event_start;
		});

	std::cout << "Done!\nSum: " << gpu_result << "\n";
	std::cout << "Command time elapsed: " << stdchr::duration<double>(stdchr::nanoseconds(gpu_command_elapsed)).count() << "s\n\n";
	std::cout << "Time elapsed: " << stdchr::duration<double>(gpu_finish - gpu_start).count() << "s\n";

	std::cout << "Equal? " << (std::equal(cpu_arr.begin(), cpu_arr.end(), gpu_arr.begin()) ? "yes" : "no") << "\n";
	
	try
	{
		command_queue.wait_and_throw();
	}
	catch (sycl::exception const& except)
	{
		std::cout << "Caught sync SYCL exception:\n"
			<< except.what() << "\n";
	}

	std::cout << std::endl;

	return 0;
}
