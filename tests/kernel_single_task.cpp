#include "core/core_sycl.hpp"

#include "system/device_manager.hpp"

// Kernel declarations

class kernel_vec_add;

template <class AccessorType, class ConstantType, std::size_t SizeValue>
class kernel_constant_vec_add
{
public:
    kernel_constant_vec_add(AccessorType accessor, ConstantType value)
        : _accessor(accessor)
        , _value(value)
    {
    }

    void operator()()
    {
        for (std::size_t Index = 0; Index < SizeValue; ++Index)
        {
            _accessor[Index] += _value;
        }
    }

private:
    AccessorType _accessor;
    ConstantType _value;

};

int test_single_task()
{
    gaea::device_manager device_manager;
    gaea::device device;

    device_manager.select_device(device, sycl::info::device_type::gpu);
	
    sycl::queue queue{
        device,
        [](sycl::exception_list const& except_list)
        {
            for (std::exception_ptr const& except_ptr : except_list)
            {
                try
                {
                    std::rethrow_exception(except_ptr);
                }
                catch (sycl::exception const& except)
                {
                    std::cout << "Caught async SYCL exception:\n"
                              << except.what() << "\n";
                }
            }
        }
    };

    // TEST KERNEL 1

    vfloat32x4 a = { 1.0f, 2.0f, 3.0f, 4.0f };
    vfloat32x4 b = { 4.0f, 3.0f, 2.0f, 1.0f };
    vfloat32x4 c = { 0.0f, 0.0f, 0.0f, 0.0f };

    {
        // These buffers take ownership of our data during their lifetime
        // The destruction of these buffers ensures synchronization of any device operations that use them
        sycl::buffer<vfloat32x4, 1> a_buffer{ &a, sycl::range<1>{ 1 } };
        sycl::buffer<vfloat32x4, 1> b_buffer{ &b, sycl::range<1>{ 1 } };
        sycl::buffer<vfloat32x4, 1> c_buffer{ &c, sycl::range<1>{ 1 } };

        std::cout << "Submitting kernel: " << sycl::detail::kernel_info<kernel_vec_add>::name << "\n";

        // This submit a command group for execution through a lambda that is executed before the function returns
        // A command group encapsulates exactly one device operation (a kernel), and any data dependencies it might need
        queue.submit(
            [&](sycl::handler& handler)
            {
                // This allows us to define the type of access on the device to the host data
                auto i_a = a_buffer.get_access<sycl::access::mode::read>(handler);
                auto i_b = b_buffer.get_access<sycl::access::mode::read>(handler);
                auto o_c = c_buffer.get_access<sycl::access::mode::discard_write>(handler);

                // This submits a kernel as a task that will be executed once
                // The template parameter is a unique, well-defined name that is given to the kernel
                // An incomplete class name is enough for the compiler to distinguish kernels
                handler.single_task<kernel_vec_add>(
                    [=]
                    {
                        o_c[0] = i_a[0] + i_b[0];
                    }
                    );
            }
        );
    }

    std::cout << "  A { " << a.x() << ", " << a.y() << ", " << a.z() << ", " << a.w() << " }\n"
              << "+ B { " << b.x() << ", " << b.y() << ", " << b.z() << ", " << b.w() << " }\n"
              << "------------------\n"
              << "= C { " << c.x() << ", " << c.y() << ", " << c.z() << ", " << c.w() << " }\n\n";

    // TEST KERNEL 2

    {
        sycl::buffer<vfloat32x4, 1> c_buffer{ &c, sycl::range<1>{ 1 } };

        using accessor_type = decltype(c_buffer.get_access<sycl::access::mode::read_write>(std::declval<sycl::handler&>()));

        std::cout << "Submitting kernel: " << sycl::detail::kernel_info<kernel_constant_vec_add<accessor_type, float32, 4>>::name << "\n";

        queue.submit(
            [&](sycl::handler& cgh)
            {
                auto c_acc = c_buffer.get_access<sycl::access::mode::read_write>(cgh);

                cgh.single_task(kernel_constant_vec_add<accessor_type, float32, 4>(c_acc, 5.f));
            }
        );
    }

    std::cout << "+   { 5, 5, 5, 5 }\n"
              << "------------------\n"
              << "= C { " << c.x() << ", " << c.y() << ", " << c.z() << ", " << c.w() << " }\n\n";

    try
    {
        queue.wait_and_throw();
    }
    catch (sycl::exception const& except)
    {
        std::cout << "Caught sync SYCL exception:\n"
            << except.what() << "\n";
    }

    std::cout << std::endl;

    return 0;
}
