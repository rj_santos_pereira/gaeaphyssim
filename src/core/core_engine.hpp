#ifndef CORE_ENGINE_INCLUDE_GUARD
#define CORE_ENGINE_INCLUDE_GUARD

#include "core/core_sycl.hpp"

namespace gaea
{
	// Engine abstract base types

	class base_engine;
	class base_renderer;
	class base_simulator;
	class base_world;
	class base_entity;

	class base_engine
	{
	protected:
		base_engine() = default;
		~base_engine() = default;

	public:
		base_engine(base_engine const&) = delete;
		base_engine(base_engine&&) = delete;

		base_engine& operator=(base_engine const&) = delete;
		base_engine& operator=(base_engine&&) = delete;

		virtual bool initialize(void* parameters) = 0;
		virtual void finalize() = 0;

		virtual void run() = 0;

		virtual uint64 make_unique_id() = 0;
		virtual uint64 make_sequential_id() = 0;

		virtual uint64 frame_number() const = 0;
		virtual float64 frame_delta() const = 0;
		virtual float64 frame_actual_delta() const = 0;
		virtual float64 max_frame_rate() const = 0;

		virtual bool is_paused() const = 0;

		virtual bool is_running() const = 0;
		virtual bool is_initializing() const = 0;
		virtual bool is_finalizing() const = 0;

		virtual class random_engine* access_random_engine() = 0;

		virtual class input_manager* access_input_manager() = 0;
		virtual class config_manager* access_config_manager() = 0;
		virtual class device_manager* access_device_manager() = 0;
		virtual class timer_manager* access_timer_manager() = 0;

		virtual void register_thread(thread_type type) = 0;
		virtual bool is_engine_thread() const = 0;
		virtual bool is_render_thread() const = 0;
		virtual bool is_simulate_thread() const = 0;

		virtual stdfs::path compose_engine_path(stdfs::path const& path) const = 0;
		
		virtual void log_message(log_type type, stdchr::system_clock::time_point&& time, std::string&& message) = 0;
		virtual void display_message(basic_color color, std::string&& message) = 0;

		template <class WorldType>
		WorldType* access_world()
		{
			return static_cast<WorldType*>(access_world_internal(typeid(WorldType)));
		}

	protected:
		virtual void* access_world_internal(const type_info& world_type) = 0;

	};

	class base_renderer
	{
	protected:
		base_renderer() = default;
		~base_renderer() = default;

	public:
		using render_function_type = std::function<void(void*)>;

		using window_minimize_callback_type = std::function<void(bool minimized)>;
		using window_maximize_callback_type = std::function<void(bool maximized)>;

		using window_focus_callback_type = std::function<void(bool focused)>;
		using window_close_callback_type = std::function<void()>;

		using window_move_callback_type = std::function<void(int32 pos_x, int32 pos_y)>;
		using window_resize_callback_type = std::function<void(int32 width, int32 height)>;

		using event_key_button_callback_type = std::function<void(input_key key, input_state state, input_modifier modifier)>;
		using event_mouse_button_callback_type = std::function<void(input_mouse mouse, input_state state, input_modifier modifier)>;
		using event_mouse_drag_callback_type = std::function<void(float32 pos_x, float32 pos_y, float32 pos_delta_x, float32 pos_delta_y, input_modifier modifier)>;
		using event_mouse_scroll_callback_type = std::function<void(float32 delta_x, float32 delta_y, input_modifier modifier)>;

		base_renderer(base_renderer const&) = delete;
		base_renderer(base_renderer&&) = delete;

		base_renderer& operator=(base_renderer const&) = delete;
		base_renderer& operator=(base_renderer&&) = delete;

		virtual bool initialize(void* parameters) = 0;
		virtual void finalize() = 0;

		virtual void draw(uint64 frame_number) = 0;

		virtual void flush() = 0;

		virtual uint64 enqueue_render_command(render_function_type&& function, render_priority priority, bool is_persistent) = 0;
		virtual void dequeue_render_command(uint64 id) = 0;

		// TODO FIXME these should probably have matching unregister methods
		virtual void register_window_minimize_callback(window_minimize_callback_type&& callback) = 0;
		virtual void register_window_maximize_callback(window_maximize_callback_type&& callback) = 0;

		virtual void register_window_focus_callback(window_focus_callback_type&& callback) = 0;
		virtual void register_window_close_callback(window_close_callback_type&& callback) = 0;

		virtual void register_window_move_callback(window_move_callback_type&& callback) = 0;
		virtual void register_window_resize_callback(window_resize_callback_type&& callback) = 0;
		
		virtual void register_event_key_button_callback(event_key_button_callback_type&& callback) = 0;
		virtual void register_event_mouse_button_callback(event_mouse_button_callback_type&& callback) = 0;
		virtual void register_event_mouse_drag_callback(event_mouse_drag_callback_type&& callback) = 0;
		virtual void register_event_mouse_scroll_callback(event_mouse_scroll_callback_type&& callback) = 0;
	};

	class base_simulator
	{
	protected:
		base_simulator() = default;
		~base_simulator() = default;

	public:
		using simulation_vector_type = std::tuple<float64, float64, float64>;
		
		base_simulator(base_simulator const&) = delete;
		base_simulator(base_simulator&&) = delete;

		base_simulator& operator=(base_simulator const&) = delete;
		base_simulator& operator=(base_simulator&&) = delete;

		virtual bool initialize(void* parameters) = 0;
		virtual void finalize() = 0;

		virtual void step(float64 frame_delta) = 0;
		
		virtual void finish_step() = 0;

		virtual float32 query_step_duration() const = 0;
		virtual float32 query_collision_detection_duration() const = 0;
		virtual float32 query_broad_phase_duration() const = 0;
		virtual float32 query_narrow_phase_duration() const = 0;
		virtual float32 query_collision_resolution_duration() const = 0;

		virtual uint32 num_threads() const = 0;

		virtual bool is_real_time() const = 0;

		virtual void register_body(simulation_object type, void* object) = 0;
		virtual void unregister_body(simulation_object type, void* object) = 0;

		virtual void apply_world_force(simulation_vector_type const& force) = 0;
		virtual void apply_world_torque(simulation_vector_type const& force) = 0;
		
		virtual void apply_world_lin_accel(simulation_vector_type const& accel) = 0;
		virtual void apply_world_ang_accel(simulation_vector_type const& accel) = 0;

	};

	class base_world
	{
	protected:
		base_world() = default;
		~base_world() = default;

	public:
		base_world(base_world const&) = delete;
		base_world(base_world&&) = delete;

		base_world& operator=(base_world const&) = delete;
		base_world& operator=(base_world&&) = delete;

		virtual void spawn_world(void* parameters) = 0;
		
		virtual uint64 spawn_entity(base_entity* entity) = 0;
		virtual void despawn_entity(uint64 uid) = 0;

		virtual void begin_run() = 0;
		virtual void end_run() = 0;

		virtual void tick(tick_stage stage, float32 delta) = 0;
		virtual bool is_ticking() const = 0;

		virtual tick_stage current_tick_stage() const = 0;

		virtual uint64 num_entities() const = 0;
		virtual base_entity* find_entity(uint64 uid) const = 0;

		virtual void set_spawn_limit_per_frame(uint64 limit) = 0;
		virtual void set_despawn_limit_per_frame(uint64 limit) = 0;

		virtual uint64 get_spawn_limit_per_frame() const = 0;
		virtual uint64 get_despawn_limit_per_frame() const = 0;
		
		virtual std::string serialize() = 0;
		virtual void deserialize(std::string&& world) = 0;

	};

	class base_entity
	{
	protected:
		base_entity() = default;
		~base_entity() = default;

	public:
		base_entity(base_entity const&) = delete;
		base_entity(base_entity&&) = delete;

		base_entity& operator=(base_entity const&) = delete;
		base_entity& operator=(base_entity&&) = delete;

		virtual void begin_run() = 0;
		virtual void end_run() = 0;

		virtual void tick(float32 delta) = 0;
		virtual bool should_tick() const = 0;

		virtual float32 get_tick_frequency() const = 0;
		virtual tick_stage get_tick_stage() const = 0;
		virtual body_visibility get_body_visibility() const = 0;
		virtual body_mobility get_body_mobility() const = 0;

		virtual void set_tick_frequency(float32 frequency) = 0;
		virtual void set_tick_stage(tick_stage stage) = 0;
		virtual void set_body_visibility(body_visibility visibility) = 0;
		virtual void set_body_mobility(body_mobility mobility) = 0;

		virtual uint64 uid() const = 0;
		virtual std::string name() const = 0;

		virtual std::string serialize() = 0;
		virtual void deserialize(std::string&& entity) = 0;
		
	};

	// Engine global accessors

	extern base_engine* engine;
	extern base_renderer* renderer;
	extern base_simulator* simulator;
}

#endif
