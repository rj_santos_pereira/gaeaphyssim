#ifndef CORE_OPENGL_INCLUDE_GUARD
#define CORE_OPENGL_INCLUDE_GUARD

// OpenGL API docs: https://docs.gl/

// GLAD must be included before GLFW
#include "glad/glad.h"
#define GLFW_INCLUDE_NONE
#include "GLFW/glfw3.h"

#endif