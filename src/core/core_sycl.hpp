#ifndef CORE_SYCL_INCLUDE_GUARD
#define CORE_SYCL_INCLUDE_GUARD

#include "core/core_common.hpp"

// Specification:
// https://www.khronos.org/registry/SYCL/specs/sycl-1.2.1.pdf

#if GAEA_USING_SYCL

#ifdef _MSC_VER
#pragma warning(push)

#pragma warning(disable: 4100) // CompWarn level 4: unreferenced formal parameter
#pragma warning(disable: 4127) // CompWarn level 4: conditional expression is constant
#pragma warning(disable: 4310) // CompWarn level 3: cast truncates constant value
#endif

// Avoid potential compilation failure due to existing alias
#define COMPUTECPP_DISABLE_SYCL_NAMESPACE_ALIAS

#include "CL/sycl.hpp"

namespace sycl = ::cl::sycl;

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#if 0
using vint8x2 = sycl::vec<int8, 2>;
using vint8x3 = sycl::vec<int8, 3>;
using vint8x4 = sycl::vec<int8, 4>;
using vint8x8 = sycl::vec<int8, 8>;
using vint8x16 = sycl::vec<int8, 16>;

using vint16x2 = sycl::vec<int16, 2>;
using vint16x3 = sycl::vec<int16, 3>;
using vint16x4 = sycl::vec<int16, 4>;
using vint16x8 = sycl::vec<int16, 8>;
using vint16x16 = sycl::vec<int16, 16>;

using vint32x2 = sycl::vec<int32, 2>;
using vint32x3 = sycl::vec<int32, 3>;
using vint32x4 = sycl::vec<int32, 4>;
using vint32x8 = sycl::vec<int32, 8>;
using vint32x16 = sycl::vec<int32, 16>;

using vint64x2 = sycl::vec<int64, 2>;
using vint64x3 = sycl::vec<int64, 3>;
using vint64x4 = sycl::vec<int64, 4>;
using vint64x8 = sycl::vec<int64, 8>;
using vint64x16 = sycl::vec<int64, 16>;

using vuint8x2 = sycl::vec<uint8, 2>;
using vuint8x3 = sycl::vec<uint8, 3>;
using vuint8x4 = sycl::vec<uint8, 4>;
using vuint8x8 = sycl::vec<uint8, 8>;
using vuint8x16 = sycl::vec<uint8, 16>;

using vuint16x2 = sycl::vec<uint16, 2>;
using vuint16x3 = sycl::vec<uint16, 3>;
using vuint16x4 = sycl::vec<uint16, 4>;
using vuint16x8 = sycl::vec<uint16, 8>;
using vuint16x16 = sycl::vec<uint16, 16>;

using vuint32x2 = sycl::vec<uint32, 2>;
using vuint32x3 = sycl::vec<uint32, 3>;
using vuint32x4 = sycl::vec<uint32, 4>;
using vuint32x8 = sycl::vec<uint32, 8>;
using vuint32x16 = sycl::vec<uint32, 16>;

using vuint64x2 = sycl::vec<uint64, 2>;
using vuint64x3 = sycl::vec<uint64, 3>;
using vuint64x4 = sycl::vec<uint64, 4>;
using vuint64x8 = sycl::vec<uint64, 8>;
using vuint64x16 = sycl::vec<uint64, 16>;

using vfloat32x2 = sycl::vec<float32, 2>;
using vfloat32x3 = sycl::vec<float32, 3>;
using vfloat32x4 = sycl::vec<float32, 4>;
using vfloat32x8 = sycl::vec<float32, 8>;
using vfloat32x16 = sycl::vec<float32, 16>;

using vfloat64x2 = sycl::vec<float64, 2>;
using vfloat64x3 = sycl::vec<float64, 3>;
using vfloat64x4 = sycl::vec<float64, 4>;
using vfloat64x8 = sycl::vec<float64, 8>;
using vfloat64x16 = sycl::vec<float64, 16>;
#endif

#endif

#endif
