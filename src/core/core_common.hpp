#ifndef CORE_COMMON_INCLUDE_GUARD
#define CORE_COMMON_INCLUDE_GUARD

// Common includes

#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

#if _HAS_CXX20
// 	Fundamental library concepts
#	include <concepts>
// 	Coroutine support library
#	include <coroutine>
#endif

// 	General purpose utilities: program control, dynamic memory allocation, random numbers, sort and search
#	include <cstdlib>
// 	Functions and macro constants for signal management
#	include <csignal>
// 	Macro (and function) that saves (and jumps) to an execution context
#	include <csetjmp>
// 	Handling of variable length argument lists
#	include <cstdarg>
// 	Runtime type information utilities
#	include <typeinfo>
// 	std::type_index
#	include <typeindex>
// 	Compile-time type information
#	include <type_traits>
// 	std::bitset class template
#	include <bitset>
// 	Function objects, Function invocations, Bind operations and Reference wrappers
#	include <functional>
// 	Various utility components
#	include <utility>
// 	C-style time/date utilites
#	include <ctime>
// 	C++ time utilites
#	include <chrono>
// 	Standard macros and typedefs
#	include <cstddef>
// 	std::initializer_list class template
#	include <initializer_list>
// 	std::tuple class template
#	include <tuple>

#if _HAS_CXX17
// 	std::any class
#	include <any>
// 	std::optional class template
#	include <optional>
// 	std::variant class template
#	include <variant>
#endif

#if _HAS_CXX20
// 	Three-way comparison operator support
#	include <compare>
// 	Supplies implementation-dependent library information
#	include <version>
// 	Supplies means to obtain source code location
#	include <source_location>
#endif

// 	Low-level memory management utilities
#	include <new>
// 	High-level memory management utilities
#	include <memory>
// 	Nested allocator class
#	include <scoped_allocator>

#if _HAS_CXX17
// 	Polymorphic allocators and memory resources
#	include <memory_resource>
#endif

// 	Limits of integral types
#	include <climits>
// 	Limits of floating-point types
#	include <cfloat>
// 	Fixed-width integer types and limits of other types
#	include <cstdint>
// 	Formatting macros, intmax_t and uintmax_t math and conversions
#	include <cinttypes>
// 	Uniform way to query properties of arithmetic types
#	include <limits>
// 	Exception handling utilities
#	include <exception>
// 	Standard exception objects
#	include <stdexcept>
// 	Conditionally compiled macro that compares its argument to zero
#	include <cassert>
// 	Defines std::error_code, a platform-dependent error code
#	include <system_error>
// 	Macro containing the last error number
#	include <cerrno>
// 	Functions to determine the category of narrow characters
#	include <cctype>
// 	Functions to determine the catagory of wide characters
#	include <cwctype>
// 	Various narrow character string handling functions
#	include <cstring>
// 	Various wide and multibyte string handling functions
#	include <cwchar>
// 	C-style Unicode character conversion functions
#	include <cuchar>
// 	std::basic_string class template
#	include <string>

#if _HAS_CXX17
// 	std::basic_string_view class template
#	include <string_view>
// 	std::to_chars and std::from_chars
#	include <charconv>
#endif

#if _HAS_CXX20
// 	Formatting library including std::format
#	include <format>
#endif

// 	std::array container
#	include <array>
// 	std::vector container
#	include <vector>
// 	std::deque container
#	include <deque>
// 	std::list container
#	include <list>
// 	std::forward_list container
#	include <forward_list>
// 	std::set and std::multiset associative containers
#	include <set>
// 	std::map and std::multimap associative containers
#	include <map>
// 	std::unordered_set and std::unordered_multiset unordered associative containers
#	include <unordered_set>
// 	std::unordered_map and std::unordered_multimap unordered associative containers
#	include <unordered_map>
// 	std::stack container adaptor
#	include <stack>
// 	std::queue and std::priority_queue container adaptors
#	include <queue>

#if _HAS_CXX20
// 	std::span view
#	include <span>
#endif

// 	Range iterators
#	include <iterator>

#if _HAS_CXX20
// 	Range access, primitives, requirements, utilities and adaptors
#	include <ranges>
#endif

// 	Algorithms that operate on ranges
#	include <algorithm>

#if _HAS_CXX17
// 	Predefined execution policies for parallel versions of the algorithms
#	include <execution>
#endif

// 	Common mathematics functions
#	include <cmath>
// 	Complex number type
#	include <complex>
// 	Class for representing and manipulating arrays of values
#	include <valarray>
// 	Random number generators and distributions
#	include <random>
// 	Numeric operations on values in ranges
#	include <numeric>
// 	Compile-time rational arithmetic
#	include <ratio>
// 	Floating-point environment access functions
#	include <cfenv>

#if _HAS_CXX20
// 	Bit manipulation functions
#	include <bit>
// 	Math constants
#	include <numbers>
#endif

// 	Localization utilities
#	include <locale>
// 	C localization utilities
#	include <clocale>
// 	Unicode conversion facilities
#	include <codecvt>
// 	std::ios_base class, std::basic_ios class template and several typedefs
#	include <ios>
// 	std::basic_istream class template and several typedefs
#	include <istream>
// 	std::basic_ostream, std::basic_iostream class templates and several typedefs
#	include <ostream>
// 	Several standard stream objects
#	include <iostream>
// 	std::basic_fstream, std::basic_ifstream, std::basic_ofstream class templates and several typedefs
#	include <fstream>
// 	std::basic_stringstream, std::basic_istringstream, std::basic_ostringstream class templates and several typedefs
#	include <sstream>

#if _HAS_CXX20
// 	std::basic_osyncstream, std::basic_syncbuf, and typedefs
#	include <syncstream>
#endif

// 	Helper functions to control the format of input and output
#	include <iomanip>
// 	std::basic_streambuf class template
#	include <streambuf>
// 	C-style input-output functions
#	include <cstdio>

#if _HAS_CXX17
// 	std::path class and supporting functions
#	include <filesystem>
#endif

// 	Classes, algorithms and iterators to support regular expression processing
#	include <regex>
// 	Atomic operations library
#	include <atomic>
// 	std::thread class and supporting functions
#	include <thread>

#if _HAS_CXX20
// 	Stop tokens for std::jthread
#	include <stop_token>
#endif

// 	Mutual exclusion primitives
#	include <mutex>
// 	Shared mutual exclusion primitives
#	include <shared_mutex>
// 	Primitives for asynchronous computations
#	include <future>
// 	Thread waiting conditions
#	include <condition_variable>

#if _HAS_CXX20
// 	Semaphores
#	include <semaphore>
// 	Latches
#	include <latch>
// 	Barriers
#	include <barrier>
#endif

// Token manipulation

#define GAEA_TOKEN_STRINGIFICATION_internal(token) #token
#define GAEA_TOKEN_STRINGIFICATION(token) GAEA_TOKEN_STRINGIFICATION_internal(token)

#define GAEA_TOKEN_CONCATENATION_internal(token1, token2) token1 ## token2
#define GAEA_TOKEN_CONCATENATION(token1, token2) GAEA_TOKEN_CONCATENATION_internal(token1, token2)

// This allows us to work around some problems in some compilers,
// namely https://stackoverflow.com/questions/5134523/msvc-doesnt-expand-va-args-correctly
#define GAEA_TOKEN_EXPANSION(token) token

// Based on https://stackoverflow.com/a/44479664

#define GAEA_TOKEN_LIST_SIZE_internal(\
		_32, _31,\
		_30, _29, _28, _27, _26, _25, _24, _23, _22, _21,\
		_20, _19, _18, _17, _16, _15, _14, _13, _12, _11,\
		_10, _9, _8, _7, _6, _5, _4, _3, _2, _1,\
	token, ...) token

#define GAEA_TOKEN_LIST_SIZE(...) \
	GAEA_TOKEN_EXPANSION(\
		GAEA_TOKEN_LIST_SIZE_internal(__VA_ARGS__,\
		32, 31,\
		30, 29, 28, 27, 26, 25, 24, 23, 22, 21,\
		20, 19, 18, 17, 16, 15, 14, 13, 12, 11,\
		10, 9, 8, 7, 6, 5, 4, 3, 2, 1)\
	)

#define GAEA_TOKEN_LIST_HEAD(token, ...) token
#define GAEA_TOKEN_LIST_TAIL(token, ...) __VA_ARGS__

#define GAEA_MACRO_APPLY_1(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__))
#define GAEA_MACRO_APPLY_2(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_1(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_3(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_2(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_4(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_3(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_5(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_4(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_6(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_5(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_7(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_6(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_8(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_7(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_9(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_8(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_10(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_9(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_11(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_10(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_12(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_11(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_13(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_12(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_14(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_13(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_15(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_14(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_16(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_15(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_17(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_16(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_18(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_17(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_19(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_18(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_20(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_19(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_21(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_20(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_22(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_21(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_23(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_22(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_24(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_23(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_25(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_24(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_26(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_25(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_27(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_26(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_28(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_27(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_29(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_28(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_30(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_29(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_31(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_30(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))
#define GAEA_MACRO_APPLY_32(func, ...) func(GAEA_TOKEN_LIST_HEAD(__VA_ARGS__)), GAEA_MACRO_APPLY_31(func, GAEA_TOKEN_LIST_TAIL(__VA_ARGS__))

#define GAEA_MACRO_APPLY(func, ...)  GAEA_TOKEN_CONCATENATION(GAEA_MACRO_APPLY_, GAEA_TOKEN_LIST_SIZE(__VA_ARGS__))(func, __VA_ARGS__)

// Development utilities

 /// <summary>Declare a counter that can only be incremented at namespace scope but read in any expression, at any scope.
 /// <para>The counter is bound to the namespace where it is declared.
 /// </para>
 /// <para>\b Examples:
 /// </para>
 /// <code>
 /// 
 ///	GAEA_DECLARE_CONSTANT_COUNTER(Counter);
 ///	
 ///	namespace Namespace {
 ///		GAEA_DECLARE_CONSTANT_COUNTER(Counter);
 ///	}
 ///	
 ///	GAEA_INCREMENT_CONSTANT_COUNTER(Counter);
 ///	
 ///	constexpr auto Value0 = GAEA_READ_CONSTANT_COUNTER(Namespace::Counter); // Value0 == 0
 ///	
 ///	namespace Namespace {
 ///		GAEA_INCREMENT_CONSTANT_COUNTER(Counter);
 ///		GAEA_INCREMENT_CONSTANT_COUNTER(Counter);
 ///	}
 ///	
 ///	constexpr auto Value1 = GAEA_READ_CONSTANT_COUNTER(Counter); // Value1 == 1
 ///	constexpr auto Value2 = GAEA_READ_CONSTANT_COUNTER(Namespace::Counter); // Value2 == 2
 /// </code>
 /// </summary>
 /// <param name="identifier">Name of the counter.
 /// </param>
#define GAEA_DECLARE_CONSTANT_COUNTER_internal(identifier, limit) \
	template <::std::size_t StateValue>\
	struct identifier ## _counter_state\
	{\
		static constexpr ::std::size_t value = StateValue;\
	};\
	template <::std::size_t StateValue>\
	struct identifier ## _counter_check\
	{\
		static_assert(StateValue <= limit,\
			"Cannot increment constant counter '" GAEA_TOKEN_STRINGIFICATION(identifier) "' above '" GAEA_TOKEN_STRINGIFICATION(limit) "'");\
		\
		using type = identifier ## _counter_state<StateValue>;\
	};\
	template <::std::size_t MemoryValue, ::std::size_t BitValue>\
	constexpr identifier ## _counter_state<MemoryValue> identifier ## _counter_rule(\
		identifier ## _counter_state<MemoryValue>,\
		identifier ## _counter_state<BitValue>\
	);

#define GAEA_DECLARE_CONSTANT_COUNTER_overload(identifier, limit, ...) GAEA_DECLARE_CONSTANT_COUNTER_internal(identifier, limit)
#define GAEA_DECLARE_CONSTANT_COUNTER(...) GAEA_TOKEN_EXPANSION(GAEA_DECLARE_CONSTANT_COUNTER_overload(__VA_ARGS__, 0xFFFFFFFF))

/// <summary>Read the value of the counter at the current point of a translation unit.
/// </summary>
/// <param name="identifier">Name of the counter.
/// </param>
/// <returns>Current value of the counter.
/// </returns>
#define GAEA_READ_CONSTANT_COUNTER(identifier) \
	(decltype(\
		identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
			identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
				identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
					identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
						identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
							identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
								identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
									identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(identifier ## _counter_rule(\
										identifier ## _counter_state<0x00000000>(),\
									identifier ## _counter_state<0x80000000>()), identifier ## _counter_state<0x40000000>()), identifier ## _counter_state<0x20000000>()), identifier ## _counter_state<0x10000000>()),\
								identifier ## _counter_state<0x08000000>()), identifier ## _counter_state<0x04000000>()), identifier ## _counter_state<0x02000000>()), identifier ## _counter_state<0x01000000>()),\
							identifier ## _counter_state<0x00800000>()), identifier ## _counter_state<0x00400000>()), identifier ## _counter_state<0x00200000>()), identifier ## _counter_state<0x00100000>()),\
						identifier ## _counter_state<0x00080000>()), identifier ## _counter_state<0x00040000>()), identifier ## _counter_state<0x00020000>()), identifier ## _counter_state<0x00010000>()),\
					identifier ## _counter_state<0x00008000>()), identifier ## _counter_state<0x00004000>()), identifier ## _counter_state<0x00002000>()), identifier ## _counter_state<0x00001000>()),\
				identifier ## _counter_state<0x00000800>()), identifier ## _counter_state<0x00000400>()), identifier ## _counter_state<0x00000200>()), identifier ## _counter_state<0x00000100>()),\
			identifier ## _counter_state<0x00000080>()), identifier ## _counter_state<0x00000040>()), identifier ## _counter_state<0x00000020>()), identifier ## _counter_state<0x00000010>()),\
		identifier ## _counter_state<0x00000008>()), identifier ## _counter_state<0x00000004>()), identifier ## _counter_state<0x00000002>()), identifier ## _counter_state<0x00000001>())\
	)::value)

///	<summary>Increment the value of the counter. This statement can only appear at namespace scope.
///	</summary>
/// <param name="identifier">Name of the counter.
/// </param>
#define GAEA_INCREMENT_CONSTANT_COUNTER(identifier) \
	constexpr typename identifier ## _counter_check<(GAEA_READ_CONSTANT_COUNTER(identifier) + 1)>::type identifier ## _counter_rule(\
		identifier ## _counter_state<(GAEA_READ_CONSTANT_COUNTER(identifier) + 1) & GAEA_READ_CONSTANT_COUNTER(identifier)>,\
		identifier ## _counter_state<(GAEA_READ_CONSTANT_COUNTER(identifier) + 1) & ~GAEA_READ_CONSTANT_COUNTER(identifier)>\
	);\

#define GAEA_CRASH(message) throw std::runtime_error(message);

#define GAEA_UNUSED(...) [] (auto&&...) constexpr -> void {}(__VA_ARGS__)

// Source code checks

#if defined(_DEBUG)
#	define GAEA_DEBUG 1
#else
#	define GAEA_DEBUG 0
#endif

#define GAEA_USING_PRECISE_RSQRT 0

// Source code decorators

#if defined(_MSC_VER)
#	define GAEA_FORCE_INLINE __forceinline
#	if GAEA_DEBUG
#		define GAEA_DEBUG_INLINE __declspec(noinline)
#	else
#		define GAEA_DEBUG_INLINE __forceinline
#	endif
#else
#	error Not implemented for this platform!
#	define GAEA_FORCE_INLINE
#	define GAEA_DEBUG_INLINE
#endif

// Engine scalar types 

using int8 = std::int8_t;
using int16 = std::int16_t;
using int32 = std::int32_t;
using int64 = std::int64_t;

using uint8 = std::uint8_t;
using uint16 = std::uint16_t;
using uint32 = std::uint32_t;
using uint64 = std::uint64_t;

using float32 = float;
using float64 = double;

//using sizeint = std::size_t;
//using diffint = std::ptrdiff_t;

//using pfloat = float32;

// Engine literal operators

//constexpr std::size_t operator "" _sz(unsigned long long value)
//{
//    return std::size_t(value);
//}
//
//constexpr std::ptrdiff_t operator "" _df(unsigned long long value)
//{
//    return std::ptrdiff_t(value);
//}

//constexpr pfloat operator "" _pf(long double value)
//{
//	return pfloat(value);
//}

// Engine namespace aliases

namespace stdfs = std::filesystem;
namespace stdchr = std::chrono;
	
//using namespace std::chrono_literals;

namespace gaea::implementation
{
	// Taken from https://stackoverflow.com/a/34134071

	template <class FloatType>
	constexpr FloatType constant_sqrt_newton_raphson(FloatType new_value, FloatType value, FloatType old_value)
	{
		return value != old_value // NOLINT(clang-diagnostic-float-equal)
			? constant_sqrt_newton_raphson(new_value, (value + new_value / value) / FloatType(2.0), value)
			: value;
	}

	template <class FloatType>
	constexpr FloatType constant_sqrt(FloatType value)
	{
		static_assert(std::is_floating_point_v<FloatType>);
		return value >= FloatType(0.0) && value < std::numeric_limits<FloatType>::infinity()
			? constant_sqrt_newton_raphson(value, value, FloatType(0.0))
			: std::numeric_limits<FloatType>::quiet_NaN();
	}

	template <class ArithType>
	constexpr ArithType constant_power(ArithType base, ArithType exponent)
	{
		return exponent > ArithType(0)
			? base * constant_power(base, exponent - ArithType(1))
			: ArithType(1);
	}

	template <uint64 ... DigitValues>
	constexpr float64 constant_pi_bbp_formula(std::integer_sequence<uint64, DigitValues ...>)
	{
		return (... + (1.0 / constant_power(16.0, float64(DigitValues)) 
					* (4.0 / (8.0 * float64(DigitValues) + 1.0) 
						- 2.0 / (8.0 * float64(DigitValues) + 4.0) 
						- 1.0 / (8.0 * float64(DigitValues) + 5.0) 
						- 1.0 / (8.0 * float64(DigitValues) + 6.0))));
	}

	template <class FloatType>
	constexpr FloatType constant_pi()
	{
		static_assert(std::is_floating_point_v<FloatType>);
		return FloatType(constant_pi_bbp_formula(std::conditional_t<std::is_same_v<FloatType, float32>, 
			std::make_integer_sequence<uint64, 9>, 
			std::make_integer_sequence<uint64, 17>>{}));
	}
}

namespace gaea
{
	// Default tolerance for floating-point comparison

	inline constexpr float64 epsilon						= implementation::constant_sqrt(std::numeric_limits<float64>::epsilon());
	inline constexpr float32 epsilon_f						= implementation::constant_sqrt(std::numeric_limits<float32>::epsilon());
	
	inline constexpr float64 tiny_epsilon					= std::numeric_limits<float64>::epsilon();
	inline constexpr float32 tiny_epsilon_f					= std::numeric_limits<float32>::epsilon();

	// Common mathematical constants

	inline constexpr float64 pi								= implementation::constant_pi<float64>();
	inline constexpr float32 pi_f							= implementation::constant_pi<float32>();

	// Radians-degrees conversion

	inline constexpr float64 rad2deg						= 180.0 / pi;
	inline constexpr float32 rad2deg_f						= float32(rad2deg);

	// Degrees-radians conversion

	inline constexpr float64 deg2rad						= pi / 180.0;
	inline constexpr float32 deg2rad_f						= float32(deg2rad);

	// Common ratio constants

	inline constexpr float64 sqrt_one_half					= implementation::constant_sqrt(1.0 / 2.0);
	inline constexpr float32 sqrt_one_half_f				= float32(sqrt_one_half);

	inline constexpr float64 sqrt_one_third					= implementation::constant_sqrt(1.0 / 3.0);
	inline constexpr float32 sqrt_one_third_f				= float32(sqrt_one_third);

	inline constexpr float64 sqrt_two_third					= implementation::constant_sqrt(2.0 / 3.0);
	inline constexpr float32 sqrt_two_third_f				= float32(sqrt_two_third);

	inline constexpr float64 sqrt_one_fourth				= implementation::constant_sqrt(1.0 / 4.0);
	inline constexpr float32 sqrt_one_fourth_f				= float32(sqrt_one_fourth);

	inline constexpr float64 sqrt_three_fourth				= implementation::constant_sqrt(3.0 / 4.0);
	inline constexpr float32 sqrt_three_fourth_f			= float32(sqrt_three_fourth);

	inline constexpr float64 sqrt_two						= implementation::constant_sqrt(2.0);
	inline constexpr float32 sqrt_two_f						= float32(sqrt_two);

	inline constexpr float64 sqrt_three						= implementation::constant_sqrt(3.0);
	inline constexpr float32 sqrt_three_f					= float32(sqrt_three);

	inline constexpr float64 sqrt_six						= implementation::constant_sqrt(6.0);
	inline constexpr float32 sqrt_six_f						= float32(sqrt_six);

	// Trigonometric constants

	inline constexpr float64 sin_0							= 0.0;
	inline constexpr float32 sin_0_f						= 0.0f;

	inline constexpr float64 cos_0							= 1.0;
	inline constexpr float32 cos_0_f						= 1.0f;

	inline constexpr float64 tan_0							= 0.0;
	inline constexpr float32 tan_0_f						= 0.0f;

	inline constexpr float64 sin_15							= (sqrt_six - sqrt_two) / 4.0;
	inline constexpr float32 sin_15_f						= float32(sin_15);

	inline constexpr float64 cos_15							= (sqrt_six + sqrt_two) / 4.0;
	inline constexpr float32 cos_15_f						= float32(cos_15);

	inline constexpr float64 tan_15							= 2.0 - sqrt_three;
	inline constexpr float32 tan_15_f						= float32(tan_15);

	inline constexpr float64 sin_30							= 0.5;
	inline constexpr float32 sin_30_f						= 0.5f;

	inline constexpr float64 cos_30							= sqrt_three_fourth;
	inline constexpr float32 cos_30_f						= sqrt_three_fourth_f;

	inline constexpr float64 tan_30							= sqrt_one_third;
	inline constexpr float32 tan_30_f						= sqrt_one_third_f;

	inline constexpr float64 sin_45							= sqrt_one_half;
	inline constexpr float32 sin_45_f						= sqrt_one_half_f;

	inline constexpr float64 cos_45							= sqrt_one_half;
	inline constexpr float32 cos_45_f						= sqrt_one_half_f;

	inline constexpr float64 tan_45							= 1.0;
	inline constexpr float32 tan_45_f						= 1.0f;

	inline constexpr float64 sin_60							= sqrt_three_fourth;
	inline constexpr float32 sin_60_f						= sqrt_three_fourth_f;

	inline constexpr float64 cos_60							= 0.5;
	inline constexpr float32 cos_60_f						= 0.5f;

	inline constexpr float64 tan_60							= sqrt_three;
	inline constexpr float32 tan_60_f						= sqrt_three_f;

	inline constexpr float64 sin_75							= (sqrt_six + sqrt_two) / 4.0;
	inline constexpr float32 sin_75_f						= float32(sin_75);

	inline constexpr float64 cos_75							= (sqrt_six - sqrt_two) / 4.0;
	inline constexpr float32 cos_75_f						= float32(cos_75);

	inline constexpr float64 tan_75							= 2.0 + sqrt_three;
	inline constexpr float32 tan_75_f						= float32(tan_75);

	inline constexpr float64 sin_90							= 1.0;
	inline constexpr float32 sin_90_f						= 1.0f;

	inline constexpr float64 cos_90							= 0.0;
	inline constexpr float32 cos_90_f						= 0.0f;

	inline constexpr float64 tan_90							= std::numeric_limits<float64>::infinity();
	inline constexpr float32 tan_90_f						= std::numeric_limits<float32>::infinity();

	// Engine and system common enumerators
	
	// WARNING: if these change, check flags bitsizes

	enum class basic_color              : uint8
	{
		black                           = 0x0,
		dark_gray						= 0x1,
		light_gray						= 0x2,
		white                           = 0x3,

		// Primary
		
		red                             = 0x4,
		green                           = 0x5,
		blue                            = 0x6,

		// Secondary
		
		yellow                          = 0x7,
		cyan                            = 0x8,
		magenta                         = 0x9,

		// Tertiary
		
		orange							= 0xA,
		chartreuse						= 0xB,
		spring							= 0xC,
		azure							= 0xD,
		violet							= 0xE,
		rose							= 0xF,
	};

	enum class log_type                 : uint8
	{
		null                            = 0xFF,
		
		debug                           = 0x00,
		info                            = 0x01,
		warn                            = 0x02,
		error                           = 0x03,
		fatal                           = 0x04,
		
		custom							= 0x80,
	};

	enum class thread_type				: uint8
	{
		engine							= 0x0,
		render							= 0x1,
		simulate						= 0x2,
		other							= 0x3,
	};

	enum class simulation_object		: uint8
	{
		rigid_body                      = 0x0,
//      soft_body                       = 0x1,
		
//		constraint						= 0x2,
	};
	
	enum class render_priority			: uint8
	{
		critical						= 0x0,
		above_high						= 0x1,
		high							= 0x2,
		above_normal					= 0x3,
		normal							= 0x4,
		below_normal					= 0x5,
		low								= 0x6,
		below_low						= 0x7,
	};

	enum class tick_stage               : uint8
	{
		none                            = 0b00,
		pre_physics                     = 0b01,
		during_physics                  = 0b10,
		post_physics                    = 0b11,
	};

	enum class body_visibility          : uint8
	{
		invisible                       = 0b0,
		visible                         = 0b1,
	};

	enum class body_mobility            : uint8
	{
		immovable                       = 0b0,
		movable                         = 0b1,
	};
	
	enum class input_key	            : uint8
	{// ISO/IEC 9995-2 - QWERTY
		key_escape                      = 0x00,
		
		key_f1,
		key_f2,
		key_f3,
		key_f4,
		
		key_f5,
		key_f6,
		key_f7,
		key_f8,
		
		key_f9,
		key_f10,
		key_f11,
		key_f12,

		key_RESERVED_1,
		key_1,
		key_2,
		key_3,
		key_4,
		key_5,
		key_6,
		key_7,
		key_8,
		key_9,
		key_0,
		key_RESERVED_2,
		key_RESERVED_3,
		key_backspace,
		key_tab,
		key_Q,
		key_W,
		key_E,
		key_R,
		key_T,
		key_Y,
		key_U,
		key_I,
		key_O,
		key_P,
		key_RESERVED_4,
		key_RESERVED_5,
		key_enter,
		key_caps_lock,
		key_A,
		key_S,
		key_D,
		key_F,
		key_G,
		key_H,
		key_J,
		key_K,
		key_L,
		key_RESERVED_6,
		key_RESERVED_7,
		key_RESERVED_8,
		key_left_shift,
		key_RESERVED_9,
		key_Z,
		key_X,
		key_C,
		key_V,
		key_B,
		key_N,
		key_M,
		key_RESERVED_10,
		key_RESERVED_11,
		key_RESERVED_12,
		key_right_shift,
		key_left_ctrl,
		key_left_system,
		key_left_alt,
		key_space,
		key_right_alt,
		key_right_system,
		key_menu,
		key_right_ctrl,

		key_print_screen,
		key_scroll_lock,
		key_pause,

		key_insert,
		key_delete,
		key_home,
		key_end,
		key_page_up,
		key_page_down,

		key_up,
		key_down,
		key_left,
		key_right,

		key_num_lock,
		key_numpad_sep,
		key_numpad_0,
		key_numpad_1,
		key_numpad_2,
		key_numpad_3,
		key_numpad_4,
		key_numpad_5,
		key_numpad_6,
		key_numpad_7,
		key_numpad_8,
		key_numpad_9,
		key_numpad_enter,
		key_numpad_add,
		key_numpad_sub,
		key_numpad_mul,
		key_numpad_div,

		key_UNKNOWN,
	};

	enum class input_mouse	            : uint8
	{
		mouse_lmb			            = 0x00,
		mouse_mmb,
		mouse_rmb,

		mouse_scroll_up,
		mouse_scroll_down,

		mouse_UNKNOWN
	};

	enum class input_state              : uint8
	{
		state_released					= 0x00,
		state_pressed					= 0x01,
		state_repeating					= 0x02,
		
		state_UNKNOWN					= 0x80,
	};

	enum class input_modifier           : uint8
	{
		modifier_NONE                   = 0x00,
		modifier_ALL					= 0x77,
		
		modifier_shift                  = 0x01,
		modifier_ctrl                   = 0x02,
		modifier_alt                    = 0x04,
		
		modifier_caps                   = 0x10,
		modifier_scroll                 = 0x20,
		modifier_num                    = 0x40,
		
		modifier_UNKNOWN                = 0x80,
	};

	inline bool has_unknown_modifiers(input_modifier flag)
	{
		return (uint8(flag) & uint8(input_modifier::modifier_UNKNOWN)) == uint8(input_modifier::modifier_UNKNOWN);
	}

	inline bool has_all_modifiers(input_modifier flag)
	{
		return flag == input_modifier::modifier_ALL;
	}

	inline bool has_any_modifiers(input_modifier flag)
	{
		return flag != input_modifier::modifier_NONE;
	}

	inline bool has_none_modifiers(input_modifier flag)
	{
		return flag == input_modifier::modifier_NONE;
	}
	
	template <class ... ModifierTypes>
	bool has_all_modifiers(input_modifier flag, ModifierTypes ... values)
	{
		static_assert((... && std::is_same_v<input_modifier, ModifierTypes>));
		uint8 all_modifiers = (... | uint8(values));
		return (uint8(flag) & all_modifiers) == all_modifiers;
	}
	
	template <class ... ModifierTypes>
	bool has_any_modifiers(input_modifier flag, ModifierTypes ... values)
	{
		static_assert((... && std::is_same_v<input_modifier, ModifierTypes>));
		return (uint8(flag) & (... | uint8(values))) != uint8(input_modifier::modifier_NONE);
	}
	
	template <class ... ModifierTypes>
	bool has_none_modifiers(input_modifier flag, ModifierTypes ... values)
	{
		static_assert((... && std::is_same_v<input_modifier, ModifierTypes>));
		return (uint8(flag) & (... | uint8(values))) == uint8(input_modifier::modifier_NONE);
	}

	inline input_modifier operator&(input_modifier a, input_modifier b)
	{
		return input_modifier(uint8(a) & uint8(b));
	}

	inline input_modifier operator|(input_modifier a, input_modifier b)
	{
		return input_modifier(uint8(a) | uint8(b));
	}

	inline input_modifier operator^(input_modifier a, input_modifier b)
	{
		return input_modifier(uint8(a) ^ uint8(b));
	}

	inline input_modifier operator~(input_modifier value)
	{
		return value ^ input_modifier::modifier_ALL;
	}

	// Engine utility types and functions
	
	struct skip_init_tag {};

	template <class>
	class _unique_flag final
	{
	public:
		explicit operator bool() const
		{
			return std::exchange(_flag, false);
		}

	private:
		inline thread_local static bool _flag = true;

	};

	template <class UniqueType>
	_unique_flag<UniqueType> _make_unique_flag(UniqueType)
	{
		return _unique_flag<UniqueType>{};
	}

	template <std::size_t LengthValue>
	class _formattable_string final
	{
	public:
		_formattable_string(char const(&format_string)[LengthValue])
			: _format_string{ format_string }
		{
		}

		_formattable_string(_formattable_string const&) = delete;
		_formattable_string& operator=(_formattable_string const&) = delete;

		_formattable_string(_formattable_string&&) = delete;
		_formattable_string& operator=(_formattable_string&&) = delete;

		~_formattable_string() = default;

		template <class ... ArgTypes>
		std::string format(ArgTypes&&... args)
		{
			std::string result;

			// Only bother with printf if we actually have arguments
			if constexpr (sizeof...(args) > 0)
			{
				char buffer[LengthValue + 128 * sizeof...(args)];
				int buffer_size = sizeof buffer;

				// Try to build the message with a statically-allocated buffer
				char* buffer_ptr = buffer;
				int rebuffer_size = std::snprintf(buffer_ptr, std::size_t(buffer_size), _format_string, std::forward<ArgTypes>(args) ...); // NOLINT(clang-diagnostic-format-security)

				// Otherwise, create a dynamically-allocated buffer to support the message
				if (rebuffer_size >= buffer_size)
				{
					rebuffer_size += 1; // Need to account for null-character

					char* rebuffer = static_cast<char*>(std::malloc(rebuffer_size));
					if (!rebuffer)
					{
						return result;
					}

					buffer_ptr = rebuffer;
					buffer_size = rebuffer_size;

					rebuffer_size = std::snprintf(buffer_ptr, std::size_t(buffer_size), _format_string, std::forward<ArgTypes>(args) ...); // NOLINT(clang-diagnostic-format-security)
				}

				// If we successfully built the message, copy it to result
				if (rebuffer_size >= 0 && rebuffer_size < buffer_size)
				{
					result = std::string{ buffer_ptr };
				}

				// If we used a dynamic buffer, deallocate it now
				if (buffer_ptr != buffer)
				{
					std::free(buffer_ptr);
				}
			}
			else
			{
				result = std::string{ _format_string };
			}

			return result;
		}

	private:
		char const(&_format_string)[LengthValue];

	};

	template <std::size_t LengthValue, class ... ArgTypes>
	std::string make_formattable_string(char const(&format)[LengthValue], ArgTypes&& ... args)
	{
		return _formattable_string<LengthValue>{ format }.format(std::forward<ArgTypes>(args) ...);
	}

	template <class Type>
	class scope_guard final
	{
	public:
		scope_guard(Type& var_ref, Type&& new_val)
			: _var_ref{ var_ref }
			, _old_val{ std::move(_var_ref) }
		{
			_var_ref = std::move(new_val);
		}

		scope_guard(scope_guard const&) = delete;
		scope_guard& operator=(scope_guard const&) = delete;

		scope_guard(scope_guard&&) = delete;
		scope_guard& operator=(scope_guard&&) = delete;

		~scope_guard()
		{
			_var_ref = std::move(_old_val);
		}

	private:
		Type& _var_ref;
		Type _old_val;

	};

	template <class RatioType = std::ratio<1>>
	class scope_stopwatch final
	{
	public:
		scope_stopwatch(float32& split_ref)
			: _split_ref{ split_ref }
			, _start_time{ stdchr::steady_clock::now() }
		{
		}

		scope_stopwatch(scope_stopwatch const&) = delete;
		scope_stopwatch& operator=(scope_stopwatch const&) = delete;

		scope_stopwatch(scope_stopwatch&&) = delete;
		scope_stopwatch& operator=(scope_stopwatch&&) = delete;

		~scope_stopwatch()
		{
			_split_ref = stdchr::duration<float32, RatioType>(stdchr::steady_clock::now() - _start_time).count();
		}

	private:
		float32& _split_ref;
		stdchr::steady_clock::time_point _start_time;

	};
}

#endif
