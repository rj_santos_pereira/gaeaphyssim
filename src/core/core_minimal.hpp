#ifndef CORE_MINIMAL_INCLUDE_GUARD
#define CORE_MINIMAL_INCLUDE_GUARD

////////////////////////////////////////////////////////////////////////////////////////////////////
/// WARNING! This file cannot be included by a sycl_target!
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "core/core_engine.hpp"

// Platform identification

#if (defined(_WIN32) || defined(__CYGWIN__)) // Windows or Cygwin POSIX under Windows
#define GAEA_PLATFORM_WINDOWS 1
#else
#define GAEA_PLATFORM_WINDOWS 0
#endif
#if (defined(__linux__) && !defined(__ANDROID__)) // Debian, Ubuntu, Gentoo, Fedora, openSUSE, RedHat, CentOS and others
#define GAEA_PLATFORM_LINUX 1
#else
#define GAEA_PLATFORM_LINUX 0
#endif
#if (defined(__ANDROID__)) // Android
#define GAEA_PLATFORM_ANDROID 1
#else
#define GAEA_PLATFORM_ANDROID 0
#endif
#if defined(__APPLE__) && defined(__MACH__) // Apple OSX and iOS
#include <TargetConditionals.h>
#define GAEA_PLATFORM_OSX (!TARGET_OS_IPHONE)
#define GAEA_PLATFORM_IOS (TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR)
#else
#define GAEA_PLATFORM_OSX 0
#define GAEA_PLATFORM_IOS 0
#endif

// Platform includes

#if GAEA_PLATFORM_WINDOWS
#	ifndef NOMINMAX
#		define NOMINMAX
#	endif
#	ifndef WIN32_LEAN_AND_MEAN
#		define WIN32_LEAN_AND_MEAN
#	endif

#	include <Windows.h>
#	include <fcntl.h>
#	include <io.h>
#	include <shellapi.h>
#	include <ShObjIdl.h>
#	include <timeapi.h>
#endif

// Debug utilities

#define GAEA_EXECUTE_ONCE() ::gaea::_make_unique_flag([]{})

#if GAEA_PLATFORM_LINUX
#	include <signal.h>
#	define GAEA_DEBUG_BREAK() raise(SIGTRAP) // TODO FIXME
//#   define GAEA_COMPILER_BARRIER() ((void)0)
#elif GAEA_PLATFORM_WINDOWS
#	include <intrin.h>
#	define GAEA_DEBUG_BREAK() do if (IsDebuggerPresent()) { (__nop(), __debugbreak()); } while (0)
//#   define GAEA_COMPILER_BARRIER() (_ReadWriteBarrier())
#else
#	error Not implemented for this platform!
#	define GAEA_DEBUG_BREAK() std::abort()
//#   define GAEA_COMPILER_BARRIER() ((void)0)
#endif

#define GAEA_DEBUG_BREAK_ONCE() \
	do { if (GAEA_EXECUTE_ONCE()) { GAEA_DEBUG_BREAK(); } } while (0)

#define GAEA_DEBUG_BREAK_CRASH(message) \
	do { GAEA_DEBUG_BREAK(); GAEA_CRASH(message) } while (0)

// Logging utilities

#define GAEA_LOG_internal(type, message, ...) \
	do																													\
	{																													\
		if (engine)																										\
		{																												\
			auto GAEA_TOKEN_CONCATENATION(time_, __LINE__) = ::stdchr::system_clock::now();								\
			engine->log_message(																						\
				type,																									\
				::std::move(GAEA_TOKEN_CONCATENATION(time_, __LINE__)),													\
				::gaea::_formattable_string{ message }.format(__VA_ARGS__)												\
			);																											\
		}																												\
	}																													\
	while (0)

#define GAEA_LOG_DEBUGGER(message, ...) \
	GAEA_LOG_internal(::gaea::log_type::debug, message, __VA_ARGS__)

#define GAEA_INFO(category, message, ...) \
	GAEA_LOG_internal(::gaea::log_type::info, "<" GAEA_TOKEN_STRINGIFICATION(category) "> " message, __VA_ARGS__)

#define GAEA_WARN(category, message, ...) \
	GAEA_LOG_internal(::gaea::log_type::warn, "<" GAEA_TOKEN_STRINGIFICATION(category) "> " message, __VA_ARGS__)

#define GAEA_ERROR(category, message, ...) \
	GAEA_LOG_internal(::gaea::log_type::error, "<" GAEA_TOKEN_STRINGIFICATION(category) "> " message, __VA_ARGS__);		\
	GAEA_DEBUG_BREAK_ONCE()

#define GAEA_FATAL(category, message, ...) \
	GAEA_LOG_internal(::gaea::log_type::fatal, "<" GAEA_TOKEN_STRINGIFICATION(category) "> " message, __VA_ARGS__);		\
	GAEA_CRASH("0xDEADBEEF")

// Timing utilities

#define GAEA_SCOPE_STOPWATCH_internal_1(delta) ::gaea::scope_stopwatch<>{ delta }
#define GAEA_SCOPE_STOPWATCH_internal_2(delta, period) ::gaea::scope_stopwatch<period>{ delta }

#define GAEA_SCOPE_STOPWATCH_overload(_1, _2, overload, ...) \
	GAEA_TOKEN_CONCATENATION(GAEA_SCOPE_STOPWATCH_internal, overload)

#define GAEA_SCOPE_STOPWATCH(...) \
	auto GAEA_TOKEN_CONCATENATION(GAEA_SCOPE_STOPWATCH_, __LINE__) \
		= GAEA_TOKEN_EXPANSION(GAEA_SCOPE_STOPWATCH_overload(__VA_ARGS__, _2, _1)(__VA_ARGS__))

#endif
