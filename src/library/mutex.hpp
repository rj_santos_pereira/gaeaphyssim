#ifndef LIBRARY_MUTEX_INCLUDE_GUARD
#define LIBRARY_MUTEX_INCLUDE_GUARD

#include "core/core_common.hpp"
#include "library/atomic.hpp"

namespace gaea
{
	using mutex = std::mutex;

	using shared_mutex = std::shared_mutex;

	class semaphore
	{
	public:
		explicit semaphore(int64 initial_count);

		semaphore(semaphore const&) = delete;
		semaphore(semaphore&&) = delete;

		semaphore& operator=(semaphore const&) = delete;
		semaphore& operator=(semaphore&&) = delete;
		
		void acquire();
		bool try_acquire();
		void release(int64 count = 1);

	private:
		int64 _counter;

	};

	class barrier
	{
	public:
		explicit barrier(int64 initial_count);

		barrier(barrier const&) = delete;
		barrier(barrier &&) = delete;
		
		barrier& operator=(barrier const&) = delete;
		barrier& operator=(barrier &&) = delete;

		void wait();
		void busy_wait();

		void set_initial_count(int64 initial_count);
		
		int64 get_initial_count() const;
		
		int64 num_waiting() const;

	private:
		int64 _phase_counter;
		int64 _counter;
		int64 _initial_count;
		
	};

	class spin_mutex final
	{
	public:
		explicit spin_mutex() noexcept;
		
		spin_mutex(spin_mutex const&) = delete;
		spin_mutex(spin_mutex&&) = delete;
		
		spin_mutex& operator=(spin_mutex const&) = delete;
		spin_mutex& operator=(spin_mutex&&) = delete;
		
		~spin_mutex() = default;
		
		void lock() noexcept;
		bool try_lock() noexcept;
		void unlock() noexcept;

	private:
		atomic_flag _flag;

	};

	class shared_spin_mutex final
	{
	public:
		explicit shared_spin_mutex() noexcept;

		shared_spin_mutex(shared_spin_mutex const&) = delete;
		shared_spin_mutex(shared_spin_mutex&&) = delete;
		
		shared_spin_mutex& operator=(shared_spin_mutex const&) = delete;
		shared_spin_mutex& operator=(shared_spin_mutex&&) = delete;
		
		~shared_spin_mutex() = default;

		void lock();
		bool try_lock();
		void unlock();

		void lock_shared();
		bool try_lock_shared();
		void unlock_shared();

		bool upgrade();
		void downgrade();

	private:
		// Ensures state is valid for shared locking, returns whether flag_state was modified
		static bool _make_share_state(int64& flag_state);

		atomic_int64 _flag;
		
		spin_mutex _upgrade_mutex;

		static constexpr int64 _exclusive_state = std::numeric_limits<int64>::min();

	};
}

#endif
