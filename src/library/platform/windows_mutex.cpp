#include "library/platform/windows_mutex.hpp"

// Platform specific includes
#include <Windows.h>

namespace gaea::platform::windows
{
	void semaphore_acquire(int64* counter)
	{
		static int64 zero_count = 0;

		int64 old_count;
		int64 new_count = *counter;

		do
		{
			old_count = new_count;

			while (old_count == zero_count)
			{
				WaitOnAddress(counter, &zero_count, sizeof zero_count, INFINITE);
				old_count = *counter;
			}

			new_count = _InterlockedCompareExchange64(counter, old_count - 1, old_count);
		}
		while (new_count != old_count);
	}

	bool semaphore_try_acquire(int64* counter)
	{
		int64 old_count;
		int64 new_count = *counter;

		do
		{
			old_count = new_count;

			if (old_count == 0)
			{
				return false;
			}

			new_count = _InterlockedCompareExchange64(counter, old_count - 1, old_count);
		}
		while (new_count != old_count);

		return true;
	}

	void semaphore_release(int64* counter, int64 initial_count)
	{
		_InterlockedExchangeAdd64(counter, initial_count);
		WakeByAddressAll(counter);
	}

	void barrier_wait(int64* phase_counter, int64* counter, int64 initial_count)
	{
		int64 last_phase = *phase_counter;
		int64 curr_count = _InterlockedExchangeAdd64(counter, -1) - 1;
		
		if (curr_count)
		{
			// Wait time, sleep until phase_counter advances
			while (*phase_counter == last_phase)
			{
				WaitOnAddress(phase_counter, &last_phase, sizeof last_phase, INFINITE);
			}
		}
		else
		{
			// Sync time: reset counter, advance phase_counter, and wake all threads
			_InterlockedExchange64(counter, initial_count);
			_InterlockedExchange64(phase_counter, last_phase + 1);
			
			WakeByAddressAll(phase_counter);
		}
	}

	void barrier_busy_wait(int64* phase_counter, int64* counter, int64 initial_count)
	{
		int64 last_phase = *phase_counter;
		int64 curr_count = _InterlockedExchangeAdd64(counter, -1) - 1;

		if (curr_count)
		{
			// Wait time, yield until phase_counter advances
			while (*phase_counter == last_phase)
			{
				std::this_thread::yield();
			}
		}
		else
		{
			// Sync time: reset counter, advance phase_counter, and wake all threads
			_InterlockedExchange64(counter, initial_count);
			_InterlockedExchange64(phase_counter, last_phase + 1);
			
			WakeByAddressAll(phase_counter);
		}
	}

	int64 barrier_num_waiting(int64 const* counter)
	{
		return *counter;
	}
}
