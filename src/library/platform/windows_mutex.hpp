#ifndef LIBRARY_PLATFORM_WINDOWS_MUTEX_INCLUDE_GUARD
#define LIBRARY_PLATFORM_WINDOWS_MUTEX_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea::platform::windows
{
	void semaphore_acquire(int64* counter);

	bool semaphore_try_acquire(int64* counter);

	void semaphore_release(int64* counter, int64 initial_count);

	void barrier_wait(int64* phase_counter, int64* counter, int64 initial_count);

	void barrier_busy_wait(int64* phase_counter, int64* counter, int64 initial_count);

	int64 barrier_num_waiting(int64 const* counter);
}

#endif
