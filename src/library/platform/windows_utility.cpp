#include "library/platform/windows_utility.hpp"

// Platform specific includes
#include <Windows.h>

#include "library/utility.hpp"

namespace gaea::platform::windows::util
{
	std::string system_time_to_string(std::string const& format, stdchr::system_clock::time_point const& time)
	{
		std::time_t tm_src = stdchr::system_clock::to_time_t(time);
		std::tm tm_dst{};

		std::string tm_str;

		if (localtime_s(&tm_dst, &tm_src) == 0)
		{
			char buffer[1024];
			
			char* buffer_ptr = buffer;
			std::size_t buffer_size = 1024;

			while (std::strftime(buffer_ptr, buffer_size, format.c_str(), &tm_dst) == 0)
			{
				buffer_size <<= 1;

				char* rebuffer_ptr = static_cast<char*>(buffer_ptr == buffer ? std::malloc(buffer_size) : std::realloc(buffer_ptr, buffer_size));
				if (!rebuffer_ptr)
				{
					break;
				}

				buffer_ptr = rebuffer_ptr;
			}

			tm_str = std::string{ buffer_ptr };

			if (buffer_ptr != buffer)
			{
				std::free(buffer_ptr);
			}
		}

		return tm_str;
	}

	bool rename_thread(std::string const& thread_name)
	{
		if (FAILED(SetThreadDescription(GetCurrentThread(), gaea::util::narrow_to_wide(thread_name).c_str())))
		{
			return false;
		}
		return true;
	}
}
