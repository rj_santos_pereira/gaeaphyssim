#ifndef LIBRARY_PLATFORM_WINDOWS_UTILITY_INCLUDE_GUARD
#define LIBRARY_PLATFORM_WINDOWS_UTILITY_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea::platform::windows::util
{
	std::string system_time_to_string(std::string const& format, stdchr::system_clock::time_point const& time);

	bool rename_thread(std::string const& thread_name);
}

#endif
