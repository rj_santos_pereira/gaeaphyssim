#ifndef LIBRARY_MATH_QUATERNION_INCLUDE_GUARD
#define LIBRARY_MATH_QUATERNION_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/utility.hpp"
#include "library/math/generic_math.hpp"
#include "library/math/matrix.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	// https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation
	// https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	// https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion

	template <class FloatType>
	struct gquaternion
	{
		static_assert(std::is_floating_point_v<FloatType>);

		explicit constexpr gquaternion() = default;
		explicit constexpr gquaternion(FloatType value)
			: w{ value }
			, x{ value }
			, y{ value }
			, z{ value }
		{
		}
		explicit constexpr gquaternion(FloatType nw, FloatType nx, FloatType ny, FloatType nz)
			: w{ nw }
			, x{ nx }
			, y{ ny }
			, z{ nz }
		{
		}
		explicit constexpr gquaternion(FloatType scalar, gvector<FloatType> const& vectorial)
			: w{ scalar }
			, x{ vectorial.x }
			, y{ vectorial.y }
			, z{ vectorial.z }
		{
		}

		GAEA_FORCE_INLINE gquaternion& operator+=(gquaternion const& quat)
		{
			w += quat.w;
			x += quat.x;
			y += quat.y;
			z += quat.z;

			return *this;
		}

		GAEA_FORCE_INLINE gquaternion& operator+=(FloatType val)
		{
			w += val;
			x += val;
			y += val;
			z += val;

			return *this;
		}

		GAEA_FORCE_INLINE gquaternion operator+(gquaternion const& quat) const
		{
			return gquaternion{ *this } += quat;
		}

		GAEA_FORCE_INLINE gquaternion operator+(FloatType val) const
		{
			return gquaternion{ *this } += val;
		}

		GAEA_FORCE_INLINE friend gquaternion operator+(FloatType val, gquaternion const& quat)
		{
			return gquaternion{ quat } += val;
		}

		GAEA_FORCE_INLINE gquaternion& operator*=(gquaternion const& quat)
		{
			FloatType nw = w * quat.w - x * quat.x - y * quat.y - z * quat.z;
			FloatType nx = w * quat.x + x * quat.w + y * quat.z - z * quat.y;
			FloatType ny = w * quat.y - x * quat.z + y * quat.w + z * quat.x;
			FloatType nz = w * quat.z + x * quat.y - y * quat.x + z * quat.w;

			w = nw;
			x = nx;
			y = ny;
			z = nz;

			return *this;
		}

		GAEA_FORCE_INLINE gquaternion& operator*=(FloatType val)
		{
			w *= val;
			x *= val;
			y *= val;
			z *= val;

			return *this;
		}

		GAEA_FORCE_INLINE gquaternion operator*(gquaternion const& quat) const
		{
			return gquaternion{ *this } *= quat;
		}

		GAEA_FORCE_INLINE gquaternion operator*(FloatType val) const
		{
			return gquaternion{ *this } *= val;
		}

		GAEA_FORCE_INLINE friend gquaternion operator*(FloatType val, gquaternion const& quat)
		{
			return gquaternion{ quat } *= val;
		}

		GAEA_FORCE_INLINE friend gvector<FloatType> operator*(gquaternion<FloatType> const& quat, gvector<FloatType> const& vec)
		{
#if 0
			// 18 adds, 21 muls
			FloatType qqx = -(quat.x * quat.x);
			FloatType qqy = -(quat.y * quat.y);
			FloatType qqz = -(quat.z * quat.z);
			
			FloatType qwx = quat.w * quat.x;
			FloatType qwy = quat.w * quat.y;
			FloatType qwz = quat.w * quat.z;
			
			FloatType qxy = quat.x * quat.y;
			FloatType qyz = quat.y * quat.z;
			FloatType qzx = quat.z * quat.x;

			// Note that reciprocal requires only negating all qw components in the following expression
			return vec + FloatType(2.0) * vector{
				vec.x * (qqy + qqz) + vec.y * (-qwz + qxy) + vec.z * (qwy + qzx),
				vec.y * (qqz + qqx) + vec.z * (-qwx + qyz) + vec.x * (qwz + qxy),
				vec.z * (qqx + qqy) + vec.x * (-qwy + qzx) + vec.y * (qwx + qyz),
			};
#endif
			// 12 adds, 18 muls
			FloatType qs = quat.w;
			gvector<FloatType> qv{ quat.x, quat.y, quat.z };

			gvector<FloatType> n = FloatType(2.0) * gvector<FloatType>::cross(qv, vec);
			return vec + (qs * n) + gvector<FloatType>::cross(qv, n);
		}

		GAEA_FORCE_INLINE gquaternion& normalize()
		{
			FloatType magn_sq = magnitude_square();

			if (!math::nearly_equal<FloatType>(magn_sq, FloatType(1.0)) && !math::nearly_zero<FloatType>(magn_sq))
			{
				FloatType factor;
#if GAEA_USING_PRECISE_RSQRT
				factor = FloatType(1.0) / std::sqrt(magn_sq);
#else
				if constexpr (std::is_same_v<FloatType, float32>)
				{
					factor = math::rsqrt_single_abs_2NR(magn_sq);
				}
				else
				{
					factor = math::rsqrt_double_rel_1NR(magn_sq);
				}
#endif

				w *= factor;
				x *= factor;
				y *= factor;
				z *= factor;
			}

			return *this;
		}

		GAEA_FORCE_INLINE gquaternion& normalize_unsafe()
		{
			FloatType magn_sq = magnitude_square();
			FloatType factor;
#if GAEA_USING_PRECISE_RSQRT
			factor = FloatType(1.0) / std::sqrt(magn_sq);
#else
			if constexpr (std::is_same_v<FloatType, float32>)
			{
				factor = math::rsqrt_single_abs_2NR(magn_sq);
			}
			else
			{
				factor = math::rsqrt_double_rel_1NR(magn_sq);
			}
#endif

			w *= factor;
			x *= factor;
			y *= factor;
			z *= factor;

			return *this;
		}

		GAEA_FORCE_INLINE gquaternion unit() const
		{
			return gquaternion{ *this }.normalize();
		}

		GAEA_FORCE_INLINE gquaternion reciprocal() const
		{
			return gquaternion{ w, -x, -y, -z };
		}

		GAEA_FORCE_INLINE FloatType magnitude() const
		{
			return std::sqrt(magnitude_square());
		}

		GAEA_FORCE_INLINE FloatType magnitude_square() const
		{
			return w * w + x * x + y * y + z * z;
		}

		GAEA_FORCE_INLINE bool nearly_equal(gquaternion const& quat, FloatType tolerance = math::select_epsilon<FloatType>::epsilon) const
		{
			return math::nearly_equal<FloatType>(w, quat.w, tolerance)
				&& math::nearly_equal<FloatType>(x, quat.x, tolerance)
				&& math::nearly_equal<FloatType>(y, quat.y, tolerance)
				&& math::nearly_equal<FloatType>(z, quat.z, tolerance);
		}

		GAEA_FORCE_INLINE bool almost_identity(FloatType tolerance = math::select_epsilon<FloatType>::epsilon) const
		{
			return math::nearly_equal<FloatType>(w, FloatType(1.0), tolerance)
				&& math::nearly_zero<FloatType>(x, tolerance)
				&& math::nearly_zero<FloatType>(y, tolerance)
				&& math::nearly_zero<FloatType>(z, tolerance);
		}

		GAEA_FORCE_INLINE bool is_unit() const
		{
			return math::nearly_equal<FloatType>(magnitude_square(), FloatType(1.0));
		}

		GAEA_FORCE_INLINE bool is_finite() const
		{
			return math::is_finite(x) && math::is_finite(y) && math::is_finite(z);
		}

		GAEA_FORCE_INLINE bool has_nan() const
		{
			return math::is_nan(x) && math::is_nan(y) && math::is_nan(z);
		}

		GAEA_FORCE_INLINE FloatType* data()
		{
			return &w;
		}

		GAEA_FORCE_INLINE constexpr FloatType& operator[](std::size_t idx)
		{
			return *(&w + idx);
		}
		GAEA_FORCE_INLINE constexpr FloatType const& operator[](std::size_t idx) const
		{
			return *(&w + idx);
		}

		GAEA_FORCE_INLINE std::string to_string(std::size_t precision = 3) const
		{
			std::string str;
			str.reserve(15 + 4 * std::numeric_limits<FloatType>::max_digits10);
			return str.append("w=").append(util::trim_fraction(std::to_string(w), precision))
					  .append(",x=").append(util::trim_fraction(std::to_string(x), precision))
					  .append(",y=").append(util::trim_fraction(std::to_string(y), precision))
					  .append(",z=").append(util::trim_fraction(std::to_string(z), precision));
		}

		GAEA_FORCE_INLINE gmatrix<FloatType, 3, 3> to_matrix() const
		{
			FloatType ww = w * w;
			FloatType xx = x * x;
			FloatType yy = y * y;
			FloatType zz = z * z;

			FloatType wx = w * x;
			FloatType wy = w * y;
			FloatType wz = w * z;

			FloatType xy = x * y;
			FloatType yz = y * z;
			FloatType xz = z * x;

			return gmatrix<FloatType, 3, 3>{ {
				ww + xx - yy - zz, FloatType(2.0) * (xy - wz), FloatType(2.0) * (wy + xz),
				FloatType(2.0) * (wz + xy), ww - xx + yy - zz, FloatType(2.0) * (yz - wx),
				FloatType(2.0) * (xz - wy), FloatType(2.0) * (wx + yz), ww - xx - yy + zz,
			} };
		}

		// Encoded as x=roll, y=pitch, z=yaw, and interpreted as z, y', x''
		GAEA_FORCE_INLINE gvector<FloatType> to_euler() const
		{
			FloatType ww = w * w;
			FloatType xx = x * x;
			FloatType yy = y * y;
			FloatType zz = z * z;

			FloatType wx = w * x;
			FloatType wy = w * y;
			FloatType wz = w * z;

			FloatType xy = x * y;
			FloatType yz = y * z;
			FloatType xz = z * x;

			return gvector<FloatType>::unwind_euler(gvector{
				FloatType(std::atan2(2.0 * (wx + yz), ww - xx - yy + zz) * rad2deg),
				FloatType(std::asin(2.0 * (wy - xz)) * rad2deg),
				FloatType(std::atan2(2.0 * (wz + xy), ww + xx - yy - zz) * rad2deg)
			});
		}

		GAEA_FORCE_INLINE static gquaternion from_matrix(gmatrix<FloatType, 3, 3> const& mat)
		{
			// See https://d3cw3dd2w32x2b.cloudfront.net/wp-content/uploads/2015/01/matrix-to-quat.pdf
			
			gquaternion res_quat;
			FloatType nz_elem;
			
			if (mat(2, 2) > FloatType(0.0))
			{
				if (mat(0, 0) > -mat(1, 1))
				{
					nz_elem = FloatType(1.0) + mat(0, 0) + mat(1, 1) + mat(2, 2);
					res_quat = gquaternion{ nz_elem, mat(1, 2) - mat(2, 1), mat(2, 0) - mat(0, 2), mat(0, 1) - mat(1, 0) };
				}
				else
				{
					nz_elem = FloatType(1.0) - mat(0, 0) - mat(1, 1) + mat(2, 2);
					res_quat = gquaternion{ mat(0, 1) - mat(1, 0), mat(2, 0) + mat(0, 2), mat(1, 2) + mat(2, 1), nz_elem };
				}
			}
			else
			{
				if (mat(0, 0) > mat(1, 1))
				{
					nz_elem = FloatType(1.0) + mat(0, 0) - mat(1, 1) - mat(2, 2);
					res_quat = gquaternion{ mat(1, 2) - mat(2, 1), nz_elem, mat(0, 1) + mat(1, 0), mat(2, 0) + mat(0, 2), };
				}
				else
				{
					nz_elem = FloatType(1.0) - mat(0, 0) + mat(1, 1) - mat(2, 2);
					res_quat = gquaternion{ mat(2, 0) - mat(0, 2), mat(0, 1) + mat(1, 0), nz_elem, mat(1, 2) + mat(2, 1) };
				}
			}

			res_quat *= FloatType(0.5) / std::sqrt(nz_elem);
			return res_quat.normalize();
		}

		// Decoded as x=roll, y=pitch, z=yaw and interpreted as z, y', x''
		GAEA_FORCE_INLINE static gquaternion from_euler(gvector<FloatType> const& vec)
		{
			auto euler = gvector<FloatType>::unwind_euler(vec);
			
			FloatType roll_cos = FloatType(std::cos(euler.x * 0.5 * deg2rad));
			FloatType roll_sin = FloatType(std::sin(euler.x * 0.5 * deg2rad));
			FloatType pitch_cos = FloatType(std::cos(euler.y * 0.5 * deg2rad));
			FloatType pitch_sin = FloatType(std::sin(euler.y * 0.5 * deg2rad));
			FloatType yaw_cos = FloatType(std::cos(euler.z * 0.5 * deg2rad));
			FloatType yaw_sin = FloatType(std::sin(euler.z * 0.5 * deg2rad));

			return gquaternion{
				roll_cos * pitch_cos * yaw_cos + roll_sin * pitch_sin * yaw_sin,
				roll_sin * pitch_cos * yaw_cos - roll_cos * pitch_sin * yaw_sin,
				roll_cos * pitch_sin * yaw_cos + roll_sin * pitch_cos * yaw_sin,
				roll_cos * pitch_cos * yaw_sin - roll_sin * pitch_sin * yaw_cos
			};
		}

		FloatType w;
		FloatType x;
		FloatType y;
		FloatType z;
	};

	gquaternion(float32) -> gquaternion<float32>;
	gquaternion(float32, float32, float32, float32) -> gquaternion<float32>;
	gquaternion(float64) -> gquaternion<float64>;
	gquaternion(float64, float64, float64, float64) -> gquaternion<float64>;

	using squaternion = gquaternion<float32>;
	using dquaternion = gquaternion<float64>;

	using quaternion = squaternion;

	static_assert(std::is_standard_layout_v<squaternion>);
	static_assert(std::is_standard_layout_v<dquaternion>);

	inline constexpr squaternion identity_squaternion{ 1.0f, 0.0f, 0.0f, 0.0f };
	inline constexpr dquaternion identity_dquaternion{ 1.0, 0.0, 0.0, 0.0 };
	
	inline constexpr quaternion identity_quaternion{ 1.0f, 0.0f, 0.0f, 0.0f };
}

#endif
