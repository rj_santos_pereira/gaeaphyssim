#ifndef LIBRARY_MATH_POLYHEDRON_INCLUDE_GUARD
#define LIBRARY_MATH_POLYHEDRON_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/containers.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

// https://www.sidefx.com/docs/houdini/vex/halfedges
// https://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml

namespace gaea
{
	class polyhedron;
	class polyhedron_face_query;
	class polyhedron_vertex_query;
	class polyhedron_halfedge_query;

	struct polyhedron_edge
	{
		vector vertex_1;
		vector vertex_2;
		int32 face_idx;
		int32 pair_face_idx;
	};

	class polyhedron_face_query
	{
	public:
		explicit polyhedron_face_query() = default;
		explicit polyhedron_face_query(polyhedron const* poly_ptr, int32 face_idx);

		vector const& normal() const;

		vector& mutable_normal();

		polyhedron_halfedge_query halfedge() const;

		int32 index() const;

	private:
		polyhedron const* _poly_ptr = nullptr;
		int32 _face_idx = -1;

	};

	class polyhedron_vertex_query
	{
	public:
		explicit polyhedron_vertex_query() = default;
		explicit polyhedron_vertex_query(polyhedron const* poly_ptr, int32 vertex_idx);

		vector const& point() const;

		vector& mutable_point();

		polyhedron_halfedge_query halfedge() const;

		int32 index() const;

	private:
		polyhedron const* _poly_ptr = nullptr;
		int32 _vertex_idx = -1;

	};

	class polyhedron_halfedge_query
	{
	public:
		explicit polyhedron_halfedge_query() = default;
		explicit polyhedron_halfedge_query(polyhedron const* poly_ptr, int32 edge_idx);

		vector const& face_normal() const;

		vector& mutable_face_normal();

		polyhedron_face_query face() const;

		vector const& vertex_point() const;

		vector& mutable_vertex_point();

		polyhedron_vertex_query vertex() const;

		polyhedron_halfedge_query pair() const;

		polyhedron_halfedge_query next() const;

		int32 index() const;

	private:
		polyhedron const* _poly_ptr = nullptr;
		int32 _edge_idx = -1;

	};
	
	struct polyhedron_face_data
	{
		vector normal;
		int32 edge_index = -1;
	};

	static_assert(std::is_standard_layout_v<polyhedron_face_data>);

	struct polyhedron_vertex_data
	{
		vector point;
		int32 edge_index = -1;
	};

	static_assert(std::is_standard_layout_v<polyhedron_vertex_data>);

	struct polyhedron_halfedge_data
	{
		int32 face_index = -1;
		int32 vertex_index = -1;
		int32 pair_edge_index = -1;
		int32 next_edge_index = -1;
	};

	static_assert(std::is_standard_layout_v<polyhedron_halfedge_data>);

	class polyhedron
	{
	public:
		explicit polyhedron() = default;
		explicit polyhedron(array<polyhedron_edge> edge_array);
		explicit polyhedron(polyhedron_face_data const* face_data, std::size_t face_count,
							polyhedron_vertex_data const* vertex_data, std::size_t vertex_count,
							polyhedron_halfedge_data const* halfedge_data, std::size_t halfedge_count);

		polyhedron move_by(vector const& translation, quaternion const& rotation) const;

		vector support_vertex(vector const& direction) const;

		vector const& center() const;

		polyhedron_face_query query_face(std::size_t index) const;

		polyhedron_vertex_query query_vertex(std::size_t index) const;

		polyhedron_halfedge_query query_halfedge(std::size_t index) const;

		std::size_t face_count() const;

		std::size_t vertex_count() const;

		std::size_t halfedge_count() const;

		polyhedron_face_data* face_data_ptr() const;
		polyhedron_vertex_data* vertex_data_ptr() const;
		polyhedron_halfedge_data* halfedge_data_ptr() const;

		std::size_t face_data_size() const;
		std::size_t vertex_data_size() const;
		std::size_t halfedge_data_size() const;

	private:
		mutable array<polyhedron_face_data> _face_array;
		mutable array<polyhedron_vertex_data> _vertex_array;
		mutable array<polyhedron_halfedge_data> _halfedge_array;
		
		vector _center;

		friend class polyhedron_face_query;
		friend class polyhedron_vertex_query;
		friend class polyhedron_halfedge_query;

	};
}

#include "library/math/polyhedron.ipp"

#endif
