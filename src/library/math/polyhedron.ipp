namespace gaea
{
	inline polyhedron_face_query::polyhedron_face_query(polyhedron const* poly_ptr, int32 face_idx)
		: _poly_ptr{ poly_ptr }
		, _face_idx{ face_idx }
	{
	}

	inline vector const& polyhedron_face_query::normal() const
	{
		return _poly_ptr->_face_array[_face_idx].normal;
	}

	inline vector& polyhedron_face_query::mutable_normal()
	{
		return const_cast<vector&>(normal());
	}

	inline polyhedron_halfedge_query polyhedron_face_query::halfedge() const
	{
		return polyhedron_halfedge_query{ _poly_ptr, _poly_ptr->_face_array[_face_idx].edge_index };
	}

	inline int32 polyhedron_face_query::index() const
	{
		return _face_idx;
	}

	inline polyhedron_vertex_query::polyhedron_vertex_query(polyhedron const* poly_ptr, int32 vertex_idx)
		: _poly_ptr{ poly_ptr }
		, _vertex_idx{ vertex_idx }
	{
	}

	inline vector const& polyhedron_vertex_query::point() const
	{
		return _poly_ptr->_vertex_array[_vertex_idx].point;
	}

	inline vector& polyhedron_vertex_query::mutable_point()
	{
		return const_cast<vector&>(point());
	}

	inline polyhedron_halfedge_query polyhedron_vertex_query::halfedge() const
	{
		return polyhedron_halfedge_query{ _poly_ptr, _poly_ptr->_vertex_array[_vertex_idx].edge_index };
	}

	inline int32 polyhedron_vertex_query::index() const
	{
		return _vertex_idx;
	}

	inline polyhedron_halfedge_query::polyhedron_halfedge_query(polyhedron const* poly_ptr, int32 edge_idx)
		: _poly_ptr{ poly_ptr }
		, _edge_idx{ edge_idx }
	{
	}

	inline vector const& polyhedron_halfedge_query::face_normal() const
	{
		return _poly_ptr->_face_array[_poly_ptr->_halfedge_array[_edge_idx].face_index].normal;
	}

	inline vector& polyhedron_halfedge_query::mutable_face_normal()
	{
		return const_cast<vector&>(face_normal());
	}

	inline polyhedron_face_query polyhedron_halfedge_query::face() const
	{
		return polyhedron_face_query{ _poly_ptr, _poly_ptr->_halfedge_array[_edge_idx].face_index };
	}

	inline vector const& polyhedron_halfedge_query::vertex_point() const
	{
		return _poly_ptr->_vertex_array[_poly_ptr->_halfedge_array[_edge_idx].vertex_index].point;
	}

	inline vector& polyhedron_halfedge_query::mutable_vertex_point()
	{
		return const_cast<vector&>(vertex_point());
	}

	inline polyhedron_vertex_query polyhedron_halfedge_query::vertex() const
	{
		return polyhedron_vertex_query{ _poly_ptr, _poly_ptr->_halfedge_array[_edge_idx].vertex_index };
	}

	inline polyhedron_halfedge_query polyhedron_halfedge_query::pair() const
	{
		return polyhedron_halfedge_query{ _poly_ptr, _poly_ptr->_halfedge_array[_edge_idx].pair_edge_index };
	}

	inline polyhedron_halfedge_query polyhedron_halfedge_query::next() const
	{
		return polyhedron_halfedge_query{ _poly_ptr, _poly_ptr->_halfedge_array[_edge_idx].next_edge_index };
	}

	inline int32 polyhedron_halfedge_query::index() const
	{
		return _edge_idx;
	}

	inline polyhedron::polyhedron(array<polyhedron_edge> edge_array)
		: _face_array{}
		, _vertex_array{}
		, _halfedge_array{}
		, _center{ zero_vector }
	{
		auto order_edge_lambda =
			[](polyhedron_edge const& edge1, polyhedron_edge const& edge2)
			{
				return edge1.face_idx < edge2.face_idx;
			};

		if (!std::is_sorted(edge_array.begin(), edge_array.end(), order_edge_lambda))
		{
			std::sort(edge_array.begin(), edge_array.end(), order_edge_lambda);
		}

		int32 curr_face_idx = -1;
		int32 first_edge_idx = -1;
		int32 last_edge_idx = -1;

		for (auto const& edge : edge_array)
		{
			// Construct vertexes and half-edges

			// Construct vertex if necessary
			auto vertex_it = std::find_if(_vertex_array.begin(), _vertex_array.end(),
				[&point = edge.vertex_1](polyhedron_vertex_data const& vertex)
			{
				return vertex.point.nearly_equal(point);
			});

			bool vertex_exists = vertex_it != _vertex_array.end();
			int32 vertex_idx = vertex_exists
				? int32(std::distance(_vertex_array.begin(), vertex_it))
				: int32(_vertex_array.size());

			if (!vertex_exists)
			{
				_vertex_array.push_back(polyhedron_vertex_data{ edge.vertex_1 });
			}

			// Construct pair vertex if necessary
			auto pair_vertex_it = std::find_if(_vertex_array.begin(), _vertex_array.end(),
				[&point = edge.vertex_2](polyhedron_vertex_data const& vertex)
			{
				return vertex.point.nearly_equal(point);
			});

			bool pair_vertex_exists = pair_vertex_it != _vertex_array.end();
			int32 pair_vertex_idx = pair_vertex_exists
				? int32(std::distance(_vertex_array.begin(), pair_vertex_it))
				: int32(_vertex_array.size());

			if (!pair_vertex_exists)
			{
				_vertex_array.push_back(polyhedron_vertex_data{ edge.vertex_2 });
			}

			// Construct half-edges if necessary
			auto edge_it = std::find_if(_halfedge_array.begin(), _halfedge_array.end(),
				[this, &edge](polyhedron_halfedge_data const& halfedge)
				{
					return halfedge.face_index == edge.face_idx && _vertex_array[halfedge.vertex_index].point.
						nearly_equal(edge.vertex_1);
				});

			bool edge_exists = edge_it != _halfedge_array.end();
			int32 edge_idx = edge_exists
				? int32(std::distance(_halfedge_array.begin(), edge_it))
				: int32(_halfedge_array.size());
			int32 pair_edge_idx = !edge_exists || edge_idx % 2 == 0
				? edge_idx + 1
				: edge_idx - 1;

			if (!edge_exists)
			{
				_halfedge_array.push_back(polyhedron_halfedge_data{ edge.face_idx, vertex_idx, pair_edge_idx });
				_halfedge_array.push_back(polyhedron_halfedge_data{ edge.pair_face_idx, pair_vertex_idx, edge_idx });
			}

			auto& vertex_edge_idx = _vertex_array[vertex_idx].edge_index;
			if (vertex_edge_idx == -1)
			{
				vertex_edge_idx = edge_idx;
			}

			auto& pair_vertex_edge_idx = _vertex_array[pair_vertex_idx].edge_index;
			if (pair_vertex_edge_idx == -1)
			{
				pair_vertex_edge_idx = pair_edge_idx;
			}

			// Construct face if necessary
			if (edge.face_idx > curr_face_idx)
			{
				_face_array.emplace_back();
			}

			// Change the half-edge index of the face
			auto& face_edge_idx = _face_array[edge.face_idx].edge_index;
			if (face_edge_idx == -1)
			{
				if (curr_face_idx != -1)
				{
					_halfedge_array[last_edge_idx].next_edge_index = first_edge_idx;
				}
				curr_face_idx = edge.face_idx;

				first_edge_idx = edge_idx;
			}
			else
			{
				_halfedge_array[face_edge_idx].next_edge_index = edge_idx;

				last_edge_idx = edge_idx;
			}

			face_edge_idx = edge_idx;
		}

		_halfedge_array[last_edge_idx].next_edge_index = first_edge_idx;

		for (auto& face : _face_array)
		{
			auto edge_idx = face.edge_index;
			auto vertex_0 = _vertex_array[_halfedge_array[edge_idx].vertex_index].point;
			edge_idx = _halfedge_array[edge_idx].next_edge_index;
			auto vertex_1 = _vertex_array[_halfedge_array[edge_idx].vertex_index].point;
			edge_idx = _halfedge_array[edge_idx].next_edge_index;
			auto vertex_2 = _vertex_array[_halfedge_array[edge_idx].vertex_index].point;

			face.normal = vector::cross(vertex_1 - vertex_0, vertex_2 - vertex_0).normalize();
		}

		std::for_each(_vertex_array.begin(), _vertex_array.end(),
			[this](polyhedron_vertex_data const& vertex) { _center += vertex.point; });
		
		_center /= float32(_vertex_array.size());
	}
	
	inline polyhedron::polyhedron(polyhedron_face_data const* face_data, std::size_t face_count,
								  polyhedron_vertex_data const* vertex_data, std::size_t vertex_count,
								  polyhedron_halfedge_data const* halfedge_data, std::size_t halfedge_count)
		: _face_array(face_count)
		, _vertex_array(vertex_count)
		, _halfedge_array(halfedge_count)
		, _center{ zero_vector }
	{
		std::memcpy(face_data_ptr(), face_data, face_data_size());
		std::memcpy(_vertex_array.data(), vertex_data, vertex_data_size());
		std::memcpy(_halfedge_array.data(), halfedge_data, halfedge_data_size());
		
		std::for_each(_vertex_array.begin(), _vertex_array.end(),
			[this](polyhedron_vertex_data const& vertex) { _center += vertex.point; });

		_center /= float32(vertex_count);
	}

	inline polyhedron polyhedron::move_by(vector const& translation, quaternion const& rotation) const
	{
		polyhedron poly{ *this };

		auto rot_mat = rotation.to_matrix();

		std::for_each(poly._face_array.begin(), poly._face_array.end(),
			[&rot_mat](polyhedron_face_data& face) { face.normal = rot_mat * face.normal; });
		std::for_each(poly._vertex_array.begin(), poly._vertex_array.end(),
			[&translation, &rot_mat](polyhedron_vertex_data& vertex) { vertex.point = rot_mat * vertex.point + translation; });
		poly._center = rot_mat * poly._center + translation;

		return poly;
	}

	inline vector polyhedron::support_vertex(vector const& direction) const
	{
		vector support_vertex = zero_vector;
		float32 maximal_projection = -std::numeric_limits<float32>::max();

		for (std::size_t index = 0; index < _vertex_array.size(); ++index)
		{
			auto vertex = _vertex_array[index].point;
			float32 projection = vector::dot(vertex, direction);

			if (projection > maximal_projection)
			{
				maximal_projection = projection;
				support_vertex = vertex;
			}
		}

		return support_vertex;
	}

	inline vector const& polyhedron::center() const
	{
		return _center;
	}

	inline polyhedron_face_query polyhedron::query_face(std::size_t index) const
	{
		return polyhedron_face_query{ this, int32(index) };
	}

	inline polyhedron_vertex_query polyhedron::query_vertex(std::size_t index) const
	{
		return polyhedron_vertex_query{ this, int32(index) };
	}

	inline polyhedron_halfedge_query polyhedron::query_halfedge(std::size_t index) const
	{
		return polyhedron_halfedge_query{ this, int32(index) };
	}

	inline std::size_t polyhedron::face_count() const
	{
		return _face_array.size();
	}

	inline std::size_t polyhedron::vertex_count() const
	{
		return _vertex_array.size();
	}

	inline std::size_t polyhedron::halfedge_count() const
	{
		return _halfedge_array.size();
	}

	inline polyhedron_face_data* polyhedron::face_data_ptr() const
	{
		return _face_array.data();
	}

	inline polyhedron_vertex_data* polyhedron::vertex_data_ptr() const
	{
		return _vertex_array.data();
	}

	inline polyhedron_halfedge_data* polyhedron::halfedge_data_ptr() const
	{
		return _halfedge_array.data();
	}

	inline std::size_t polyhedron::face_data_size() const
	{
		return _face_array.size() * sizeof(polyhedron_face_data);
	}

	inline std::size_t polyhedron::vertex_data_size() const
	{
		return _vertex_array.size() * sizeof(polyhedron_vertex_data);
	}

	inline std::size_t polyhedron::halfedge_data_size() const
	{
		return _halfedge_array.size() * sizeof(polyhedron_halfedge_data);
	}
}
