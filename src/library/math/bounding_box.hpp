#ifndef LIBRARY_MATH_BOUNDING_BOX_INCLUDE_GUARD
#define LIBRARY_MATH_BOUNDING_BOX_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/math/generic_math.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	// Also see https://en.wikipedia.org/wiki/Rotating_calipers for generic (non-convex) shapes

	class base_axis_aligned_bounding_box
	{
	protected:
		explicit base_axis_aligned_bounding_box()
			: _min{ +std::numeric_limits<float32>::max() }
			, _max{ -std::numeric_limits<float32>::max() }
		{
		}
		explicit base_axis_aligned_bounding_box(vector const& min, vector const& max)
			: _min{ min }
			, _max{ max }
		{
		}

	public:
		GAEA_FORCE_INLINE bool contains_point(vector const& point) const
		{
			return point.nearly_equal(vector{
				math::clamp(point.x, _min.x, _max.x),
				math::clamp(point.y, _min.y, _max.y),
				math::clamp(point.z, _min.z, _max.z),
				}, 0.f);
		}

		GAEA_FORCE_INLINE vector center() const
		{
			return (_max + _min) / 2.f;
		}

		GAEA_FORCE_INLINE vector extent() const
		{
			return _max - _min;
		}

		GAEA_FORCE_INLINE vector const& min() const
		{
			return _min;
		}

		GAEA_FORCE_INLINE vector const& max() const
		{
			return _max;
		}

		GAEA_FORCE_INLINE static bool overlap(base_axis_aligned_bounding_box const& box1, base_axis_aligned_bounding_box const& box2)
		{
			return box1.min().x <= box2.max().x && box2.min().x <= box1.max().x
				&& box1.min().y <= box2.max().y && box2.min().y <= box1.max().y
				&& box1.min().z <= box2.max().z && box2.min().z <= box1.max().z;
		}

	protected:
		vector _min;
		vector _max;
	};
	
	class cubic_axis_aligned_bounding_box : public base_axis_aligned_bounding_box
	{
		using super = base_axis_aligned_bounding_box;
		
	public:
		explicit cubic_axis_aligned_bounding_box() = default;
		explicit cubic_axis_aligned_bounding_box(vector const& min, vector const& max)
			: super{ min, max }
		{
		}
		
		GAEA_FORCE_INLINE cubic_axis_aligned_bounding_box& add_point(vector const& point)
		{
			float32 magn = point.magnitude();
			
			float32 new_max = math::max(_max.x, magn);
			
			_min = vector{ -new_max };
			_max = vector{ new_max };
			
			return *this;
		}
		
		GAEA_FORCE_INLINE cubic_axis_aligned_bounding_box move_by(vector const& translation, quaternion const& rotation) const
		{
			GAEA_UNUSED(rotation);
			
			cubic_axis_aligned_bounding_box box;

			box._min = _min + translation;
			box._max = _max + translation;

			return box;
		}

		GAEA_FORCE_INLINE cubic_axis_aligned_bounding_box scale_by(vector const& scaling) const
		{
			cubic_axis_aligned_bounding_box box;
			
			vector box_extent = extent() * 0.5f;
			vector box_center = center();
			
			box._min = box_center - box_extent * scaling;
			box._max = box_center + box_extent * scaling;

			return box;
		}
		
	};
	
	class adaptive_axis_aligned_bounding_box : public base_axis_aligned_bounding_box
	{
		using super = base_axis_aligned_bounding_box;

	public:
		explicit adaptive_axis_aligned_bounding_box() = default;
		explicit adaptive_axis_aligned_bounding_box(vector const& min, vector const& max)
			: super{ min, max }
		{
		}

		GAEA_FORCE_INLINE adaptive_axis_aligned_bounding_box& add_point(vector const& point)
		{
			_min = vector{ math::min(_min.x, point.x), math::min(_min.y, point.y), math::min(_min.z, point.z) };
			_max = vector{ math::max(_max.x, point.x), math::max(_max.y, point.y), math::max(_max.z, point.z) };

			return *this;
		}

		GAEA_FORCE_INLINE adaptive_axis_aligned_bounding_box move_by(vector const& translation, quaternion const& rotation) const
		{
			adaptive_axis_aligned_bounding_box box;
			
			auto rot_mat = rotation.to_matrix();
			
			box.add_point(rot_mat * _min + translation);
			box.add_point(rot_mat * vector{ _max.x, _min.y, _min.z } + translation);
			box.add_point(rot_mat * vector{ _min.x, _max.y, _min.z } + translation);
			box.add_point(rot_mat * vector{ _min.x, _min.y, _max.z } + translation);
			box.add_point(rot_mat * vector{ _min.x, _max.y, _max.z } + translation);
			box.add_point(rot_mat * vector{ _max.x, _min.y, _max.z } + translation);
			box.add_point(rot_mat * vector{ _max.x, _max.y, _min.z } + translation);
			box.add_point(rot_mat * _max + translation);
			
			return box;
		}

		GAEA_FORCE_INLINE adaptive_axis_aligned_bounding_box scale_by(vector const& scaling) const
		{
			adaptive_axis_aligned_bounding_box box;
			
			vector box_extent = extent() * 0.5f;
			vector box_center = center();

			box._min = box_center - box_extent * scaling;
			box._max = box_center + box_extent * scaling;
			
			return box;
		}
		
	};
}

#endif
