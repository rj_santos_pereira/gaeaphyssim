#ifndef LIBRARY_MATH_VECTOR_INCLUDE_GUARD
#define LIBRARY_MATH_VECTOR_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/utility.hpp"
#include "library/math/generic_math.hpp"

namespace gaea
{
	template <class FloatType>
	struct gvector
	{
		static_assert(std::is_floating_point_v<FloatType>);

		explicit constexpr gvector() = default;
		explicit constexpr gvector(FloatType value)
			: x{ value }
			, y{ value }
			, z{ value }
		{
		}
		explicit constexpr gvector(FloatType nx, FloatType ny, FloatType nz)
			: x{ nx }
			, y{ ny }
			, z{ nz }
		{
		}

#define DECLARE_ARITHMETIC_OPERATOR(op) \
		GAEA_FORCE_INLINE gvector& operator op ## = (gvector const& vec)                            \
		{                                                                                           \
			x op ## = vec.x;                                                                        \
			y op ## = vec.y;                                                                        \
			z op ## = vec.z;                                                                        \
			return *this;                                                                           \
		}                                                                                           \
		GAEA_FORCE_INLINE gvector& operator op ## = (FloatType val)									\
		{                                                                                           \
			x op ## = val;                                                                          \
			y op ## = val;                                                                          \
			z op ## = val;                                                                          \
			return *this;                                                                           \
		}                                                                                           \
																									\
		GAEA_FORCE_INLINE gvector operator op (gvector const& vec) const                            \
		{                                                                                           \
			return gvector{ *this } op ## = vec;                                                    \
		}                                                                                           \
		GAEA_FORCE_INLINE gvector operator op (FloatType val) const                                 \
		{                                                                                           \
			return gvector{ *this } op ## = gvector{ val };                                         \
		}                                                                                           \
																									\
		GAEA_FORCE_INLINE friend gvector operator op (FloatType val, gvector const& vec)            \
		{                                                                                           \
			return gvector{ vec } op ## = gvector{ val };                                           \
		}

		DECLARE_ARITHMETIC_OPERATOR(+)
		DECLARE_ARITHMETIC_OPERATOR(-)
		DECLARE_ARITHMETIC_OPERATOR(*)
		DECLARE_ARITHMETIC_OPERATOR(/)

#undef  DECLARE_ARITHMETIC_OPERATOR

		GAEA_FORCE_INLINE gvector operator+() const
		{
			return *this;
		}

		GAEA_FORCE_INLINE gvector operator-() const
		{
			return gvector{ -x, -y, -z };
		}

		GAEA_FORCE_INLINE gvector& normalize()
		{
			FloatType magn_sq = magnitude_square();

			if (!math::nearly_equal<FloatType>(magn_sq, FloatType(1.0)) && !math::nearly_zero<FloatType>(magn_sq))
			{
				FloatType factor;
#if GAEA_USING_PRECISE_RSQRT
				factor = FloatType(1.0) / std::sqrt(magn_sq);
#else
				if constexpr (std::is_same_v<FloatType, float32>)
				{
					factor = math::rsqrt_single_abs_2NR(magn_sq);
				}
				else
				{
					factor = math::rsqrt_double_rel_1NR(magn_sq);
				}
#endif

				x *= factor;
				y *= factor;
				z *= factor;
			}

			return *this;
		}
		
		GAEA_FORCE_INLINE gvector unit() const
		{
			return gvector{ *this }.normalize();
		}

		GAEA_FORCE_INLINE gvector trunc() const
		{
			//return gvector{ std::trunc(x), std::trunc(y), std::trunc(z) };
			return gvector{ FloatType(int64(x)), FloatType(int64(y)), FloatType(int64(z)) };
		}
		
		GAEA_FORCE_INLINE gvector& safe_div_assign(gvector const& vec, FloatType tolerance = math::select_epsilon<FloatType>::epsilon)
		{
			x = math::nearly_zero(vec.x, tolerance) ? 0.f : x / vec.x;
			y = math::nearly_zero(vec.y, tolerance) ? 0.f : y / vec.y;
			z = math::nearly_zero(vec.z, tolerance) ? 0.f : z / vec.z;
			
			return *this;
		}
		
		GAEA_FORCE_INLINE gvector safe_div(gvector const& vec, FloatType tolerance = math::select_epsilon<FloatType>::epsilon) const
		{
			return gvector{ *this }.safe_div_assign(vec, tolerance);
		}

		GAEA_FORCE_INLINE FloatType magnitude() const
		{
			return std::sqrt(magnitude_square());
		}

		GAEA_FORCE_INLINE FloatType magnitude_square() const
		{
			return x * x + y * y + z * z;
		}

		GAEA_FORCE_INLINE bool nearly_equal(gvector const& vec, FloatType tolerance = math::select_epsilon<FloatType>::epsilon) const
		{
			return math::nearly_equal<FloatType>(x, vec.x, tolerance)
				&& math::nearly_equal<FloatType>(y, vec.y, tolerance)
				&& math::nearly_equal<FloatType>(z, vec.z, tolerance);
		}

		GAEA_FORCE_INLINE bool nearly_zero(FloatType tolerance = math::select_epsilon<FloatType>::epsilon) const
		{
			return math::nearly_zero<FloatType>(x, tolerance)
				&& math::nearly_zero<FloatType>(y, tolerance)
				&& math::nearly_zero<FloatType>(z, tolerance);
		}

		GAEA_FORCE_INLINE bool exactly_equal(gvector const& vec) const
		{
			return x == vec.x && y == vec.y && z == vec.z;
		}

		GAEA_FORCE_INLINE bool exactly_zero() const
		{
			return x == FloatType(0.0) && y == FloatType(0.0) && z == FloatType(0.0);
		}

		GAEA_FORCE_INLINE bool is_unit() const
		{
			return math::nearly_equal<FloatType>(magnitude_square(), FloatType(1.0));
		}

		GAEA_FORCE_INLINE bool is_finite() const
		{
			return math::is_finite(x) && math::is_finite(y) && math::is_finite(z);
		}

		GAEA_FORCE_INLINE bool has_nan() const
		{
			return math::is_nan(x) && math::is_nan(y) && math::is_nan(z);
		}

		GAEA_FORCE_INLINE FloatType* data()
		{
			return &x;
		}

		GAEA_FORCE_INLINE constexpr FloatType& operator[](std::size_t idx)
		{
			return *(&x + idx);
		}
		GAEA_FORCE_INLINE constexpr FloatType const& operator[](std::size_t idx) const
		{
			return *(&x + idx);
		}

		GAEA_FORCE_INLINE std::string to_string(std::size_t precision = 3) const
		{
			std::string str;
			str.reserve(11 + 3 * std::numeric_limits<FloatType>::max_digits10);
			return str.append("x=").append(util::trim_fraction(std::to_string(x), precision))
					  .append(",y=").append(util::trim_fraction(std::to_string(y), precision))
					  .append(",z=").append(util::trim_fraction(std::to_string(z), precision));
		}

		GAEA_FORCE_INLINE static FloatType dot(gvector const& vec1, gvector const& vec2)
		{
			return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
		}

		GAEA_FORCE_INLINE static gvector cross(gvector const& vec1, gvector const& vec2)
		{
			return gvector{
				vec1.y * vec2.z - vec1.z * vec2.y,
				vec1.z * vec2.x - vec1.x * vec2.z,
				vec1.x * vec2.y - vec1.y * vec2.x,
			};
		}

		GAEA_FORCE_INLINE static gvector min(gvector const& vec1, gvector const& vec2)
		{
			return gvector{
				math::min(vec1.x, vec2.x),
				math::min(vec1.y, vec2.y),
				math::min(vec1.z, vec2.z)
			};
		}

		GAEA_FORCE_INLINE static gvector max(gvector const& vec1, gvector const& vec2)
		{
			return gvector{
				math::max(vec1.x, vec2.x),
				math::max(vec1.y, vec2.y),
				math::max(vec1.z, vec2.z)
			};
		}

		GAEA_FORCE_INLINE static gvector clamp(gvector const& vec, gvector const& min_vec, gvector const& max_vec)
		{
			return gvector{
				math::clamp(vec.x, min_vec.x, max_vec.x),
				math::clamp(vec.y, min_vec.y, max_vec.y),
				math::clamp(vec.z, min_vec.z, max_vec.z)
			};
		}

		GAEA_FORCE_INLINE static gvector project_point_onto_plane(gvector const& point, gvector const& plane_normal, gvector const& plane_point)
		{
			return project_point_onto_plane(point, plane_normal, plane_point, plane_normal);
		}

		GAEA_FORCE_INLINE static gvector project_point_onto_plane(gvector const& point, gvector const& plane_normal, gvector const& plane_point, gvector const& direction)
		{
			return direction * (dot(plane_point - point, plane_normal) / dot(direction, plane_normal)) + point;
		}

		GAEA_FORCE_INLINE static bool create_orthonormal_basis(gvector const& initial_vec, gvector& basis_i, gvector& basis_j, gvector& basis_k)
		{
			// Gram-Schmidt algorithm
			// https://en.wikipedia.org/wiki/Gram-Schmidt_process
			// https://math.stackexchange.com/questions/1714365/finding-an-orthonormal-basis-with-only-one-vector
			
			static constexpr gvector x_axis{ FloatType(1.0), FloatType(0.0), FloatType(0.0) };
			static constexpr gvector y_axis{ FloatType(0.0), FloatType(1.0), FloatType(0.0) };
			static constexpr gvector z_axis{ FloatType(0.0), FloatType(0.0), FloatType(1.0) };
			
			if (initial_vec.nearly_zero()) 
			{
				return false;
			}

			// Normalize first so we can properly initialize the other basis vectors
			
			basis_i = initial_vec.unit();
			basis_j = basis_i.nearly_equal(x_axis) ? z_axis : x_axis;
			basis_k = basis_i.nearly_equal(y_axis) ? z_axis : y_axis;

			// Normalize immediately following transformation, to simplify further calculations
			
			basis_j -= dot(basis_i, basis_j) * basis_i;
			basis_j.normalize();
			
			basis_k -= dot(basis_i, basis_k) * basis_i + dot(basis_j, basis_k) * basis_j;
			basis_k.normalize();
			
			return true;
		}

		GAEA_FORCE_INLINE static gvector unwind_euler(gvector const& vec)
		{
			int32 pitch_revolutions;
			FloatType pitch = std::remquo(vec.y, FloatType(180.0), &pitch_revolutions);
			pitch *= FloatType(pitch_revolutions & 0x1 ? -1.0 : +1.0);

			FloatType roll = std::remainder(vec.x + pitch_revolutions * FloatType(180.0), FloatType(360.0));
			FloatType yaw = std::remainder(vec.z + pitch_revolutions * FloatType(180.0), FloatType(360.0));

			return gvector{ roll, pitch, yaw };
		}

		FloatType x;
		FloatType y;
		FloatType z;
	};

	gvector(float32) -> gvector<float32>;
	gvector(float32, float32, float32) -> gvector<float32>;
	gvector(float64) -> gvector<float64>;
	gvector(float64, float64, float64) -> gvector<float64>;

	using svector = gvector<float32>;
	using dvector = gvector<float64>;
	
	using vector = svector;

	static_assert(std::is_standard_layout_v<svector>);
	static_assert(std::is_standard_layout_v<dvector>);

	inline constexpr svector zero_svector{ 0.0f, 0.0f, 0.0f };
	inline constexpr dvector zero_dvector{ 0.0, 0.0, 0.0 };
	
	inline constexpr vector zero_vector{ 0.0f, 0.0f, 0.0f };

	inline constexpr svector one_svector{ 1.0f, 1.0f, 1.0f };
	inline constexpr dvector one_dvector{ 1.0, 1.0, 1.0 };
	
	inline constexpr vector one_vector{ 1.0f, 1.0f, 1.0f };

	inline constexpr svector x_axis_svector{ 1.0f, 0.0f, 0.0f };
	inline constexpr dvector x_axis_dvector{ 1.0, 0.0, 0.0 };
	
	inline constexpr vector x_axis_vector{ 1.0f, 0.0f, 0.0f };

	inline constexpr svector y_axis_svector{ 0.0f, 1.0f, 0.0f };
	inline constexpr dvector y_axis_dvector{ 0.0, 1.0, 0.0 };
	
	inline constexpr vector y_axis_vector{ 0.0f, 1.0f, 0.0f };

	inline constexpr svector z_axis_svector{ 0.0f, 0.0f, 1.0f };
	inline constexpr dvector z_axis_dvector{ 0.0, 0.0, 1.0 };
	
	inline constexpr vector z_axis_vector{ 0.0f, 0.0f, 1.0f};
}

#endif
