#ifndef LIBRARY_MATH_MATRIX_INCLUDE_GUARD
#define LIBRARY_MATH_MATRIX_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/math/generic_math.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	template <class FloatType, std::size_t RowValue, std::size_t ColValue, class ActualType>
	class _gmatrix;

	template <class FloatType, std::size_t RowValue, std::size_t ColValue>
	struct gmatrix;
}

namespace gaea::implementation
{
	template <class FloatType, std::size_t RowValue, std::size_t ColValue, class Matrix1Type, class Matrix2Type>
	struct _gmatrix_common_actual_type_selector_2
	{
		using type = void;
	};
	
#if 0
	template <class FloatType, std::size_t NewRowValue, std::size_t NewColValue,
		std::size_t OldRow2Value, std::size_t OldCol2Value,
		template <class, std::size_t, std::size_t> class Actual1Template,
		template <class, std::size_t, std::size_t> class Actual2Template>
	struct _gmatrix_common_actual_type_selector_1<FloatType, NewRowValue, NewColValue,
		_gmatrix<FloatType, NewRowValue, NewColValue, Actual1Template<FloatType, NewRowValue, NewColValue>>,
		_gmatrix<FloatType, OldRow2Value, OldCol2Value, Actual2Template<FloatType, OldRow2Value, OldCol2Value>>>
	{
		using type = Actual1Template<FloatType, NewRowValue, NewColValue>;
	};

	template <class FloatType, std::size_t NewRowValue, std::size_t NewColValue,
		std::size_t OldRow1Value, std::size_t OldCol1Value,
		template <class, std::size_t, std::size_t> class Actual1Template,
		template <class, std::size_t, std::size_t> class Actual2Template>
	struct _gmatrix_common_actual_type_selector_1<FloatType, NewRowValue, NewColValue,
		_gmatrix<FloatType, OldRow1Value, OldCol1Value, Actual1Template<FloatType, OldRow1Value, OldCol1Value>>,
		_gmatrix<FloatType, NewRowValue, NewColValue, Actual2Template<FloatType, NewRowValue, NewColValue>>>
	{
		using type = Actual2Template<FloatType, NewRowValue, NewColValue>;
	};
#endif

	// Same new sizes as Matrix1Type sizes
	template <class FloatType, std::size_t NewRowValue, std::size_t NewColValue,
		std::size_t OldRow2Value, std::size_t OldCol2Value,
		class Actual1Type, class Actual2Type>
	struct _gmatrix_common_actual_type_selector_2<FloatType, NewRowValue, NewColValue,
		_gmatrix<FloatType, NewRowValue, NewColValue, Actual1Type>,
		_gmatrix<FloatType, OldRow2Value, OldCol2Value, Actual2Type>>
	{
		using type = Actual1Type;
	};

	// Same new sizes as Matrix2Type sizes
	template <class FloatType, std::size_t NewRowValue, std::size_t NewColValue,
		std::size_t OldRow1Value, std::size_t OldCol1Value,
		class Actual1Type, class Actual2Type>
	struct _gmatrix_common_actual_type_selector_2<FloatType, NewRowValue, NewColValue,
		_gmatrix<FloatType, OldRow1Value, OldCol1Value, Actual1Type>,
		_gmatrix<FloatType, NewRowValue, NewColValue, Actual2Type>>
	{
		using type = Actual2Type;
	};

	template <class FloatType, std::size_t RowValue, std::size_t ColValue, class Matrix1Type, class Matrix2Type>
	struct _gmatrix_common_actual_type_selector_1 : _gmatrix_common_actual_type_selector_2<FloatType, RowValue, ColValue, Matrix1Type, Matrix2Type>
	{
	};

	// Same actual template type
	template <class FloatType, std::size_t RowValue, std::size_t ColValue,
		std::size_t OldRow1Value, std::size_t OldCol1Value,
		std::size_t OldRow2Value, std::size_t OldCol2Value,
		template <class, std::size_t, std::size_t> class ActualTemplateType>
	struct _gmatrix_common_actual_type_selector_1<FloatType, RowValue, ColValue,
		_gmatrix<FloatType, OldRow1Value, OldCol1Value, ActualTemplateType<FloatType, OldRow1Value, OldCol1Value>>,
		_gmatrix<FloatType, OldRow2Value, OldCol2Value, ActualTemplateType<FloatType, OldRow2Value, OldCol2Value>>>
	{
		using type = ActualTemplateType<FloatType, RowValue, ColValue>;
	};
	
	template <class FloatType, std::size_t RowValue, std::size_t ColValue, class Matrix1Type, class Matrix2Type>
	struct gmatrix_common_actual_type : _gmatrix_common_actual_type_selector_1<FloatType, RowValue, ColValue, Matrix1Type, Matrix2Type>
	{
	};

	// Same actual class type
	template <class FloatType, std::size_t RowValue, std::size_t ColValue, class ActualType>
	struct gmatrix_common_actual_type<FloatType, RowValue, ColValue,
		_gmatrix<FloatType, RowValue, ColValue, ActualType>,
		_gmatrix<FloatType, RowValue, ColValue, ActualType>>
	{
		using type = ActualType;
	};

	template <class MatrixType>
	struct gmatrix_transpose_type
	{
		using type = MatrixType;
	};

	template <template <class, std::size_t, std::size_t> class TemplateType, class FloatType, std::size_t RowValue, std::size_t ColValue>
	struct gmatrix_transpose_type<TemplateType<FloatType, RowValue, ColValue>>
	{
		using type = TemplateType<FloatType, ColValue, RowValue>;
	};
}

namespace gaea
{	
	struct zero_matrix_tag{};
	struct identity_matrix_tag{};

	template <class FloatType, std::size_t RowValue, std::size_t ColValue, class ActualType>
	class _gmatrix
	{
		static_assert(std::is_floating_point_v<FloatType>);
		static_assert(RowValue > 0 && ColValue > 0);

	protected:
		struct _product_zero_matrix_tag{};
		struct _inverse_zero_matrix_tag{};
		struct _inverse_identity_matrix_tag{};
		struct _transpose_matrix_tag{};

		static constexpr std::size_t _element_count = RowValue * ColValue;
		static constexpr std::size_t _row_stride = ColValue;
		
		constexpr _gmatrix() = default;
		
		constexpr _gmatrix(_product_zero_matrix_tag)
			: _gmatrix{ zero_matrix_tag{}, std::make_index_sequence<_element_count>{} }
		{
		}
		constexpr _gmatrix(_inverse_zero_matrix_tag)
			: _gmatrix{ zero_matrix_tag{}, std::make_index_sequence<_element_count>{} }
		{
		}
		constexpr _gmatrix(_inverse_identity_matrix_tag)
			: _gmatrix{ identity_matrix_tag{}, std::make_index_sequence<_element_count>{} }
		{
		}
		constexpr _gmatrix(_transpose_matrix_tag, FloatType const(& elements)[_element_count])
			: _gmatrix{ _transpose_matrix_tag{}, elements, elements[_element_count - 1], std::make_index_sequence<_element_count - 1>{} }
		{
		}
		
		constexpr _gmatrix(zero_matrix_tag)
			: _gmatrix{ zero_matrix_tag{}, std::make_index_sequence<_element_count>{} }
		{
		}
		constexpr _gmatrix(identity_matrix_tag)
			: _gmatrix{ identity_matrix_tag{}, std::make_index_sequence<_element_count>{} }
		{
		}
		
		constexpr _gmatrix(FloatType value)
			: _gmatrix{ value, std::make_index_sequence<_element_count>{} }
		{
		}
		constexpr _gmatrix(FloatType const(& elements)[_element_count])
			: _gmatrix{ elements, std::make_index_sequence<_element_count>{} }
		{
		}

	private:
		template <std::size_t ... IndexValues>
		constexpr _gmatrix(zero_matrix_tag, std::index_sequence<IndexValues ...>)
			: _elements{ (IndexValues, FloatType(0.0)) ... } // NOLINT(clang-diagnostic-unused-value)
		{
		}
		template <std::size_t ... IndexValues>
		constexpr _gmatrix(identity_matrix_tag, std::index_sequence<IndexValues ...>)
			: _elements{ FloatType(IndexValues % (_row_stride + 1) == 0) ... }
		{
		}
		template <std::size_t ... IndexValues>
		constexpr _gmatrix(_transpose_matrix_tag, FloatType const(& elements)[_element_count], FloatType last_element, std::index_sequence<IndexValues ...>)
			: _elements{ elements[(IndexValues * RowValue) % (_element_count - 1)] ..., last_element }
		{
		}
		template <std::size_t ... IndexValues>
		constexpr _gmatrix(FloatType value, std::index_sequence<IndexValues ...>)
			: _elements{ (IndexValues, value) ... } // NOLINT(clang-diagnostic-unused-value)
		{
		}
		template <std::size_t ... IndexValues>
		constexpr _gmatrix(FloatType const(& elements)[_element_count], std::index_sequence<IndexValues ...>)
			: _elements{ elements[IndexValues] ... }
		{
		}
		
	public:
#define DECLARE_ARITHMETIC_OPERATOR(op) \
		GAEA_FORCE_INLINE ActualType& operator op ## = (FloatType val)                              \
		{                                                                                           \
			for (std::size_t idx = 0; idx < _element_count; ++idx)                                  \
			{                                                                                       \
				_elements[idx] op ## = val;                                                         \
			}                                                                                       \
			return *static_cast<ActualType*>(this);                                                 \
		}                                                                                           \
																									\
		GAEA_FORCE_INLINE ActualType operator op (FloatType val) const                              \
		{                                                                                           \
			return ActualType{ *static_cast<ActualType const*>(this) } op ## = val;					\
		}                                                                                           \
																									\
		GAEA_FORCE_INLINE friend ActualType operator op (FloatType val, ActualType const& mat)		\
		{                                                                                           \
			return ActualType{ mat } op ## = val;													\
		}

		DECLARE_ARITHMETIC_OPERATOR(+)
		DECLARE_ARITHMETIC_OPERATOR(-)
		DECLARE_ARITHMETIC_OPERATOR(*)
		DECLARE_ARITHMETIC_OPERATOR(/)

#undef  DECLARE_ARITHMETIC_OPERATOR

#define DECLARE_ARITHMETIC_OPERATOR(op) \
		GAEA_FORCE_INLINE ActualType& operator op ## = (ActualType const& mat)                      \
		{                                                                                           \
			for (std::size_t idx = 0; idx < _element_count; ++idx)                                  \
			{                                                                                       \
				_elements[idx] op ## = mat._elements[idx];                                          \
			}                                                                                       \
			return *static_cast<ActualType*>(this);                                                 \
		}                                                                                           \
																									\
		GAEA_FORCE_INLINE ActualType operator op (ActualType const& mat) const                      \
		{                                                                                           \
			return ActualType{ *static_cast<ActualType const*>(this) } op ## = mat;					\
		}

		DECLARE_ARITHMETIC_OPERATOR(+)
		DECLARE_ARITHMETIC_OPERATOR(-)

#undef  DECLARE_ARITHMETIC_OPERATOR

		GAEA_FORCE_INLINE ActualType& elem_mul_assign(ActualType const& mat)
		{
			for (std::size_t idx = 0; idx < _element_count; ++idx)
			{
				_elements[idx] *= mat._elements[idx];
			}
			return *static_cast<ActualType*>(this);
		}

		GAEA_FORCE_INLINE ActualType elem_mul(ActualType const& mat) const
		{
			return ActualType{ *this }.elem_mul_assign(mat);
		}

		GAEA_FORCE_INLINE ActualType& elem_div_assign(ActualType const& mat)
		{
			for (std::size_t idx = 0; idx < _element_count; ++idx)
			{
				_elements[idx] /= mat._elements[idx];
			}
			return *static_cast<ActualType*>(this);
		}

		GAEA_FORCE_INLINE ActualType elem_div(ActualType const& mat) const
		{
			return ActualType{ *this }.elem_div_assign(mat);
		}

		GAEA_FORCE_INLINE FloatType determinant_unsafe() const
		{
			static_assert(RowValue == ColValue);
			return _echelon<false, false>(*static_cast<ActualType const*>(this));
		}

		GAEA_FORCE_INLINE ActualType inverse_unsafe() const
		{
			static_assert(RowValue == ColValue);
			return _echelon<true, false>(*static_cast<ActualType const*>(this));
		}

		GAEA_FORCE_INLINE FloatType determinant() const
		{
			static_assert(RowValue == ColValue);
			return _echelon<false, true>(*static_cast<ActualType const*>(this));
		}

		GAEA_FORCE_INLINE ActualType inverse() const
		{
			static_assert(RowValue == ColValue);
			return _echelon<true, true>(*static_cast<ActualType const*>(this));
		}

		GAEA_FORCE_INLINE typename implementation::gmatrix_transpose_type<ActualType>::type transpose() const
		{
			using transpose_type = typename implementation::gmatrix_transpose_type<ActualType>::type;
			return transpose_type{ typename transpose_type::_transpose_matrix_tag{}, _elements };
		}

		GAEA_FORCE_INLINE bool is_diagonal() const
		{
			if constexpr (RowValue != ColValue)
			{
				return false;
			}

			for (std::size_t i = 0; i < RowValue; ++i)
			{
				for (std::size_t j = 0; j < ColValue; ++j)
				{
					if ((i == j) == (_elements[i * _row_stride + j] == FloatType(0.0)))
					{
						return false;
					}
				}
			}

			return true;
		}

		GAEA_FORCE_INLINE FloatType* data()
		{
			return _elements;
		}

		GAEA_FORCE_INLINE constexpr FloatType& operator()(std::size_t i, std::size_t j)
		{
			return _elements[i * _row_stride + j];
		}
		GAEA_FORCE_INLINE constexpr FloatType const& operator()(std::size_t i, std::size_t j) const
		{
			return _elements[i * _row_stride + j];
		}

		GAEA_FORCE_INLINE constexpr std::size_t num_rows() const
		{
			return RowValue;
		}

		GAEA_FORCE_INLINE constexpr std::size_t num_cols() const
		{
			return ColValue;
		}

	protected:
		template <bool CalcInverseValue, bool UsePivotingValue>
		static std::conditional_t<CalcInverseValue, ActualType, FloatType> _echelon(ActualType mat)
		{
			ActualType mat_inv = ActualType{ _inverse_identity_matrix_tag{} };
			FloatType mat_det = FloatType(1.0);

			for (std::ptrdiff_t p = 0; p < RowValue - 1; ++p)
			{
				// Begin partial pivoting

				FloatType pivot = FloatType(0.0);
				FloatType pivot_abs = FloatType(0.0);
				std::ptrdiff_t pivot_idx = p;

				// Search absolute maximum

				if constexpr (UsePivotingValue)
				{
					for (std::ptrdiff_t idx = p; idx < RowValue; ++idx)
					{
						FloatType new_pivot = mat._elements[idx * _row_stride + p];
						FloatType new_pivot_abs = std::abs(new_pivot);

						if (pivot_abs < new_pivot_abs)
						{
							pivot = new_pivot;
							pivot_abs = new_pivot_abs;
							pivot_idx = idx;
						}
					}
				}
				else
				{
					pivot = mat._elements[p * _row_stride + p];
				}

				// Ensure not null

				if (math::nearly_equal(pivot, FloatType(0.0)))
				{
					if constexpr (CalcInverseValue)
					{
						return ActualType{ _inverse_zero_matrix_tag{} };
					}
					else
					{
						return FloatType(0.0);
					}
				}

				// Pivot rows

				if constexpr (UsePivotingValue)
				{
					if (pivot_idx != p)
					{
						FloatType pivot_row[_row_stride];

						std::memcpy(pivot_row, (mat._elements + p * _row_stride), sizeof(FloatType) * _row_stride);
						std::memcpy(mat._elements + p * _row_stride, mat._elements + pivot_idx * _row_stride, sizeof(FloatType) * _row_stride);
						std::memcpy(mat._elements + pivot_idx * _row_stride, pivot_row, sizeof(FloatType) * _row_stride);

						std::memcpy(pivot_row, (mat_inv._elements + p * _row_stride), sizeof(FloatType) * _row_stride);
						std::memcpy(mat_inv._elements + p * _row_stride, mat_inv._elements + pivot_idx * _row_stride, sizeof(FloatType) * _row_stride);
						std::memcpy(mat_inv._elements + pivot_idx * _row_stride, pivot_row, sizeof(FloatType) * _row_stride);
					}
				}

				// End partial pivoting

				for (std::ptrdiff_t i = p + 1; i < RowValue; ++i)
				{
					FloatType factor = -mat._elements[i * _row_stride + p] / pivot;

					for (std::ptrdiff_t j = 0; j < ColValue; ++j)
					{
						mat._elements[i * _row_stride + j] += factor * mat._elements[p * _row_stride + j];
						mat_inv._elements[i * _row_stride + j] += factor * mat_inv._elements[p * _row_stride + j];
					}
				}

				if constexpr (!CalcInverseValue)
				{
					mat_det *= pivot * (pivot_idx == p ? 1 : -1);
				}
			}

			if constexpr (CalcInverseValue)
			{
				for (std::ptrdiff_t p = RowValue - 1; p > 0; --p)
				{
					FloatType pivot = mat._elements[p * _row_stride + p];

					for (std::ptrdiff_t i = p - 1; i > -1; --i)
					{
						FloatType factor = -mat._elements[i * _row_stride + p] / pivot;

						for (std::ptrdiff_t j = 0; j < ColValue; ++j)
						{
							mat._elements[i * _row_stride + j] += factor * mat._elements[p * _row_stride + j];
							mat_inv._elements[i * _row_stride + j] += factor * mat_inv._elements[p * _row_stride + j];
						}
					}
				}

				for (std::ptrdiff_t i = 0; i < RowValue; ++i)
				{
					FloatType factor = mat._elements[i * _row_stride + i];

					for (std::ptrdiff_t j = 0; j < ColValue; ++j)
					{
						mat_inv._elements[i * _row_stride + j] /= factor;
					}
				}

				return mat_inv;
			}
			else
			{
				return mat_det * mat._elements[_element_count - 1];
			}
		}

		FloatType _elements[_element_count];

		template <class OtherFloatType, std::size_t OtherRowValue, std::size_t OtherColValue, class OtherActualType>
		friend class _gmatrix;

		template <class OtherFloatType, std::size_t Row1Value, std::size_t Col1Row2Value, std::size_t Col2Value, class Actual1Type, class Actual2Type>
		friend auto _matrix_product(_gmatrix<OtherFloatType, Row1Value, Col1Row2Value, Actual1Type> const& mat1, _gmatrix<OtherFloatType, Col1Row2Value, Col2Value, Actual2Type> const& mat2);
		template <class OtherFloatType, std::size_t Row1Value, std::size_t Col1Row2Value, std::size_t Col2Value, class Actual1Type, class Actual2Type>
		friend auto operator*(_gmatrix<OtherFloatType, Row1Value, Col1Row2Value, Actual1Type> const& mat1, _gmatrix<OtherFloatType, Col1Row2Value, Col2Value, Actual2Type> const& mat2);
	};
	
	template <class FloatType, std::size_t Row1Value, std::size_t Col1Row2Value, std::size_t Col2Value, class Actual1Type, class Actual2Type>
	auto _matrix_product(_gmatrix<FloatType, Row1Value, Col1Row2Value, Actual1Type> const& mat1, _gmatrix<FloatType, Col1Row2Value, Col2Value, Actual2Type> const& mat2)
	{
		using actual_type = typename implementation::gmatrix_common_actual_type<
			FloatType, Row1Value, Col2Value,
			_gmatrix<FloatType, Row1Value, Col1Row2Value, Actual1Type>,
			_gmatrix<FloatType, Col1Row2Value, Col2Value, Actual2Type>
		>::type;

		actual_type mat_res{ typename actual_type::_product_zero_matrix_tag{} };

		for (std::size_t i = 0; i < Row1Value; ++i)
		{
			for (std::size_t k = 0; k < Col1Row2Value; ++k)
			{
				for (std::size_t j = 0; j < Col2Value; ++j)
				{
					mat_res(i, j) += mat1(i, k) * mat2(k, j);
				}
			}
		}

		return mat_res;
	}

	template <class FloatType, std::size_t Row1Value, std::size_t Col1Row2Value, std::size_t Col2Value, class Actual1Type, class Actual2Type>
	GAEA_FORCE_INLINE auto operator*(_gmatrix<FloatType, Row1Value, Col1Row2Value, Actual1Type> const& mat1, _gmatrix<FloatType, Col1Row2Value, Col2Value, Actual2Type> const& mat2)
	{
		return _matrix_product(mat1, mat2);
	}

	template <class FloatType, std::size_t RowValue, std::size_t ColValue>
	struct gmatrix : _gmatrix<FloatType, RowValue, ColValue, gmatrix<FloatType, RowValue, ColValue>>
	{
		using _base = _gmatrix<FloatType, RowValue, ColValue, gmatrix<FloatType, RowValue, ColValue>>;
		
		explicit constexpr gmatrix() = default;
		
		explicit constexpr gmatrix(typename _base::_product_zero_matrix_tag)
			: _base{ typename _base::_product_zero_matrix_tag{} }
		{
		}
		explicit constexpr gmatrix(typename _base::_inverse_zero_matrix_tag)
			: _base{ typename _base::_inverse_zero_matrix_tag{} }
		{
		}
		explicit constexpr gmatrix(typename _base::_inverse_identity_matrix_tag)
			: _base{ typename _base::_inverse_identity_matrix_tag{} }
		{
		}
		explicit constexpr gmatrix(typename _base::_transpose_matrix_tag, FloatType const(& elements)[_base::_element_count])
			: _base{ typename _base::_transpose_matrix_tag{}, elements }
		{
		}
		
		explicit constexpr gmatrix(zero_matrix_tag)
			: _base{ zero_matrix_tag{} }
		{
		}
		explicit constexpr gmatrix(identity_matrix_tag)
			: _base{ identity_matrix_tag{} }
		{
			static_assert(RowValue == ColValue);
		}
		
		explicit constexpr gmatrix(FloatType value)
			: _base{ value }
		{
		}
		explicit constexpr gmatrix(FloatType const(& elements)[_base::_element_count])
			: _base{ elements }
		{
		}

		template <class OtherFloatType>
		friend gvector<OtherFloatType> operator*(gmatrix<OtherFloatType, 3, 3> const& mat, gvector<OtherFloatType> const& vec);
		template <class OtherFloatType>
		friend gvector<OtherFloatType> operator*(gvector<OtherFloatType> const& vec, gmatrix<OtherFloatType, 3, 3> const& mat);
	};

	template <class FloatType>
	GAEA_FORCE_INLINE gvector<FloatType> operator*(gmatrix<FloatType, 3, 3> const& mat, gvector<FloatType> const& vec)
	{
#if 0
		gvector<FloatType> vec_res{ FloatType(0.0) };

		for (std::size_t i = 0; i < 3; ++i)
		{
			for (std::size_t j = 0; j < 3; ++j)
			{
				vec_res(i) += mat(i, j) * vec(j);
			}
		}

		return vec_res;
#endif
		return gvector<FloatType>{
			mat(0, 0) * vec[0] + mat(0, 1) * vec[1] + mat(0, 2) * vec[2],
			mat(1, 0) * vec[0] + mat(1, 1) * vec[1] + mat(1, 2) * vec[2],
			mat(2, 0) * vec[0] + mat(2, 1) * vec[1] + mat(2, 2) * vec[2],
		};
	}

	template <class FloatType>
	GAEA_FORCE_INLINE gvector<FloatType> operator*(gvector<FloatType> const& vec, gmatrix<FloatType, 3, 3> const& mat)
	{
#if 0
		gvector<FloatType> vec_res{ FloatType(0.0) };

		for (std::size_t i = 0; i < 3; ++i)
		{
			for (std::size_t j = 0; j < 3; ++j)
			{
				vec_res(i) += vec(i) * mat(i, j);
			}
		}

		return vec_res;
#endif
		return gvector<FloatType>{
			vec[0] * mat(0, 0) + vec[0] * mat(0, 1) + vec[0] * mat(0, 2),
			vec[1] * mat(1, 0) + vec[1] * mat(1, 1) + vec[1] * mat(1, 2),
			vec[2] * mat(2, 0) + vec[2] * mat(2, 1) + vec[2] * mat(2, 2),
		};
	}

	using smatrix2 = gmatrix<float32, 2, 2>;
	using smatrix3 = gmatrix<float32, 3, 3>;
	using smatrix4 = gmatrix<float32, 4, 4>;

	using dmatrix2 = gmatrix<float64, 2, 2>;
	using dmatrix3 = gmatrix<float64, 3, 3>;
	using dmatrix4 = gmatrix<float64, 4, 4>;

	using matrix2 = smatrix2;
	using matrix3 = smatrix3;
	using matrix4 = smatrix4;

	static_assert(std::is_standard_layout_v<_gmatrix<float32, 1, 1, gmatrix<float32, 1, 1>>>);
	static_assert(std::is_standard_layout_v<_gmatrix<float64, 1, 1, gmatrix<float64, 1, 1>>>);

}

#endif
