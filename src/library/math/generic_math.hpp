#ifndef LIBRARY_MATH_GENERIC_MATH_INCLUDE_GUARD
#define LIBRARY_MATH_GENERIC_MATH_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea::math
{
	template <class>
	struct select_epsilon;
	
	template <>
	struct select_epsilon<float32>
	{
		static constexpr float32 epsilon = gaea::epsilon_f;
		static constexpr float32 tiny_epsilon = gaea::tiny_epsilon_f;
	};
	
	template <>
	struct select_epsilon<float64>
	{
		static constexpr float64 epsilon = gaea::epsilon;
		static constexpr float64 tiny_epsilon = gaea::tiny_epsilon;
	};
	
	/*template <class FloatType, std::enable_if_t<std::is_floating_point_v<FloatType>, int> = 0>
	bool nearly_equal(FloatType value1, FloatType value2,
					  FloatType epsilon = std::numeric_limits<FloatType>::epsilon(),
					  FloatType precision = std::numeric_limits<FloatType>::round_error(),
					  bool consider_magnitude = true)
	{
		// Default to the epsilon of the machine, scaled to the precision of the machine (in ULPs), and scaled to the magnitude of the values considered
		return std::abs(value1 - value2) <= epsilon * precision * (consider_magnitude ? std::abs(value1 + value2) : FloatType(1.0));
	}*/

	template <class ArithType>
	GAEA_FORCE_INLINE ArithType min(ArithType value1, ArithType value2)
	{
		static_assert(std::is_arithmetic_v<ArithType>);
		return value1 < value2 ? value1 : value2;
	}

	template <class ArithType>
	GAEA_FORCE_INLINE ArithType max(ArithType value1, ArithType value2)
	{
		static_assert(std::is_arithmetic_v<ArithType>);
		return value2 < value1 ? value1 : value2;
	}

	template <class ArithType>
	GAEA_FORCE_INLINE ArithType clamp(ArithType value, ArithType min_value, ArithType max_value)
	{
		static_assert(std::is_arithmetic_v<ArithType>);
		return value < min_value ? min_value : (max_value < value ? max_value : value);
	}

	template <class ArithType>
	GAEA_FORCE_INLINE ArithType abs(ArithType value)
	{
		static_assert(std::is_arithmetic_v<ArithType>);
		return value < ArithType{} ? -value : value;
	}

	template <class FloatType>
	GAEA_FORCE_INLINE bool nearly_equal(FloatType value1, FloatType value2, FloatType tolerance = select_epsilon<FloatType>::epsilon)
	{
		static_assert(std::is_floating_point_v<FloatType>);
		return math::abs(value1 - value2) <= tolerance;
	}

	template <class FloatType>
	GAEA_FORCE_INLINE bool nearly_zero(FloatType value, FloatType tolerance = select_epsilon<FloatType>::epsilon)
	{
		static_assert(std::is_floating_point_v<FloatType>);
		return math::abs(value) <= tolerance;
	}

	template <class FloatType>
	GAEA_FORCE_INLINE bool is_finite(FloatType value)
	{
		static_assert(std::is_floating_point_v<FloatType>);
        return value != std::numeric_limits<FloatType>::infinity() && value != -std::numeric_limits<FloatType>::infinity();
	}

	template <class FloatType>
	GAEA_FORCE_INLINE bool is_nan(FloatType value)
	{
		static_assert(std::is_floating_point_v<FloatType>);
        // ReSharper disable once CppIdenticalOperandsInBinaryExpression
        return value != value;
	}

	// Reciprocal square-root
	//
	// Single precision
	// 
	// Original magic constant: 0x5F3759DF
	//
	// From https://arxiv.org/pdf/1603.04483.pdf
	//
	// Minimal Relative Error:
	// 0x5F37642F - 0 Newton-Raphson iterations
	// 0x5F375A86 - n Newton-Raphson iterations
	//
	// Minimal Absolute Error:
	// 0x5F3863F7 - 0 Newton-Raphson iterations
	// 0x5F37E75A - 1 Newton-Raphson iterations
	// 0x5F37ADD5 - 2 Newton-Raphson iterations

	GAEA_FORCE_INLINE float32 rsqrt_single_rel_0NR(float32 val)
	{
		uint32 bitval = *reinterpret_cast<uint32*>(&val);
		bitval = 0x5F37642F - (bitval >> 1);
		val = *reinterpret_cast<float32*>(&bitval);

		return val;
	}
	
	GAEA_FORCE_INLINE float32 rsqrt_single_rel_nNR(float32 val, uint32 iter = 1)
	{
		float32 halfval = val * 0.5f;

		uint32 bitval = *reinterpret_cast<uint32*>(&val);
		bitval = 0x5F375A86 - (bitval >> 1);
		val = *reinterpret_cast<float32*>(&bitval);

		// Always do at least one iteration
		val *= 1.5f - halfval * val * val;
		while (iter-- > 1)
		{
			val *= 1.5f - halfval * val * val;
		}

		return val;
	}

	GAEA_FORCE_INLINE float32 rsqrt_single_abs_0NR(float32 val)
	{
		uint32 bitval = *reinterpret_cast<uint32*>(&val);
		bitval = 0x5F3863F7 - (bitval >> 1);
		val = *reinterpret_cast<float32*>(&bitval);

		return val;
	}
	
	GAEA_FORCE_INLINE float32 rsqrt_single_abs_1NR(float32 val)
	{
		float32 halfval = val * 0.5f;

		uint32 bitval = *reinterpret_cast<uint32*>(&val);
		bitval = 0x5F37E75A - (bitval >> 1);
		val = *reinterpret_cast<float32*>(&bitval);

		val *= 1.5f - halfval * val * val;

		return val;
	}
	
	GAEA_FORCE_INLINE float32 rsqrt_single_abs_2NR(float32 val)
	{
		float32 halfval = val * 0.5f;

		uint32 bitval = *reinterpret_cast<uint32*>(&val);
		bitval = 0x5F37ADD5 - (bitval >> 1);
		val = *reinterpret_cast<float32*>(&bitval);

		val *= 1.5f - halfval * val * val;
		val *= 1.5f - halfval * val * val;

		return val;
	}

	// Double precision
	//
	// From https://cs.uwaterloo.ca/~m32rober/rsqrt.pdf
	//
	// Minimal relative error:
	// 0x5FE6EB50C7B537A9 - 1 Newton-Raphson iterations
	
	GAEA_FORCE_INLINE float64 rsqrt_double_rel_1NR(float64 val)
	{
		float64 halfval = val * 0.5;

		uint64 bitval = *reinterpret_cast<uint64*>(&val);
		bitval = 0x5FE6EB50C7B537A9 - (bitval >> 1);
		val = *reinterpret_cast<float64*>(&bitval);

		val *= 1.5 - halfval * val * val;

		return val;
	}

	template <class ArithType>
	GAEA_FORCE_INLINE ArithType square(ArithType base)
	{
		static_assert(std::is_arithmetic_v<ArithType>);
		return base * base;
	}

	template <class ArithType>
	GAEA_FORCE_INLINE ArithType cube(ArithType base)
	{
		static_assert(std::is_arithmetic_v<ArithType>);
		return base * base * base;
	}

	GAEA_FORCE_INLINE constexpr std::size_t hash_bytes(void const* data, std::size_t size)
	{
		// FNV-1a hashing algorithm
		// https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function#FNV-1a_hash

		constexpr std::size_t fnv_prime = 0x0000'0100'0000'01B3;
		std::size_t hash				= 0xCBF2'9CE4'8422'2325;

		uint8 const* byte = static_cast<uint8 const*>(data);
		while (size-- > 0)
		{
			hash ^= *byte++;
			hash *= fnv_prime;
		}

		return hash;
	}

#pragma warning(push)
#pragma warning(disable: 4146)
	template <class IntType>
	GAEA_FORCE_INLINE std::pair<IntType, IntType> calc_min_max(IntType value1, IntType value2)
	{
		// https://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax
		static_assert(std::is_integral_v<IntType>);
		
		IntType value_mask = (value1 ^ value2) & -uint64(value1 < value2);
		return std::make_pair(value2 ^ value_mask, value1 ^ value_mask);
	}
#pragma warning(pop)

	template <class IntType>
	GAEA_FORCE_INLINE constexpr IntType log_two(IntType value)
	{
		// https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogObvious
		static_assert(std::is_integral_v<IntType>);

	    IntType result = 0;
		while (value >>= 1)
		{
			++result;
		}
		return result;
	}

	template <class IntType>
	GAEA_FORCE_INLINE constexpr IntType mod_power_two(IntType numerator, IntType denominator)
	{
		return numerator & (denominator - 1);
	}

	template <class IntType>
	GAEA_FORCE_INLINE constexpr IntType floor_power_two(IntType value)
	{
		static_assert(std::is_integral_v<IntType>);
		
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		
		if constexpr (sizeof value > 1)
		{
			value |= value >> 8;
		}
		if constexpr (sizeof value > 2)
		{
			value |= value >> 16;
		}
		if constexpr (sizeof value > 4)
		{
			value |= value >> 32;
		}

		value = (value + 1) >> 1;
		
		return value;
	}
	
	template <class IntType>
	GAEA_FORCE_INLINE constexpr IntType ceil_power_two(IntType value)
	{
		static_assert(std::is_integral_v<IntType>);
		
		--value;
		
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		
		if constexpr (sizeof value > 1)
		{
			value |= value >> 8;
		}
		if constexpr (sizeof value > 2)
		{
			value |= value >> 16;
		}
		if constexpr (sizeof value > 4)
		{
			value |= value >> 32;
		}
		
		++value;

		return value;
	}

	/*template <class Type>
	Type lerp(Type const& a, Type const& b, Type const& alpha)
	{
		return a + alpha * (b - a);
	}

	template <class Type>
	Type lerp_stable(Type const& a, Type const& b, Type const& alpha)
	{
		return a * (1.0 - alpha) + b * alpha;
	}*/
}

#endif
