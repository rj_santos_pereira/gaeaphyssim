#ifndef LIBRARY_MATH_TRANSFORM_INCLUDE_GUARD
#define LIBRARY_MATH_TRANSFORM_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/math/matrix.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	struct transform : _gmatrix<float32, 4, 4, transform>
	{
		using _base = _gmatrix<float32, 4, 4, transform>;
		
		explicit transform(svector const& t = zero_svector, squaternion const& r = identity_squaternion, svector const& s = one_svector)
			: _base{}
		{
			transform res_mat
				= make_translation(t)
				* make_rotation(r)
				* make_scaling(s);

			std::memcpy(data(), res_mat.data(), sizeof(float32) * _element_count);
		}
		explicit constexpr transform(skip_init_tag)
			: _base{ zero_matrix_tag{} }
		{
		}
		explicit constexpr transform(float32 const(& elements)[_element_count])
			: _base{ elements }
		{
		}

		explicit constexpr transform(_product_zero_matrix_tag)
			: _base{ _product_zero_matrix_tag{} }
		{
		}
		explicit constexpr transform(_inverse_zero_matrix_tag)
			: _base{ _inverse_zero_matrix_tag{} }
		{
		}
		explicit constexpr transform(_inverse_identity_matrix_tag)
			: _base{ _inverse_identity_matrix_tag{} }
		{
		}
		explicit constexpr transform(_transpose_matrix_tag, float32 const(& elements)[_element_count])
			: _base{ _transpose_matrix_tag{}, elements }
		{
		}

		GAEA_FORCE_INLINE transform& translate(svector const& t)
		{
			auto new_mat = *this * make_translation(t);
			std::memcpy(data(), new_mat.data(), sizeof(float32) * _element_count);
			return *this;
		}
		
		GAEA_FORCE_INLINE transform& rotate(squaternion const& r)
		{
			auto new_mat = *this * make_rotation(r);
			std::memcpy(data(), new_mat.data(), sizeof(float32) * _element_count);
			return *this;
		}
		
		GAEA_FORCE_INLINE transform& scale(svector const& s)
		{
			auto new_mat = *this * make_scaling(s);
			std::memcpy(data(), new_mat.data(), sizeof(float32) * _element_count);
			return *this;
		}

		GAEA_FORCE_INLINE transform& inverse_translate(svector const& t)
		{
			auto res_mat = make_translation(t) * *this;
			std::memcpy(data(), res_mat.data(), sizeof(float32) * _element_count);
			return *this;
		}
		
		GAEA_FORCE_INLINE transform& inverse_rotate(squaternion const& r)
		{
			auto res_mat = make_rotation(r) * *this;
			std::memcpy(data(), res_mat.data(), sizeof(float32) * _element_count);
			return *this;
		}
		
		GAEA_FORCE_INLINE transform& inverse_scale(svector const& s)
		{
			auto res_mat = make_scaling(s) * *this;
			std::memcpy(data(), res_mat.data(), sizeof(float32) * _element_count);
			return *this;
		}

		GAEA_FORCE_INLINE static transform make_translation(svector const& t)
		{
			return transform{ {
				1.0f, 0.0f, 0.0f, t.x,
				0.0f, 1.0f, 0.0f, t.y,
				0.0f, 0.0f, 1.0f, t.z,
				0.0f, 0.0f, 0.0f, 1.0f
			} };
		}

		GAEA_FORCE_INLINE static transform make_rotation(squaternion const& r)
		{
			float32 ww = r.w * r.w;
			float32 xx = r.x * r.x;
			float32 yy = r.y * r.y;
			float32 zz = r.z * r.z;

			float32 wx = r.w * r.x;
			float32 wy = r.w * r.y;
			float32 wz = r.w * r.z;

			float32 xy = r.x * r.y;
			float32 yz = r.y * r.z;
			float32 xz = r.z * r.x;
			
			return transform{ {
				ww + xx - yy - zz, 2.f * (xy - wz), 2.f * (wy + xz), 0.f,
				2.f * (wz + xy), ww - xx + yy - zz, 2.f * (yz - wx), 0.f,
				2.f * (xz - wy), 2.f * (wx + yz), ww - xx - yy + zz, 0.f,
							0.f,             0.f,               0.f, 1.f
			} };
		}

		GAEA_FORCE_INLINE static transform make_scaling(svector const& s)
		{
			return transform{ {
				s.x, 0.f, 0.f, 0.f,
				0.f, s.y, 0.f, 0.f,
				0.f, 0.f, s.z, 0.f,
				0.f, 0.f, 0.f, 1.f
			} };
		}

	};

	static_assert(std::is_standard_layout_v<transform>);
}

#endif
