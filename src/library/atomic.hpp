#ifndef ENGINE_LIBRARY_ATOMIC_INCLUDE_GUARD
#define ENGINE_LIBRARY_ATOMIC_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea
{
	template <class Type>
	using atomic = std::atomic<Type>;
	
	using atomic_bool = std::atomic_bool;

	using atomic_int8 = std::atomic_int8_t;
	using atomic_int16 = std::atomic_int16_t;
	using atomic_int32 = std::atomic_int32_t;
	using atomic_int64 = std::atomic_int64_t;

	using atomic_uint8 = std::atomic_uint8_t;
	using atomic_uint16 = std::atomic_uint16_t;
	using atomic_uint32 = std::atomic_uint32_t;
	using atomic_uint64 = std::atomic_uint64_t;

	using atomic_float32 = std::atomic<float32>;
	using atomic_float64 = std::atomic<float64>;

	class atomic_flag final
	{
	public:
		explicit constexpr atomic_flag() noexcept {}

		bool test_and_set(std::memory_order order = std::memory_order_seq_cst) noexcept
		{
			return _flag.test_and_set(order);
		}

		void clear(std::memory_order order = std::memory_order_seq_cst) noexcept
		{
			_flag.clear(order);
		}

	private:
		std::atomic_flag _flag = ATOMIC_FLAG_INIT;
		
	};
}

#endif
