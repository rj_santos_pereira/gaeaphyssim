#ifndef LIBRARY_UTILITY_INCLUDE_GUARD
#define LIBRARY_UTILITY_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea::util
{
	std::string trim_space(std::string_view const& string);
	std::string trim_space(std::string const& string);
	std::string trim_space(std::string&& string);
	std::string& trim_space(std::string& string);
	
	std::string trim_fraction(std::string_view const& string, std::size_t count);
	std::string trim_fraction(std::string const& string, std::size_t count);
	std::string trim_fraction(std::string&& string, std::size_t count);
	std::string& trim_fraction(std::string& string, std::size_t count);

	std::string to_lowercase(std::string_view const& string);
	std::string to_lowercase(std::string const& string);
	std::string to_lowercase(std::string&& string);
	std::string& to_lowercase(std::string& string);

	std::string to_uppercase(std::string_view const& string);
	std::string to_uppercase(std::string const& string);
	std::string to_uppercase(std::string&& string);
	std::string& to_uppercase(std::string& string);

	bool to_boolean(std::string_view const& string, bool& value);

	//template <class IntType, std::enable_if_t<std::is_integral_v<IntType>, int> = 0>
	//bool to_integer(std::string_view const& string, IntType& value);

	bool to_integer(std::string_view const& string, int8& value);
	bool to_integer(std::string_view const& string, int16& value);
	bool to_integer(std::string_view const& string, int32& value);
	bool to_integer(std::string_view const& string, int64& value);
	bool to_integer(std::string_view const& string, uint8& value);
	bool to_integer(std::string_view const& string, uint16& value);
	bool to_integer(std::string_view const& string, uint32& value);
	bool to_integer(std::string_view const& string, uint64& value);

	std::tuple<float32, float32, float32> color_to_tuple(basic_color color);

	std::wstring narrow_to_wide(std::string const& narrow);
	std::string wide_to_narrow(std::wstring const& wide);
	
	std::string system_time_to_string(std::string const& format, stdchr::system_clock::time_point const& time);
	
	bool rename_thread(std::string const& thread_name);

	bool create_directory_tree(stdfs::path const & path);
}

#endif
