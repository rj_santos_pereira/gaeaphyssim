#include "library/utility.hpp"

#ifdef _WIN32
#include "library/platform/windows_utility.hpp"
#endif

namespace gaea::util
{
	std::string trim_space(std::string_view const& string)
	{
		std::string new_string{ string };
		return trim_space(new_string);
	}

	std::string trim_space(std::string const& string)
	{
		std::string new_string{ string };
		return trim_space(new_string);
	}

	std::string trim_space(std::string&& string)
	{
		return trim_space(string);
	}

	std::string& trim_space(std::string& string)
	{
		{
			auto r_it = string.rbegin();
			auto r_end = string.rend();

			auto end = r_it.base();

			for (; r_it != r_end && std::isspace(*r_it); ++r_it);
			string.erase(r_it.base(), end);
		}

		{
			auto it = string.begin();
			auto end = string.end();

			auto beg = it;

			for (; it != end && std::isspace(*it); ++it);
			string.erase(beg, it);
		}

		return string;
	}
	
	std::string trim_fraction(std::string_view const& string, std::size_t count)
	{
		std::string new_string{ string };
		return trim_fraction(new_string, count);
	}

	std::string trim_fraction(std::string const& string, std::size_t count)
	{
		std::string new_string{ string };
		return trim_fraction(new_string, count);
	}

	std::string trim_fraction(std::string&& string, std::size_t count)
	{
		return trim_fraction(string, count);
	}

	std::string& trim_fraction(std::string& string, std::size_t count)
	{
		std::size_t pos = string.find('.');

		if (pos != std::string::npos)
		{
			string = string.substr(0, pos + count + 1);
		}

		return string;
	}

	std::string to_lowercase(std::string_view const& string)
	{
		std::string new_string{ string };
		return trim_space(new_string);
	}

	std::string to_lowercase(std::string const& string)
	{
		std::string new_string{ string };
		return to_lowercase(new_string);
	}

	std::string to_lowercase(std::string&& string)
	{
		return to_lowercase(string);
	}

	std::string& to_lowercase(std::string& string)
	{
		std::transform(string.cbegin(), string.cend(), string.begin(), 
			[] (unsigned char c) { return static_cast<unsigned char>(std::tolower(c)); });
		return string;
	}

	std::string to_uppercase(std::string_view const& string)
	{
		std::string new_string{ string };
		return to_uppercase(new_string);
	}

	std::string to_uppercase(std::string const& string)
	{
		std::string new_string{ string };
		return to_uppercase(new_string);
	}

	std::string to_uppercase(std::string&& string)
	{
		return to_uppercase(string);
	}

	std::string& to_uppercase(std::string& string)
	{
		std::transform(string.cbegin(), string.cend(), string.begin(),
			[] (unsigned char c) { return static_cast<unsigned char>(std::toupper(c)); });
		return string;
	}

	bool to_boolean(std::string_view const& string, bool& value)
	{
		auto canonical_string = to_lowercase(string);
		return (canonical_string == "true" && (value = true) == true)
			|| (canonical_string == "false" && (value = false) == false);
	}

	/*
	template <class IntType, std::enable_if_t<std::is_integral_v<IntType>, int>>
	bool to_integer(std::string_view const& string, IntType& value) // TODO test with negative ints (maybe revert to old impl)
	{
		IntType parsed_value;

		auto string_beg = string.c_str();
		auto string_end = string_beg + string.size();

		auto result = std::from_chars(string_beg, string_end, parsed_value);

		if (!std::error_condition{ result.ec } && result.ptr == string_end)
		{
			value = parsed_value;
			return true;
		}

		return false;
	}

	template bool to_integer<int8, 0>(std::string_view const& string, int8&value);
	template bool to_integer<int16, 0>(std::string_view const& string, int16& value);
	template bool to_integer<int32, 0>(std::string_view const& string, int32& value);
	template bool to_integer<int64, 0>(std::string_view const& string, int64& value);
	template bool to_integer<uint8, 0>(std::string_view const& string, uint8&value);
	template bool to_integer<uint16, 0>(std::string_view const& string, uint16& value);
	template bool to_integer<uint32, 0>(std::string_view const& string, uint32& value);
	template bool to_integer<uint64, 0>(std::string_view const& string, uint64& value);
	*/

	bool to_integer(std::string_view const& string, int8& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = int8(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, int16& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = int16(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, int32& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = int32(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, int64& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = int64(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, uint8& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = uint8(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, uint16& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = uint16(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, uint32& value)
	{
		uint64 new_value;
		bool is_int = to_integer(string, new_value);

		value = uint32(new_value);
		return is_int;
	}

	bool to_integer(std::string_view const& string, uint64& value)
	{
		if (string.empty() || (!std::isdigit(string.front()) && string.front() != '+' && string.front() != '-'))
		{
			return false;
		}

		const char* str_beg = string.data();
		char* str_end;

		uint64 new_value = std::strtoull(str_beg, &str_end, 10);

		if (str_end - str_beg != std::ptrdiff_t(string.size()))
		{
			return false;
		}

		value = new_value;

		return true;
	}

	std::tuple<float32, float32, float32> color_to_tuple(basic_color color)
	{
		switch (color)
		{
		default:
		case basic_color::black:
			return { 0.0f, 0.0f, 0.0f };
		case basic_color::dark_gray:
			return { 0.33f, 0.33f, 0.33f };
		case basic_color::light_gray:
			return { 0.66f, 0.66f, 0.66f };
		case basic_color::white:
			return { 1.0f, 1.0f, 1.0f };
		case basic_color::red:
			return { 1.0f, 0.0f, 0.0f };
		case basic_color::green:
			return { 0.0f, 1.0f, 0.0f };
		case basic_color::blue:
			return { 0.0f, 0.0f, 1.0f };
		case basic_color::yellow:
			return { 1.0f, 1.0f, 0.0f };
		case basic_color::cyan:
			return { 0.0f, 1.0f, 1.0f };
		case basic_color::magenta:
			return { 1.0f, 0.0f, 1.0f };
		case basic_color::orange:
			return { 1.0f, 0.5f, 0.0f };
		case basic_color::chartreuse:
			return { 0.5f, 1.0f, 0.0f };
		case basic_color::spring:
			return { 0.0f, 1.0f, 0.5f };
		case basic_color::azure:
			return { 0.0f, 0.5f, 1.0f };
		case basic_color::violet:
			return { 0.5f, 0.0f, 1.0f };
		case basic_color::rose:
			return { 1.0f, 0.0f, 0.5f };
		}
	}

	std::wstring narrow_to_wide(std::string const& narrow)
	{
		std::wstring_convert<std::codecvt<wchar_t, char, std::mbstate_t>> converter;
		return converter.from_bytes(narrow);
	}

	std::string wide_to_narrow(std::wstring const& wide)
	{
		std::wstring_convert<std::codecvt<wchar_t, char, std::mbstate_t>> converter;
		return converter.to_bytes(wide);
	}

#pragma warning(suppress: 4702)
	std::string system_time_to_string(std::string const& format, stdchr::system_clock::time_point const& time)
	{
#ifdef _WIN32
		return platform::windows::util::system_time_to_string(format, time);
#else
#error Method not implemented on this platform!
#endif
	}
	
#pragma warning(suppress: 4702)
	bool rename_thread(std::string const& thread_name)
	{
#ifdef _WIN32
		return platform::windows::util::rename_thread(thread_name);
#else
#error Method not implemented on this platform!
#endif
	}

	bool create_directory_tree(stdfs::path const& path)
	{
		return path.has_parent_path()
				&& (exists(path.parent_path()) || util::create_directory_tree(path.parent_path()))
				&& (exists(path) || stdfs::create_directory(path));
	}
}
