#ifndef LIBRARY_CONTAINERS_INCLUDE_GUARD
#define LIBRARY_CONTAINERS_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea
{
	template <class Type, class Allocator = std::allocator<Type>>
	using array = std::vector<Type, Allocator>;

	template <class Type, std::size_t Size>
	using fixed_array = std::array<Type, Size>;

	template <class Type>
	using list = std::list<Type>;

	template <class Type>
	using queue = std::queue<Type, std::deque<Type>>;

	template <class Type>
	using stack = std::stack<Type, std::deque<Type>>;

	template <class Type, class Hash = std::hash<Type>, class Compare = std::equal_to<Type>>
	using set = std::unordered_set<Type, Hash, Compare>;

	template <class Key, class Value, class Hash = std::hash<Key>, class Compare = std::equal_to<Key>>
	using map = std::unordered_map<Key, Value, Hash, Compare>;

	template <class Type, class Compare = std::less<Type>>
	using sorted_set = std::set<Type, Compare>;

	template <class Key, class Value, class Compare = std::less<Key>>
	using sorted_map = std::map<Key, Value, Compare>;

	template <class Type, class Compare = std::less<Type>>
	class array_set : protected array<Type>
	{
		using _base = array<Type>;
		
	public:
		using key_type = typename _base::value_type;
		using value_type = typename _base::value_type;
		using size_type = typename _base::size_type;
		using difference_type = typename _base::difference_type;
		using key_compare = Compare;
		using value_compare = Compare;
		using reference = typename _base::reference;
		using const_reference = typename _base::const_reference;
		using pointer = typename _base::pointer;
		using const_pointer = typename _base::const_pointer;
		using iterator = typename _base::iterator;
		using const_iterator = typename _base::const_iterator;
		using reverse_iterator = typename _base::reverse_iterator;
		using const_reverse_iterator = typename _base::const_reverse_iterator;
		
		array_set(Compare const& comp = Compare{})
			: _comp{ comp }
		{
		}
		template <class InputIt>
		array_set(InputIt first, InputIt last, Compare const& comp = Compare{})
			: _comp{ comp }
		{
			insert(first, last);
		}
		array_set(std::initializer_list<value_type> init, Compare const& comp = Compare{})
			: _comp{ comp }
		{
			insert(init);
		}
		
		array_set(array_set const& other)
			: _base{ other }
			, _comp{ other._comp }
		{
		}
		array_set(array_set&& other) noexcept
			: _base{ std::move(other) }
			, _comp{ std::move(other._comp) }
		{
		}
		
		~array_set() = default;

		array_set& operator=(array_set const& other)
		{
			_base::operator=(other);
			_comp = other._comp;
			return *this;
		}
		array_set& operator=(array_set&& other) noexcept
		{
			if (this != &other)
			{
				_base::operator=(std::move(other));
				_comp = std::move(other._comp);
			}
			return *this;
		}
		array_set& operator=(std::initializer_list<Type> init)
		{
			clear();
			insert(init);
			return *this;
		}

		using _base::begin;
		using _base::cbegin;
		
		using _base::end;
		using _base::cend;
		
		using _base::rbegin;
		using _base::crbegin;
		
		using _base::rend;
		using _base::crend;
		
		using _base::empty;
		using _base::size;
		using _base::max_size;
		using _base::reserve;
		using _base::capacity;
		
		using _base::clear;
		
		std::pair<iterator, bool> insert(value_type const& value)
		{
			auto it = std::lower_bound(begin(), end(), value, _comp);
			bool is_new = it == end() || _comp(*it, value) || _comp(value, *it);

			if (is_new)
			{
				it = _base::insert(it, value);
			}
			
			return std::make_pair(std::move(it), is_new);
		}
		std::pair<iterator, bool> insert(value_type&& value)
		{
			auto it = std::lower_bound(begin(), end(), value, _comp);
			bool is_new = it == end() || _comp(*it, value) || _comp(value, *it);

			if (is_new)
			{
				it = _base::insert(it, std::move(value));
			}
			
			return std::make_pair(std::move(it), is_new);
		}
		
		iterator insert(const_iterator hint, value_type const& value)
		{
			GAEA_UNUSED(hint);
			return insert(value).first;
		}
		iterator insert(const_iterator hint, value_type&& value)
		{
			GAEA_UNUSED(hint);
			return insert(std::move(value)).first;
		}
		
		template <class InputIt>
		void insert(InputIt first, InputIt last)
		{
			reserve(std::distance(first, last));
			while (first != last)
			{
				insert(*first++);
			}
		}
		void insert(std::initializer_list<value_type> init)
		{
			reserve(init.size());
			for (value_type& value : init)
			{
				insert(std::move(value));
			}
		}
		
		using _base::erase;
		size_type erase(key_type const& key)
		{
			auto it = find(key);
			if (it != end())
			{
				_base::erase(it);
				return 1;
			}
			return 0;
		}

		iterator find(key_type const& key)
		{
			auto it = std::lower_bound(begin(), end(), key, _comp);
			if (it != end() && !_comp(*it, key) && !_comp(key, *it))
			{
				return it;
			}
			return end();
		}
		const_iterator find(key_type const& key) const
		{
			return const_cast<array_set*>(this)->find(key);
		}

		bool contains(key_type const& key) const
		{
			return find(key) != end();
		}
		
		iterator lower_bound(key_type const& key)
		{
			return const_cast<iterator>(static_cast<array_set const*>(this)->lower_bound(key));
		}
		const_iterator lower_bound(key_type const& key) const
		{
			return std::lower_bound(begin(), end(), key, _comp);
		}
		
		iterator upper_bound(key_type const& key)
		{
			return const_cast<iterator>(static_cast<array_set const*>(this)->upper_bound(key));
		}
		const_iterator upper_bound(key_type const& key) const
		{
			return std::upper_bound(begin(), end(), key, _comp);
		}

		key_compare key_comp() const
		{
			return _comp;
		}
		
		value_compare value_comp() const
		{
			return _comp;
		}
		
	private:
		Compare _comp;
		
	};

#if 0
	template <class Type, std::size_t Size>
	class double_array : protected fixed_array<Type, (Size << 1)>
	{
		using _base = fixed_array<Type, Size * 2>;

		static constexpr auto _size_value = Size;
		
	public:
		using value_type = typename _base::value_type;
		using size_type = typename _base::size_type;
		using difference_type = typename _base::difference_type;
		using reference = typename _base::reference;
		using const_reference = typename _base::const_reference;
		using pointer = typename _base::pointer;
		using const_pointer = typename _base::const_pointer;
		using iterator = typename _base::iterator;
		using const_iterator = typename _base::const_iterator;
		using reverse_iterator = typename _base::reverse_iterator;
		using const_reverse_iterator = typename _base::const_reverse_iterator;

		constexpr reference at(size_type pos)
		{
			if (pos >= _size_value)
			{
				throw std::out_of_range("pos must be less than Size");
			}
			return _base::operator[](_size_value * _offset + pos);
		}
		constexpr const_reference at(size_type pos) const
		{
			if (pos >= _size_value)
			{
				throw std::out_of_range("pos must be less than Size");
			}
			return _base::operator[](_size_value * _offset + pos);
		}
		
		constexpr reference at(size_type index, size_type pos)
		{
			if (pos >= _size_value || index > 1)
			{
				throw std::out_of_range("index must be either 0 or 1 and pos must be less than size");
			}
			return _base::operator[](_size_value * index + pos);
		}
		constexpr const_reference at(size_type index, size_type pos) const
		{
			if (pos >= _size_value || index > 1)
			{
				throw std::out_of_range("index must be either 0 or 1 and pos must be less than size");
			}
			return _base::operator[](_size_value * index + pos);
		}
		
		constexpr reference operator[](size_type pos)
		{
			return _base::operator[](_size_value * _offset + pos);
		}
		constexpr const_reference operator[](size_type pos) const
		{
			return _base::operator[](_size_value * _offset + pos);
		}
		
		constexpr reference front()
		{
			return _base::operator[](_size_value * _offset);
		}
		constexpr const_reference front() const
		{
			return _base::operator[](_size_value * _offset);
		}
		
		constexpr reference back()
		{
			return _base::operator[](_size_value * _offset + _size_value);
		}
		constexpr const_reference back() const
		{
			return _base::operator[](_size_value * _offset + _size_value);
		}

		constexpr pointer data() noexcept
		{
			return _base::data() + _size_value * _offset;
		}
		constexpr const_pointer data() const noexcept
		{
			return _base::data() + _size_value * _offset;
		}

		constexpr iterator begin() noexcept
		{
			return std::next(_base::begin(), _size_value * _offset);
		}
		constexpr const_iterator begin() const noexcept
		{
			return std::next(_base::begin(), _size_value * _offset);
		}
		constexpr const_iterator cbegin() const noexcept
		{
			return begin();
		}

		constexpr iterator end() noexcept
		{
			return std::prev(_base::end(), _size_value * (1 - _offset));
		}
		constexpr const_iterator end() const noexcept
		{
			return std::prev(_base::end(), _size_value * (1 - _offset));
		}
		constexpr const_iterator cend() const noexcept
		{
			return end();
		}

		constexpr iterator rbegin() noexcept
		{
			return std::next(_base::rbegin(), _size_value * (1 - _offset));
		}
		constexpr const_iterator rbegin() const noexcept
		{
			return std::next(_base::rbegin(), _size_value * (1 - _offset));
		}
		constexpr const_iterator crbegin() const noexcept
		{
			return rbegin();
		}

		constexpr iterator rend() noexcept
		{
			return std::prev(_base::rend(), _size_value * _offset);
		}
		constexpr const_iterator rend() const noexcept
		{
			return std::prev(_base::rend(), _size_value * _offset);
		}
		constexpr const_iterator crend() const noexcept
		{
			return rend();
		}

		constexpr bool empty() const noexcept
		{
			return _base::begin() == _base::end();
		}
		constexpr size_type size() const noexcept
		{
			return _size_value;
		}
		constexpr size_type max_size() const noexcept
		{
			return size();
		}

		constexpr void fill(const_reference value)
		{
			std::fill(begin(), end(), value);
		}
		
		constexpr void copy_array(uint8 index)
		{
			if (index > 1)
			{
				throw std::out_of_range("index must be either 0 or 1");
			}
			// TODO
		}
		constexpr void move_array(uint8 index)
		{
			if (index > 1)
			{
				throw std::out_of_range("index must be either 0 or 1");
			}
			// TODO
		}

		constexpr void swap_arrays() noexcept
		{
			_offset = 1 - _offset;
		}

		constexpr uint8 current_index() const noexcept
		{
			return _offset;
		}
		
	private:
		uint8 _offset = 0;
		
	};
#endif
}

#endif
