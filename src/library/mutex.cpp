#include "library/mutex.hpp"

#if _WIN32
#include "library/platform/windows_mutex.hpp"
#endif

namespace gaea
{
	semaphore::semaphore(int64 initial_count)
		: _counter{ initial_count }
	{
	}

	void semaphore::acquire()
	{
#if _WIN32
		platform::windows::semaphore_acquire(&_counter);
#else
#error Method not implemented on this platform!
#endif
	}

#pragma warning(suppress: 4702)
	bool semaphore::try_acquire()
	{
#if _WIN32
		return platform::windows::semaphore_try_acquire(&_counter);
#else
#error Method not implemented on this platform!
#endif
		return false;
	}

	void semaphore::release(int64 count)
	{
#if _WIN32
		platform::windows::semaphore_release(&_counter, count);
#else
#error Method not implemented on this platform!
#endif
	}

	barrier::barrier(int64 initial_count)
		: _phase_counter{ 0 }
		, _counter{ initial_count }
		, _initial_count{ initial_count }
	{
	}

	void barrier::wait()
	{
#if _WIN32
		return platform::windows::barrier_wait(&_phase_counter, &_counter, _initial_count);
#else
#error Method not implemented on this platform!
#endif
	}

	void barrier::busy_wait()
	{
#if _WIN32
		return platform::windows::barrier_busy_wait(&_phase_counter, &_counter, _initial_count);
#else
#error Method not implemented on this platform!
#endif
	}

	void barrier::set_initial_count(int64 initial_count)
	{
		_initial_count = initial_count;
		_counter = _initial_count;
	}

	int64 barrier::get_initial_count() const
	{
		return _initial_count;
	}

	int64 barrier::num_waiting() const
	{
#if _WIN32
		return platform::windows::barrier_num_waiting(&_counter);
#else
#error Method not implemented on this platform!
#endif
	}

	spin_mutex::spin_mutex() noexcept
	{
	}

	void spin_mutex::lock() noexcept
	{
		while (_flag.test_and_set(std::memory_order_acquire))
		{
			std::this_thread::yield();
		}
	}

	bool spin_mutex::try_lock() noexcept
	{
		return !_flag.test_and_set(std::memory_order_acquire);
	}

	void spin_mutex::unlock() noexcept
	{
		_flag.clear(std::memory_order_release);
	}

	shared_spin_mutex::shared_spin_mutex() noexcept
		: _flag{ 0 }
	{
	}

	void shared_spin_mutex::lock()
	{
		int64 flag_state = 0;

		while (!_flag.compare_exchange_weak(flag_state, _exclusive_state, std::memory_order_acquire))
		{
			flag_state = 0;

			std::this_thread::yield();
		}
	}

	bool shared_spin_mutex::try_lock()
	{
		int64 flag_state = 0;

		return _flag.compare_exchange_strong(flag_state, _exclusive_state, std::memory_order_acquire);
	}

	void shared_spin_mutex::unlock()
	{
		_flag.store(0, std::memory_order_release);
	}

	void shared_spin_mutex::lock_shared()
	{
		int64 flag_state = _flag.load(std::memory_order_relaxed);

		do
		{
			if (_make_share_state(flag_state))
			{
				std::this_thread::yield();
			}
		}
		while (!_flag.compare_exchange_weak(flag_state, flag_state + 1, std::memory_order_acquire));
	}

	bool shared_spin_mutex::try_lock_shared()
	{
		int64 flag_state = _flag.load(std::memory_order_relaxed);

		_make_share_state(flag_state);

		return _flag.compare_exchange_strong(flag_state, flag_state + 1, std::memory_order_acquire);
	}

	void shared_spin_mutex::unlock_shared()
	{
		_flag.fetch_sub(1, std::memory_order_release);
	}

	bool shared_spin_mutex::upgrade()
	{
		std::unique_lock<spin_mutex> upgrade_lock{ _upgrade_mutex, std::try_to_lock };

		if (upgrade_lock.owns_lock())
		{
			int64 flag_state = 1;

			// Edge case, will avoid loops when only this thread holds the mutex
			if (!_flag.compare_exchange_strong(flag_state, _exclusive_state, std::memory_order_acquire))
			{
				while (!_flag.compare_exchange_weak(flag_state, _exclusive_state + flag_state - 1, std::memory_order_acquire));

				while (_flag.load(std::memory_order_acquire) != _exclusive_state)
				{
					std::this_thread::yield();
				}
			}
		}

		return upgrade_lock.owns_lock();
	}

	void shared_spin_mutex::downgrade()
	{
		_flag.store(1, std::memory_order_release);
	}

	bool shared_spin_mutex::_make_share_state(int64& flag_state)
	{
		if (flag_state < 0)
		{
			flag_state = 0;
			return true;
		}

		// Realistically, this should never happen
#if 0
		// Edge case, we'll try decrementing now; we might get lucky on this attempt if one thread releases the mutex
		if (flag_state == std::numeric_limits<int64>::max())
		{
			--flag_state;
			return true;
		}
#endif

		return false;
	}
}
