#include "core/core_minimal.hpp"
#include "system/random_engine.hpp"

#include "library/utility.hpp"

namespace gaea::implementation
{
	uint32 random_engine_detail::bound_generate(engine_type& generator, uint32 range)
	{
		uint32 x = generator();
		uint64 m = uint64(x) * uint64(range);
		uint32 l = uint32(m);
		if (l < range)
		{
#pragma warning(suppress: 4146)
			uint32 t = -range;
			if (t >= range)
			{
				t -= range;
				if (t >= range)
				{
					t %= range;
				}
			}
			while (l < t)
			{
				x = generator();
				m = uint64(x) * uint64(range);
				l = uint32(m);
			}
		}
		return m >> 32;
	}
}

namespace gaea
{
	random_engine_seed::random_engine_seed(_seed_type const& seed)
		: _seed{ seed }
	{
	}

	random_engine_seed::operator _seed_type const&() const
	{
		return _seed;
	}

	std::string random_engine_seed::serialize(random_engine_seed const& seed)
	{
		static constexpr std::size_t elem_size = std::numeric_limits<implementation::random_engine_detail::seed_element_type>::digits10 + 1;
		static constexpr std::size_t data_size = (elem_size + 1) * implementation::random_engine_detail::seed_element_number;

		std::string data;

		data.reserve(data_size);

		for (auto const& elem : seed._seed)
		{
			data += std::to_string(elem);
			data += '/';
		}

		data.shrink_to_fit();

		return data;
	}

	random_engine_seed random_engine_seed::deserialize(std::string const& data)
	{
		random_engine_seed seed;

		char const* data_ptr = data.c_str();
		char const* curr_ptr = data_ptr;
		char const* next_ptr = data_ptr + data.find('/');

		for (auto& elem : seed._seed)
		{
			std::string_view view{ curr_ptr, std::size_t(next_ptr - curr_ptr) };

			if (!util::to_integer(view, elem))
			{
				GAEA_WARN(random_engine_seed, "Error during seed deserialization, expected integer, found '%c'", elem);
				return null();
			}

			curr_ptr = next_ptr + 1;
			next_ptr = data_ptr + data.find('/', curr_ptr - data_ptr);
		}

		return seed;
	}

	random_engine_seed random_engine_seed::null()
	{
		return random_engine_seed{};
	}

	bool random_engine_seed::is_null() const
	{
		return std::all_of(_seed.begin(), _seed.end(), [] (auto&& elem) { return elem == 0; });
	}

	random_engine::random_engine()   // NOLINT(cert-msc51-cpp)
	{
		seed_engine();
	}

	random_engine::random_engine(skip_init_tag)  // NOLINT(cert-msc51-cpp)
	{
		// no-op
	}

	bool random_engine::generate_bool()
	{
		return bool(_engine() & 1);
	}

	int32 random_engine::generate_int()
	{
		return int32(_engine() & std::numeric_limits<int32>::max());
	}

	int32 random_engine::generate_int(int32 min, int32 max)
	{
		uint64 range = uint64(max - min);
		return int32(
			range < std::numeric_limits<uint32>::max()
			? int64(implementation::random_engine_detail::bound_generate(_engine, uint32(range + 1))) + min // Range is [min, max]
			: _engine()
		);
	}

	float32 random_engine::generate_float()
	{
		float32 result;
		do result = _float_distribution(_engine);
		while (result == 1.f); // Handle LWG issue 2524 // NOLINT(clang-diagnostic-float-equal)
		return result;
	}

	float32 random_engine::generate_float(float32 min, float32 max)
	{
		float32 result;
		do result = _float_distribution(_engine, std::uniform_real_distribution<float32>::param_type{ min, max });
		while (result == max); // Handle LWG issue 2524 // NOLINT(clang-diagnostic-float-equal)
		return result;
	}

	bool random_engine::generate_bool_sync()
	{
		std::scoped_lock<spin_mutex> lock{ _engine_mutex };
		return generate_bool();
	}

	int32 random_engine::generate_int_sync()
	{
		std::scoped_lock<spin_mutex> lock{ _engine_mutex };
		return generate_int();
	}

	int32 random_engine::generate_int_sync(int32 min, int32 max)
	{
		std::scoped_lock<spin_mutex> lock{ _engine_mutex };
		return generate_int(min, max);
	}

	float32 random_engine::generate_float_sync()
	{
		std::scoped_lock<spin_mutex> lock{ _engine_mutex };
		return generate_float();
	}

	float32 random_engine::generate_float_sync(float32 min, float32 max)
	{
		std::scoped_lock<spin_mutex> lock{ _engine_mutex };
		return generate_float(min, max);
	}

	void random_engine::seed_engine()
	{
		// TODO IDEA use better random_device and seed_seq (https://www.pcg-random.org/posts/cpp-seeding-surprises.html)
		std::random_device device;
		std::seed_seq seeder(_seed.begin(), std::generate_n(_seed.begin(), _seed.size(), std::ref(device)));

		_seed_and_check_engine(seeder);
	}

	void random_engine::reset_engine()
	{
		std::seed_seq seeder(_seed.begin(), _seed.end());

		_seed_and_check_engine(seeder);
	}

	random_engine_seed random_engine::get_seed() const
	{
		return random_engine_seed{ _seed };
	}

	void random_engine::set_seed(random_engine_seed const& seed)
	{
		_seed = _seed_type(seed);

		reset_engine();
	}

	void random_engine::_seed_and_check_engine(std::seed_seq& seeder)
	{
		_engine.seed(seeder);
		_float_distribution.reset();

		// seed_seq may generate an all-zero state, which will break mt19937
		// Since recreating and checking the seed_seq generation is slow,
		// let's not tempt the fates (however unlikely),
		// just crash now if the engine generates a zero

		if (_engine() == 0)
		{
			GAEA_FATAL(random_engine, "First number generated was a zero, crashing...");
		}
	}
}
