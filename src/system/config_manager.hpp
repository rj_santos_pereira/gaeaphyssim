#ifndef SYSTEM_CONFIG_MANAGER_INCLUDE_GUARD
#define SYSTEM_CONFIG_MANAGER_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/containers.hpp"

namespace gaea
{
	class config_manager
	{
		using _property_value_t
			= std::variant<
				std::monostate, // 0
				bool, uint64, std::string, // 1, 2, 3
				array<bool>, array<uint64>, array<std::string> // 4, 5, 6
			>;
	
	public:
		explicit config_manager();

		config_manager(config_manager const&) = delete;
		config_manager(config_manager&&) = delete;

		config_manager& operator=(config_manager const&) = delete;
		config_manager& operator=(config_manager&&) = delete;
	
		bool load_config(std::string const& config_file);
		bool save_config(std::string const& config_file);
	
		bool get_bool_property(std::string const& section, std::string const& key, bool& value) const;
		bool set_bool_property(std::string const& section, std::string const& key, bool const& value);

		bool get_int_property(std::string const& section, std::string const& key, int8& value) const;
		bool set_int_property(std::string const& section, std::string const& key, int8 const& value);

		bool get_int_property(std::string const& section, std::string const& key, int16& value) const;
		bool set_int_property(std::string const& section, std::string const& key, int16 const& value);

		bool get_int_property(std::string const& section, std::string const& key, int32& value) const;
		bool set_int_property(std::string const& section, std::string const& key, int32 const& value);

		bool get_int_property(std::string const& section, std::string const& key, int64& value) const;
		bool set_int_property(std::string const& section, std::string const& key, int64 const& value);

		bool get_int_property(std::string const& section, std::string const& key, uint8& value) const;
		bool set_int_property(std::string const& section, std::string const& key, uint8 const& value);

		bool get_int_property(std::string const& section, std::string const& key, uint16& value) const;
		bool set_int_property(std::string const& section, std::string const& key, uint16 const& value);

		bool get_int_property(std::string const& section, std::string const& key, uint32& value) const;
		bool set_int_property(std::string const& section, std::string const& key, uint32 const& value);

		bool get_int_property(std::string const& section, std::string const& key, uint64& value) const;
		bool set_int_property(std::string const& section, std::string const& key, uint64 const& value);
	
		bool get_string_property(std::string const& section, std::string const& key, std::string& value) const;
		bool set_string_property(std::string const& section, std::string const& key, std::string const& value);

		bool get_bool_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, bool& value) const;
		bool set_bool_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, bool const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int8& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int8 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int16& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int16 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int32& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int32 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int64& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, int64 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint8& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint8 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint16& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint16 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint32& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint32 const& value);

		bool get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint64& value) const;
		bool set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, uint64 const& value);

		bool get_string_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, std::string& value) const;
		bool set_string_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, std::string const& value);

		bool has_unsaved_properties() const;
		
	private:
		template <class PropertyType>
		bool _get_property(std::string const& section, std::string const& key, PropertyType& value) const;
		template <class PropertyType>
		bool _set_property(std::string const& section, std::string const& key, PropertyType const& value);

		// TODO adapt this to support multiple files (with unsaved flag per file)
		map<std::string, map<std::string, _property_value_t>> _registry;
		bool _unsaved = false;
	
	};

}

#endif
