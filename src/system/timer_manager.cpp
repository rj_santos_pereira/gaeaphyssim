#include "core/core_minimal.hpp"
#include "system/timer_manager.hpp"

namespace gaea
{
	timer_manager::timer_manager()
		: _timer_map{}
		, _time_offset{ 0 }
		, _time_scale{ 1.0 }
	{
	}
	
	timer_manager::~timer_manager()
	{
		for (auto timer : _timer_map)
		{
			delete timer;
		}
	}

	timer_handle* timer_manager::add_timer(std::function<void()> lambda, bool should_repeat,
										   stdchr::steady_clock::duration trigger_delta, 
										   stdchr::steady_clock::duration initial_delta)
	{
		if (trigger_delta < stdchr::steady_clock::duration::zero())
		{
			GAEA_WARN(timer_manager, "Trying to create timer with negative trigger delta!");
		}
		
		auto trigger_time = stdchr::steady_clock::now();
		auto first_delta = initial_delta >= stdchr::steady_clock::duration::zero() ? initial_delta : trigger_delta;
		auto next_delta = trigger_delta;
				
		auto timer = new implementation::timer{
			std::move(lambda),
			trigger_time,
			first_delta,
			next_delta,
			should_repeat,
			false
		};

		while (_timer_map.insert(timer).second)
		{
			timer->trigger_time += stdchr::nanoseconds(1);
		}

		return reinterpret_cast<timer_handle*>(timer);
	}

	void timer_manager::remove_timer(timer_handle* handle)
	{
		if (auto timer = reinterpret_cast<implementation::timer*>(handle))
		{
			timer->should_erase = true;
		}
	}

	void timer_manager::tick_timers()
	{
		auto it = _timer_map.begin();
		while (it != _timer_map.end())
		{
			auto timer = *it;
			
			if (stdchr::steady_clock::now() + stdchr::steady_clock::duration(_time_offset) < timer->trigger_time + timer->trigger_delta * _time_scale)
			{
				break;
			}

			if (timer->should_erase)
			{
				delete timer;
				it = _timer_map.erase(it);
				continue;
			}
			
			timer->lambda();
			
			if (!timer->should_repeat)
			{
				delete timer;
				it = _timer_map.erase(it);
				continue;
			}

			timer->trigger_delta = timer->next_delta;
			timer->trigger_time += timer->trigger_delta;
			
			it = _timer_map.erase(it);

			std::ptrdiff_t prev_distance = std::distance(_timer_map.begin(), it);
			bool inserted;
			do
			{
				timer->trigger_time += stdchr::nanoseconds(1);
				auto [next_it, has_inserted] = _timer_map.insert(timer);

				if (std::distance(_timer_map.begin(), next_it) <= prev_distance)
				{
					it = ++next_it;
				}
				
				inserted = has_inserted;
			}
			while (!inserted);
		}
	}

	void timer_manager::set_time_offset(int64 time_offset)
	{
		_time_offset = time_offset;
	}

	void timer_manager::set_time_scale(float64 time_scale)
	{
		_time_scale = time_scale;
	}
}
