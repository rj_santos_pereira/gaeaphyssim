#include "core/core_minimal.hpp"
#include "system/config_manager.hpp"

#include "library/utility.hpp"

// ReSharper disable CppClangTidyClangDiagnosticExitTimeDestructors

namespace gaea
{
	config_manager::config_manager()
	{
	}

	bool config_manager::load_config(std::string const& config_file)
	{
		static std::regex const section_regex{ R"regex(\[(\w+)\].*)regex" };
		static std::regex const property_regex{ R"regex((\w+)=(.*))regex" };
		static std::regex const array_property_regex{ R"regex((\+|-)(\w+)=(.*))regex" };

		// Clear current file (TODO adapt for multiple files)
		_registry.clear();
		_unsaved = false;

		stdfs::path config_path{ config_file };

		if (config_path.is_relative())
		{
			config_path = engine ? engine->compose_engine_path(config_path) : absolute(config_path);
		}

		if (!exists(config_path) || !is_regular_file(config_path))
		{
			GAEA_WARN(config_manager, "Could not find file with path %s", config_path.string().c_str());
			return false;
		}

		// Create file stream for reading
		std::fstream config_stream{ config_path, std::ios::in };
		std::string config_line;

		// We need to persist section across several lines
		std::string section;
		int32 line_counter = 0;

		while (std::getline(config_stream, config_line))
		{
			++line_counter;

			// Trim whitespace and then check if empty, skip if so, proceed otherwise
			if (util::trim_space(config_line).empty())
			{
				continue;
			}

			std::smatch section_match;
			if (std::regex_match(config_line.cbegin(), config_line.cend(), section_match, section_regex))
			{
				// First match will be the string, second match will be the first capture group
				section = section_match[1];
				// This line won't have anything else, continue
				continue;
			}

			switch (config_line.front())
			{
			case ';':
				// Comment line, ignore it
				continue;
			case '+':
			case '-':
				// TODO array line
				break;
			default:
				std::smatch property_match;
				if (!std::regex_match(config_line.cbegin(), config_line.cend(), property_match, property_regex))
				{
					GAEA_WARN(config_manager, "Found invalid data in line %d ('%s')", line_counter, config_line.c_str());
					continue;
				}

				std::string key = property_match[1];
				std::string value = util::trim_space(property_match[2]);

				bool bool_value;
				uint64 uint_value;

				if (util::to_boolean(value, bool_value))
				{
					_registry[section][key].emplace<bool>(bool_value);
				}
				else if (util::to_integer(value, uint_value))
				{
					_registry[section][key].emplace<uint64>(uint_value);
				}
				else
				{
					_registry[section][key].emplace<std::string>(std::move(value));
				}

				break;
			}
		}

		return true;
	}

	bool config_manager::save_config(std::string const& config_file)
	{
		stdfs::path config_path{ config_file };

		if (config_path.is_relative())
		{
			config_path = engine ? engine->compose_engine_path(config_path) : absolute(config_path);
		}

		// TODO different code paths for existing files

		// Create file stream for writing, clear existing content
		std::fstream config_stream{ config_path, std::ios::out | std::ios::trunc };

		for (auto const& section : _registry)
		{
			std::string section_string = "[" + section.first + "]\n\n";

			config_stream << section_string;

			for (auto const& property : section.second)
			{
				std::string property_string = property.first + "=";

				switch (property.second.index())
				{
				case 0:
					// This definitely should not happen
					GAEA_DEBUG_BREAK();
					continue;
				case 1:
					property_string += std::get<bool>(property.second) ? "true\n" : "false\n";
					break;
				case 2:
					property_string += std::to_string(std::get<uint64>(property.second)) + "\n";
					break;
				case 3:
					property_string += std::get<std::string>(property.second) + "\n";
					break;
				case 4:
				case 5:
				case 6:
					// TODO array lines
					break;
				}

				config_stream << property_string;
			}

			config_stream << "\n";
		}

		// Clear unsaved flag (TODO adapt for multiple files)
		_unsaved = false;

		return true;
	}

	bool config_manager::get_bool_property(std::string const& section, std::string const& key, bool& value) const
	{
		return _get_property<bool>(section, key, value);
	}

	bool config_manager::set_bool_property(std::string const& section, std::string const& key, bool const& value)
	{
		return _set_property<bool>(section, key, value);
	}

#define DEFINE_GET_INT_PROPERTY(IntType) \
	bool config_manager::get_int_property(std::string const& section, std::string const& key, IntType& value) const												\
	{																																							\
		uint64 new_value;																																		\
		bool has_value = _get_property<uint64>(section, key, new_value);																						\
		if (has_value)																																			\
		{																																						\
			value = IntType(new_value);																															\
		}																																						\
		return has_value;																																		\
	}

	DEFINE_GET_INT_PROPERTY(int8)
	DEFINE_GET_INT_PROPERTY(int16)
	DEFINE_GET_INT_PROPERTY(int32)
	DEFINE_GET_INT_PROPERTY(int64)
	DEFINE_GET_INT_PROPERTY(uint8)
	DEFINE_GET_INT_PROPERTY(uint16)
	DEFINE_GET_INT_PROPERTY(uint32)
	DEFINE_GET_INT_PROPERTY(uint64)

#undef DEFINE_GET_INT_PROPERTY

#define DEFINE_SET_INT_PROPERTY(IntType) \
	bool config_manager::set_int_property(std::string const& section, std::string const& key, IntType const& value)												\
	{																																							\
		auto new_value = uint64(value);																															\
		return _set_property<uint64>(section, key, new_value);																									\
	}

	DEFINE_SET_INT_PROPERTY(int8)
	DEFINE_SET_INT_PROPERTY(int16)
	DEFINE_SET_INT_PROPERTY(int32)
	DEFINE_SET_INT_PROPERTY(int64)
	DEFINE_SET_INT_PROPERTY(uint8)
	DEFINE_SET_INT_PROPERTY(uint16)
	DEFINE_SET_INT_PROPERTY(uint32)
	DEFINE_SET_INT_PROPERTY(uint64)

#undef DEFINE_SET_INT_PROPERTY

	bool config_manager::get_string_property(std::string const& section, std::string const& key, std::string& value) const
	{
		return _get_property<std::string>(section, key, value);
	}

	bool config_manager::set_string_property(std::string const& section, std::string const& key, std::string const& value)
	{
		return _set_property<std::string>(section, key, value);
	}

	bool config_manager::get_bool_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, bool& value) const
	{
		// TODO not implemented
		GAEA_UNUSED(section, key, index, value);
		GAEA_DEBUG_BREAK();
		return false;
	}

	bool config_manager::set_bool_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, bool const& value)
	{
		// TODO not implemented
		GAEA_UNUSED(section, key, index, value);
		GAEA_DEBUG_BREAK();
		return false;
	}

#define DEFINE_GET_INT_ARRAY_PROPERTY(IntType) \
	bool config_manager::get_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, IntType& value) const					\
	{																																							\
		/* TODO not implemented	*/																																\
		GAEA_UNUSED(section, key, index, value);																												\
		GAEA_DEBUG_BREAK();																																		\
		return false;																																			\
	}

	DEFINE_GET_INT_ARRAY_PROPERTY(int8)
	DEFINE_GET_INT_ARRAY_PROPERTY(int16)
	DEFINE_GET_INT_ARRAY_PROPERTY(int32)
	DEFINE_GET_INT_ARRAY_PROPERTY(int64)
	DEFINE_GET_INT_ARRAY_PROPERTY(uint8)
	DEFINE_GET_INT_ARRAY_PROPERTY(uint16)
	DEFINE_GET_INT_ARRAY_PROPERTY(uint32)
	DEFINE_GET_INT_ARRAY_PROPERTY(uint64)

#undef DEFINE_GET_INT_ARRAY_PROPERTY

#define DEFINE_SET_INT_ARRAY_PROPERTY(IntType) \
	bool config_manager::set_int_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, IntType const& value)					\
	{																																							\
		/* TODO not implemented */																																\
		GAEA_UNUSED(section, key, index, value);																												\
		GAEA_DEBUG_BREAK();																																		\
		return false;																																			\
	}

	DEFINE_SET_INT_ARRAY_PROPERTY(int8)
	DEFINE_SET_INT_ARRAY_PROPERTY(int16)
	DEFINE_SET_INT_ARRAY_PROPERTY(int32)
	DEFINE_SET_INT_ARRAY_PROPERTY(int64)
	DEFINE_SET_INT_ARRAY_PROPERTY(uint8)
	DEFINE_SET_INT_ARRAY_PROPERTY(uint16)
	DEFINE_SET_INT_ARRAY_PROPERTY(uint32)
	DEFINE_SET_INT_ARRAY_PROPERTY(uint64)

#undef DEFINE_SET_INT_ARRAY_PROPERTY

	bool config_manager::get_string_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, std::string& value) const
	{
		// TODO not implemented
		GAEA_UNUSED(section, key, index, value);
		GAEA_DEBUG_BREAK();
		return false;
	}

	bool config_manager::set_string_array_property(std::string const& section, std::string const& key, std::ptrdiff_t index, std::string const& value)
	{
		// TODO not implemented
		GAEA_UNUSED(section, key, index, value);
		GAEA_DEBUG_BREAK();
		return false;
	}

	bool config_manager::has_unsaved_properties() const
	{
		return _unsaved;
	}

	template <class PropertyType>
	bool config_manager::_get_property(std::string const& section, std::string const& key, PropertyType& value) const
	{
		if (auto section_it = _registry.find(section); section_it != _registry.end())
		{
			auto& section_registry = section_it->second;

			if (auto property_it = section_registry.find(key); property_it != section_registry.end())
			{
				auto& property = property_it->second;

				if (std::holds_alternative<PropertyType>(property))
				{
					value = std::get<PropertyType>(property);

					return true;
				}
			}
		}

		return false;
	}

	template bool config_manager::_get_property<bool>(std::string const& section, std::string const& key, bool& value) const;
	template bool config_manager::_get_property<uint64>(std::string const& section, std::string const& key, uint64& value) const;
	template bool config_manager::_get_property<std::string>(std::string const& section, std::string const& key, std::string& value) const;

	template <class PropertyType>
	bool config_manager::_set_property(std::string const& section, std::string const& key, PropertyType const& value)
	{
		auto& property = _registry[section][key];

		switch (property.index())
		{
		case 0:
		case 1:
		case 2:
		case 3:
			property.emplace<PropertyType>(value);

			_unsaved = true; // TODO adapt for multiple files
			return true;
		}

		return false;
	}

	template bool config_manager::_set_property<bool>(std::string const& section, std::string const& key, bool const& value);
	template bool config_manager::_set_property<uint64>(std::string const& section, std::string const& key, uint64 const& value);
	template bool config_manager::_set_property<std::string>(std::string const& section, std::string const& key, std::string const& value);

}
