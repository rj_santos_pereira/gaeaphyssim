#include "system/device_manager.hpp"

#include "library/utility.hpp"

#if GAEA_USING_SYCL

namespace gaea::implementation
{
	int custom_device_selector::operator()(sycl::device const& device) const
	{
		if (device.get_info<sycl::info::device::device_type>() == sycl::info::device_type::gpu)
		{
			int compute_units_score = int(device.get_info<sycl::info::device::max_compute_units>() * 1'000'000);

			// Transform from bytes to kilobytes
			int global_mem_score = int(device.get_info<sycl::info::device::global_mem_size>() / 1'000);
			int local_mem_score = int(device.get_info<sycl::info::device::local_mem_size>() / 1'000);

			if (compute_units_score < 0 || global_mem_score < 0 || local_mem_score < 0)
			{
				GAEA_FATAL(custom_device_selector, "Detected negative device score: cu=%d; gm=%d; lm=%d", compute_units_score, global_mem_score, local_mem_score);
			}

			return compute_units_score + global_mem_score + local_mem_score;
		}

		if (device.get_info<sycl::info::device::device_type>() == sycl::info::device_type::cpu)
		{
			return device.get_info<sycl::info::device::max_compute_units>();
		}

		if (device.get_info<sycl::info::device::device_type>() == sycl::info::device_type::host)
		{
			return 0;
		}

		return -1;
	}
}

namespace gaea
{
	device::device(sycl::device const & device)
		: _device{ device }
	{
	}

	sycl::string_class device::name() const
	{
		return _device.get_info<sycl::info::device::name>();
	}

	sycl::info::device_type device::type() const
	{
		return _device.get_info<sycl::info::device::device_type>();
	}

	int device::score() const
	{
		static implementation::custom_device_selector device_selector;
		return device_selector(_device);
	}

	sycl::cl_uint device::num_compute_units() const
	{
		return _device.get_info<sycl::info::device::max_compute_units>();
	}

	sycl::cl_uint device::num_constant_arguments() const
	{
		return _device.get_info<sycl::info::device::max_constant_args>();
	}

	sycl::cl_uint device::num_samplers() const
	{
		return _device.get_info<sycl::info::device::max_samplers>();
	}

	std::size_t device::parameter_size() const
	{
		return _device.get_info<sycl::info::device::max_parameter_size>();
	}

	std::size_t device::work_group_size() const
	{
		return _device.get_info<sycl::info::device::max_work_group_size>();
	}

	sycl::cl_uint device::work_item_dimensions() const
	{
		return _device.get_info<sycl::info::device::max_work_item_dimensions>();
	}

	sycl::id<3> device::work_item_sizes() const
	{
		return _device.get_info<sycl::info::device::max_work_item_sizes>();
	}

	sycl::cl_ulong device::constant_memory_size() const
	{
		return _device.get_info<sycl::info::device::max_constant_buffer_size>();
	}

	sycl::cl_ulong device::global_memory_size() const
	{
		return _device.get_info<sycl::info::device::global_mem_size>();
	}

	sycl::cl_ulong device::local_memory_size() const
	{
		return _device.get_info<sycl::info::device::local_mem_size>();
	}

	device::operator sycl::device() const
	{
		return _device;
	}

	std::string device::description() const
	{
		thread_local static std::ostringstream stream;
		stream.clear();
		
		stream.str(std::string{});
		stream << *this;
		return stream.str();
	}

	std::ostream& operator<<(std::ostream& stream, device const& properties)
	{
		return stream << std::boolalpha
			// Device characteristics
			<< "Device: " << properties._device.get_info<sycl::info::device::name>() << "\n"
				<< "\tCompute units: " << properties._device.get_info<sycl::info::device::max_compute_units>() << "\n"
				<< "\tClock frequency: " << properties._device.get_info<sycl::info::device::max_clock_frequency>() << "\n"
				// Kernel characteristics
				<< "\tConstant arguments number: " << properties._device.get_info<sycl::info::device::max_constant_args>() << "\n"
				<< "\tSamplers number: " << properties._device.get_info<sycl::info::device::max_samplers>() << "\n"
				<< "\tParameter size: " << properties._device.get_info<sycl::info::device::max_parameter_size>() << "\n"
				<< "\tWork group size: " << properties._device.get_info<sycl::info::device::max_work_group_size>() << "\n"
				<< "\tWork item dimensions: " << properties._device.get_info<sycl::info::device::max_work_item_dimensions>() << "\n"
				<< "\tWork item size: "
					<< [work_item_size = properties._device.get_info<sycl::info::device::max_work_item_sizes>()]
					{
						return "X=" + std::to_string(work_item_size[0])
							+ ",Y=" + std::to_string(work_item_size[1])
							+ ",Z=" + std::to_string(work_item_size[2])
							+ "\n";
					} ()
				// Processor characteristics
				<< "\tInt8 vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_char>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_char>() << "(preferred)\n"
				<< "\tInt16 vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_short>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_short>() << "(preferred)\n"
				<< "\tInt32 vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_int>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_int>() << "(preferred)\n"
				<< "\tInt64 vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_long>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_long>() << "(preferred)\n"
				<< "\tHFloat vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_half>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_half>() << "(preferred)\n"
				<< "\tHFloat configuration: "
					<< [hfp_config = properties._device.get_info<sycl::info::device::half_fp_config>()]
					{
						std::string config = hfp_config.empty() ? "\n\t\t----\n" : "\n";
						for (auto cfg : hfp_config)
						{
							switch (cfg)
							{
								case sycl::info::fp_config::denorm: config += "\t\tdenorm\n"; break;
								case sycl::info::fp_config::inf_nan: config += "\t\tinf_nan\n"; break;
								case sycl::info::fp_config::round_to_nearest: config += "\t\tround_to_nearest\n"; break;
								case sycl::info::fp_config::round_to_zero: config += "\t\tround_to_zero\n"; break;
								case sycl::info::fp_config::round_to_inf: config += "\t\tround_to_inf\n"; break;
								case sycl::info::fp_config::fma: config += "\t\tfma\n"; break;
								case sycl::info::fp_config::correctly_rounded_divide_sqrt: config += "\t\tcorrectly_rounded_divide_sqrt\n"; break;
								case sycl::info::fp_config::soft_float: config += "\t\tsoft_float\n"; break;
							}
						}
						return config;
					} ()
				<< "\tSFloat vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_float>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_float>() << "(preferred)\n"
				<< "\tSFloat configuration: "
					<< [sfp_config = properties._device.get_info<sycl::info::device::single_fp_config>()]
					{
						std::string config = sfp_config.empty() ? "\n\t\t----\n" : "\n";
						for (auto cfg : sfp_config)
						{
							switch (cfg)
							{
								case sycl::info::fp_config::denorm: config += "\t\tdenorm\n"; break;
								case sycl::info::fp_config::inf_nan: config += "\t\tinf_nan\n"; break;
								case sycl::info::fp_config::round_to_nearest: config += "\t\tround_to_nearest\n"; break;
								case sycl::info::fp_config::round_to_zero: config += "\t\tround_to_zero\n"; break;
								case sycl::info::fp_config::round_to_inf: config += "\t\tround_to_inf\n"; break;
								case sycl::info::fp_config::fma: config += "\t\tfma\n"; break;
								case sycl::info::fp_config::correctly_rounded_divide_sqrt: config += "\t\tcorrectly_rounded_divide_sqrt\n"; break;
								case sycl::info::fp_config::soft_float: config += "\t\tsoft_float\n"; break;
							}
						}
						return config;
					} ()
				<< "\tDFloat vector width: "
					<< properties._device.get_info<sycl::info::device::native_vector_width_float>() << "(native); "
					<< properties._device.get_info<sycl::info::device::preferred_vector_width_float>() << "(preferred)\n"
				<< "\tDFloat configuration: "
					<< [dfp_config = properties._device.get_info<sycl::info::device::double_fp_config>()]
					{
						std::string config = dfp_config.empty() ? "\n\t\t----\n" : "\n";
						for (auto cfg : dfp_config)
						{
							switch (cfg)
							{
								case sycl::info::fp_config::denorm: config += "\t\tdenorm\n"; break;
								case sycl::info::fp_config::inf_nan: config += "\t\tinf_nan\n"; break;
								case sycl::info::fp_config::round_to_nearest: config += "\t\tround_to_nearest\n"; break;
								case sycl::info::fp_config::round_to_zero: config += "\t\tround_to_zero\n"; break;
								case sycl::info::fp_config::round_to_inf: config += "\t\tround_to_inf\n"; break;
								case sycl::info::fp_config::fma: config += "\t\tfma\n"; break;
								case sycl::info::fp_config::correctly_rounded_divide_sqrt: config += "\t\tcorrectly_rounded_divide_sqrt\n"; break;
								case sycl::info::fp_config::soft_float: config += "\t\tsoft_float\n"; break;
							}
						}
						return config;
					} ()
				<< "\tPrintf buffer size: " << properties._device.get_info<sycl::info::device::printf_buffer_size>() << "\n"
				// Memory characteristics
				<< "\tUnified memory: " << properties._device.get_info<sycl::info::device::host_unified_memory>() << "\n"
				<< "\tNative interop sync: " << !properties._device.get_info<sycl::info::device::preferred_interop_user_sync>() << "\n"
				<< "\tImage support: " << properties._device.get_info<sycl::info::device::image_support>() << "\n"
				<< "\tMemory address bits: " << properties._device.get_info<sycl::info::device::address_bits>() << "\n"
				<< "\tMemory base address alignment: " << properties._device.get_info<sycl::info::device::mem_base_addr_align>() << "\n"
				<< "\tMemory max allocation size: " << properties._device.get_info<sycl::info::device::max_mem_alloc_size>() << "\n"
				<< "\tGlobal memory size: " << properties._device.get_info<sycl::info::device::global_mem_size>() << "\n"
				<< "\tGlobal memory cache type: "
					<< [cache_type = properties._device.get_info<sycl::info::device::global_mem_cache_type>()]
					{
						switch (cache_type)
						{
							default:
							case sycl::info::global_mem_cache_type::none: return "none\n";
							case sycl::info::global_mem_cache_type::read_only: return "read_only\n";
							case sycl::info::global_mem_cache_type::read_write: return "read_write\n";
						}
					} ()
				<< "\tGlobal memory cache size: " << properties._device.get_info<sycl::info::device::global_mem_cache_size>() << "\n"
				<< "\tGlobal memory cache line size: " << properties._device.get_info<sycl::info::device::global_mem_cache_line_size>() << "\n"
				<< "\tLocal memory type: "
					<< [mem_type = properties._device.get_info<sycl::info::device::local_mem_type>()]
					{
						switch (mem_type)
						{
							default:
							case sycl::info::local_mem_type::none: return "none\n";
							case sycl::info::local_mem_type::local: return "local\n";
							case sycl::info::local_mem_type::global: return "global\n";
						}
					} ()
				<< "\tLocal memory size: " << properties._device.get_info<sycl::info::device::local_mem_size>() << "\n"
				<< "\tConstant buffer size: " << properties._device.get_info<sycl::info::device::max_constant_buffer_size>() << "\n"
				<< "\tImage read arguments number: " << properties._device.get_info<sycl::info::device::max_read_image_args>() << "\n"
				<< "\tImage write arguments number: " << properties._device.get_info<sycl::info::device::max_write_image_args>() << "\n"
				<< "\tImage 2D width: " << properties._device.get_info<sycl::info::device::image2d_max_width>() << "\n"
				<< "\tImage 2D height: " << properties._device.get_info<sycl::info::device::image2d_max_height>() << "\n"
				<< "\tImage 3D width: " << properties._device.get_info<sycl::info::device::image3d_max_width>() << "\n"
				<< "\tImage 3D height: " << properties._device.get_info<sycl::info::device::image3d_max_height>() << "\n"
				<< "\tImage 3D depth: " << properties._device.get_info<sycl::info::device::image3d_max_depth>() << "\n"
				<< "\tImage buffer size: " << properties._device.get_info<sycl::info::device::image_max_buffer_size>() << "\n"
				<< "\tImage array size: " << properties._device.get_info<sycl::info::device::image_max_array_size>() << "\n"
				// Misc characteristics
				<< "\tTimer resolution: " << properties._device.get_info<sycl::info::device::profiling_timer_resolution>() << "\n"
				<< "\tExtensions: "
					<< [extensions_array = properties._device.get_info<sycl::info::device::extensions>()]
					{
						std::string extensions = extensions_array.empty() ? "\n\t\t----\n" : "\n";
						for (auto const& ext : extensions_array)
						{
							extensions += "\t\t" + ext + "\n";
						}
						return extensions;
					} ();
	}

	device_manager::device_manager()
	{
		static implementation::custom_device_selector device_selector;
		static sycl::host_selector host_selector;

		_host_device = device{ host_selector.select_device() };

		auto& reg_dev_array = _registry[sycl::info::device_type::all];
        auto dev_array = sycl::device::get_devices(sycl::info::device_type::all);

		reg_dev_array.resize(dev_array.size());
		std::transform(dev_array.begin(), dev_array.end(), reg_dev_array.begin(),
			[](sycl::device const& dev)
			{
				return device{ dev };
			});

		for (auto const& dev : dev_array)
		{
			auto new_dev = device{ dev };
			if (device_selector(dev) > 0)
			{
				auto& new_dev_array = _registry[new_dev.type()];

				auto it = std::lower_bound(
					new_dev_array.begin(), new_dev_array.end(), new_dev,
					[](sycl::device const& elem1, sycl::device const& elem2)
					{
						return device_selector(elem1) > device_selector(elem2);
					}
				);

				if (it == new_dev_array.end() || it->name() != new_dev.name())
				{
					new_dev_array.insert(it, std::move(new_dev));
				}
			}
		}
	}

	bool device_manager::select_device(device& dev, sycl::info::device_type type, device_capability preference) const
	{
		if (type == sycl::info::device_type::all)
		{
			GAEA_ERROR(device_manager, "Query with device_type == 'all' is not allowed!");
			return false;
		}
		
		if (type == sycl::info::device_type::host)
		{
			dev = _host_device;
			return true;
		}
		
		auto& dev_array = _registry[type];

		if (dev_array.size())
		{
			switch (preference)
			{
			case device_capability::any:
			case device_capability::precision:
				dev = dev_array.front();
				return true;
			case device_capability::performance:
				dev = dev_array.back();
				return true;
			}
		}

		return false;
	}

	bool device_manager::select_device(device& dev, std::string const& name, sycl::info::device_type hint) const
	{
		if (hint == sycl::info::device_type::all)
		{
			GAEA_ERROR(device_manager, "Query with device_type == 'all' is not allowed!");
			return false;
		}
		
		if (util::to_lowercase(name) == "host")
		{
			dev = _host_device;
			return true;
		}

		auto& hint_dev_array = _registry[hint];

		for (auto const& hint_dev : hint_dev_array)
		{
			if (name == hint_dev.name())
			{
				dev = hint_dev;
				return true;
			}
		}

		for (auto const& [_, reg_dev_array] : _registry)
		{
			for (auto const& reg_dev : reg_dev_array)
			{
				if (name == reg_dev.name())
				{
					dev = reg_dev;
					return true;
				}
			}
		}
		
		return false;
	}

	array<device> const& device_manager::list_devices(sycl::info::device_type type) const
	{
		return _registry[type];
	}
}

#endif
