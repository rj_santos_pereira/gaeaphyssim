#ifndef SYSTEM_INPUT_MANAGER_INCLUDE_GUARD
#define SYSTEM_INPUT_MANAGER_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/containers.hpp"

namespace gaea
{
	class input_manager
	{
	public:
		using event_key_button_callback_type = base_renderer::event_key_button_callback_type;
		using event_mouse_button_callback_type = base_renderer::event_mouse_button_callback_type;
		using event_mouse_drag_callback_type = base_renderer::event_mouse_drag_callback_type;
		using event_mouse_scroll_callback_type = base_renderer::event_mouse_scroll_callback_type;
		
		explicit input_manager();

		input_manager(input_manager const&) = delete;
		input_manager(input_manager&&) = delete;

		input_manager& operator=(input_manager const&) = delete;
		input_manager& operator=(input_manager&&) = delete;

		bool setup_handlers();
		void setdown_handlers();

		input_state query_key_state(input_key key) const;
		input_state query_mouse_state(input_mouse mouse) const;

		void bind_key_button(input_key key, void* object, event_key_button_callback_type&& callback);
		void bind_mouse_button(input_mouse mouse, void* object, event_mouse_button_callback_type&& callback);
		void bind_mouse_drag(void* object, event_mouse_drag_callback_type&& callback);
		void bind_mouse_scroll(void* object, event_mouse_scroll_callback_type&& callback);
		
		void unbind_key_button(input_key key, void* object);
		void unbind_mouse_button(input_mouse mouse, void* object);
		void unbind_mouse_drag(void* object);
		void unbind_mouse_scroll(void* object);

		bool handle_events();

		void set_repeat_delay(int64 repeat_delay);

		int64 get_repeat_delay() const;

	private:
		void _on_key_button_event(input_key key, input_state state, input_modifier modifier);
		void _on_mouse_button_event(input_mouse mouse, input_state state, input_modifier modifier);
		void _on_mouse_drag_event(float32 pos_x, float32 pos_y, float32 pos_delta_x, float32 pos_delta_y, input_modifier modifier);
		void _on_mouse_scroll_event(float32 delta_x, float32 delta_y, input_modifier modifier);
		
		map<input_key, map<void*, event_key_button_callback_type>> _key_button_callback_map;
		map<input_mouse, map<void*, event_mouse_button_callback_type>> _mouse_button_callback_map;
		map<void*, event_mouse_drag_callback_type> _mouse_drag_callback_map;
		map<void*, event_mouse_scroll_callback_type> _mouse_scroll_callback_map;

		mutable sorted_map<input_key, input_state> _key_state_map;
		mutable sorted_map<input_mouse, input_state> _mouse_state_map;

		input_modifier _last_modifier_state;

		stdchr::milliseconds _repeat_delay_duration;
		stdchr::steady_clock::time_point _next_repeat_time;

		uint8 _is_mouse_dragging : 1;
		uint8 _is_mouse_scrolling : 1;
		uint8 _has_mouse_drag_event : 1;
		uint8 _has_mouse_scroll_event : 1;
		
	};
}

#endif
