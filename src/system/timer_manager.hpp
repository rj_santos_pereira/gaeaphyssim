#ifndef SYSTEM_TIMER_MANAGER_INCLUDE_GUARD
#define SYSTEM_TIMER_MANAGER_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/containers.hpp"

namespace gaea::implementation
{
	struct timer
	{
		std::function<void()> lambda;

		stdchr::steady_clock::time_point trigger_time;
		stdchr::steady_clock::duration trigger_delta;
		stdchr::steady_clock::duration next_delta;

		uint8 should_repeat : 1;
		uint8 should_erase : 1;
	};

	struct timer_compare
	{
		bool operator()(timer* elem1, timer* elem2) const
		{
			return elem1->trigger_time + elem1->trigger_delta < elem2->trigger_time + elem2->trigger_delta;
		}
	};
}

namespace gaea
{
	struct timer_handle{};
	
	class timer_manager
	{
	public:
		explicit timer_manager();
		~timer_manager();

		timer_manager(timer_manager const&) = delete;
		timer_manager(timer_manager&&) = delete;

		timer_manager& operator=(timer_manager const&) = delete;
		timer_manager& operator=(timer_manager&&) = delete;
		
		timer_handle* add_timer(std::function<void()> lambda, bool should_repeat, 
								stdchr::steady_clock::duration trigger_delta,
								stdchr::steady_clock::duration initial_delta = stdchr::steady_clock::duration::min());

		void remove_timer(timer_handle* handle);

		void tick_timers();

		void set_time_offset(int64 time_offset);

		void set_time_scale(float64 time_scale);
		
	private:
		array_set<implementation::timer*, implementation::timer_compare> _timer_map;
		int64 _time_offset;
		float64 _time_scale;
		
	};
}

#endif
