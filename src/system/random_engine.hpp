#ifndef SYSTEM_RANDOM_ENGINE_INCLUDE_GUARD
#define SYSTEM_RANDOM_ENGINE_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/containers.hpp"
#include "library/mutex.hpp"

namespace gaea::implementation
{
	struct random_engine_detail
	{
		using engine_type = std::mt19937;

		using seed_element_type = std::mt19937::result_type;
		static constexpr auto seed_element_number =
			std::mt19937::state_size * (std::mt19937::word_size / 32 + std::size_t(bool(std::mt19937::word_size % 32)));

		using seed_type = fixed_array<seed_element_type, seed_element_number>;

		// Based on: https://www.pcg-random.org/posts/bounded-rands.html
		static uint32 bound_generate(engine_type& generator, uint32 range);
	};
}

namespace gaea
{
	class random_engine_seed
	{
		using _seed_type = implementation::random_engine_detail::seed_type;

		explicit random_engine_seed() = default;
		explicit random_engine_seed(_seed_type const& seed);

		explicit operator _seed_type const& () const;

	public:
		bool is_null() const;

		static random_engine_seed null();

		static std::string serialize(random_engine_seed const& seed);

		static random_engine_seed deserialize(std::string const& data);

	private:
		_seed_type _seed{};

		friend class random_engine;

	};

	class random_engine
	{
		using _engine_type = implementation::random_engine_detail::engine_type;
		using _seed_type = implementation::random_engine_detail::seed_type;

	public:
		explicit random_engine();
		explicit random_engine(skip_init_tag);

		random_engine(random_engine const&) = delete;
		random_engine(random_engine&&) = delete;

		random_engine& operator=(random_engine const&) = delete;
		random_engine& operator=(random_engine&&) = delete;
		
		bool generate_bool();

		int32 generate_int();
		int32 generate_int(int32 min, int32 max);

		float32 generate_float();
		float32 generate_float(float32 min, float32 max);

		bool generate_bool_sync();

		int32 generate_int_sync();
		int32 generate_int_sync(int32 min, int32 max);

		float32 generate_float_sync();
		float32 generate_float_sync(float32 min, float32 max);

		void seed_engine();

		void reset_engine();

		random_engine_seed get_seed() const;

		void set_seed(random_engine_seed const& seed);

	private:
		void _seed_and_check_engine(std::seed_seq& seeder);

		_seed_type _seed{};
		_engine_type _engine;

		// TODO FIXME improve float gen performance: distributions are slow
		// TODO IDEA maybe use xoroshiro+ variant together with bound_generate?
		std::uniform_real_distribution<float32> _float_distribution;

		spin_mutex _engine_mutex;

	};
}

#endif
