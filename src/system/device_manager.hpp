#ifndef SYSTEM_DEVICE_MANAGER_INCLUDE_GUARD
#define SYSTEM_DEVICE_MANAGER_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "library/containers.hpp"

#if GAEA_USING_SYCL

namespace gaea::implementation
{
	class custom_device_selector : public sycl::device_selector
	{
	public:
		virtual int operator()(sycl::device const& device) const override;

	};
}

#endif

namespace gaea
{
	enum class device_capability
	{
		// Prioritizes device with highest compute capability
		precision,
		// Prioritizes device with lowest compute capability
		performance,
		// Priority is implementation-defined
		any,
	};

	class device
	{
	public:
		explicit device() = default;
#if GAEA_USING_SYCL
		explicit device(sycl::device const& device);

		sycl::string_class name() const;

		sycl::info::device_type type() const;

		int score() const;

		sycl::cl_uint num_compute_units() const;

		sycl::cl_uint num_constant_arguments() const;

		sycl::cl_uint num_samplers() const;

		std::size_t parameter_size() const;

		std::size_t work_group_size() const;

		sycl::cl_uint work_item_dimensions() const;

		sycl::id<3> work_item_sizes() const;

		sycl::cl_ulong constant_memory_size() const;

		sycl::cl_ulong global_memory_size() const;

		sycl::cl_ulong local_memory_size() const;

		std::string description() const;
		
		operator sycl::device() const;

		friend std::ostream& operator<<(std::ostream& stream, device const& properties);

	private:
		sycl::device _device;
#endif

	};

	class device_manager
	{
	public:
#if GAEA_USING_SYCL
        explicit device_manager();
#else
        explicit device_manager() = default;
#endif

		device_manager(device_manager const&) = delete;
		device_manager(device_manager&&) = delete;

		device_manager& operator=(device_manager const&) = delete;
		device_manager& operator=(device_manager&&) = delete;

#if GAEA_USING_SYCL
		bool select_device(device& dev, sycl::info::device_type type, device_capability preference = device_capability::any) const;
		bool select_device(device& dev, std::string const& name, sycl::info::device_type hint = sycl::info::device_type::gpu) const;

		array<device> const& list_devices(sycl::info::device_type type) const;

	private:
		mutable map<sycl::info::device_type, array<device>> _registry;
		device _host_device;
#endif
	};
}

#endif
