#ifndef SYSTEM_LOG_MANAGER_INCLUDE_GUARD
#define SYSTEM_LOG_MANAGER_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/containers.hpp"
#include "library/mutex.hpp"

namespace gaea::implementation
{
	class scope_log_stream final
	{
	public:
		scope_log_stream(spin_mutex& mutex, std::ostream& stream);

		~scope_log_stream() noexcept;

		std::ostream& operator<<(std::string const& str) const;

	private:
		spin_mutex& _stream_mutex;
		std::ostream& _stream;
	};

	class log_stream
	{
	protected:
		log_stream() = default;
		
	public:
		log_stream(std::ostream& stream, log_type type);

		log_stream(log_stream const& other);
		log_stream(log_stream&& other) noexcept;
		
		log_stream& operator=(log_stream const&) = delete;
		log_stream& operator=(log_stream&&) noexcept = delete;

		scope_log_stream operator()() const;

		bool should_log(log_type type) const;

	protected:
		void _init(std::ostream& stream, log_type type);

		mutable spin_mutex _stream_mutex;
		std::ostream* _stream;
		uint8 _stream_type;

	};

	class custom_log_stream : protected log_stream
	{
	public:
		custom_log_stream(std::string const& path, uint8 id);

		custom_log_stream(custom_log_stream const&) = delete;
		custom_log_stream(custom_log_stream&& other) noexcept;

		~custom_log_stream();

		using log_stream::operator();

	private:
		static std::fstream _create_file(std::string const& path);

		std::fstream _file_stream;
		
	};
}

namespace gaea
{
	class log_manager
	{
	public:
		explicit log_manager();

		log_manager(log_manager const&) = delete;
		log_manager(log_manager &&) = delete;

		log_manager& operator=(log_manager const&) = delete;
		log_manager& operator=(log_manager &&) = delete;

		void register_device_stream(std::ostream& stream, log_type type);

		void register_file_stream(std::ofstream& stream, log_type type);

		uint8 create_file_stream(std::string const& path);

		void destroy_file_stream(uint8 id);

		void log(uint8 id, std::string&& message, stdchr::system_clock::time_point&& time = stdchr::system_clock::time_point::min());

	private:
		array<implementation::log_stream> _log_stream_array;
		map<uint8, implementation::custom_log_stream> _custom_log_stream_map;
		stack<uint8> _custom_id_stack;

	};
}

#endif
