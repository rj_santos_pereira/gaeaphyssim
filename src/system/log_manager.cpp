#include "core/core_minimal.hpp"
#include "system/log_manager.hpp"

#include "library/utility.hpp"

namespace gaea::implementation
{
	scope_log_stream::scope_log_stream(spin_mutex& mutex, std::ostream& stream)
		: _stream_mutex{ mutex }
		, _stream{ stream }
	{
		_stream_mutex.lock();
	}

	scope_log_stream::~scope_log_stream() noexcept
	{
		_stream_mutex.unlock();
	}

	std::ostream& scope_log_stream::operator<<(std::string const& str) const
	{
		return _stream << str;
	}

	log_stream::log_stream(std::ostream& stream, log_type type)
		: _stream_mutex{}
		, _stream{ &stream }
		, _stream_type{ uint8(type) }
	{
	}

	log_stream::log_stream(log_stream const& other)
		: _stream_mutex{}
		, _stream{ other._stream }
		, _stream_type{ other._stream_type }
	{
	}

	log_stream::log_stream(log_stream&& other) noexcept
		: _stream_mutex{}
		, _stream{ other._stream }
		, _stream_type{ other._stream_type }
	{
		other._stream = nullptr;
		other._stream_type = uint8(log_type::null);
	}

	scope_log_stream log_stream::operator()() const
	{
		return scope_log_stream{ _stream_mutex, *_stream };
	}

	bool log_stream::should_log(log_type type) const
	{
		return uint8(type) >= _stream_type;
	}

	void log_stream::_init(std::ostream& stream, log_type type)
	{
		if (_stream != nullptr)
		{
			GAEA_DEBUG_BREAK_CRASH("Invalid invocation on already-init stream object");
		}

		_stream = &stream;
		_stream_type = uint8(type);
	}

	custom_log_stream::custom_log_stream(std::string const& path, uint8 id)
		: log_stream{}
		, _file_stream{ _create_file(path) }
	{
		if (_file_stream.is_open())
		{
			_init(_file_stream, log_type(id));

			//auto time = util::system_time_to_string("[%d/%m/%y - %H:%M:%S]", stdchr::system_clock::now());
			//_file_stream <<
			//	"\n"
			//	"//////////////////////////////////////////////////\n"
			//	"/// === BEGIN LOG ===\n"
			//	"/// " << time << "\n\n"
			//	<< std::flush;
		}
	}

	custom_log_stream::custom_log_stream(custom_log_stream&& other) noexcept
		: log_stream{ std::move(other) }
		, _file_stream{ std::move(other._file_stream) }
	{
		_stream = &_file_stream;
	}

	custom_log_stream::~custom_log_stream()
	{
		if (_file_stream.is_open())
		{
			//auto time = util::system_time_to_string("[%d/%m/%y - %H:%M:%S]", stdchr::system_clock::now());
			//_file_stream <<
			//	"\n"
			//	"/// === END LOG ===\n"
			//	"/// " << time << "\n"
			//	"//////////////////////////////////////////////////\n"
			//	<< std::flush;
			_file_stream << std::flush;
		}
	}

	std::fstream custom_log_stream::_create_file(std::string const& path)
	{
		stdfs::path file_path{ path };

		if (file_path.is_relative())
		{
			file_path = engine ? engine->compose_engine_path(file_path) : absolute(file_path);
		}
		
		if (!util::create_directory_tree(file_path.parent_path()))
		{
			GAEA_DEBUG_BREAK_CRASH("Could not create file path!");
		}

		if (!exists(file_path))
		{
            // Create file and immediately close it
			std::fstream NewFile{ path, std::ios::out };

            static_cast<void>(NewFile);
		}
		
		return std::fstream{ path, std::ios::in | std::ios::out | std::ios::ate };
	}
}

namespace gaea
{
	log_manager::log_manager()
		: _log_stream_array{}
		, _custom_log_stream_map{}
		, _custom_id_stack{}
	{
		for (uint8 id = uint8(log_type::null) - 1; id >= uint8(log_type::custom); --id)
		{
			_custom_id_stack.push(id);
		}
	}

	void log_manager::register_device_stream(std::ostream& stream, log_type type)
	{
		_log_stream_array.emplace_back(stream, type);
	}

	void log_manager::register_file_stream(std::ofstream& stream, log_type type)
	{
		_log_stream_array.emplace_back(stream, type);
	}

	uint8 log_manager::create_file_stream(std::string const& path)
	{
		if (_custom_id_stack.empty())
		{
			GAEA_ERROR(log_manager, "Cannot create file stream, no free ids!");
			return uint8(log_type::null);
		}

		uint8 id = _custom_id_stack.top();
		_custom_id_stack.pop();

		_custom_log_stream_map.try_emplace(id, path, id);
		return id;
	}

	void log_manager::destroy_file_stream(uint8 id)
	{
		auto it = _custom_log_stream_map.find(id);
		if (it != _custom_log_stream_map.end())
		{
			_custom_log_stream_map.erase(it);
			_custom_id_stack.push(id);
		}
	}

	void log_manager::log(uint8 id, std::string&& message, stdchr::system_clock::time_point&& time)
	{
		std::string log_message;
		log_message.reserve(32 + message.size());

		bool is_custom_log = false;
		switch (log_type(id))
		{
		case log_type::null:
			return;
		case log_type::debug:
			log_message.append("[DEBUG]");
			break;
		case log_type::info:
			log_message.append("[INFO]");
			break;
		case log_type::warn:
			log_message.append("[WARN]");
			break;
		case log_type::error:
			log_message.append("[ERROR]");
			break;
		case log_type::fatal:
			log_message.append("[FATAL]");
			break;
		default:
			is_custom_log = id >= uint8(log_type::custom);
			break;
		}

		if (!is_custom_log)
		{
			if (time != stdchr::system_clock::time_point::min())
			{
				log_message.append(util::system_time_to_string("(%d/%m/%y@%H:%M:%S)", time));
			}

			log_message.append(message).append("\n");

			std::for_each(_log_stream_array.begin(), _log_stream_array.end(),
				[message = std::move(log_message), type = log_type(id)](implementation::log_stream const& stream)
				{
					if (stream.should_log(type))
					{
						stream() << message << std::flush;
					}
				});
		}
		else
		{
			auto it = _custom_log_stream_map.find(id);
			if (it != _custom_log_stream_map.end())
			{
				if (time != stdchr::system_clock::time_point::min())
				{
					log_message.append(util::system_time_to_string("[%d/%m/%y - %H:%M:%S]\n", time));
				}

				log_message.append(message).append("\n");

				it->second() << log_message << std::flush;
			}
		}
	}
}
