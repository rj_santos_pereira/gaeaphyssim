#include "core/core_minimal.hpp"
#include "system/input_manager.hpp"

#include "engine/graphics/glfw_renderer.hpp"

namespace gaea
{
	input_manager::input_manager()
		: _key_button_callback_map{}
		, _mouse_button_callback_map{}
		, _mouse_drag_callback_map{}
		, _mouse_scroll_callback_map{}
		, _key_state_map{}
		, _mouse_state_map{}
		, _last_modifier_state{ input_modifier::modifier_NONE }
		, _repeat_delay_duration{ 0 }
		, _next_repeat_time{ stdchr::steady_clock::duration::zero() }
		, _is_mouse_dragging{ false }
		, _is_mouse_scrolling{ false }
		, _has_mouse_drag_event{ false }
		, _has_mouse_scroll_event{ false }
	{
#if 0
		std::size_t num_keys = uint8(input_key::key_UNKNOWN);

		_key_state_map.reserve(num_keys);

		for (std::size_t index = 0; index < num_keys; ++index)
		{
			_key_state_map[input_key(index)] = input_state::state_UNKNOWN;
		}

		std::size_t num_mouse = uint8(input_mouse::mouse_UNKNOWN);

		_mouse_state_map.reserve(num_mouse);

		for (std::size_t index = 0; index < num_mouse; ++index)
		{
			_mouse_state_map[input_mouse(index)] = input_state::state_UNKNOWN;
		}
#endif
	}

	bool input_manager::setup_handlers()
	{
		if (!renderer)
		{
			GAEA_ERROR(input_manager, "Trying to setup events using GLFW but global renderer accessor is not initialized!");
			return false;
		}
		
		renderer->register_event_key_button_callback(
			std::bind(&input_manager::_on_key_button_event, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) // NOLINT(modernize-avoid-bind)
		);
		renderer->register_event_mouse_button_callback(
			std::bind(&input_manager::_on_mouse_button_event, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) // NOLINT(modernize-avoid-bind)
		);
		renderer->register_event_mouse_drag_callback(
			std::bind(&input_manager::_on_mouse_drag_event, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5) // NOLINT(modernize-avoid-bind)
		);
		renderer->register_event_mouse_scroll_callback(
			std::bind(&input_manager::_on_mouse_scroll_event, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) // NOLINT(modernize-avoid-bind)
		);

		return true;
	}

	void input_manager::setdown_handlers()
	{
	}

    input_state input_manager::query_key_state(input_key key) const
    {
		return _key_state_map[key];
    }

    input_state input_manager::query_mouse_state(input_mouse mouse) const
    {
		return _mouse_state_map[mouse];
    }

    void input_manager::bind_key_button(input_key key, void* object, event_key_button_callback_type&& callback)
	{
		auto& callback_map = _key_button_callback_map[key];

		callback_map.insert_or_assign(object, std::move(callback));
	}

	void input_manager::bind_mouse_button(input_mouse mouse, void* object, event_mouse_button_callback_type&& callback)
	{
		auto& callback_map = _mouse_button_callback_map[mouse];

		callback_map.insert_or_assign(object, std::move(callback));
	}

	void input_manager::bind_mouse_drag(void* object, event_mouse_drag_callback_type&& callback)
	{
		_mouse_drag_callback_map.insert_or_assign(object, std::move(callback));
	}

	void input_manager::bind_mouse_scroll(void* object, event_mouse_scroll_callback_type&& callback)
	{
		_mouse_scroll_callback_map.insert_or_assign(object, std::move(callback));
	}

	void input_manager::unbind_key_button(input_key key, void* object)
	{
		auto& callback_map = _key_button_callback_map[key];

		callback_map.erase(object);
	}

	void input_manager::unbind_mouse_button(input_mouse mouse, void* object)
	{
		auto& callback_map = _mouse_button_callback_map[mouse];

		callback_map.erase(object);
	}

	void input_manager::unbind_mouse_drag(void* object)
	{
		_mouse_drag_callback_map.erase(object);
	}

	void input_manager::unbind_mouse_scroll(void* object)
	{
		_mouse_scroll_callback_map.erase(object);
	}

	bool input_manager::handle_events()
	{
		if (auto custom_renderer = static_cast<glfw_renderer*>(renderer))
		{
			bool handled_repeating = false;
			auto handled_time = stdchr::steady_clock::now();

			for (auto key_it = _key_state_map.begin(), key_it_end = _key_state_map.end();
				key_it != key_it_end;
				++key_it)
			{
				switch (key_it->second)
				{
				case input_state::state_pressed:
				{
					key_it->second = input_state::state_repeating;

					handled_repeating = true;
					break;
				}
				case input_state::state_repeating:
				{
					if (handled_time > _next_repeat_time)
					{
						auto& callback_map = _key_button_callback_map[key_it->first];

						std::for_each(callback_map.begin(), callback_map.end(),
							[this, key_it](auto&& callback)
							{
								callback.second(key_it->first, key_it->second, _last_modifier_state);
							});

						handled_repeating = true;
						break;
					}
				}
				}
			}

			for (auto mouse_it = _mouse_state_map.begin(), mouse_it_end = _mouse_state_map.end();
				mouse_it != mouse_it_end;
				++mouse_it)
			{
				switch (mouse_it->second)
				{
				case input_state::state_pressed:
				{
					mouse_it->second = input_state::state_repeating;

					handled_repeating = true;
					break;
				}
				case input_state::state_repeating:
				{
					if (handled_time > _next_repeat_time)
					{
						auto& callback_map = _mouse_button_callback_map[mouse_it->first];

						std::for_each(callback_map.begin(), callback_map.end(),
							[this, mouse_it](auto&& callback)
							{
								callback.second(mouse_it->first, mouse_it->second, _last_modifier_state);
							});

						handled_repeating = true;
						break;
					}
				}
				}
			}

			if (handled_repeating)
			{
				_next_repeat_time = handled_time + _repeat_delay_duration;
			}

			_has_mouse_drag_event = false;
			_has_mouse_scroll_event = false;

			custom_renderer->handle_events();

			bool was_mouse_dragging = _is_mouse_dragging;
			bool was_mouse_scrolling = _is_mouse_scrolling;

			_is_mouse_dragging = _has_mouse_drag_event;
			_is_mouse_scrolling = _has_mouse_scroll_event;

			if (was_mouse_dragging && !_is_mouse_dragging)
			{
				// Send zero-drag event to indicate drag stop
				_on_mouse_drag_event(0.f, 0.f, 0.f, 0.f, input_modifier::modifier_UNKNOWN);
				_is_mouse_dragging = false;
			}
			if (was_mouse_scrolling && !_is_mouse_scrolling)
			{
				// Send zero-scroll event to indicate scroll stop
				_on_mouse_scroll_event(0.f, 0.f, input_modifier::modifier_UNKNOWN);
				_is_mouse_scrolling = false;
			}

			return true;
		}

		return false;
	}

	void input_manager::set_repeat_delay(int64 repeat_delay)
	{
		_repeat_delay_duration = stdchr::milliseconds(repeat_delay);
	}

	int64 input_manager::get_repeat_delay() const
	{
		return _repeat_delay_duration.count();
	}

	void input_manager::_on_key_button_event(input_key key, input_state state, input_modifier modifier)
	{
		if (key != input_key::key_UNKNOWN)
		{
			auto& callback_map = _key_button_callback_map[key];

			_key_state_map[key] = state;

			_last_modifier_state = modifier;

			if (state != input_state::state_repeating)
			{
				std::for_each(callback_map.begin(), callback_map.end(),
					[key, state, modifier](auto&& callback_pair)
					{
						callback_pair.second(key, state, modifier);
					});
			}
		}
	}

	void input_manager::_on_mouse_button_event(input_mouse mouse, input_state state, input_modifier modifier)
	{
		if (mouse != input_mouse::mouse_UNKNOWN)
		{
			auto& callback_map = _mouse_button_callback_map[mouse];

			_mouse_state_map[mouse] = state;

			_last_modifier_state = modifier;

			if (state != input_state::state_repeating)
			{
				std::for_each(callback_map.begin(), callback_map.end(),
					[mouse, state, modifier](auto&& callback_pair)
					{
						callback_pair.second(mouse, state, modifier);
					});
			}
		}
	}

	void input_manager::_on_mouse_drag_event(float32 pos_x, float32 pos_y, float32 pos_delta_x, float32 pos_delta_y, input_modifier modifier)
	{
		_has_mouse_drag_event = true;

		_last_modifier_state = modifier;

		std::for_each(_mouse_drag_callback_map.begin(), _mouse_drag_callback_map.end(), 
			[pos_x, pos_y, pos_delta_x, pos_delta_y, modifier](auto&& callback_pair)
			{
			    callback_pair.second(pos_x, pos_y, pos_delta_x, pos_delta_y, modifier);
			});
	}

	void input_manager::_on_mouse_scroll_event(float32 delta_x, float32 delta_y, input_modifier modifier)
	{
		_has_mouse_scroll_event = true;

		_last_modifier_state = modifier;

		std::for_each(_mouse_scroll_callback_map.begin(), _mouse_scroll_callback_map.end(),
			[delta_x, delta_y, modifier](auto&& callback_pair)
			{
			    callback_pair.second(delta_x, delta_y, modifier);
			});
	}
}
