#include "core/core_minimal.hpp"

#include "engine/engine_config.hpp"
#include "engine/gaea_engine.hpp"

namespace
{	
	void print_debugger_message(const char* message)
	{
		if (IsDebuggerPresent())
		{
			OutputDebugStringA(message);
            std::fwrite(message, 1, std::strlen(message), stdout);
		}
	}
	
	// Adapted from https://stackoverflow.com/a/25927081

	std::FILE* reopen_stdio_streams(HANDLE win_handle, std::FILE* std_file, const char* mode)
	{
		// Re-initialize the C runtime "FILE" handles with clean handles bound to "nul". We do this because it has been
		// observed that the file number of our standard handle file objects can be assigned internally to a value of -2
		// when not bound to a valid target, which represents some kind of unknown internal invalid state. In this state our
		// call to "_dup2" fails, as it specifically tests to ensure that the target file number isn't equal to this value
		// before allowing the operation to continue. We can resolve this issue by first "re-opening" the target files to
		// use the "nul" device, which will place them into a valid state, after which we can redirect them to our target
		// using the "_dup2" function.

		std::FILE* nul_std_file;
		freopen_s(&nul_std_file, "nul", mode, std_file);

		// We need to duplicate the handle so that _open_osfhandle doesn't make the original one invalid,
		// causing problems on shutdown when the CRT tries to destroy them.
		HANDLE dup_win_handle = INVALID_HANDLE_VALUE;
		DuplicateHandle(GetCurrentProcess(), win_handle, GetCurrentProcess(), &dup_win_handle, 0, FALSE, DUPLICATE_SAME_ACCESS);
		if (dup_win_handle == INVALID_HANDLE_VALUE)
		{
			return nullptr;
		}

		// See https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/open-osfhandle?view=msvc-160
		int win_fd = _open_osfhandle(reinterpret_cast<std::intptr_t>(dup_win_handle), _O_TEXT);
		if (win_fd == -1)
		{
			return nullptr;
		}

		std::FILE* win_file = _fdopen(win_fd, mode);
		if (win_file == nullptr)
		{
			// If _fdopen fails, we need to close the file using the descriptor
			_close(win_fd);
			return nullptr;
		}

		if (_dup2(_fileno(win_file), _fileno(std_file)) == 0)
		{
			if (setvbuf(std_file, nullptr, _IOFBF, BUFSIZ) == 0)
			{
				return win_file;
			}
		}

		// Otherwise, if _dup2 or setvbuf fails, we need to close the file using the pointer
		fclose(win_file);
		return nullptr;
	}

	void close_stdio_streams(std::FILE* win_file, std::FILE* std_file, const char* mode)
	{
		if (win_file)
		{
			fclose(win_file);
		}

		std::FILE* nul_std_file;
		freopen_s(&nul_std_file, "nul", mode, std_file);
	}
}

namespace
{
	int seh_handler(unsigned long code, struct _EXCEPTION_POINTERS* info)
	{
		char buffer[128];

		std::string code_str;
		switch (code)
		{
#define MAKE_EXCEPTION_CASE(desc) case desc: code_str = #desc; break;
			MAKE_EXCEPTION_CASE(EXCEPTION_ACCESS_VIOLATION)
			MAKE_EXCEPTION_CASE(EXCEPTION_DATATYPE_MISALIGNMENT)
			MAKE_EXCEPTION_CASE(EXCEPTION_BREAKPOINT)
			MAKE_EXCEPTION_CASE(EXCEPTION_SINGLE_STEP)
			MAKE_EXCEPTION_CASE(EXCEPTION_ARRAY_BOUNDS_EXCEEDED)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_DENORMAL_OPERAND)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_DIVIDE_BY_ZERO)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_INEXACT_RESULT)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_INVALID_OPERATION)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_OVERFLOW)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_STACK_CHECK)
			MAKE_EXCEPTION_CASE(EXCEPTION_FLT_UNDERFLOW)
			MAKE_EXCEPTION_CASE(EXCEPTION_INT_DIVIDE_BY_ZERO)
			MAKE_EXCEPTION_CASE(EXCEPTION_INT_OVERFLOW)
			MAKE_EXCEPTION_CASE(EXCEPTION_PRIV_INSTRUCTION)
			MAKE_EXCEPTION_CASE(EXCEPTION_IN_PAGE_ERROR)
			MAKE_EXCEPTION_CASE(EXCEPTION_ILLEGAL_INSTRUCTION)
			MAKE_EXCEPTION_CASE(EXCEPTION_NONCONTINUABLE_EXCEPTION)
			MAKE_EXCEPTION_CASE(EXCEPTION_STACK_OVERFLOW)
			MAKE_EXCEPTION_CASE(EXCEPTION_INVALID_DISPOSITION)
			MAKE_EXCEPTION_CASE(EXCEPTION_GUARD_PAGE)
			MAKE_EXCEPTION_CASE(EXCEPTION_INVALID_HANDLE)
			//MAKE_EXCEPTION_CASE(EXCEPTION_POSSIBLE_DEADLOCK)
#undef MAKE_EXCEPTION_CASE
		default: code_str = "UNKNOWN_EXCEPTION"; break;
		}

		int bytes = snprintf(buffer, 128, "Exception code: 0x%08lx (%s)\n", code, code_str.c_str());
		if (bytes > 0 && bytes < 128)
		{
			OutputDebugStringA(buffer);
		}

		// TODO print info
		GAEA_UNUSED(info);

		return EXCEPTION_EXECUTE_HANDLER;
	}

	bool seh_initialize(void* parameters, unsigned long* exit_code)
	{
		bool should_run;

		__try
		{
			should_run = gaea::engine->initialize(parameters);
		}
		__except (seh_handler(GetExceptionCode(), GetExceptionInformation()))
		{
			*exit_code = GetExceptionCode();
			should_run = false;
		}

		return should_run;
	}

	void seh_run(unsigned long* exit_code)
	{
		__try
		{
			gaea::engine->run();
		}
		__except (seh_handler(GetExceptionCode(), GetExceptionInformation()))
		{
			*exit_code = GetExceptionCode();
		}
	}
	
	void seh_finalize(unsigned long* exit_code)
	{
		__try
		{
			gaea::engine->finalize();
		}
		__except (seh_handler(GetExceptionCode(), GetExceptionInformation()))
		{
			if (*exit_code == 0)
			{
				*exit_code = GetExceptionCode();
			}
		}
	}
	
	void seh_main(void* parameters)
	{
		unsigned long exit_code = 0;

		auto engine_ptr = std::make_unique<gaea::gaea_engine>();
		gaea::engine = engine_ptr.get();

		bool should_run = false;
		
		try
		{
			should_run = seh_initialize(parameters, &exit_code);
		}
		catch (std::exception const& ex)
		{
			print_debugger_message(std::string{ ex.what() }.append("\n").c_str());
			print_debugger_message("\nstd exception detected! Trying to finalize engine...\n");
		}
#if GAEA_USING_SYCL
		catch (sycl::exception const& ex)
		{
			print_debugger_message(std::string{ ex.what() }.append("\n").c_str());
			print_debugger_message("\nSYCL exception detected! Trying to finalize engine...\n");
		}
#endif

		if (should_run)
		{
			try
			{
				seh_run(&exit_code);
			}
			catch (std::exception const& ex)
			{
				print_debugger_message(std::string{ ex.what() }.append("\n").c_str());
				print_debugger_message("\nstd exception detected! Trying to finalize engine...\n");
			}
#if GAEA_USING_SYCL
			catch (sycl::exception const& ex)
			{
				print_debugger_message(std::string{ ex.what() }.append("\n").c_str());
				print_debugger_message("\nSYCL exception detected! Trying to finalize engine...\n");
			}
#endif
		}

        if (exit_code != 0)
		{
			print_debugger_message("\nSEH exception detected! Trying to finalize engine...\n");
		}

		try
		{
			seh_finalize(&exit_code);
		}
		catch (std::exception const& ex)
		{
			print_debugger_message(std::string{ ex.what() }.append("\n").c_str());
		}
#if GAEA_USING_SYCL
		catch (sycl::exception const& ex)
		{
			print_debugger_message(std::string{ ex.what() }.append("\n").c_str());
		}
#endif

		gaea::engine = nullptr;

		if (exit_code != 0)
		{
			print_debugger_message("\n\tAborting GaeaPhysSim execution!\n\n");
			std::exit(int(exit_code));
		}
	}
}

int main()
{
#if GAEA_DEBUG_EXECUTION_BREAK_AT_START
	GAEA_DEBUG_BREAK();
#endif

	// Access handle to process instance, and command-line argument list and count
	HINSTANCE instance_handle;
	if (BOOL retval = GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, NULL, &instance_handle); !retval)
	{
		ExitProcess(retval);
	}
	
	int arg_count;
	LPWSTR* arg_array = CommandLineToArgvW(GetCommandLineW(), &arg_count);

	// Make these static so that they have internal linkage and can be reached from the atexit function
	static std::FILE* win_in_file = nullptr;
	static std::FILE* win_out_file = nullptr;
	static std::FILE* win_err_file = nullptr;

	// Configure program termination
	if (int retval = std::atexit([] {
		// Flush all open streams
		_flushall();

		// Close redirected standard streams and deallocate console
		//close_stdio_streams(win_in_file, stdin, "r");
		//close_stdio_streams(win_out_file, stdout, "w");
		//close_stdio_streams(win_err_file, stderr, "w");

		FreeConsole();

		// Reset timer resolution
		timeEndPeriod(1);
	}))
	{
		print_debugger_message(std::string{ "\nError during program termination registration\nCode: " }.append(std::to_string(retval)).c_str());
		return EXIT_FAILURE;
	}
	
	// Set minimum timer resolution to 1 millisecond
	timeBeginPeriod(1);

	// Allocate console
	AllocConsole();

	HWND console_window_handle = GetConsoleWindow();

	// Remove system menu from console window
	SetWindowLong(console_window_handle, GWL_STYLE, GetWindowLong(console_window_handle, GWL_STYLE) & ~WS_SYSMENU);

	// Set icon on console window and taskbar
    // Adapted from https://stackoverflow.com/a/18315727
	std::wstring console_icon_path = stdfs::weakly_canonical(stdfs::current_path() / stdfs::path{ gaea::config::path::default_engine_icon }.replace_extension(".ico"));
	LONG_PTR console_icon_handle = (LONG_PTR)LoadImage(nullptr, console_icon_path.c_str(), IMAGE_ICON, 0, 0, LR_LOADFROMFILE | LR_DEFAULTSIZE);

	if (console_icon_handle)
	{
		// Change both icons to the same icon handle
		SendMessage(console_window_handle, WM_SETICON, ICON_SMALL, console_icon_handle);
		SendMessage(console_window_handle, WM_SETICON, ICON_BIG, console_icon_handle);

		// Ensure that the application icon gets changed too
		SendMessage(GetWindow(console_window_handle, GW_OWNER), WM_SETICON, ICON_SMALL, console_icon_handle);
		SendMessage(GetWindow(console_window_handle, GW_OWNER), WM_SETICON, ICON_BIG, console_icon_handle);
	}

	// Rename console window title (must be done after removing menu)
	SetWindowText(console_window_handle, L"GaeaPhysSim Console");

	// Remove console window from taskbar
	// Adapted from https://stackoverflow.com/a/16044822
	/*{
		ITaskbarList* taskbar_list = NULL;

		HRESULT init_retval = CoInitialize(NULL);
		if (init_retval == S_OK)
		{
			HRESULT create_retval = CoCreateInstance(CLSID_TaskbarList, NULL, CLSCTX_INPROC_SERVER, IID_ITaskbarList, reinterpret_cast<LPVOID*>(&taskbar_list));
			if (create_retval == S_OK)
			{
				taskbar_list->DeleteTab(console_window_handle);

				taskbar_list->Release();
			}
		}

		CoUninitialize();
	}*/

	//std::ios::sync_with_stdio(false);

    // FIXME CLion doesn't like this too much
	// Reopen standard streams redirected to console
	//win_in_file = reopen_stdio_streams(GetStdHandle(STD_INPUT_HANDLE), stdin, "r");
	//win_out_file = reopen_stdio_streams(GetStdHandle(STD_OUTPUT_HANDLE), stdout, "w");
	//win_err_file = reopen_stdio_streams(GetStdHandle(STD_ERROR_HANDLE), stderr, "w");

    freopen_s(&win_in_file, "CONIN$", "r", stdin);
    freopen_s(&win_out_file, "CONOUT$", "w", stdout);
    freopen_s(&win_err_file, "CONOUT$", "w", stderr);

	// Clear streams to reset error states
	std::cin.clear();
	std::wcin.clear();
	std::cout.clear();
	std::wcout.clear();
	std::cerr.clear();
	std::wcerr.clear();

	print_debugger_message("\n\tInitiating GaeaPhysSim execution...\n\n");

	std::string executable_path = arg_count > 0
		? gaea::util::wide_to_narrow(arg_array[0])
		: stdfs::absolute(stdfs::path{ "./GaeaPhysSim.exe" }).string();

	gaea::gaea_engine_parameters parameters
	{
		[]() -> bool { return IsDebuggerPresent(); },
		[](std::string const& message) -> void { OutputDebugStringA(message.c_str()); std::fwrite(message.c_str(), 1, message.size(), stdout); },
		std::move(executable_path),
	};
	
#if GAEA_TEST_EXECUTION
	extern int GAEA_TEST_FUNCTION_DEFINITION(); GAEA_TEST_FUNCTION_DEFINITION();
	
	GAEA_DEBUG_BREAK();
#elif GAEA_PROFILE_EXECUTION
	gaea::array<uint32> num_entities_array{ 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000 };
	//gaea::array<uint32> num_entities_array{ 5, 10, 15 };
	gaea::array<uint32> num_threads_array{ 1, 2, 4, 8, 16, 32, 64, 128 };

#	if GAEA_PROFILE_CPU
    {
		uint32 max_threads = std::thread::hardware_concurrency();

		for (uint32 num_threads : num_threads_array)
		{
			if (num_threads > max_threads)
			{
				break;
			}

			parameters.profiling_num_threads = num_threads;

			for (uint32 num_entities : num_entities_array)
			{
				parameters.profiling_num_entities = num_entities;

				// Define log file name, using unique date
				std::string log_file_name = gaea::util::system_time_to_string("GaeaPhysSim_profiling_%Y%m%d_%H%M%S.log", stdchr::system_clock::now());
				if (log_file_name.empty())
				{
					log_file_name = "GaeaPhysSim.log";
				}

				parameters.log_file_name = std::move(log_file_name);

				seh_main(&parameters);
			}
		}
	}
#	else
	{
		parameters.profiling_num_threads = 1;

		for (uint32 num_entities : num_entities_array)
		{
			parameters.profiling_num_entities = num_entities;

			// Define log file name, using unique date
			std::string log_file_name = gaea::util::system_time_to_string("GaeaPhysSim_profiling_%Y%m%d_%H%M%S.log", stdchr::system_clock::now());
			if (log_file_name.empty())
			{
				log_file_name = "GaeaPhysSim.log";
				}

			parameters.log_file_name = std::move(log_file_name);

			seh_main(&parameters);
		}
	}
#	endif
#else
	// Initialize parameters and engine object
	{
		// Define log file name, using unique date
		std::string log_file_name = gaea::util::system_time_to_string("GaeaPhysSim_%Y%m%d_%H%M%S.log", stdchr::system_clock::now());
		if (log_file_name.empty())
		{
			log_file_name = "GaeaPhysSim.log";
		}

		parameters.log_file_name = std::move(log_file_name);
		
		// Invoke engine through SEH-protected context
		seh_main(&parameters);
	}
#endif

	print_debugger_message("\n\tTerminating GaeaPhysSim execution.\n\n");

	return EXIT_SUCCESS;
}
