cmake_minimum_required (VERSION 3.8)

target_sources(GaeaPhysSim 
	PUBLIC
		"${CMAKE_CURRENT_SOURCE_DIR}/engine_config.hpp"
		"${CMAKE_CURRENT_SOURCE_DIR}/gaea_engine.hpp"
	PRIVATE
		"${CMAKE_CURRENT_SOURCE_DIR}/gaea_engine.cpp"
	)
