#ifndef ENGINE_GAEA_ENGINE_INCLUDE_GUARD
#define ENGINE_GAEA_ENGINE_INCLUDE_GUARD

#include "core/core_engine.hpp"

#include "engine/graphics/glfw_renderer.hpp"
#include "engine/scene/gaea_world.hpp"

#include "system/config_manager.hpp"
#include "system/device_manager.hpp"
#include "system/input_manager.hpp"
#include "system/log_manager.hpp"
#include "system/random_engine.hpp"
#include "system/timer_manager.hpp"

namespace gaea::implementation
{
	struct engine_frame_duration
	{
		// Duration of frame
		float32 frame = 0.f;

		// Duration of timer ticking
		float32 timer = 0.f;

		// Duration of I/O and window event handling
		float32 event = 0.f;

		// Duration of world and entities logic
		float32 scene = 0.f;

		// Duration of physics simulation
		float32 physics = 0.f;

		// Duration of graphics rendering
		float32 graphics = 0.f;
	};
}

namespace gaea
{
	struct gaea_engine_parameters
	{
		std::function<bool()> is_debugger_attached_func;
		std::function<void(std::string const&)> print_debugger_message_func;

		std::string executable_path;
		std::string log_file_name;

		int32 profiling_num_threads;
		int32 profiling_num_entities;
	};

	class gaea_engine final : public base_engine
	{
	public:
		gaea_engine();

		virtual bool initialize(void* parameters) override;
		virtual void finalize() override;

		virtual void run() override;

		virtual uint64 make_unique_id() override;
		virtual uint64 make_sequential_id() override;

		virtual uint64 frame_number() const override;
		virtual float64 frame_delta() const override;
		virtual float64 frame_actual_delta() const override;
		virtual float64 max_frame_rate() const override;

		virtual bool is_paused() const override;

		virtual bool is_running() const override;
		virtual bool is_initializing() const override;
		virtual bool is_finalizing() const override;

		virtual random_engine* access_random_engine() override;

		virtual input_manager* access_input_manager() override;
		virtual config_manager* access_config_manager() override;
		virtual device_manager* access_device_manager() override;
		virtual timer_manager* access_timer_manager() override;

		virtual void register_thread(thread_type type) override;
		virtual bool is_engine_thread() const override;
		virtual bool is_render_thread() const override;
		virtual bool is_simulate_thread() const override;

		virtual stdfs::path compose_engine_path(stdfs::path const& path) const override;
		
		virtual void log_message(log_type type, stdchr::system_clock::time_point&& time, std::string&& message) override;
		virtual void display_message(basic_color color, std::string&& message) override;

		uint32 profiling_num_threads() const;
		uint32 profiling_num_entities() const;

		bool is_using_mesh_instancing() const;
		bool is_using_sycl_simulator() const;

	private:
		virtual void* access_world_internal(const type_info& world_type) override;

		void _update_frame_times();
		
		void _display_stat_messages(implementation::engine_frame_duration const& frame_duration);
		void _log_simulation_times(implementation::engine_frame_duration const& frame_duration);

		void _on_window_focus(bool focused);
		void _on_window_close();

		void _terminate_engine();

		mutable map<thread_type, set<std::thread::id>> _thread_map;

		glfw_renderer* _renderer;
		base_simulator* _simulator;
		
		gaea_world* _world;

		random_engine _random_engine;

		config_manager _config_manager;
		device_manager _device_manager;
		input_manager _input_manager;
		log_manager _log_manager;
		timer_manager _timer_manager;

		stdfs::path _executable_path;

		std::ofstream _general_log_file;
		std::ostream _debugger_log_device;

		uint8 _profiling_log_id;

		uint32 _profiling_num_threads;
		uint32 _profiling_num_entities;

		float64 _max_frame_delta;

		float64 _time_dilation;
		int64 _hitch_time;

		uint64 _last_swap_buffers_frame;
		float32 _last_swap_buffers_duration;
		
		uint64 _last_update_buffers_frame;
		float32 _last_update_buffers_duration;
		
		stdchr::system_clock::duration _world_time;
		stdchr::steady_clock::time_point _last_frame_time;
		
		float64 _frame_delta;
		float64 _frame_fps;
		
		uint64 _frame_number;

		bool _is_initializing;
		bool _is_finalizing;

		uint8 _should_run : 1;
		uint8 _is_paused : 1;
		uint8 _is_tick_paused : 1;
		uint8 _should_display_detail_messages : 1;
		uint8 _using_mesh_instancing : 1;
		uint8 _using_sycl_simulator : 1;
		
	};

}

#endif
