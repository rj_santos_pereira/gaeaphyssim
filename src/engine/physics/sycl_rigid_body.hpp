#ifndef ENGINE_PHYSICS_SYCL_RIGID_BODY_INCLUDE_GUARD
#define ENGINE_PHYSICS_SYCL_RIGID_BODY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/engine_config.hpp"
#include "engine/graphics/gl_box_frame.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/physics/sycl_physics.hpp"
#include "library/containers.hpp"
#include "library/mutex.hpp"
#include "library/math/bounding_box.hpp"
#include "library/math/matrix.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

#if GAEA_USING_SYCL

namespace gaea::implementation
{
	class sycl_rigid_body_data
	{
	public:
		explicit sycl_rigid_body_data(std::size_t initial_capacity);

		void allocate_body_data(void* body_ptr, float32 mass, matrix3 const& inertia,
								vector const& position, quaternion const& rotation,
								vector const& scaling, polyhedron const& geometry,
								float32 restitution_coefficient,
								float32 friction_coefficient,
								collision_entity::type entity_type,
								collision_behavior::type behavior_type,
								body_mobility mobility,
								bool calculate_bounds);

		void deallocate_body_data(void* body_ptr);

		// Bulk data accessors

		sycl_data::force_data* force_data_ptr();
		std::size_t force_data_size() const;

		sycl_data::mass_data* mass_data_ptr();
		std::size_t mass_data_size() const;

		sycl_data::statics_data* statics_data_ptr();
		std::size_t statics_data_size() const;

		sycl_data::momentum_data* momentum_data_ptr();
		std::size_t momentum_data_size() const;

		sycl_data::position_data* position_data_ptr();
		std::size_t position_data_size() const;

		sycl_data::geometry_data* geometry_data_ptr();
		std::size_t geometry_data_size() const;

		sycl_data::geometry_face_data* geometry_face_data_ptr();
		std::size_t geometry_face_data_size() const;

		sycl_data::geometry_vertex_data* geometry_vertex_data_ptr();
		std::size_t geometry_vertex_data_size() const;

		sycl_data::geometry_halfedge_data* geometry_halfedge_data_ptr();
		std::size_t geometry_halfedge_data_size() const;

		sycl_data::flags_data* flags_data_ptr();
		std::size_t flags_data_size() const;

		void reset_force_data();
		void reset_flags_data();

		// Modifiers

		void apply_world_force(void* body_ptr, vector const& force);
		void apply_body_force(void* body_ptr, vector const& force);

		void apply_world_torque(void* body_ptr, vector const& torque);
		void apply_body_torque(void* body_ptr, vector const& torque);

		void clear_force_and_torque(void* body_ptr);

		// Computing accessors

		vector query_world_linear_acceleration(void const* body_ptr) const;
		vector query_body_linear_acceleration(void const* body_ptr) const;
		
		vector query_world_angular_acceleration(void const* body_ptr) const;
		vector query_body_angular_acceleration(void const* body_ptr) const;

		vector query_world_linear_velocity(void const* body_ptr) const;
		vector query_body_linear_velocity(void const* body_ptr) const;
		
		vector query_world_angular_velocity(void const* body_ptr) const;
		vector query_body_angular_velocity(void const* body_ptr) const;

		// Mutating accessors

		void set_worldspace_force(void* body_ptr, vector const& force);
		void set_worldspace_torque(void* body_ptr, vector const& torque);
		
		void set_bodyspace_force(void* body_ptr, vector const& force);
		void set_bodyspace_torque(void* body_ptr, vector const& torque);

		void set_linear_momentum(void* body_ptr, vector const& linear_momentum);
		void set_angular_momentum(void* body_ptr, vector const& angular_momentum);

		void set_position(void* body_ptr, vector const& position);
		void set_rotation(void* body_ptr, quaternion const& rotation);

		void set_mass(void* body_ptr, float32 mass);
		void set_inertia(void* body_ptr, matrix3 const& inertia_tensor);

		// Non-mutating accessors

		vector const& get_worldspace_force(void const* body_ptr) const;
		vector const& get_worldspace_torque(void const* body_ptr) const;
		
		vector const& get_bodyspace_force(void const* body_ptr) const;
		vector const& get_bodyspace_torque(void const* body_ptr) const;

		vector const& get_linear_momentum(void const* body_ptr) const;
		vector const& get_angular_momentum(void const* body_ptr) const;

		vector const& get_position(void const* body_ptr) const;
		quaternion const& get_rotation(void const* body_ptr) const;

		float32 const& get_mass(void const* body_ptr) const;
		float32 const& get_mass_inverse(void const* body_ptr) const;

		matrix3 const& get_inertia(void const* body_ptr) const;
		matrix3 const& get_inertia_inverse(void const* body_ptr) const;

		// Geometry and bounds accessors

		void set_scaling(void* body_ptr, vector const& scaling);

		void set_geometry(void* body_ptr, polyhedron geometry);

		vector const& get_scaling(void const* body_ptr) const;

		polyhedron get_geometry(void const* body_ptr) const;

		adaptive_axis_aligned_bounding_box get_bounds(void const* body_ptr) const;

		// Flags accessors

		void set_restitution_coefficient(void* body_ptr, float32 restitution_coefficient);

		void set_friction_coefficient(void* body_ptr, float32 friction_coefficient);

		void set_collision_entity_type(void* body_ptr, collision_entity::type entity_type);

		void set_collision_behavior_type(void* body_ptr, collision_behavior::type behavior_type);

		void set_mobility(void* body_ptr, body_mobility mobility);

		float32 get_restitution_coefficient(void const* body_ptr) const;

		float32 get_friction_coefficient(void const* body_ptr) const;

		collision_entity::type get_collision_entity_type(void const* body_ptr) const;

		collision_behavior::type get_collision_behavior_type(void const* body_ptr) const;

		body_mobility get_mobility(void const* body_ptr) const;

		// TODO FIXME create way to coalesce memory
		//void coalesce();

	private:
		void _recalc_bodyspace_geometry(std::size_t body_index, polyhedron const& geometry);
		void _recalc_bodyspace_bounds(std::size_t body_index, vector const& ratio_scaling);

		static void _mark_has_new_data(uint8 data_flag);
		
		map<void const*, std::size_t> _body_index_map;
		
		array<sycl_data::force_data> _force_data_array;
		array<sycl_data::mass_data> _mass_data_array;
		
		array<sycl_data::statics_data> _statics_data_array;

		array<sycl_data::momentum_data> _momentum_data_array;
		array<sycl_data::position_data> _position_data_array;

		array<sycl_data::geometry_data> _geometry_data_array;

		array<sycl_data::geometry_face_data> _geom_poly_face_data_array;
		array<sycl_data::geometry_vertex_data> _geom_poly_vertex_data_array;
		array<sycl_data::geometry_halfedge_data> _geom_poly_halfedge_data_array;

		array<sycl_data::flags_data> _flags_data_array;
		
		array<vector> _scaling_data_array;

		std::size_t _next_body_index;
		std::size_t _free_body_count;
		
	};
}

namespace gaea
{
	class sycl_rigid_body final : public base_rigid_body
	{
	public:
		explicit sycl_rigid_body(float32 mass, matrix3 const& inertia, 
								 vector const& position, quaternion const& rotation, 
								 vector const& scaling = one_vector, polyhedron const& geometry = polyhedron{}, 
								 float32 restitution_coefficient = 0.5f,
								 float32 friction_coefficient = 0.5f,
								 collision_entity::type entity_type = collision_entity::world_dynamic,
								 collision_behavior::type behavior_type = collision_behavior::world_dynamic,
								 body_mobility mobility = body_mobility::movable);

		virtual void setup_body() override;
		void setup_body_data(std::shared_ptr<implementation::sycl_rigid_body_data> const& body_data);

		virtual void setdown_body() override;
		void setdown_body_data(bool preserve_data);

		// Modifiers

		virtual void apply_world_force(vector const& force) override;
		virtual void apply_body_force(vector const& force) override;

		virtual void apply_world_torque(vector const& torque) override;
		virtual void apply_body_torque(vector const& torque) override;

		virtual void clear_force_and_torque() override;

		// Computing accessors

		virtual vector query_world_linear_acceleration() const override;
		virtual vector query_body_linear_acceleration() const override;

		virtual vector query_world_angular_acceleration() const override;
		virtual vector query_body_angular_acceleration() const override;

		virtual vector query_world_linear_velocity() const override;
		virtual vector query_body_linear_velocity() const override;

		virtual vector query_world_angular_velocity() const override;
		virtual vector query_body_angular_velocity() const override;

		// Mutating accessors

		virtual vector& access_force() override;
		virtual vector& access_torque() override;
		
		virtual vector& access_linear_momentum() override;
		virtual vector& access_angular_momentum() override;
		
		virtual vector& access_position() override;
		virtual quaternion& access_rotation() override;

		void set_worldspace_force(vector const& force);
		void set_worldspace_torque(vector const& torque);

		void set_bodyspace_force(vector const& force);
		void set_bodyspace_torque(vector const& torque);

		virtual void set_force(vector const& force) override;
		virtual void set_torque(vector const& torque) override;
		
		virtual void set_linear_momentum(vector const& linear_momentum) override;
		virtual void set_angular_momentum(vector const& angular_momentum) override;

		virtual void set_position(vector const& position) override;
		virtual void set_rotation(quaternion const& rotation) override;

		virtual void set_mass(float32 mass) override;
		virtual void set_inertia(matrix3 const& inertia_tensor) override;

		// Non-mutating accessors

		vector const& get_worldspace_force() const;
		vector const& get_worldspace_torque() const;

		vector const& get_bodyspace_force() const;
		vector const& get_bodyspace_torque() const;
		
		virtual vector const& get_force() const override;
		virtual vector const& get_torque() const override;
		
		virtual vector const& get_linear_momentum() const override;
		virtual vector const& get_angular_momentum() const override;

		virtual vector const& get_position() const override;
		virtual quaternion const& get_rotation() const override;

		virtual float32 const& get_mass() const override;
		virtual float32 const& get_mass_inverse() const override;
		
		virtual matrix3 const& get_inertia() const override;
		virtual matrix3 const& get_inertia_inverse() const override;

		// Geometry and bounds accessors
		
		virtual void set_scaling(vector const& scaling) override;

		virtual void set_geometry(polyhedron geometry) override;
		
		virtual vector const& get_scaling() const override;

		polyhedron get_geometry() const;

		adaptive_axis_aligned_bounding_box get_bounds() const;

		// Flags accessors

		virtual void set_restitution_coefficient(float32 restitution_coefficient) override;
		
		virtual void set_friction_coefficient(float32 friction_coefficient) override;

		virtual void set_collision_entity_type(collision_entity::type entity_type) override;

		virtual void set_collision_behavior_type(collision_behavior::type behavior_type) override;

		virtual void set_mobility(body_mobility mobility) override;

		virtual float32 get_restitution_coefficient() const override;
		
		virtual float32 get_friction_coefficient() const override;

		virtual collision_entity::type get_collision_entity_type() const override;

		virtual collision_behavior::type get_collision_behavior_type() const override;

		virtual body_mobility get_mobility() const override;

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		void update_collision_state(adaptive_axis_aligned_bounding_box const& bounds, collision_state state);

		collision_state query_collision_state() const;

		void toggle_bounds_frame();

		bool is_bounds_frame_visible() const;
#endif

	private:
		void _copy_body_data(std::shared_ptr<implementation::sycl_rigid_body_data> const& new_body_data);
		
		std::shared_ptr<implementation::sycl_rigid_body_data> _body_data_ptr;

		// Synchronization

		spin_mutex _body_mutex;

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		gl_box_frame _debug_bounds_frame;
		collision_state _collision_state;
#endif

	};
	
}

#endif

#endif
