#ifndef ENGINE_PHYSICS_SYCL_SIMULATOR_INCLUDE_GUARD
#define ENGINE_PHYSICS_SYCL_SIMULATOR_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/physics/sycl_rigid_body.hpp"
#include "engine/physics/sycl_physics.hpp"
#include "engine/physics/dynamics/sycl_bcd_sort_sweep_solver.hpp"
#include "engine/physics/dynamics/sycl_cr_local_symplectic_euler_solver.hpp"
#include "engine/physics/dynamics/sycl_ncd_separating_axis_test_solver.hpp"
#include "library/containers.hpp"
#include "library/mutex.hpp"
#include "system/device_manager.hpp"

#if GAEA_USING_SYCL

namespace gaea::implementation
{
	template <class Type>
	class ring_buffer
	{
	public:
		explicit ring_buffer()
			: ring_buffer{ 0 }
		{
		}

		template <class ... ArgTypes>
		explicit ring_buffer(std::size_t prev_count, ArgTypes&& ... args)
			: _prev_elem_array(prev_count)
			, _curr_elem{ std::forward<ArgTypes>(args) ... }
			, _next_elem{ _curr_elem }
			, _prev_elem_count{ prev_count }
			, _prev_elem_index{ 0 }
		{
		}

		Type& previous_element(std::size_t index = 0)
		{
			if (index >= _prev_elem_count)
			{
				GAEA_FATAL(ring_buffer, "Trying to access an element beyond the capacity of the array!");
			}

			return _prev_elem_array[(_prev_elem_index - index - 1) % _prev_elem_count];
		}

		Type& current_element()
		{
			return _curr_elem;
		}

		Type& next_element()
		{
			return _next_elem;
		}

		void advance_element(bool keep_curr_elem)
		{
			if (_prev_elem_count && keep_curr_elem)
			{
				_prev_elem_array[_prev_elem_index % _prev_elem_count] = std::move(_curr_elem);
				++_prev_elem_index;
			}

			_curr_elem = _next_elem;
		}

		bool has_previous_element(std::size_t index = 0) const
		{
			if (index >= _prev_elem_count)
			{
				GAEA_ERROR(ring_buffer, "Trying to access an element beyond the capacity of the array!");
				return false;
			}

			return index < _prev_elem_index;
		}

		std::size_t num_previous_elements() const
		{
			return _prev_elem_count;
		}

	private:
		array<Type> _prev_elem_array;
		Type _curr_elem;
		Type _next_elem;

		std::size_t _prev_elem_count;
		std::size_t _prev_elem_index;
	};

}

namespace gaea
{
	enum class sycl_update_data_flags	: uint8
	{
		no_data							= 0x00,
		
		mass_data						= 0x01,
		statics_data					= 0x02,
		momentum_data					= 0x04,
		position_data					= 0x08,
		geometry_data					= 0x10,

		// Meta flag used to force full sort
		any_data						= 0x20,

		all_data						= 0x1F,
	};

#if 0
	enum class sycl_single_body_dynamics_strategy
	{
		// Body-parallel approach, each body is integrated by a separate thread.
		body_parallel,

		// Data-parallel approach, each body is integrated by a total of 6 threads:
		//  - 3 threads for force, linear momentum, and position integration;
		//  - 3 threads for torque and angular momentum integration;
		//  - 4 threads handle rotation integration and quaternion normalization afterwards,
		//      with the remaining two idling.
		data_parallel_3pos_4rot,

		// TODO
		data_parallel_4lin_4ang,
	};
#endif

	struct sycl_simulator_parameters
	{
		// Which device should be used to drive the simulation
		device command_device;

		// How many frames the engine should lag behind the simulation
		uint32 num_lag_frames = 1;
		
		uint32 num_batches = 10;
		uint32 num_iterations = 5;
		
		float32 depenetration_weight = 0.2f;
		float32 depenetration_bias = 0.005f;
		float32 restitution_threshold = 10.0f;
		float32 restitution_attenuation = 5.0f;
		
		collision_mode restitution_mode = collision_mode::arithmetic;
		collision_mode friction_mode = collision_mode::geometric;
		
		float32 max_substep_delta = 1.0f / 120.0f;
		uint32 max_substep_count = 6;

		//uint32 thread_count = 0;
		//uint32 thread_ratio = 2;
	};
	
	class sycl_simulator final : public base_simulator
	{
		static constexpr std::size_t _num_events = 7;

		static constexpr std::size_t _sb_event_idx = 0;	//	Swap Buffers
		static constexpr std::size_t _ub_event_idx = 1;	//	Update Buffers
														//	Collision Detection
		static constexpr std::size_t _bp_event_idx = 2;	//		- Broad-Phase
		static constexpr std::size_t _np_event_idx = 3;	//		- Narrow-Phase
		static constexpr std::size_t _cg_event_idx = 4;	//			- Contact Generation
														//	Collision Resolution
		static constexpr std::size_t _cb_event_idx = 5;	//		- Constraint Batching
		static constexpr std::size_t _cs_event_idx = 6;	//		- Constraint Solving
		
	public:
		explicit sycl_simulator();

		virtual bool initialize(void* parameters) override;
		virtual void finalize() override;
		
		virtual void step(float64 frame_delta) override;
		
		virtual void finish_step() override;

		virtual bool is_real_time() const override;

		virtual void register_body(simulation_object type, void* object) override;
		virtual void unregister_body(simulation_object type, void * object) override;
		
		virtual void apply_world_force(simulation_vector_type const& force) override;
		virtual void apply_world_torque(simulation_vector_type const& force) override;
		
		virtual void apply_world_lin_accel(simulation_vector_type const& accel) override;
		virtual void apply_world_ang_accel(simulation_vector_type const& accel) override;

		void mark_has_new_data(sycl_update_data_flags data_flag);

		virtual float32 query_step_duration() const override;
		virtual float32 query_collision_detection_duration() const override;
		virtual float32 query_broad_phase_duration() const override;
		virtual float32 query_narrow_phase_duration() const override;
		virtual float32 query_collision_resolution_duration() const override;

		virtual uint32 num_threads() const override;
		
		float32 query_swap_buffers_duration();
		float32 query_update_buffers_duration();

		float32 query_contact_generation_duration() const;
		
		float32 query_constraint_batching_duration() const;
		float32 query_constraint_solving_duration() const;

		void toggle_bounding_box_display();

		bool is_displaying_bounding_boxes() const;

	private:
		void _simulation_loop(std::size_t thread_index, std::size_t thread_count);

		void _resize_data_buffers();
		void _swap_data_buffers();

		void _update_overlap_work_sizes();

		array<sycl_rigid_body*> _rigid_body_array;
		std::shared_ptr<implementation::sycl_rigid_body_data> _rigid_body_data_ptr;

		array<std::thread> _simulate_thread_array;
		semaphore _begin_simulate_semaphore;
		semaphore _end_simulate_semaphore;

		std::atomic<int32> _finished_step_count;
		
		device _command_device;
		sycl::queue _command_queue;

		mutable sycl_type::sequence_event_buffer<_num_events> _events_buffer;

		// Buffer naming:
		// host - created on the host side to send updated data to the device
		// device - created on the host side to receive updated data from the device
		// compute - created on the device side to use for intermediate computations

		// Host-write, device-read data
		sycl_type::ring_buffer<sycl_data::force_data> _force_host_buffer;
		sycl_type::ring_buffer<sycl_data::flags_data> _flags_host_buffer;

		// Host-rare-write, device-read data
		sycl_type::ring_buffer<sycl_data::mass_data> _mass_host_buffer;
		sycl_type::ring_buffer<sycl_data::mass_data> _mass_device_buffer;

		sycl_type::ring_buffer<sycl_data::statics_data> _statics_host_buffer;
		sycl_type::ring_buffer<sycl_data::statics_data> _statics_device_buffer;

		sycl_type::ring_buffer<sycl_data::geometry_data> _geometry_host_buffer;
		sycl_type::ring_buffer<sycl_data::geometry_data> _geometry_device_buffer;

		sycl_type::ring_buffer<sycl_data::geometry_face_data> _geometry_face_host_buffer;
		sycl_type::ring_buffer<sycl_data::geometry_face_data> _geometry_face_device_buffer;
		
		sycl_type::ring_buffer<sycl_data::geometry_vertex_data> _geometry_vertex_host_buffer;
		sycl_type::ring_buffer<sycl_data::geometry_vertex_data> _geometry_vertex_device_buffer;
		
		sycl_type::ring_buffer<sycl_data::geometry_halfedge_data> _geometry_halfedge_host_buffer;
		sycl_type::ring_buffer<sycl_data::geometry_halfedge_data> _geometry_halfedge_device_buffer;

		// Host-read-write, device-read-write data
		sycl_type::ring_buffer<sycl_data::momentum_data> _momentum_host_buffer;
		sycl_type::ring_buffer<sycl_data::momentum_data> _momentum_device_buffer;

		sycl_type::ring_buffer<sycl_data::position_data> _position_host_buffer;
		sycl_type::ring_buffer<sycl_data::position_data> _position_device_buffer;

		// Host-read, device-read-write data
		sycl_type::ring_buffer<sycl_data::dynamics_data> _dynamics_compute_buffer;

		sycl_type::ring_buffer<sycl_data::geometry_data> _geometry_compute_buffer;
		
		sycl_type::ring_buffer<sycl_data::geometry_face_data> _geometry_face_compute_buffer;
		sycl_type::ring_buffer<sycl_data::geometry_vertex_data> _geometry_vertex_compute_buffer;
		
		sycl_type::ring_buffer<sycl_data::bcd_body_data> _bcd_body_compute_buffer;
		sycl_type::ring_buffer<sycl_data::bcd_sort_data> _bcd_sort_compute_buffer;
		sycl_type::ring_buffer<sycl_data::bcd_sweep_data> _bcd_sweep_compute_buffer;
		sycl_type::ring_buffer<sycl_data::ncd_overlap_data> _ncd_overlap_compute_buffer;
		sycl_type::ring_buffer<sycl_data::cr_constraint_data> _cr_constraint_compute_buffer;

		sycl_type::ring_buffer<uint32> _cd_overlap_count_compute_buffer;
		sycl_type::ring_buffer<uint32> _cr_overlap_count_compute_buffer;
		sycl_type::ring_buffer<uint32> _overlap_limit_compute_buffer;
		
		sycl_type::ring_buffer<uint32> _constraint_count_compute_buffer;
		sycl_type::ring_buffer<uint32> _constraint_index_compute_buffer;

		uint32 _overlap_limit;
		uint32 _overlap_default_limit;

		uint32 _num_constraint_work_groups;
		uint32 _num_constraint_work_items_per_group;

		uint32 _constraint_grid_side_size;

		// Number of bodies introduced into the simulation
		uint32 _total_body_count;
		// Number of bodies to simulate in the next step
		uint32 _simulate_body_count;
		// Number of bodies to update with copyback data
		implementation::ring_buffer<uint32> _update_body_count_buffer;

		uint32 _work_group_size;

		sycl_data::physics_vec_data _world_force;
		sycl_data::physics_vec_data _world_accel;

		uint32 _num_lag_frames;

		uint32 _num_batches;
		uint32 _num_iterations;
		
		float32 _depenetration_weight;
		float32 _depenetration_bias;
		float32 _restitution_threshold;
		float32 _restitution_attenuation;
		float32 _restitution_offset;

		collision_mode _restitution_mode;
		collision_mode _friction_mode;

		float32 _max_substep_delta;
		uint32 _max_substep_count;

		float32 _step_delta;
		
		uint8 _should_run : 1;
		uint8 _should_step : 1;

		uint8 _should_display_bounding_boxes : 1;
		uint8 _is_real_time : 1;
		uint8 _has_new_data : 5;
	};
}

#endif

#endif
