#include "engine/physics/sycl_rigid_body.hpp"

#include "engine/physics/sycl_simulator.hpp"
#include "engine/physics/sycl_physics.hpp"

#if GAEA_USING_SYCL

namespace gaea::implementation
{
	sycl_rigid_body_data::sycl_rigid_body_data(std::size_t initial_capacity)
		: _body_index_map{}
		, _force_data_array{ initial_capacity }
		, _mass_data_array{ initial_capacity }
		, _statics_data_array{ initial_capacity }
		, _momentum_data_array{ initial_capacity }
		, _position_data_array{ initial_capacity }
		, _geometry_data_array{ initial_capacity }
		, _flags_data_array{ initial_capacity }
		, _scaling_data_array{ initial_capacity }
		, _next_body_index{ 0 }
		, _free_body_count{ 0 }
	{
	}

	void sycl_rigid_body_data::allocate_body_data(void* body_ptr, float32 mass, matrix3 const& inertia, 
												  vector const& position, quaternion const& rotation, 
												  vector const& scaling, polyhedron const& geometry, 
												  float32 restitution_coefficient,
												  float32 friction_coefficient,
												  collision_entity::type entity_type, 
												  collision_behavior::type behavior_type,
												  body_mobility mobility,
												  bool calculate_bounds)
	{
		std::size_t body_index;
#if 0
		if (_free_body_count == 0)
#endif
		{
			body_index = _next_body_index++;
			
			if (body_index == _statics_data_array.size())
			{
				std::size_t new_capacity = _statics_data_array.size() * 2;

				_force_data_array.resize(new_capacity);
				_mass_data_array.resize(new_capacity);
				_statics_data_array.resize(new_capacity);
				_momentum_data_array.resize(new_capacity);
				_position_data_array.resize(new_capacity);
				_geometry_data_array.resize(new_capacity);
				_flags_data_array.resize(new_capacity);
				_scaling_data_array.resize(new_capacity);
			}
		}
#if 0
		else
		{
			auto it = std::find_if(_statics_data_array.begin(), _statics_data_array.end(),
				[](sycl_data::statics_data const& data)
				{
					return data.entity_type == 0;
				});

			body_index = std::distance(_statics_data_array.begin(), it);

			--_free_body_count;
		}
#endif	
		_body_index_map[body_ptr] = body_index;

		_force_data_array[body_index].worldspace_force = zero_vector;
		_force_data_array[body_index].worldspace_torque = zero_vector;
		_force_data_array[body_index].bodyspace_force = zero_vector;
		_force_data_array[body_index].bodyspace_torque = zero_vector;

		_mass_data_array[body_index].mass = mass;
		_mass_data_array[body_index].mass_inverse = 1.f / mass;
		_mass_data_array[body_index].inertia = inertia;
		_mass_data_array[body_index].inertia_inverse = inertia.inverse();

		_statics_data_array[body_index].restitution_coefficient = restitution_coefficient;
		_statics_data_array[body_index].friction_coefficient = friction_coefficient;
		_statics_data_array[body_index].behavior_type = behavior_type;
		_statics_data_array[body_index].entity_type = entity_type;
		_statics_data_array[body_index].mobility = uint8(mobility);

		_momentum_data_array[body_index].linear_momentum = zero_vector;
		_momentum_data_array[body_index].angular_momentum = zero_vector;
		
		_position_data_array[body_index].position = position;
		_position_data_array[body_index].rotation = rotation;

		sycl_data::geometry_poly_data poly_data{
			uint32(_geom_poly_face_data_array.size()),
			uint32(geometry.face_count()),
			uint32(_geom_poly_vertex_data_array.size()),
			uint32(geometry.vertex_count()),
			uint32(_geom_poly_halfedge_data_array.size()),
			uint32(geometry.halfedge_count())
		};

		_geometry_data_array[body_index].poly = poly_data;
		_geom_poly_face_data_array.resize(poly_data.face_index + poly_data.face_count);
		_geom_poly_vertex_data_array.resize(poly_data.vertex_index + poly_data.vertex_count);
		_geom_poly_halfedge_data_array.resize(poly_data.halfedge_index + poly_data.halfedge_count);

		_flags_data_array[body_index].has_update = true;
		_flags_data_array[body_index].should_update_mass = true;
		_flags_data_array[body_index].should_update_geometry = true;
		_flags_data_array[body_index].should_update_statics = true;
		_flags_data_array[body_index].should_update_momentum = true;
		_flags_data_array[body_index].should_update_position = true;

		_scaling_data_array[body_index] = scaling;

		_recalc_bodyspace_geometry(body_index, geometry);
		if (calculate_bounds)
		{
			_recalc_bodyspace_bounds(body_index, scaling);
		}
	}

	void sycl_rigid_body_data::deallocate_body_data(void* body_ptr)
	{
		auto it = _body_index_map.find(body_ptr);

		if (it == _body_index_map.end())
		{
			GAEA_ERROR(sycl_rigid_body_data, "Invalid body index");
			return;
		}

		auto [_, body_index] = *it;

		// We'll use the entity channel to mark invalid objects
		_statics_data_array[body_index].entity_type = collision_entity::null;

		_body_index_map.erase(it);

		++_free_body_count;
	}

	sycl_data::force_data* sycl_rigid_body_data::force_data_ptr()
	{
		return _force_data_array.data();
	}

	std::size_t sycl_rigid_body_data::force_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::force_data);
	}

	sycl_data::mass_data* sycl_rigid_body_data::mass_data_ptr()
	{
		return _mass_data_array.data();
	}

	std::size_t sycl_rigid_body_data::mass_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::mass_data);
	}
	
	sycl_data::statics_data* sycl_rigid_body_data::statics_data_ptr()
	{
		return _statics_data_array.data();
	}

	std::size_t sycl_rigid_body_data::statics_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::statics_data);
	}

	sycl_data::momentum_data* sycl_rigid_body_data::momentum_data_ptr()
	{
		return _momentum_data_array.data();
	}

	std::size_t sycl_rigid_body_data::momentum_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::momentum_data);
	}

	sycl_data::position_data* sycl_rigid_body_data::position_data_ptr()
	{
		return _position_data_array.data();
	}

	std::size_t sycl_rigid_body_data::position_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::position_data);
	}

	sycl_data::geometry_data* sycl_rigid_body_data::geometry_data_ptr()
	{
		return _geometry_data_array.data();
	}

	std::size_t sycl_rigid_body_data::geometry_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::geometry_data);
	}

	sycl_data::geometry_face_data* sycl_rigid_body_data::geometry_face_data_ptr()
	{
		return _geom_poly_face_data_array.data();
	}

	std::size_t sycl_rigid_body_data::geometry_face_data_size() const
	{
		return _geom_poly_face_data_array.size() * sizeof(sycl_data::geometry_face_data);
	}

	sycl_data::geometry_vertex_data* sycl_rigid_body_data::geometry_vertex_data_ptr()
	{
		return _geom_poly_vertex_data_array.data();
	}

	std::size_t sycl_rigid_body_data::geometry_vertex_data_size() const
	{
		return _geom_poly_vertex_data_array.size() * sizeof(sycl_data::geometry_vertex_data);
	}

	sycl_data::geometry_halfedge_data* sycl_rigid_body_data::geometry_halfedge_data_ptr()
	{
		return _geom_poly_halfedge_data_array.data();
	}

	std::size_t sycl_rigid_body_data::geometry_halfedge_data_size() const
	{
		return _geom_poly_halfedge_data_array.size() * sizeof(sycl_data::geometry_halfedge_data);
	}

	sycl_data::flags_data* sycl_rigid_body_data::flags_data_ptr()
	{
		return _flags_data_array.data();
	}

	std::size_t sycl_rigid_body_data::flags_data_size() const
	{
		return _next_body_index * sizeof(sycl_data::flags_data);
	}

	void sycl_rigid_body_data::reset_force_data()
	{
		std::memset(force_data_ptr(), 0, force_data_size());
	}

	void sycl_rigid_body_data::reset_flags_data()
	{
		std::memset(flags_data_ptr(), 0, flags_data_size());
	}

	void sycl_rigid_body_data::apply_world_force(void* body_ptr, vector const& force)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].worldspace_force += force;
	}

	void sycl_rigid_body_data::apply_body_force(void* body_ptr, vector const& force)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].bodyspace_force += force;
	}

	void sycl_rigid_body_data::apply_world_torque(void* body_ptr, vector const& torque)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].worldspace_torque += torque;
	}

	void sycl_rigid_body_data::apply_body_torque(void* body_ptr, vector const& torque)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].bodyspace_torque += torque;
	}

	void sycl_rigid_body_data::clear_force_and_torque(void* body_ptr)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].worldspace_force = zero_vector;
		_force_data_array[index].worldspace_torque = zero_vector;
		_force_data_array[index].bodyspace_force = zero_vector;
		_force_data_array[index].bodyspace_torque = zero_vector;
	}

	vector sycl_rigid_body_data::query_world_linear_acceleration(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].mass_inverse
			* (_force_data_array[index].worldspace_force + _position_data_array[index].rotation * _force_data_array[index].bodyspace_force);
	}

	vector sycl_rigid_body_data::query_body_linear_acceleration(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].mass_inverse
			* (_position_data_array[index].rotation.reciprocal() * _force_data_array[index].worldspace_force + _force_data_array[index].bodyspace_force);
	}

	vector sycl_rigid_body_data::query_world_angular_acceleration(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _position_data_array[index].rotation
			* (_mass_data_array[index].inertia_inverse
				* (_position_data_array[index].rotation.reciprocal() * _force_data_array[index].worldspace_torque + _force_data_array[index].bodyspace_torque));
	}

	vector sycl_rigid_body_data::query_body_angular_acceleration(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].inertia_inverse
			* (_position_data_array[index].rotation.reciprocal() * _force_data_array[index].worldspace_torque + _force_data_array[index].bodyspace_torque);
	}

	vector sycl_rigid_body_data::query_world_linear_velocity(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].mass_inverse * _momentum_data_array[index].linear_momentum;
	}

	vector sycl_rigid_body_data::query_body_linear_velocity(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _position_data_array[index].rotation.reciprocal()
			* (_mass_data_array[index].mass_inverse * _momentum_data_array[index].linear_momentum);
	}

	vector sycl_rigid_body_data::query_world_angular_velocity(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _position_data_array[index].rotation
			* (_mass_data_array[index].inertia_inverse * (_position_data_array[index].rotation.reciprocal() * _momentum_data_array[index].angular_momentum));
	}

	vector sycl_rigid_body_data::query_body_angular_velocity(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].inertia_inverse * (_position_data_array[index].rotation.reciprocal() * _momentum_data_array[index].angular_momentum);
	}

	void sycl_rigid_body_data::set_worldspace_force(void* body_ptr, vector const& force)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].worldspace_force = force;
	}

	void sycl_rigid_body_data::set_worldspace_torque(void* body_ptr, vector const& torque)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].worldspace_torque = torque;
	}

	void sycl_rigid_body_data::set_bodyspace_force(void* body_ptr, vector const& force)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].bodyspace_force = force;
	}

	void sycl_rigid_body_data::set_bodyspace_torque(void* body_ptr, vector const& torque)
	{
		auto index = _body_index_map.at(body_ptr);
		_force_data_array[index].bodyspace_torque = torque;
	}

	void sycl_rigid_body_data::set_linear_momentum(void* body_ptr, vector const& linear_momentum)
	{
		auto index = _body_index_map.at(body_ptr);
		_momentum_data_array[index].linear_momentum = linear_momentum;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_momentum = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::momentum_data));
	}

	void sycl_rigid_body_data::set_angular_momentum(void* body_ptr, vector const& angular_momentum)
	{
		auto index = _body_index_map.at(body_ptr);
		_momentum_data_array[index].angular_momentum = angular_momentum;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_momentum = true;
		
		_mark_has_new_data(uint8(sycl_update_data_flags::momentum_data));
	}

	void sycl_rigid_body_data::set_position(void* body_ptr, vector const& position)
	{
		auto index = _body_index_map.at(body_ptr);
		_position_data_array[index].position = position;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_position = true;
		
		_mark_has_new_data(uint8(sycl_update_data_flags::position_data));
	}

	void sycl_rigid_body_data::set_rotation(void* body_ptr, quaternion const& rotation)
	{
		auto index = _body_index_map.at(body_ptr);
		_position_data_array[index].rotation = rotation;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_position = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::position_data));
	}

	void sycl_rigid_body_data::set_mass(void* body_ptr, float32 mass)
	{
		auto index = _body_index_map.at(body_ptr);
		_mass_data_array[index].mass = mass;
		_mass_data_array[index].mass_inverse = 1.f / mass;

		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_mass = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::mass_data));
	}

	void sycl_rigid_body_data::set_inertia(void* body_ptr, matrix3 const& inertia_tensor)
	{
		auto index = _body_index_map.at(body_ptr);
		_mass_data_array[index].inertia = inertia_tensor;
		_mass_data_array[index].inertia_inverse = inertia_tensor.inverse();

		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_mass = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::mass_data));
	}

	vector const& sycl_rigid_body_data::get_worldspace_force(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _force_data_array[index].worldspace_force;
	}

	vector const& sycl_rigid_body_data::get_worldspace_torque(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _force_data_array[index].worldspace_torque;
	}

	vector const& sycl_rigid_body_data::get_bodyspace_force(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _force_data_array[index].bodyspace_force;
	}

	vector const& sycl_rigid_body_data::get_bodyspace_torque(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _force_data_array[index].bodyspace_torque;
	}

	vector const& sycl_rigid_body_data::get_linear_momentum(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _momentum_data_array[index].linear_momentum;
	}

	vector const& sycl_rigid_body_data::get_angular_momentum(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _momentum_data_array[index].angular_momentum;
	}

	vector const& sycl_rigid_body_data::get_position(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _position_data_array[index].position;
	}

	quaternion const& sycl_rigid_body_data::get_rotation(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _position_data_array[index].rotation;
	}

	float32 const& sycl_rigid_body_data::get_mass(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].mass;
	}

	float32 const& sycl_rigid_body_data::get_mass_inverse(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].mass_inverse;
	}

	matrix3 const& sycl_rigid_body_data::get_inertia(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].inertia;
	}

	matrix3 const& sycl_rigid_body_data::get_inertia_inverse(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _mass_data_array[index].inertia_inverse;
	}

	void sycl_rigid_body_data::set_scaling(void* body_ptr, vector const& scaling)
	{
		auto index = _body_index_map.at(body_ptr);

		vector ratio_scaling = scaling / _scaling_data_array[index];
		_scaling_data_array[index] = scaling;
		
		_recalc_bodyspace_bounds(index, ratio_scaling);

		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_geometry = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::geometry_data));
	}

	void sycl_rigid_body_data::set_geometry(void* body_ptr, polyhedron geometry)
	{
		auto index = _body_index_map.at(body_ptr);

		vector const& ratio_scaling = _scaling_data_array[index];

		_recalc_bodyspace_geometry(index, geometry);
		_recalc_bodyspace_bounds(index, ratio_scaling);
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_geometry = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::geometry_data));
	}

	vector const& sycl_rigid_body_data::get_scaling(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _scaling_data_array[index];
	}

	polyhedron sycl_rigid_body_data::get_geometry(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		auto const& poly = _geometry_data_array[index].poly;
		
		return polyhedron{
			_geom_poly_face_data_array.data() + poly.face_index, poly.face_count,
			_geom_poly_vertex_data_array.data() + poly.vertex_index, poly.vertex_count,
			_geom_poly_halfedge_data_array.data() + poly.halfedge_index, poly.halfedge_count
		};
	}

	adaptive_axis_aligned_bounding_box sycl_rigid_body_data::get_bounds(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _geometry_data_array[index].bounds;
	}

	void sycl_rigid_body_data::set_restitution_coefficient(void* body_ptr, float32 restitution_coefficient)
	{
		auto index = _body_index_map.at(body_ptr);
		_statics_data_array[index].restitution_coefficient = restitution_coefficient;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_statics = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::statics_data));
	}

	void sycl_rigid_body_data::set_friction_coefficient(void* body_ptr, float32 friction_coefficient)
	{
		auto index = _body_index_map.at(body_ptr);
		_statics_data_array[index].friction_coefficient = friction_coefficient;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_statics = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::statics_data));
	}

	void sycl_rigid_body_data::set_collision_entity_type(void* body_ptr, collision_entity::type entity_type)
	{
		auto index = _body_index_map.at(body_ptr);
		_statics_data_array[index].entity_type = entity_type;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_statics = true;
		
		_mark_has_new_data(uint8(sycl_update_data_flags::statics_data));
	}

	void sycl_rigid_body_data::set_collision_behavior_type(void* body_ptr, collision_behavior::type behavior_type)
	{
		auto index = _body_index_map.at(body_ptr);
		_statics_data_array[index].behavior_type = behavior_type;
		
		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_statics = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::statics_data));
	}

	void sycl_rigid_body_data::set_mobility(void* body_ptr, body_mobility mobility)
	{
		auto index = _body_index_map.at(body_ptr);
		_statics_data_array[index].mobility = uint8(mobility);

		_flags_data_array[index].has_update = true;
		_flags_data_array[index].should_update_statics = true;

		_mark_has_new_data(uint8(sycl_update_data_flags::statics_data));
	}

	float32 sycl_rigid_body_data::get_restitution_coefficient(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _statics_data_array[index].restitution_coefficient;
	}

	float32 sycl_rigid_body_data::get_friction_coefficient(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return _statics_data_array[index].friction_coefficient;
	}

	collision_entity::type sycl_rigid_body_data::get_collision_entity_type(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return collision_entity::type(_statics_data_array[index].entity_type);
	}

	collision_behavior::type sycl_rigid_body_data::get_collision_behavior_type(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return collision_behavior::type(_statics_data_array[index].behavior_type);
	}

	body_mobility sycl_rigid_body_data::get_mobility(void const* body_ptr) const
	{
		auto index = _body_index_map.at(body_ptr);
		return body_mobility(_statics_data_array[index].mobility);
	}

	void sycl_rigid_body_data::_recalc_bodyspace_geometry(std::size_t body_index, polyhedron const& geometry)
	{
		auto& poly = _geometry_data_array[body_index].poly;

		if (poly.face_count != geometry.face_count() ||
			poly.vertex_count != geometry.vertex_count() ||
			poly.halfedge_count != geometry.halfedge_count())
		{
			GAEA_ERROR(sycl_rigid_body_data, "New poly data is incompatible with old data!");
			return;
		}

		//poly.center = geometry.center();
		
		std::memcpy(
			_geom_poly_face_data_array.data() + poly.face_index,
			geometry.face_data_ptr(),
			geometry.face_data_size()
		);

		std::memcpy(
			_geom_poly_vertex_data_array.data() + poly.vertex_index,
			geometry.vertex_data_ptr(),
			geometry.vertex_data_size()
		);

		std::memcpy(
			_geom_poly_halfedge_data_array.data() + poly.halfedge_index,
			geometry.halfedge_data_ptr(),
			geometry.halfedge_data_size()
		);
	}

	void sycl_rigid_body_data::_recalc_bodyspace_bounds(std::size_t body_index, vector const& ratio_scaling)
	{
		auto const& poly = _geometry_data_array[body_index].poly;
		
		vector inverse_scaling = 1.f / ratio_scaling;
		
		for (std::size_t index = poly.face_index, count = poly.face_index + poly.face_count; 
			 index < count; 
			 ++index)
		{
			vector& normal = _geom_poly_face_data_array[index].normal;
			(normal *= inverse_scaling).normalize();
		}

		adaptive_axis_aligned_bounding_box bounds;
		
		for (std::size_t index = poly.vertex_index, count = poly.vertex_index + poly.vertex_count; 
			 index < count; 
			 ++index)
		{
			vector& point = _geom_poly_vertex_data_array[index].point;
			point *= ratio_scaling;

			bounds.add_point(point);
		}
		
		_geometry_data_array[body_index].bounds = bounds.scale_by(vector{ 1.25f });
	}

	void sycl_rigid_body_data::_mark_has_new_data(uint8 data_flag)
	{
		if (!simulator)
		{
			GAEA_FATAL(sycl_rigid_body, "Trying to mark new data flag but global simulator accessor is not initialized!");
		}

		static_cast<sycl_simulator*>(simulator)->mark_has_new_data(sycl_update_data_flags(data_flag));
	}
}

namespace gaea
{
	sycl_rigid_body::sycl_rigid_body(float32 mass, matrix3 const& inertia, 
									 vector const& position, quaternion const& rotation, 
									 vector const& scaling, polyhedron const& geometry, 
									 float32 restitution_coefficient,
									 float32 friction_coefficient,
									 collision_entity::type entity_type, 
									 collision_behavior::type behavior_type, 
									 body_mobility mobility)
		: _body_data_ptr{ std::make_shared<implementation::sycl_rigid_body_data>(1) }
		, _body_mutex{}
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		, _debug_bounds_frame{}
		, _collision_state{}
#endif
	{
		// We'll use this to temporarily store this body's data
		_body_data_ptr->allocate_body_data(this, mass, inertia, position, rotation, scaling, geometry, restitution_coefficient, friction_coefficient, entity_type, behavior_type, mobility, false);
	}

	void sycl_rigid_body::setup_body()
	{
		if (!simulator)
		{
			GAEA_FATAL(sycl_rigid_body, "Trying to setup body but global simulator accessor is not initialized!");
		}

		simulator->register_body(simulation_object::rigid_body, this);

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_debug_bounds_frame.setup_commands();
#endif
	}

	void sycl_rigid_body::setup_body_data(std::shared_ptr<implementation::sycl_rigid_body_data> const& body_data)
	{
		_copy_body_data(body_data);

		_body_data_ptr = body_data;
	}

	void sycl_rigid_body::setdown_body()
	{
		if (!simulator)
		{
			GAEA_FATAL(sycl_rigid_body, "Trying to setdown body but global simulator accessor is not initialized!");
		}

		// TODO FIXME
		//simulator->unregister_body(simulation_object::rigid_body, this);

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_debug_bounds_frame.setdown_commands();
#endif
	}

	void sycl_rigid_body::setdown_body_data(bool preserve_data)
	{
		if (preserve_data)
		{
			auto body_data = std::make_shared<implementation::sycl_rigid_body_data>(1);

			_copy_body_data(body_data);

			_body_data_ptr = body_data;
		}
		else
		{
			_body_data_ptr = nullptr;
		}
	}

	void sycl_rigid_body::apply_world_force(vector const& force)
	{
		_body_data_ptr->apply_world_force(this, force);
	}

	void sycl_rigid_body::apply_body_force(vector const& force)
	{
		_body_data_ptr->apply_body_force(this, force);
	}

	void sycl_rigid_body::apply_world_torque(vector const& torque)
	{
		_body_data_ptr->apply_world_torque(this, torque);
	}

	void sycl_rigid_body::apply_body_torque(vector const& torque)
	{
		_body_data_ptr->apply_body_torque(this, torque);
	}

	void sycl_rigid_body::clear_force_and_torque()
	{
		_body_data_ptr->clear_force_and_torque(this);
	}

	vector sycl_rigid_body::query_world_linear_acceleration() const
	{
		return _body_data_ptr->query_world_linear_acceleration(this);
	}

	vector sycl_rigid_body::query_body_linear_acceleration() const
	{
		return _body_data_ptr->query_body_linear_acceleration(this);
	}

	vector sycl_rigid_body::query_world_angular_acceleration() const
	{
		return _body_data_ptr->query_world_angular_acceleration(this);
	}

	vector sycl_rigid_body::query_body_angular_acceleration() const
	{
		return _body_data_ptr->query_body_angular_acceleration(this);
	}

	vector sycl_rigid_body::query_world_linear_velocity() const
	{
		return _body_data_ptr->query_world_linear_velocity(this);
	}

	vector sycl_rigid_body::query_body_linear_velocity() const
	{
		return _body_data_ptr->query_body_linear_velocity(this);
	}

	vector sycl_rigid_body::query_world_angular_velocity() const
	{
		return _body_data_ptr->query_world_angular_velocity(this);
	}

	vector sycl_rigid_body::query_body_angular_velocity() const
	{
		return _body_data_ptr->query_body_angular_velocity(this);
	}

	vector& sycl_rigid_body::access_force()
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	vector& sycl_rigid_body::access_torque()
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	vector& sycl_rigid_body::access_linear_momentum()
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	vector& sycl_rigid_body::access_angular_momentum()
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	vector& sycl_rigid_body::access_position()
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	quaternion& sycl_rigid_body::access_rotation()
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	void sycl_rigid_body::set_worldspace_force(vector const& force)
	{
		_body_data_ptr->set_worldspace_force(this, force);
	}

	void sycl_rigid_body::set_worldspace_torque(vector const& torque)
	{
		_body_data_ptr->set_worldspace_torque(this, torque);
	}

	void sycl_rigid_body::set_bodyspace_force(vector const& force)
	{
		_body_data_ptr->set_bodyspace_force(this, force);
	}

	void sycl_rigid_body::set_bodyspace_torque(vector const& torque)
	{
		_body_data_ptr->set_bodyspace_torque(this, torque);
	}

	void sycl_rigid_body::set_force(vector const&)
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	void sycl_rigid_body::set_torque(vector const&)
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	void sycl_rigid_body::set_linear_momentum(vector const& linear_momentum)
	{
		_body_data_ptr->set_linear_momentum(this, linear_momentum);
	}

	void sycl_rigid_body::set_angular_momentum(vector const& angular_momentum)
	{
		_body_data_ptr->set_angular_momentum(this, angular_momentum);
	}

	void sycl_rigid_body::set_position(vector const& position)
	{
		_body_data_ptr->set_position(this, position);
	}

	void sycl_rigid_body::set_rotation(quaternion const& rotation)
	{
		_body_data_ptr->set_rotation(this, rotation);
	}

	void sycl_rigid_body::set_mass(float32 mass)
	{
		_body_data_ptr->set_mass(this, mass);
	}

	void sycl_rigid_body::set_inertia(matrix3 const& inertia_tensor)
	{
		_body_data_ptr->set_inertia(this, inertia_tensor);
	}

	vector const& sycl_rigid_body::get_worldspace_force() const
	{
		return _body_data_ptr->get_worldspace_force(this);
	}

	vector const& sycl_rigid_body::get_worldspace_torque() const
	{
		return _body_data_ptr->get_worldspace_torque(this);
	}

	vector const& sycl_rigid_body::get_bodyspace_force() const
	{
		return _body_data_ptr->get_bodyspace_force(this);
	}

	vector const& sycl_rigid_body::get_bodyspace_torque() const
	{
		return _body_data_ptr->get_bodyspace_torque(this);
	}

	vector const& sycl_rigid_body::get_force() const
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	vector const& sycl_rigid_body::get_torque() const
	{
		GAEA_FATAL(sycl_rigid_body, "Override not supported");
	}

	vector const& sycl_rigid_body::get_linear_momentum() const
	{
		return _body_data_ptr->get_linear_momentum(this);
	}

	vector const& sycl_rigid_body::get_angular_momentum() const
	{
		return _body_data_ptr->get_angular_momentum(this);
	}

	vector const& sycl_rigid_body::get_position() const
	{
		return _body_data_ptr->get_position(this);
	}

	quaternion const& sycl_rigid_body::get_rotation() const
	{
		return _body_data_ptr->get_rotation(this);
	}

	float32 const& sycl_rigid_body::get_mass() const
	{
		return _body_data_ptr->get_mass(this);
	}

	float32 const& sycl_rigid_body::get_mass_inverse() const
	{
		return _body_data_ptr->get_mass_inverse(this);
	}

	matrix3 const& sycl_rigid_body::get_inertia() const
	{
		return _body_data_ptr->get_inertia(this);
	}

	matrix3 const& sycl_rigid_body::get_inertia_inverse() const
	{
		return _body_data_ptr->get_inertia_inverse(this);
	}

	void sycl_rigid_body::set_scaling(vector const& scaling)
	{
		_body_data_ptr->set_scaling(this, scaling);
	}

	void sycl_rigid_body::set_geometry(polyhedron geometry)
	{
		_body_data_ptr->set_geometry(this, std::move(geometry));
	}

	vector const& sycl_rigid_body::get_scaling() const
	{
		return _body_data_ptr->get_scaling(this);
	}

	polyhedron sycl_rigid_body::get_geometry() const
	{
		return _body_data_ptr->get_geometry(this);
	}

	adaptive_axis_aligned_bounding_box sycl_rigid_body::get_bounds() const
	{
		return _body_data_ptr->get_bounds(this);
	}

	void sycl_rigid_body::set_restitution_coefficient(float32 restitution_coefficient)
	{
		_body_data_ptr->set_restitution_coefficient(this, restitution_coefficient);
	}

	void sycl_rigid_body::set_friction_coefficient(float32 friction_coefficient)
	{
		_body_data_ptr->set_friction_coefficient(this, friction_coefficient);
	}

	void sycl_rigid_body::set_collision_entity_type(collision_entity::type entity_type)
	{
		_body_data_ptr->set_collision_entity_type(this, entity_type);
	}

	void sycl_rigid_body::set_collision_behavior_type(collision_behavior::type behavior_type)
	{
		_body_data_ptr->set_collision_behavior_type(this, behavior_type);
	}

	void sycl_rigid_body::set_mobility(body_mobility mobility)
	{
		_body_data_ptr->set_mobility(this, mobility);
	}

	float32 sycl_rigid_body::get_restitution_coefficient() const
	{
		return _body_data_ptr->get_restitution_coefficient(this);
	}

	float32 sycl_rigid_body::get_friction_coefficient() const
	{
		return _body_data_ptr->get_friction_coefficient(this);
	}

	collision_entity::type sycl_rigid_body::get_collision_entity_type() const
	{
		return _body_data_ptr->get_collision_entity_type(this);
	}

	collision_behavior::type sycl_rigid_body::get_collision_behavior_type() const
	{
		return _body_data_ptr->get_collision_behavior_type(this);
	}

	body_mobility sycl_rigid_body::get_mobility() const
	{
		return _body_data_ptr->get_mobility(this);
	}

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
	void sycl_rigid_body::update_collision_state(adaptive_axis_aligned_bounding_box const& bounds, collision_state state)
	{
		_collision_state = state;
		
		basic_color color = basic_color::white;
		switch (state)
		{
		case collision_state::none:
			color = basic_color::green;
			break;
		case collision_state::bounds:
			color = basic_color::yellow;
			break;
		case collision_state::geometry:
			color = basic_color::red;
			break;
		}

		_debug_bounds_frame.set_transform(transform{ bounds.center(), identity_quaternion, bounds.extent() });
		_debug_bounds_frame.set_color(color);
	}

	collision_state sycl_rigid_body::query_collision_state() const
	{
		return _collision_state;
	}

	void sycl_rigid_body::toggle_bounds_frame()
	{
		auto visibility = uint8(_debug_bounds_frame.get_visibility());
		_debug_bounds_frame.set_visibility(body_visibility(!visibility));
	}

	bool sycl_rigid_body::is_bounds_frame_visible() const
	{
		return _debug_bounds_frame.get_visibility() == body_visibility::visible;
	}
#endif

	void sycl_rigid_body::_copy_body_data(std::shared_ptr<implementation::sycl_rigid_body_data> const& new_body_data)
	{
		new_body_data->allocate_body_data(this,
			_body_data_ptr->get_mass(this), _body_data_ptr->get_inertia(this),
			_body_data_ptr->get_position(this), _body_data_ptr->get_rotation(this),
			_body_data_ptr->get_scaling(this), _body_data_ptr->get_geometry(this),
			_body_data_ptr->get_restitution_coefficient(this),
			_body_data_ptr->get_friction_coefficient(this),
			_body_data_ptr->get_collision_entity_type(this),
			_body_data_ptr->get_collision_behavior_type(this),
			_body_data_ptr->get_mobility(this),
			true
		);

		new_body_data->set_worldspace_force(this, _body_data_ptr->get_worldspace_force(this));
		new_body_data->set_worldspace_torque(this, _body_data_ptr->get_worldspace_torque(this));
		new_body_data->set_bodyspace_force(this, _body_data_ptr->get_bodyspace_force(this));
		new_body_data->set_bodyspace_torque(this, _body_data_ptr->get_bodyspace_torque(this));
		new_body_data->set_linear_momentum(this, _body_data_ptr->get_linear_momentum(this));
		new_body_data->set_angular_momentum(this, _body_data_ptr->get_angular_momentum(this));
	}
}

#endif
