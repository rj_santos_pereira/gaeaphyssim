#ifndef ENGINE_PHYSICS_SYCL_PHYSICS_INCLUDE_GUARD
#define ENGINE_PHYSICS_SYCL_PHYSICS_INCLUDE_GUARD

#include "core/core_sycl.hpp"

#include "engine/physics/physics_globals.hpp"
#include "library/math/bounding_box.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

#if GAEA_USING_SYCL

namespace gaea::sycl_type
{
	class sequence_event
	{
	public:
		sequence_event();
		sequence_event(sycl::event const& first, sycl::event const& last);

		sequence_event(sequence_event const& other);
		sequence_event(sequence_event&& other) noexcept;

		sequence_event& operator=(sequence_event const& other);
		sequence_event& operator=(sequence_event&& other) noexcept;

		template <sycl::info::event ParamType>
		typename sycl::info::param_traits<sycl::info::event, ParamType>::return_type get_info() const;

		template <sycl::info::event_profiling ParamType>
		typename sycl::info::param_traits<sycl::info::event_profiling, ParamType>::return_type get_profiling_info() const;

		void wait();

		void wait_and_throw();

	private:
		sycl::event _first;
		sycl::event _last;

	};

	template <std::size_t NumEventsValue>
	class sequence_event_buffer
	{
	public:
		explicit sequence_event_buffer()
			: sequence_event_buffer{ 0 }
		{
		}
		explicit sequence_event_buffer(std::size_t prev_events_count)
			: _prev_events_array(prev_events_count)
			, _curr_events{}
			, _prev_events_count{ prev_events_count }
			, _prev_events_index{ 0 }
		{
		}

		fixed_array<sequence_event, NumEventsValue>& previous_events(std::size_t index = 0)
		{
			if (index >= _prev_events_count)
			{
				GAEA_CRASH("Trying to access an element beyond the capacity of the array!");
			}

			return _prev_events_array[(_prev_events_index - index - 1) % _prev_events_count];
		}
		fixed_array<sequence_event, NumEventsValue>& current_events()
		{
			return _curr_events;
		}

		void advance_events()
		{
			// TODO HACK if we need _prev_events_count == 0, uncomment this
			//if (_prev_events_count)
			{
				_prev_events_array[_prev_events_index % _prev_events_count] = std::move(_curr_events);
				++_prev_events_index;
			}

			_curr_events = fixed_array<sequence_event, NumEventsValue>();
		}

		bool has_previous_events(std::size_t index = 0) const
		{
			if (index >= _prev_events_count)
			{
				return false;
			}

			return index < _prev_events_index;
		}

		std::size_t num_previous_events() const
		{
			return _prev_events_count;
		}

	private:
		array<fixed_array<sequence_event, NumEventsValue>> _prev_events_array;
		fixed_array<sequence_event, NumEventsValue> _curr_events;

		std::size_t _prev_events_count;
		std::size_t _prev_events_index;
	};

	template <class Type>
	class ring_buffer
	{
	public:
		explicit ring_buffer()
			: ring_buffer{ 0, sycl::range<1>{ 0 } }
		{
		}

		explicit ring_buffer(std::size_t prev_count, sycl::range<1> curr_capacity)
			: _prev_buffer_array(prev_count)
			, _curr_buffer{}
			, _next_buffer{}
			, _prev_buffer_count{ prev_count }
			, _prev_buffer_index{ 0 }
		{
			if (curr_capacity.size() > 0)
			{
				_curr_buffer = sycl::buffer<Type>{ curr_capacity };
				_next_buffer = sycl::buffer<Type>{ curr_capacity };
			}
		}

		sycl::buffer<Type>& previous_buffer(std::size_t index = 0)
		{
			if (index >= _prev_buffer_count)
			{
				GAEA_CRASH("Trying to access an element beyond the capacity of the array!");
			}

			return _prev_buffer_array[(_prev_buffer_index - index - 1) % _prev_buffer_count];
		}

		sycl::buffer<Type>& current_buffer()
		{
			return _curr_buffer;
		}

		sycl::buffer<Type>& next_buffer()
		{
			return _next_buffer;
		}

		void advance_buffer(bool keep_curr_buffer)
		{
			if (_prev_buffer_count && keep_curr_buffer)
			{
				_prev_buffer_array[_prev_buffer_index % _prev_buffer_count] = std::move(_curr_buffer);
				++_prev_buffer_index;
			}

			_curr_buffer = _next_buffer;
			_next_buffer = sycl::buffer<Type>{ _curr_buffer.get_range() };
		}

		bool has_previous_buffer(std::size_t index = 0) const
		{
			if (index >= _prev_buffer_count)
			{
				return false;
			}

			return index < _prev_buffer_index
				&& _prev_buffer_array[(_prev_buffer_index - index - 1) % _prev_buffer_count].get_range().size() > 0;
		}

		std::size_t num_previous_buffers() const
		{
			return _prev_buffer_count;
		}

	private:
		array<sycl::buffer<Type>> _prev_buffer_array;
		sycl::buffer<Type> _curr_buffer;
		sycl::buffer<Type> _next_buffer;

		std::size_t _prev_buffer_count;
		std::size_t _prev_buffer_index;
	};
}

namespace gaea::sycl_data
{
	struct physics_vec_data
	{
		float32* operator[](uint32 index)
		{
			static_assert(sizeof(vector) == 12);
			return reinterpret_cast<float32*>(reinterpret_cast<std::byte*>(this) + 12 * index);
		}
		float32 const* operator[](uint32 index) const
		{
			return const_cast<physics_vec_data*>(this)->operator[](index);
		}
		
		vector linear;
		vector angular;
	};

	struct physics_vecquat_data
	{
		float32* operator[](uint32 index)
		{
			static_assert(sizeof(vector) == 12 && sizeof(quaternion) == 16);
			return reinterpret_cast<float32*>(reinterpret_cast<std::byte*>(this) + 14 * index - 2 * (index & 0x1));
		}
		float32 const* operator[](uint32 index) const
		{
			return const_cast<physics_vecquat_data*>(this)->operator[](index);
		}

		vector linear;
		quaternion angular;
	};

	struct force_data
	{
		float32* operator[](uint32 index)
		{
			static_assert(sizeof(vector) == 12);
			return reinterpret_cast<float32*>(reinterpret_cast<std::byte*>(this) + 12 * index);
		}
		float32 const* operator[](uint32 index) const
		{
			return const_cast<force_data*>(this)->operator[](index);
		}
		
		vector worldspace_force;
		vector bodyspace_force;

		vector worldspace_torque;
		vector bodyspace_torque;
	};

	struct mass_data
	{
		float32* operator[](uint32 index)
		{
			static_assert(sizeof(float32) == 4 && sizeof(matrix3) == 36);
			return reinterpret_cast<float32*>(reinterpret_cast<std::byte*>(this) + 20 * index - 16 * (index & 0x1));
		}
		float32 const* operator[](uint32 index) const
		{
			return const_cast<mass_data*>(this)->operator[](index);
		}
		
		float32 mass;
		matrix3 inertia;

		float32 mass_inverse;
		matrix3 inertia_inverse;
	};

	struct statics_data
	{
		explicit statics_data()
			: restitution_coefficient{ 0.f }
			, friction_coefficient{ 0.f }
			, entity_type{ collision_entity::null }
			, behavior_type{ collision_behavior::ignore_all }
			, mobility{ 0 }
		{
		}

		float32 restitution_coefficient;
		float32 friction_coefficient;

		collision_entity::type entity_type;
		collision_behavior::type behavior_type;

		uint8 mobility;
	};

	struct dynamics_data
	{
		explicit dynamics_data()
			: collision_index{ 0xFFFFFFFF }
			, collision_group{ 0xF }
			, collision_state{ 0x0 }
		{
		}

		uint32 collision_index;
		uint8 collision_group : 4;
		uint8 collision_state : 2;
	};

	struct momentum_data
	{
		float32* operator[](uint32 index)
		{
			static_assert(sizeof(vector) == 12);
			return reinterpret_cast<float32*>(reinterpret_cast<std::byte*>(this) + 12 * index);
		}
		float32 const* operator[](uint32 index) const
		{
			return const_cast<momentum_data*>(this)->operator[](index);
		}
		
		vector linear_momentum;
		vector angular_momentum;
	};

	struct position_data
	{
		float32* operator[](uint32 index)
		{
			static_assert(sizeof(vector) == 12 && sizeof(quaternion) == 16);
			return reinterpret_cast<float32*>(reinterpret_cast<std::byte*>(this) + 14 * index - 2 * (index & 0x1));
		}
		float32 const* operator[](uint32 index) const
		{
			return const_cast<position_data*>(this)->operator[](index);
		}
		
		vector position;
		quaternion rotation;
	};

	using geometry_face_data = gaea::polyhedron_face_data;
	using geometry_vertex_data = gaea::polyhedron_vertex_data;
	using geometry_halfedge_data = gaea::polyhedron_halfedge_data;

	struct geometry_poly_data
	{
		void move_by(vector const& translation, quaternion const& rotation,
					 sycl::accessor<geometry_face_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_face_accessor,
					 sycl::accessor<geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_accessor,
					 sycl::accessor<geometry_face_data, 1, sycl::access::mode::write, sycl::access::target::global_buffer> const& geometry_face_compute_accessor,
					 sycl::accessor<geometry_vertex_data, 1, sycl::access::mode::write, sycl::access::target::global_buffer> const& geometry_vertex_compute_accessor) const
		{
			matrix3 rot_mat = rotation.to_matrix();

			for (uint32 index = face_index, count = face_index + face_count; index < count; ++index)
			{
				geometry_face_compute_accessor[index].normal = rot_mat * geometry_face_accessor[index].normal;
				geometry_face_compute_accessor[index].edge_index = geometry_face_accessor[index].edge_index;
			}
			for (uint32 index = vertex_index, count = vertex_index + vertex_count; index < count; ++index)
			{
				geometry_vertex_compute_accessor[index].point = rot_mat * geometry_vertex_accessor[index].point + translation;
				geometry_vertex_compute_accessor[index].edge_index = geometry_vertex_accessor[index].edge_index;
			}
		}

		vector support_vertex(vector const& direction,
							  sycl::accessor<geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_accessor) const
		{
			vector support_vertex = zero_vector;
			float32 maximal_projection = -std::numeric_limits<float32>::max();

			for (uint32 index = vertex_index, count = vertex_index + vertex_count; index < count; ++index)
			{
				auto vertex = geometry_vertex_accessor[index].point;
				float32 projection = vector::dot(vertex, direction);

				if (projection > maximal_projection)
				{
					maximal_projection = projection;
					support_vertex = vertex;
				}
			}

			return support_vertex;
		}

		vector center(sycl::accessor<geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_accessor) const
		{
			vector center = zero_vector;

			for (uint32 index = vertex_index, count = vertex_index + vertex_count; index < count; ++index)
			{
				center += geometry_vertex_accessor[index].point;
			}

			return vertex_count > 0
				? center / float32(vertex_count)
				: center;
		}

		uint32 face_index = 0;
		uint32 face_count = 0;
		
		uint32 vertex_index = 0;
		uint32 vertex_count = 0;
		
		uint32 halfedge_index = 0;
		uint32 halfedge_count = 0;
	};

	struct geometry_data
	{
		geometry_poly_data poly;
		adaptive_axis_aligned_bounding_box bounds;
	};

	struct flags_data
	{
		explicit flags_data()
			: has_update{ false }
			, should_update_mass{ false }
			, should_update_statics{ false }
			, should_update_momentum{ false }
			, should_update_position{ false }
			, should_update_geometry{ false }
			, _padding{ 0 }
		{
		}

		uint8 has_update : 1;
		uint8 should_update_mass : 1;
		uint8 should_update_statics : 1;
		uint8 should_update_momentum : 1;
		uint8 should_update_position : 1;
		uint8 should_update_geometry : 1;

		uint8 _padding : 2;
	};

	static_assert(sizeof(flags_data) == 1);

}

namespace gaea::sycl_kernel
{
	inline constexpr uint32 num_partition_dimensions = 3;
	inline constexpr uint32 num_constraint_partitions = implementation::constant_power(2u, num_partition_dimensions);

	template <class UniqueName>
	struct kernel_parallel_reduce;

	template <class UniqueName, class Type, class FuncType>
	sycl::event parallel_reduce(sycl::queue& command_queue, uint32 num_elements, sycl::buffer<Type>& elements_buffer, FuncType reduce_func)
	{
		sycl::event reduce_event;

		while (num_elements > 1)
		{
			uint32 max_work_group_size = uint32(command_queue.get_device().get_info<sycl::info::device::max_work_group_size>());

			uint32 num_work_groups = uint32(std::ceil(float32(num_elements) / float32(max_work_group_size)));
			uint32 num_work_group_elements = num_work_groups > 1 ? max_work_group_size : num_elements;

			reduce_event = command_queue.submit(
				[&] (sycl::handler& command_group)
				{
					auto gl_elements_accessor = elements_buffer.template get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
					auto l_elements_accessor = sycl::accessor<Type, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_elements, command_group);

					command_group.parallel_for<kernel_parallel_reduce<UniqueName>>(
						sycl::nd_range<1>{ num_work_groups * num_work_group_elements, num_work_group_elements },
						[=] (sycl::nd_item<1> work_item)
						{
							uint32 g_elem_idx = uint32(work_item.get_group_linear_id());
							uint32 l_elem_idx = uint32(work_item.get_local_linear_id());

							uint32 work_group_element_offset = g_elem_idx * num_work_group_elements;
							
							uint32 gl_elem_idx = work_group_element_offset + l_elem_idx;

							if (gl_elem_idx < num_elements)
							{
								l_elements_accessor[l_elem_idx] = gl_elements_accessor[gl_elem_idx];
							}

							work_item.barrier(sycl::access::fence_space::local_space);

							for (uint32 stride = 1; stride < num_work_group_elements; stride <<= 1)
							{
								uint32 reduce_elem_idx = l_elem_idx * (stride << 1);
								uint32 stride_elem_idx = reduce_elem_idx + stride;

								if (stride_elem_idx < num_work_group_elements && stride_elem_idx + work_group_element_offset < num_elements)
								{
									l_elements_accessor[reduce_elem_idx]
										= reduce_func(l_elements_accessor[reduce_elem_idx], l_elements_accessor[stride_elem_idx]);
								}

								work_item.barrier(sycl::access::fence_space::local_space);
							}

							if (l_elem_idx == 0)
							{
								gl_elements_accessor[g_elem_idx] = l_elements_accessor[0];
							}
						}
					);
				}
			);

			num_elements = num_work_groups;
		}

		return reduce_event;
	}
	
	sycl_type::sequence_event swap_buffers(sycl::queue& command_queue, uint32 num_bodies,
										   sycl::buffer<sycl_data::mass_data>& old_mass_buffer, sycl::buffer<sycl_data::mass_data>& new_mass_buffer,
										   sycl::buffer<sycl_data::statics_data>& old_statics_buffer, sycl::buffer<sycl_data::statics_data>& new_statics_buffer,
										   sycl::buffer<sycl_data::momentum_data>& old_momentum_buffer, sycl::buffer<sycl_data::momentum_data>& new_momentum_buffer,
										   sycl::buffer<sycl_data::position_data>& old_position_buffer, sycl::buffer<sycl_data::position_data>& new_position_buffer,
										   sycl::buffer<sycl_data::geometry_data>& old_geometry_buffer, sycl::buffer<sycl_data::geometry_data>& new_geometry_buffer,
										   sycl::buffer<sycl_data::geometry_face_data>& old_geom_poly_face_buffer, sycl::buffer<sycl_data::geometry_face_data>& new_geom_poly_face_buffer,
										   sycl::buffer<sycl_data::geometry_vertex_data>& old_geom_poly_vertex_buffer, sycl::buffer<sycl_data::geometry_vertex_data>& new_geom_poly_vertex_buffer,
										   sycl::buffer<sycl_data::geometry_halfedge_data>& old_geom_poly_halfedge_buffer, sycl::buffer<sycl_data::geometry_halfedge_data>& new_geom_poly_halfedge_buffer);
	
	sycl_type::sequence_event update_buffers(sycl::queue& command_queue, uint32 num_bodies,
											 sycl::buffer<sycl_data::mass_data>& old_mass_buffer, sycl::buffer<sycl_data::mass_data>& new_mass_buffer,
											 sycl::buffer<sycl_data::statics_data>& old_statics_buffer, sycl::buffer<sycl_data::statics_data>& new_statics_buffer,
											 sycl::buffer<sycl_data::momentum_data>& old_momentum_buffer, sycl::buffer<sycl_data::momentum_data>& new_momentum_buffer,
											 sycl::buffer<sycl_data::position_data>& old_position_buffer, sycl::buffer<sycl_data::position_data>& new_position_buffer,
											 sycl::buffer<sycl_data::geometry_data>& old_geometry_buffer, sycl::buffer<sycl_data::geometry_data>& new_geometry_buffer,
											 sycl::buffer<sycl_data::geometry_face_data>& old_geom_poly_face_buffer, sycl::buffer<sycl_data::geometry_face_data>& new_geom_poly_face_buffer,
											 sycl::buffer<sycl_data::geometry_vertex_data>& old_geom_poly_vertex_buffer, sycl::buffer<sycl_data::geometry_vertex_data>& new_geom_poly_vertex_buffer,
											 sycl::buffer<sycl_data::geometry_halfedge_data>& old_geom_poly_halfedge_buffer, sycl::buffer<sycl_data::geometry_halfedge_data>& new_geom_poly_halfedge_buffer,
											 sycl::buffer<sycl_data::flags_data>& flags_buffer);
}

#endif

#endif
