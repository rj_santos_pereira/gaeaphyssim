
// Declare collision entity variables

GAEA_DECLARE_COLLISION_ENTITY(world_static);
GAEA_DECLARE_COLLISION_ENTITY(world_dynamic);

// Declare collision behavior variables

GAEA_DECLARE_COLLISION_BEHAVIOR(world_static,
	world_static -> ignore,
	world_dynamic -> resolve
);
GAEA_DECLARE_COLLISION_BEHAVIOR(world_dynamic,
	world_static -> resolve,
	world_dynamic -> resolve
);
