#ifndef ENGINE_PHYSICS_PHYSICS_GLOBALS_INCLUDE_GUARD
#define ENGINE_PHYSICS_PHYSICS_GLOBALS_INCLUDE_GUARD

#include "core/core_common.hpp"

#include "library/math/matrix.hpp"
#include "library/math/polyhedron.hpp"

// Quaternion integration and infinitesimal rotations:
// https://math.stackexchange.com/questions/1693067/differences-between-quaternion-integration-methods
// https://www.ashwinnarayan.com/post/how-to-integrate-quaternions/
// https://fgiesen.wordpress.com/2012/08/24/quaternion-differentiation/
// https://mathworld.wolfram.com/InfinitesimalRotation.html
// https://rotations.berkeley.edu/infinitesimal-rotations/

// Moments and products of inertia
// https://dynamicsmotioncontrol487379916.files.wordpress.com/2020/12/25-me659inertiamatrix.pdf

namespace gaea::implementation
{
	GAEA_DECLARE_CONSTANT_COUNTER(create_collision_entity, 32);

	template <class GenericEntityType, class ... EntityTypes>
	constexpr uint64 create_collision_behavior(GenericEntityType&& generic_entity, EntityTypes&& ... entities)
	{
		return sizeof...(entities) > 0 ? (0 | ... | uint64(entities)) : generic_entity;
	}

	class collision_entity_behavior
	{
		struct _collision_result
		{
			constexpr _collision_result(uint64 channel)
				: ignore{ 0b00 }
				, generate{ channel * 0b01 }
				, resolve{ channel * 0b11 }
			{
			}

			uint64 const ignore;
			uint64 const generate;
			uint64 const resolve;
		};

	public:
		constexpr collision_entity_behavior(uint64 channel)
			: _result{ channel }
		{
		}

		constexpr _collision_result const* operator->() const
		{
			return &_result;
		}

		constexpr operator uint64() const
		{
			return _result.resolve;
		}

	private:
		_collision_result _result;

	};

	// Declare special pseudo-entity used in empty declarations
	constexpr uint64 _collision_all = 0xFFFF'FFFF'FFFF'FFFF;
}

namespace gaea
{
	enum class collision_result			: uint8
	{
		ignore							= 0b00,
		generate						= 0b01,
		resolve							= 0b11,
	};
	
	enum class collision_mode			: uint8
	{
		always_zero						= 0x00,
		always_one						= 0x1F,
		
		minimum							= 0x01,
		geometric						= 0x03,
		arithmetic						= 0x07,
		maximum							= 0x0F,
	};

	enum class collision_state			: uint8
	{
		none							= 0b00,
		bounds							= 0b01,
		geometry						= 0b10,
	};

	namespace collision_entity
	{
		class type
		{
		public:
			explicit constexpr type(uint8 id)
				: _id{ id }
			{
			}

			constexpr operator uint8() const
			{
				return _id;
			}

		private:
			uint8 _id;

		};

		constexpr type null{ 0x0 };
	}
	
	namespace collision_behavior
	{
		class type
		{
		public:
			explicit constexpr type(uint64 id)
				: _id_0{ uint32(id) }
				, _id_1{ uint32(id >> 32) }
			{
			}

			constexpr operator uint64() const
			{
				return uint64(_id_0) | uint64(_id_1) << 32;
			}

		private:
			uint32 _id_0;
			uint32 _id_1;

		};

		// Declare special collision behaviors
		constexpr type ignore_all{ 0x0 };
		constexpr type generate_all{ 0x5555'5555'5555'5555 };
		constexpr type resolve_all{ 0xFFFF'FFFF'FFFF'FFFF };
	}

	inline collision_result calculate_collision_result(collision_entity::type body_1_entity, collision_behavior::type body_1_behavior, 
													   collision_entity::type body_2_entity, collision_behavior::type body_2_behavior)
	{
		return collision_result((body_2_behavior >> body_1_entity) & (body_1_behavior >> body_2_entity));
	}
	
	inline bool should_collide(collision_entity::type body_1_entity, collision_behavior::type body_1_behavior, 
							   collision_entity::type body_2_entity, collision_behavior::type body_2_behavior)
	{
		return ((body_2_behavior >> body_1_entity) & (body_1_behavior >> body_2_entity)) != 0;
	}
}

#define GAEA_QUALIFY_COLLISION_ENTITY(name) uint64(GAEA_TOKEN_CONCATENATION(implementation::_collision_, name))

#define GAEA_DECLARE_COLLISION_ENTITY(name) \
	namespace gaea::implementation																						\
	{																													\
		GAEA_INCREMENT_CONSTANT_COUNTER(create_collision_entity);														\
																														\
		constexpr collision_entity_behavior GAEA_TOKEN_CONCATENATION(_collision_, name) {								\
			uint64(1) << 2 * (GAEA_READ_CONSTANT_COUNTER(create_collision_entity) - 1)									\
		};																												\
	}																													\
	namespace gaea::collision_entity																					\
	{																													\
		constexpr type name {																							\
			2 * (GAEA_READ_CONSTANT_COUNTER(implementation::create_collision_entity) - 1)								\
		};																												\
	}

#define GAEA_DECLARE_COLLISION_BEHAVIOR(name, ...)	\
	namespace gaea::collision_behavior																					\
	{																													\
		constexpr type name {																							\
			implementation::create_collision_behavior(																	\
				GAEA_MACRO_APPLY(GAEA_QUALIFY_COLLISION_ENTITY, all, __VA_ARGS__)										\
			)																											\
		};																												\
	}

#include "engine/physics/collision_components.inl"

#undef GAEA_QUALIFY_COLLISION_ENTITY
#undef GAEA_DECLARE_COLLISION_ENTITY
#undef GAEA_DECLARE_COLLISION_BEHAVIOR

namespace gaea
{
	class base_rigid_body
	{
	protected:
		base_rigid_body() = default;
		~base_rigid_body() = default;

	public:
		base_rigid_body(base_rigid_body const&) = delete;
		base_rigid_body(base_rigid_body&&) = delete;

		base_rigid_body& operator=(base_rigid_body const&) = delete;
		base_rigid_body& operator=(base_rigid_body&&) = delete;

		virtual void setup_body() = 0;
		virtual void setdown_body() = 0;

		virtual void apply_world_force(vector const& force) = 0;
		virtual void apply_body_force(vector const& force) = 0;

		virtual void apply_world_torque(vector const& torque) = 0;
		virtual void apply_body_torque(vector const& torque) = 0;

		virtual void clear_force_and_torque() = 0;

		// Computing accessors

		virtual vector query_world_linear_acceleration() const = 0;
		virtual vector query_body_linear_acceleration() const = 0;

		virtual vector query_world_angular_acceleration() const = 0;
		virtual vector query_body_angular_acceleration() const = 0;

		virtual vector query_world_linear_velocity() const = 0;
		virtual vector query_body_linear_velocity() const = 0;

		virtual vector query_world_angular_velocity() const = 0;
		virtual vector query_body_angular_velocity() const = 0;

		// Mutating accessors

		virtual vector& access_force() = 0;
		virtual vector& access_torque() = 0;

		virtual vector& access_linear_momentum() = 0;
		virtual vector& access_angular_momentum() = 0;

		virtual vector& access_position() = 0;
		virtual quaternion& access_rotation() = 0;

		virtual void set_force(vector const& force) = 0;
		virtual void set_torque(vector const& torque) = 0;

		virtual void set_linear_momentum(vector const& linear_momentum) = 0;
		virtual void set_angular_momentum(vector const& angular_momentum) = 0;

		virtual void set_position(vector const& position) = 0;
		virtual void set_rotation(quaternion const& rotation) = 0;

		virtual void set_mass(float32 mass) = 0;
		virtual void set_inertia(matrix3 const& inertia_tensor) = 0;

		// Non-mutating accessors

		virtual vector const& get_force() const = 0;
		virtual vector const& get_torque() const = 0;

		virtual vector const& get_linear_momentum() const = 0;
		virtual vector const& get_angular_momentum() const = 0;

		virtual vector const& get_position() const = 0;
		virtual quaternion const& get_rotation() const = 0;

		virtual float32 const& get_mass() const = 0;
		virtual float32 const& get_mass_inverse() const = 0;

		virtual matrix3 const& get_inertia() const = 0;
		virtual matrix3 const& get_inertia_inverse() const = 0;

		// Geometry accessors

		virtual void set_scaling(vector const& scaling) = 0;

		virtual void set_geometry(polyhedron geometry) = 0;

		virtual vector const& get_scaling() const = 0;

		virtual polyhedron get_geometry() const = 0;

		virtual void set_restitution_coefficient(float32 restitution_coefficient) = 0;
		virtual void set_friction_coefficient(float32 friction_coefficient) = 0;

		virtual void set_collision_entity_type(collision_entity::type entity_type) = 0;
		virtual void set_collision_behavior_type(collision_behavior::type behavior_type) = 0;

		virtual void set_mobility(body_mobility mobility) = 0;

		virtual float32 get_restitution_coefficient() const = 0;
		virtual float32 get_friction_coefficient() const = 0;

		virtual collision_entity::type get_collision_entity_type() const = 0;
		virtual collision_behavior::type get_collision_behavior_type() const = 0;

		virtual body_mobility get_mobility() const = 0;
	};
	
	struct simulation_primitives
	{
		static inline polyhedron cube{ {
			polyhedron_edge{ vector{ -0.5f, -0.5f, +0.5f }, vector{ -0.5f, +0.5f, +0.5f }, 0, 5 },
			polyhedron_edge{ vector{ -0.5f, +0.5f, +0.5f }, vector{ -0.5f, +0.5f, -0.5f }, 0, 4 },
			polyhedron_edge{ vector{ -0.5f, +0.5f, -0.5f }, vector{ -0.5f, -0.5f, -0.5f }, 0, 2 },
			polyhedron_edge{ vector{ -0.5f, -0.5f, -0.5f }, vector{ -0.5f, -0.5f, +0.5f }, 0, 1 },

			polyhedron_edge{ vector{ +0.5f, -0.5f, +0.5f }, vector{ -0.5f, -0.5f, +0.5f }, 1, 5 },
			polyhedron_edge{ vector{ -0.5f, -0.5f, +0.5f }, vector{ -0.5f, -0.5f, -0.5f }, 1, 0 },
			polyhedron_edge{ vector{ -0.5f, -0.5f, -0.5f }, vector{ +0.5f, -0.5f, -0.5f }, 1, 2 },
			polyhedron_edge{ vector{ +0.5f, -0.5f, -0.5f }, vector{ +0.5f, -0.5f, +0.5f }, 1, 3 },

			polyhedron_edge{ vector{ -0.5f, -0.5f, -0.5f }, vector{ -0.5f, +0.5f, -0.5f }, 2, 0 },
			polyhedron_edge{ vector{ -0.5f, +0.5f, -0.5f }, vector{ +0.5f, +0.5f, -0.5f }, 2, 4 },
			polyhedron_edge{ vector{ +0.5f, +0.5f, -0.5f }, vector{ +0.5f, -0.5f, -0.5f }, 2, 3 },
			polyhedron_edge{ vector{ +0.5f, -0.5f, -0.5f }, vector{ -0.5f, -0.5f, -0.5f }, 2, 1 },

			polyhedron_edge{ vector{ +0.5f, +0.5f, +0.5f }, vector{ +0.5f, -0.5f, +0.5f }, 3, 5 },
			polyhedron_edge{ vector{ +0.5f, -0.5f, +0.5f }, vector{ +0.5f, -0.5f, -0.5f }, 3, 1 },
			polyhedron_edge{ vector{ +0.5f, -0.5f, -0.5f }, vector{ +0.5f, +0.5f, -0.5f }, 3, 2 },
			polyhedron_edge{ vector{ +0.5f, +0.5f, -0.5f }, vector{ +0.5f, +0.5f, +0.5f }, 3, 4 },

			polyhedron_edge{ vector{ -0.5f, +0.5f, +0.5f }, vector{ +0.5f, +0.5f, +0.5f }, 4, 5 },
			polyhedron_edge{ vector{ +0.5f, +0.5f, +0.5f }, vector{ +0.5f, +0.5f, -0.5f }, 4, 3 },
			polyhedron_edge{ vector{ +0.5f, +0.5f, -0.5f }, vector{ -0.5f, +0.5f, -0.5f }, 4, 2 },
			polyhedron_edge{ vector{ -0.5f, +0.5f, -0.5f }, vector{ -0.5f, +0.5f, +0.5f }, 4, 0 },

			polyhedron_edge{ vector{ +0.5f, -0.5f, +0.5f }, vector{ +0.5f, +0.5f, +0.5f }, 5, 3 },
			polyhedron_edge{ vector{ +0.5f, +0.5f, +0.5f }, vector{ -0.5f, +0.5f, +0.5f }, 5, 4 },
			polyhedron_edge{ vector{ -0.5f, +0.5f, +0.5f }, vector{ -0.5f, -0.5f, +0.5f }, 5, 0 },
			polyhedron_edge{ vector{ -0.5f, -0.5f, +0.5f }, vector{ +0.5f, -0.5f, +0.5f }, 5, 1 },
		} };
	};
	
	struct inertia_tensors
	{
		static matrix3 sphere(float32 mass, float32 radius, bool is_hollow = false)
		{
			float32 radius_squared = radius * radius;
			
			float32 moment = 2.0f / (is_hollow ? 3.0f : 5.0f) * mass * radius_squared;

			return matrix3{ {
				moment, 0.0f, 0.0f,
				0.0f, moment, 0.0f,
				0.0f, 0.0f, moment,
			} };
		}

		static matrix3 tetrahedron(float32 mass, float32 side, bool is_hollow = false)
		{
			float32 side_squared = side * side;
			
			float32 moment = 1.0f / (is_hollow ? 12.0f : 20.0f) * mass * side_squared;

			return matrix3{ {
				moment, 0.0f, 0.0f,
				0.0f, moment, 0.0f,
				0.0f, 0.0f, moment,
			} };
		}

		static matrix3 octahedron(float32 mass, float32 side, bool is_hollow = false)
		{
			float32 side_squared = side * side;
			
			float32 moment = 1.0f / (is_hollow ? 6.0f : 10.0f) * mass * side_squared;

			return matrix3{ {
				moment, 0.0f, 0.0f,
				0.0f, moment, 0.0f,
				0.0f, 0.0f, moment,
			} };
		}

		static matrix3 cube(float32 mass, float32 side)
		{
			float32 side_squared = side * side;
			
			float32 moment = 1.0f / 6.0f * mass * side_squared;

			return matrix3{ {
				moment, 0.0f, 0.0f,
				0.0f, moment, 0.0f,
				0.0f, 0.0f, moment,
			} };
		}

		static matrix3 cuboid(float32 mass, float32 depth, float32 width, float32 height)
		{
			float32 depth_squared = depth * depth;
			float32 width_squared = width * width;
			float32 height_squared = height * height;

			float32 moment_x = 1.0f / 12.0f * mass * (width_squared + height_squared);
			float32 moment_y = 1.0f / 12.0f * mass * (height_squared + depth_squared);
			float32 moment_z = 1.0f / 12.0f * mass * (depth_squared + width_squared);

			return matrix3{ {
				moment_x, 0.0f, 0.0f,
				0.0f, moment_y, 0.0f,
				0.0f, 0.0f, moment_z,
			} };
		}

		// Base in XY
		static matrix3 cylinder(float32 mass, float32 radius, float32 height)
		{
			float32 radius_squared = radius * radius;
			float32 height_squared = height * height;

			float32 moment_xy = 1.0f / 12.0f * mass * (3.0f * radius_squared + height_squared);
			float32 moment_z = 1.0f / 2.0f * mass * radius_squared;

			return matrix3{ {
				moment_xy, 0.0f, 0.0f,
				0.0f, moment_xy, 0.0f,
				0.0f, 0.0f, moment_z,
			} };
		}

		// Base in XY
		static matrix3 cone(float32 mass, float32 radius, float32 height)
		{
			float32 radius_squared = radius * radius;
			float32 height_squared = height * height;

			float32 moment_xy = mass * (3.0f / 20.0f * radius_squared + 3.0f / 80.0f * height_squared);
			float32 moment_z = 3.0f / 10.0f * mass * radius_squared;

			return matrix3{ {
				moment_xy, 0.0f, 0.0f,
				0.0f, moment_xy, 0.0f,
				0.0f, 0.0f, moment_z,
			} };
		}
	};
}

#endif
