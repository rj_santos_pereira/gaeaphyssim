#include "engine/physics/native_simulator.hpp"

#include "library/math/vector.hpp"

#define USING_SINGLE_THREAD 0

namespace gaea
{
	native_simulator::native_simulator()
		: _rigid_body_array{}
		, _simulate_thread_array{}
		, _begin_simulate_semaphore{ 0 }
		, _end_simulate_semaphore{ 0 }
		, _bcd_barrier{ 0 }
		, _ncd_barrier{ 0 }
		, _cr_barrier{ 0 }
		, _finished_step_count{ 0 }
		, _bcd_parallel_solver{}
		, _bcd_sequential_solver{}
		, _ncd_solver{ 5.f }
		, _cr_solver{ 3.f }
		, _bcd_parallel_mutex{}
		, _bcd_result{}
		, _ncd_result{}
		, _step_delta{ 0.0f }
		, _max_substep_delta{ 1.0f / 120.0f }
		, _max_substep_count{ 6 }
		, _num_threads{ 0 }
		, _should_run{ true }
		, _should_step{ false }
		, _should_display_bounding_boxes{ false }
		, _is_real_time{ true }
	{
	}

	bool native_simulator::initialize(void* parameters)
	{
		auto simulator_parameters = static_cast<native_simulator_parameters*>(parameters);

		uint32 thread_count = 0, thread_ratio = 2;
		if (simulator_parameters)
		{
			_max_substep_delta = simulator_parameters->max_substep_delta;
			_max_substep_count = simulator_parameters->max_substep_count;

			thread_count = simulator_parameters->thread_count;
			thread_ratio = simulator_parameters->thread_ratio;
		}

		GAEA_INFO(native_simulator, "Creating simulate threads...");

#if USING_SINGLE_THREAD
		_num_threads = 1;
#else
		static uint32 machine_parallelism = std::thread::hardware_concurrency();
		
		_num_threads = std::max<uint32>(
			thread_count > 0
				? thread_count
				: machine_parallelism / (thread_ratio > 0 ? thread_ratio : 2), 
			1
			);
#endif
		
		_simulate_thread_array.reserve(_num_threads);

		_bcd_barrier.set_initial_count(_num_threads);
		_ncd_barrier.set_initial_count(_num_threads);
		_cr_barrier.set_initial_count(_num_threads);

#if GAEA_NATIVE_SORT_SWEEP_USING_PARALLEL_SOLVER
		_bcd_parallel_solver.set_num_threads(_num_threads);
#endif
		_cr_solver.set_num_threads(_num_threads);
		
		for (std::size_t index = 0; index < _num_threads; ++index)
		{
			auto& thread = _simulate_thread_array.emplace_back(std::thread{ &native_simulator::_simulation_loop, this, index, _num_threads });

			if (thread.get_id() == std::thread::id{})
			{
				GAEA_ERROR(native_simulator, "Error creating simulate thread!");
				return false;
			}
		}

		return true;
	}

	void native_simulator::finalize()
	{
		int64 num_threads = _simulate_thread_array.size();

		GAEA_INFO(native_simulator, "Destroying simulate threads...");
		
		_should_run = false;

		_begin_simulate_semaphore.release(num_threads);
		_end_simulate_semaphore.release(num_threads);

		for (auto& thread : _simulate_thread_array)
		{
			thread.join();
		}
	}

	void native_simulator::step(float64 frame_delta)
	{
		int64 num_threads = _simulate_thread_array.size();

		_finished_step_count.store(0, std::memory_order_relaxed);

		// Calculate substep delta and count
		// See https://www.aclockworkberry.com/unreal-engine-substepping/

		_step_delta = float32(frame_delta);
		_should_step = true;

		_begin_simulate_semaphore.release(num_threads);
	}

	void native_simulator::finish_step()
	{
		int64 num_threads = _simulate_thread_array.size();

		while (_finished_step_count.load(std::memory_order_acquire) != num_threads)
		{
			std::this_thread::yield();
		}

		_should_step = false;
		
		_end_simulate_semaphore.release(num_threads);
	}

	bool native_simulator::is_real_time() const
	{
		return _is_real_time;
	}

	void native_simulator::register_body(simulation_object type, void* object)
	{
		if (_should_step)
		{
			GAEA_ERROR(native_simulator, "Trying to register object while stepping!");
			return;
		}
		
		switch (type)
		{
		case simulation_object::rigid_body:
		{
			auto rigid_body = static_cast<native_rigid_body*>(object);

			auto it = std::lower_bound(_rigid_body_array.begin(), _rigid_body_array.end(), rigid_body);
			if (it != _rigid_body_array.end() && *it == rigid_body)
			{
				GAEA_ERROR(native_simulator, "Trying to re-register object!");
				return;
			}

			_rigid_body_array.insert(it, rigid_body);

#if GAEA_NATIVE_SORT_SWEEP_USING_PARALLEL_SOLVER
			_bcd_parallel_solver.add_body(rigid_body);
#else
			_bcd_sequential_solver.add_body(rigid_body);
#endif
			_cr_solver.add_body(rigid_body);

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
			if (_should_display_bounding_boxes != rigid_body->is_bounds_frame_visible())
			{
				rigid_body->toggle_bounds_frame();
			}
#endif

			break;
		}
		default: // NOLINT(clang-diagnostic-covered-switch-default)
			GAEA_ERROR(native_simulator, "Unrecognized object type!");
			break;
		}
	}

	void native_simulator::unregister_body(simulation_object type, void * object)
	{
		if (_should_step)
		{
			GAEA_ERROR(native_simulator, "Trying to unregister object while stepping!");
			return;
		}

		switch (type)
		{
		case simulation_object::rigid_body:
		{
			auto rigid_body = static_cast<native_rigid_body*>(object);

			auto it = std::lower_bound(_rigid_body_array.begin(), _rigid_body_array.end(), rigid_body);
			if (it == _rigid_body_array.end() || *it != rigid_body)
			{
				GAEA_ERROR(native_simulator, "Trying to unregister object which is not registered!");
				return;
			}

			_rigid_body_array.erase(it);

#if GAEA_NATIVE_SORT_SWEEP_USING_PARALLEL_SOLVER
			_bcd_parallel_solver.remove_body(rigid_body);
#else
			_bcd_sequential_solver.remove_body(rigid_body);
#endif

			_cr_solver.remove_body(rigid_body);

			break;
		}
		default: // NOLINT(clang-diagnostic-covered-switch-default)
			GAEA_ERROR(native_simulator, "Unrecognized object type!");
			break;
		}
	}

	void native_simulator::apply_world_force(simulation_vector_type const& force)
	{
		if (_should_step)
		{
			GAEA_ERROR(native_simulator, "Trying to register world force while stepping!");
			return;
		}
		
		_cr_solver.apply_world_force(
			vector{ float32(std::get<0>(force)), float32(std::get<1>(force)), float32(std::get<2>(force)) }
		);
	}

	void native_simulator::apply_world_torque(simulation_vector_type const& force)
	{
		if (_should_step)
		{
			GAEA_ERROR(native_simulator, "Trying to register world force while stepping!");
			return;
		}
		
		_cr_solver.apply_world_torque(
			vector{ float32(std::get<0>(force)), float32(std::get<1>(force)), float32(std::get<2>(force)) }
		);
	}

	void native_simulator::apply_world_lin_accel(simulation_vector_type const& accel)
	{
		if (_should_step)
		{
			GAEA_ERROR(native_simulator, "Trying to register world acceleration while stepping!");
			return;
		}
		
		_cr_solver.apply_world_linear_accel(
			vector{ float32(std::get<0>(accel)), float32(std::get<1>(accel)), float32(std::get<2>(accel)) }
		);
	}

	void native_simulator::apply_world_ang_accel(simulation_vector_type const& accel)
	{
		if (_should_step)
		{
			GAEA_ERROR(native_simulator, "Trying to register world acceleration while stepping!");
			return;
		}
		
		_cr_solver.apply_world_angular_accel(
			vector{ float32(std::get<0>(accel)), float32(std::get<1>(accel)), float32(std::get<2>(accel)) }
		);
	}

	float32 native_simulator::query_step_duration() const
	{
		return stdchr::duration<float32>(_step_end_time - _bcd_begin_time).count();
	}

	float32 native_simulator::query_collision_detection_duration() const
	{
		return stdchr::duration<float32>(_cr_begin_time - _bcd_begin_time).count();
	}

	float32 native_simulator::query_broad_phase_duration() const
	{
		return stdchr::duration<float32>(_ncd_begin_time - _bcd_begin_time).count();
	}

	float32 native_simulator::query_narrow_phase_duration() const
	{
		return stdchr::duration<float32>(_cr_begin_time - _ncd_begin_time).count();
	}

	float32 native_simulator::query_collision_resolution_duration() const
	{
		return stdchr::duration<float32>(_step_end_time - _cr_begin_time).count();
	}

	uint32 native_simulator::num_threads() const
	{
		return _num_threads;
	}

	void native_simulator::toggle_bounding_box_display()
	{
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_should_display_bounding_boxes = !_should_display_bounding_boxes;

		std::for_each(_rigid_body_array.begin(), _rigid_body_array.end(),
			[this](native_rigid_body* body)
			{
				if (body->is_bounds_frame_visible() != _should_display_bounding_boxes)
				{
					body->toggle_bounds_frame();
				}
			});
#endif
	}

	bool native_simulator::is_displaying_bounding_boxes() const
	{
		return _should_display_bounding_boxes;
	}
	
	void native_simulator::_simulation_loop(std::size_t thread_index, std::size_t thread_count)
	{
		engine->register_thread(thread_type::simulate);
		
		while (_should_run)
		{
			_begin_simulate_semaphore.acquire();

			if (_should_step && _step_delta > 0.f)
			{
				uint32 substep_count = std::min(_max_substep_count, uint32(std::ceil(_step_delta / _max_substep_delta)));
				float32 substep_delta = substep_count > 0 ? _step_delta / float32(substep_count) : 0.f;

				_is_real_time = substep_delta <= _max_substep_delta;
				if (!_is_real_time)
				{
					substep_delta = _max_substep_delta;
				}

				// 1. Broad-phase collision detection

				std::unique_lock bcd_lock{ _bcd_time_mutex, std::try_to_lock };
				if (bcd_lock)
				{
					_bcd_begin_time = stdchr::steady_clock::now();
				}

#if GAEA_NATIVE_SORT_SWEEP_USING_PARALLEL_SOLVER
				_parallel_sort_sweep(thread_index, thread_count);
#else
				_sequential_sort_sweep(thread_index, thread_count);
#endif
				// 2. Narrow-phase collision detection

				std::unique_lock ncd_lock{ _ncd_time_mutex, std::try_to_lock };
				if (ncd_lock)
				{
					_ncd_begin_time = stdchr::steady_clock::now();
				}
				
				_parallel_separating_axis_test(thread_index, thread_count, _step_delta);

				// 3. Collision resolution

				std::unique_lock cr_lock{ _cr_time_mutex, std::try_to_lock };
				if (cr_lock)
				{
					_cr_begin_time = stdchr::steady_clock::now();
				}

				_parallel_local_symplectic_euler(thread_index, thread_count, substep_delta, substep_count);
			}
			else
			{
				// Even if we are not stepping, it is likely that forces are still being applied to the bodies,
				// so reset them and pretend this was part of the single-body dynamics step

				std::unique_lock cr_lock{ _cr_time_mutex, std::try_to_lock };
				if (cr_lock)
				{
					_bcd_begin_time = _ncd_begin_time = _cr_begin_time = stdchr::steady_clock::now();
				}
				
				for (std::size_t index = thread_index; index < _rigid_body_array.size(); index += thread_count)
				{
					auto rigid_body = _rigid_body_array[index];
					
					rigid_body->clear_force_and_torque();
				}
			}

			// Notify finished loop

			if (_finished_step_count.fetch_add(1, std::memory_order_release) == _num_threads - 1)
			{
				_step_end_time = stdchr::steady_clock::now();
			}

			_end_simulate_semaphore.acquire();
		}
	}

	void native_simulator::_sequential_sort_sweep(std::size_t thread_index, std::size_t thread_count)
	{
		for (std::size_t index = thread_index; index < _rigid_body_array.size(); index += thread_count)
		{
			auto body = _rigid_body_array[index];

			body->precalc_bounds();

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
			body->update_collision_state(collision_state::none);
#endif
		}

		_bcd_barrier.wait();

		if (thread_index == 0)
		{
			_bcd_result = _bcd_sequential_solver.compute_bounds_overlap();
		}

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_bcd_barrier.wait();
		
		for (std::size_t index = thread_index; index < _bcd_result.size(); index += thread_count)
		{
			bcd_overlap_data const& overlap = _bcd_result[index];

			if (auto lock = overlap.body_1->try_lock_body())
			{
				overlap.body_1->update_collision_state(collision_state::bounds);
			}

			if (auto lock = overlap.body_2->try_lock_body())
			{
				overlap.body_2->update_collision_state(collision_state::bounds);
			}
		}
#endif

		_bcd_barrier.wait();
	}

	void native_simulator::_parallel_sort_sweep(std::size_t thread_index, std::size_t thread_count)
	{
		for (std::size_t index = thread_index; index < _rigid_body_array.size(); index += thread_count)
		{
			auto body = _rigid_body_array[index];

			body->precalc_bounds();

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
			body->update_collision_state(collision_state::none);
#endif
		}

		if (thread_index == 0)
		{
			_bcd_result.clear();
		}

		_bcd_barrier.wait();

		auto overlap_array = _bcd_parallel_solver.compute_bounds_overlap();

		{
			std::scoped_lock lock{ _bcd_parallel_mutex };

			for (auto& overlap : overlap_array)
			{
				_bcd_result.reserve(_bcd_result.size() + overlap.other_body_array.size());

				auto& body_ptr = overlap.body_ptr;

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
				if (overlap.other_body_array.size() > 0)
				{
					body_ptr->update_collision_state(collision_state::bounds);
				}
#endif

				for (auto& other_body_ptr : overlap.other_body_array)
				{
					_bcd_result.push_back(bcd_overlap_data{ body_ptr, other_body_ptr });

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
					other_body_ptr->update_collision_state(collision_state::bounds);
#endif
				}
			}
		}

		_bcd_barrier.wait();
	}

	void native_simulator::_parallel_separating_axis_test(std::size_t thread_index, std::size_t thread_count, float32 step_delta)
	{
		if (thread_index == 0)
		{
			_ncd_result.clear();
			_ncd_result.resize(_bcd_result.size());
		}

		for (std::size_t index = thread_index; index < _bcd_result.size(); index += thread_count)
		{
			bcd_overlap_data const& overlap = _bcd_result[index];

			if (auto lock = overlap.body_1->try_lock_body())
			{
				overlap.body_1->precalc_geometry();
			}

			if (auto lock = overlap.body_2->try_lock_body())
			{
				overlap.body_2->precalc_geometry();
			}
		}

		_ncd_barrier.wait();
		
		for (std::size_t index = thread_index; index < _bcd_result.size(); index += thread_count)
		{
			bcd_overlap_data const& bcd_overlap = _bcd_result[index];
			
			ncd_overlap_data const& ncd_overlap = _ncd_result[index] = _ncd_solver.compute_geometry_overlap(bcd_overlap);

			if (ncd_overlap.penetration_depth > 0.f)
			{
				auto body_1_entity = bcd_overlap.body_1->get_collision_entity_type();
				auto body_1_behavior = bcd_overlap.body_1->get_collision_behavior_type();

				auto body_2_entity = bcd_overlap.body_2->get_collision_entity_type();
				auto body_2_behavior = bcd_overlap.body_2->get_collision_behavior_type();

				auto body_1_mobility = bcd_overlap.body_1->get_mobility();
				auto body_2_mobility = bcd_overlap.body_2->get_mobility();

				switch (calculate_collision_result(body_1_entity, body_1_behavior, body_2_entity, body_2_behavior))
				{
				case collision_result::resolve:
					if (uint8(body_1_mobility) | uint8(body_2_mobility))
					{
						_cr_solver.setup_constraint(bcd_overlap, ncd_overlap);
					}
				case collision_result::generate:
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
					if (auto lock = bcd_overlap.body_1->try_lock_body())
					{
						bcd_overlap.body_1->update_collision_state(collision_state::geometry);
					}

					if (auto lock = bcd_overlap.body_2->try_lock_body())
					{
						bcd_overlap.body_2->update_collision_state(collision_state::geometry);
					}
#endif

#if GAEA_DEBUG_CONTACT_MANIFOLD
					GAEA_INFO(native_simulator, "contact manifold: body_1=%llu body_2=%llu", std::size_t(bcd_overlap.body_1), std::size_t(bcd_overlap.body_2));
					GAEA_INFO(native_simulator, "axis=%s", ncd_overlap.contact_normal.to_string().c_str());
					GAEA_INFO(native_simulator, "depth=%.6f", ncd_overlap.penetration_depth);
					GAEA_INFO(native_simulator, "points:");

					for (uint32 contact_index = 0; contact_index < ncd_overlap.num_contact_points; ++contact_index)
					{
						GAEA_INFO(native_simulator, "%s", ncd_overlap.contact_point_array[contact_index].to_string().c_str());
					}
#endif
					// TODO generate overlap callback
					break;
				case collision_result::ignore:
					break;
				}
			}
		}

		_ncd_barrier.wait();

		if (thread_index == 0)
		{
			_ncd_solver.refresh_cache(step_delta);
		}

		_ncd_barrier.wait();
	}

	void native_simulator::_parallel_local_symplectic_euler(std::size_t thread_index, std::size_t thread_count, float32 substep_delta, uint32 substep_count)
	{
		GAEA_UNUSED(thread_count);
		
		_cr_solver.compute_dynamics(substep_delta, substep_count);

		_cr_barrier.wait();
		
		if (thread_index == 0)
		{
			_cr_solver.refresh_cache(substep_delta * substep_count);
		}

		_cr_barrier.wait();
	}
}

