#include "engine/physics/dynamics/sycl_bcd_sort_sweep_solver.hpp"

#if GAEA_USING_SYCL

namespace gaea::sycl_kernel
{
	struct kernel_bcd_solver_sort_sweep_compute_bounds;
	sycl::event bcd_solver_sort_sweep_compute_bounds(sycl::queue& command_queue, uint32 num_bodies, bool needs_full_sort,
													 sycl::buffer<sycl_data::statics_data>& statics_buffer,
													 sycl::buffer<sycl_data::position_data>& position_buffer,
													 sycl::buffer<sycl_data::geometry_data>& geometry_buffer,
													 sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer,
													 sycl::buffer<sycl_data::bcd_body_data>& bcd_body_compute_buffer,
													 sycl::buffer<sycl_data::bcd_sort_data>& bcd_sort_compute_buffer)
	{
		return command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				constexpr vector main_axis{ sqrt_one_third_f };

				// We'll do this for a square number of bodies because bitonic sort requires the number of elements to be square
				uint32 num_elements = math::ceil_power_two(num_bodies);

				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto position_accessor = position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_accessor = geometry_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_compute_accessor = geometry_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto bcd_body_compute_accessor = bcd_body_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto bcd_sort_compute_accessor = bcd_sort_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_bcd_solver_sort_sweep_compute_bounds>(
					sycl::range<1>{ num_elements },
					[=] (sycl::item<1> work_item)
					{
						uint32 thread_idx = uint32(work_item.get_linear_id());

						uint32 body_idx = needs_full_sort ? thread_idx : bcd_sort_compute_accessor[thread_idx].body_index;
						float32 min_bounds = std::numeric_limits<float32>::infinity();

						if (body_idx < num_bodies && statics_accessor[body_idx].entity_type != collision_entity::null)
						{
							auto position_data = position_accessor[body_idx];

							auto bounds_data = geometry_accessor[body_idx].bounds.move_by(position_data.position, position_data.rotation);

							geometry_compute_accessor[body_idx].bounds = bounds_data;

							bcd_body_compute_accessor[body_idx].max_bounds = vector::dot(bounds_data.max(), main_axis);

							min_bounds = vector::dot(bounds_data.min(), main_axis);
						}
						
						bcd_sort_compute_accessor[thread_idx] = sycl_data::bcd_sort_data{ uint32(body_idx), min_bounds };
					}
				);
			}
		);
	}
	
	struct kernel_bcd_solver_sort_sweep_init_grid;
	struct kernel_bcd_solver_sort_sweep_update_grid;
	struct kernel_bcd_solver_sort_sweep_compute_cell;
	void bcd_solver_sort_sweep_compute_group(sycl::queue& command_queue, uint32 num_bodies,
											 sycl::buffer<sycl_data::statics_data>& statics_buffer,
											 sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer,
											 sycl::buffer<vector>& grid_min_buffer,
											 sycl::buffer<vector>& grid_max_buffer,
											 sycl::buffer<vector>& cell_extent_buffer)
	{
		constexpr vector cell_extent_factor{ 2.f };

		command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto grid_min_accessor = grid_min_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto grid_max_accessor = grid_max_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto cell_extent_accessor = cell_extent_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_bcd_solver_sort_sweep_init_grid>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 thread_idx = uint32(work_item.get_linear_id());

						grid_min_accessor[thread_idx] = vector{ +std::numeric_limits<float32>::max() };
						grid_max_accessor[thread_idx] = vector{ -std::numeric_limits<float32>::max() };

						cell_extent_accessor[thread_idx] = zero_vector;
					}
				);
			}
		);

		command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				
				auto geometry_compute_accessor = geometry_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto grid_min_accessor = grid_min_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto grid_max_accessor = grid_max_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto cell_extent_accessor = cell_extent_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				
				command_group.parallel_for<kernel_bcd_solver_sort_sweep_update_grid>(
					sycl::range<1>{ num_bodies },
					[=] (sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null)
						{
							auto const& bounds = geometry_compute_accessor[body_idx].bounds;

							grid_min_accessor[body_idx] = bounds.min();
							grid_max_accessor[body_idx] = bounds.max();

							if (statics_accessor[body_idx].mobility)
							{
								cell_extent_accessor[body_idx] = bounds.scale_by(cell_extent_factor).extent();
							}
						}
					}
				);
			}
		);

		parallel_reduce<struct grid_min>(command_queue, num_bodies, grid_min_buffer, 
			[] (vector vec1, vector vec2) { return vector::min(vec1, vec2); });
		parallel_reduce<struct grid_max>(command_queue, num_bodies, grid_max_buffer, 
			[] (vector vec1, vector vec2) { return vector::max(vec1, vec2); });
		parallel_reduce<struct cell_max>(command_queue, num_bodies, cell_extent_buffer, 
			[] (vector vec1, vector vec2) { return vector::max(vec1, vec2); });
	}

	struct kernel_bcd_solver_sort_sweep_total_sort_bounds;
	struct kernel_bcd_solver_sort_sweep_partial_sort_bounds;
	struct kernel_bcd_solver_sort_sweep_index_bounds;
	void bcd_solver_sort_sweep_sort_bounds(sycl::queue& command_queue, uint32 num_bodies, bool needs_full_sort,
										   sycl::buffer<sycl_data::bcd_body_data>& bcd_body_compute_buffer,
										   sycl::buffer<sycl_data::bcd_sort_data>& bcd_sort_compute_buffer)
	{
		// Bitonic sort, based on:
		// https://raw.githubusercontent.com/KhronosGroup/SyclParallelSTL/master/include/sycl/algorithm/sort.hpp
		
		uint32 num_elements = math::ceil_power_two(num_bodies);
		uint32 num_work_items = num_elements >> 1;

		if (num_elements > 1)
		{
			if (needs_full_sort)
			{
				uint32 num_stages = uint32(std::log2(num_elements));

				for (uint32 stage = 0; stage < num_stages; ++stage)
				{
					uint32 num_substages = stage + 1;
					
					for (uint32 substage = 0; substage < num_substages; ++substage)
					{
						uint32 pair_distance_power = stage - substage;
						uint32 pair_distance = 1u << pair_distance_power;

						uint32 block_size = pair_distance << 1;

						command_queue.submit(
							[&] (sycl::handler& command_group)
							{
								auto bcd_sort_compute_accessor = bcd_sort_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

								command_group.parallel_for<kernel_bcd_solver_sort_sweep_total_sort_bounds>(
									sycl::range<1>{ num_work_items },
									[=] (sycl::item<1> work_item)
									{
										uint32 thread_idx = uint32(work_item.get_linear_id());

										// Same as (thread_idx / pair_distance) * block_size + (thread_idx % pair_distance)
										// but takes advantage of the fact that pair_distance is a power of two
										uint32 left_idx = (thread_idx >> pair_distance_power) * block_size + (thread_idx & (pair_distance - 1));
										uint32 right_idx = left_idx + pair_distance;

										sycl_data::bcd_sort_data left_elem = bcd_sort_compute_accessor[left_idx];
										sycl_data::bcd_sort_data right_elem = bcd_sort_compute_accessor[right_idx];

										bool is_reversed = (thread_idx >> stage) & 1;
										bool is_sorted = left_elem.min_bounds < right_elem.min_bounds;

										bool should_swap = is_reversed == is_sorted; // XNOR

										bcd_sort_compute_accessor[left_idx] = should_swap ? right_elem : left_elem;
										bcd_sort_compute_accessor[right_idx] = should_swap ? left_elem : right_elem;
									}
								);
							}
						);
					}
				}
			}
			else
			{
				constexpr std::size_t num_iterations = 1;

				uint32 work_group_size = uint32(command_queue.get_device().get_info<sycl::info::device::max_work_group_size>());

				for (std::size_t iteration = 0; iteration < num_iterations; ++iteration)
				{
					uint32 work_item_offset = 0;

					uint32 num_work_groups = num_work_items / work_group_size + uint32(bool(num_work_items % work_group_size));
					uint32 num_work_group_items = num_work_groups > 1 ? work_group_size : num_work_items;

					uint32 work_group_data_size = num_work_group_items << 1;

					uint32 num_stages = uint32(std::log2(work_group_data_size));
					
					auto kernel_bcd_solver_sort_sweep_partial_sort_bounds_lambda =
						[&] (sycl::handler& command_group)
						{
							auto bcd_sort_compute_accessor = bcd_sort_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

							auto sort_accessor = sycl::accessor<sycl_data::bcd_sort_data, 1, sycl::access::mode::read_write, sycl::access::target::local>(work_group_data_size, command_group);

							command_group.parallel_for<kernel_bcd_solver_sort_sweep_partial_sort_bounds>(
								sycl::nd_range<1>{ num_work_groups * num_work_group_items, num_work_group_items },
								[=] (sycl::nd_item<1> work_item)
								{
									uint32 gl_thread_idx = uint32(work_item.get_global_linear_id()) + work_item_offset;
									uint32 l_thread_idx = uint32(work_item.get_local_linear_id());

									if (gl_thread_idx < num_work_items)
									{
										sort_accessor[(l_thread_idx << 1)]			= bcd_sort_compute_accessor[(gl_thread_idx << 1)];
										sort_accessor[(l_thread_idx << 1) + 1]		= bcd_sort_compute_accessor[(gl_thread_idx << 1) + 1];

										work_item.barrier(sycl::access::fence_space::local_space);

										for (uint32 stage = 0; stage < num_stages; ++stage)
										{
											uint32 num_substages = stage + 1;

											for (uint32 substage = 0; substage < num_substages; ++substage)
											{
												uint32 pair_distance_power = stage - substage;
												uint32 pair_distance = 1u << pair_distance_power;

												uint32 block_size = pair_distance << 1;

												// Same as (thread_idx / pair_distance) * block_size + (thread_idx % pair_distance)
												// but takes advantage of the fact that pair_distance is a power of two
												uint32 left_idx = (l_thread_idx >> pair_distance_power) * block_size + (l_thread_idx & (pair_distance - 1));
												uint32 right_idx = left_idx + pair_distance;
												
												sycl_data::bcd_sort_data left_elem = sort_accessor[left_idx];
												sycl_data::bcd_sort_data right_elem = sort_accessor[right_idx];

												bool is_reversed = (l_thread_idx >> stage) & 1;
												bool is_sorted = left_elem.min_bounds < right_elem.min_bounds;

												bool should_swap = is_reversed == is_sorted; // XNOR

												sort_accessor[left_idx] = should_swap ? right_elem : left_elem;
												sort_accessor[right_idx] = should_swap ? left_elem : right_elem;

												work_item.barrier(sycl::access::fence_space::local_space);
											}
										}

										bcd_sort_compute_accessor[(gl_thread_idx << 1)]		= sort_accessor[(l_thread_idx << 1)];
										bcd_sort_compute_accessor[(gl_thread_idx << 1) + 1] = sort_accessor[(l_thread_idx << 1) + 1];
									}
								}
							);
						};

					command_queue.submit(kernel_bcd_solver_sort_sweep_partial_sort_bounds_lambda);

					if (num_work_groups > 1)
					{
						work_item_offset = work_group_size >> 1;
						
						--num_work_groups;

						command_queue.submit(kernel_bcd_solver_sort_sweep_partial_sort_bounds_lambda);
					}
				}
			}
		}

		command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto bcd_body_compute_accessor = bcd_body_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto bcd_sort_compute_accessor = bcd_sort_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_bcd_solver_sort_sweep_index_bounds>(
					sycl::range<1>{ num_bodies },
					[=] (sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						// Search for the body_idx of the thread that 'owns' this bcd_sort_data
						uint32 other_body_idx = bcd_sort_compute_accessor[body_idx].body_index;

						// By setting the sort index in that body to our body_idx,
						// all threads can immediately access the bcd_sort_data they 'own'
						// during the sweep phase
						bcd_body_compute_accessor[other_body_idx].sort_index = uint32(body_idx);
					}
				);
			}
		);
	}

	struct kernel_bcd_solver_sort_sweep_sweep_bounds;
	struct kernel_bcd_solver_sort_sweep_update_dynamics;
	void bcd_solver_sort_sweep_sweep_bounds(sycl::queue& command_queue, uint32 num_bodies, 
											sycl::buffer<sycl_data::statics_data>& statics_buffer,
											sycl::buffer<sycl_data::position_data>& position_buffer,
											sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
											sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer,
											sycl::buffer<sycl_data::bcd_body_data>& bcd_body_compute_buffer,
											sycl::buffer<sycl_data::bcd_sort_data>& bcd_sort_compute_buffer,
											sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
											sycl::buffer<uint32>& cd_overlap_count_compute_buffer, 
											sycl::buffer<uint32>& overlap_limit_compute_buffer,
											sycl::buffer<vector>& grid_min_buffer,
											sycl::buffer<vector>& grid_max_buffer,
											sycl::buffer<vector>& cell_extent_buffer,
											uint32 grid_side_size)
	{
		sycl::buffer<uint32> collision_state_buffer{ sycl::range<1>{ num_bodies } };

		std::memset(collision_state_buffer.get_access<sycl::access::mode::write>().get_pointer(), 0, collision_state_buffer.get_size());
		
		command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_compute_accessor = geometry_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto bcd_body_compute_accessor = bcd_body_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto bcd_sort_compute_accessor = bcd_sort_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto bcd_sweep_compute_accessor = bcd_sweep_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto overlap_count_compute_accessor = cd_overlap_count_compute_buffer.get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);
				auto overlap_limit_compute_accessor = overlap_limit_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto collision_state_accessor = collision_state_buffer.get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_bcd_solver_sort_sweep_sweep_bounds>(
					sycl::range<1>{ num_bodies },
					[=] (sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null)
						{
							uint32 overlap_limit = overlap_limit_compute_accessor[0];
							
							auto body_bounds = geometry_compute_accessor[body_idx].bounds;

							auto body_entity_type = statics_accessor[body_idx].entity_type;
							auto body_behavior_type = statics_accessor[body_idx].behavior_type;

							float32 max_bounds = bcd_body_compute_accessor[body_idx].max_bounds;
							uint32 sweep_idx = bcd_body_compute_accessor[body_idx].sort_index + 1;

							while (sweep_idx < num_bodies && bcd_sort_compute_accessor[sweep_idx].min_bounds < max_bounds)
							{
								uint32 other_body_idx = bcd_sort_compute_accessor[sweep_idx].body_index;

								auto other_body_entity_type = statics_accessor[other_body_idx].entity_type;
								auto other_body_behavior_type = statics_accessor[other_body_idx].behavior_type;

								if (should_collide(body_entity_type, body_behavior_type, other_body_entity_type, other_body_behavior_type))
								{
									auto other_body_bounds = geometry_compute_accessor[other_body_idx].bounds;

									// Test aabb overlap now, and possibly append the result if successful
									if (base_axis_aligned_bounding_box::overlap(body_bounds, other_body_bounds))
									{
										// This allows us to better distribute constraints when big objects exist in the world
										// Unfortunately, this messes with narrow-phase SAT for some reason,
									    // so we need to store this in the sweep info and restore the order for the test
										bool should_swap = body_bounds.extent().magnitude_square() > other_body_bounds.extent().magnitude_square();

										uint32 body_1_idx = should_swap ? other_body_idx : body_idx;
										uint32 body_2_idx = should_swap ? body_idx : other_body_idx;

										// Always increment, even if not appended; this way we'll inform
										// the simulator whether we need more memory in the next frame
										// to be able to append all overlaps
										uint32 overlap_count = overlap_count_compute_accessor[body_1_idx].fetch_add(1);

									    // Append overlap, if we have enough room
										if (overlap_count < overlap_limit)
										{
											bcd_sweep_compute_accessor[body_1_idx * overlap_limit + overlap_count]
												= sycl_data::bcd_sweep_data{ body_2_idx, should_swap };
										}

										collision_state_accessor[body_1_idx].store(uint32(collision_state::bounds));
										collision_state_accessor[body_2_idx].store(uint32(collision_state::bounds));
									}
								}

								++sweep_idx;
							}
						}
					}
				);
			}
		);

		command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				
				auto position_accessor = position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				
				auto dynamics_compute_accessor = dynamics_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto grid_min_accessor = grid_min_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto grid_max_accessor = grid_max_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto cell_extent_accessor = cell_extent_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto collision_state_accessor = collision_state_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_bcd_solver_sort_sweep_update_dynamics>(
					sycl::range<1>{ num_bodies },
					[=] (sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null)
						{
							vector cell_extent = cell_extent_accessor[0];
							if (!cell_extent.exactly_zero())
							{
								vector position = position_accessor[body_idx].position;

								vector grid_min = grid_min_accessor[0];
								vector grid_max = grid_max_accessor[0];

								vector grid_extent = grid_max - grid_min;
								vector grid_size = vector::min((grid_extent / cell_extent).trunc(), vector{ float32(grid_side_size) });

								cell_extent = grid_extent / grid_size;

								vector cell_index = ((position - grid_min) / cell_extent).trunc();

								// Since num_groups = 2 ^ dimensions, the root of whichever dimension we are working with will always be 2
								cell_extent *= 0.5f;

								vector cell_group = ((position - grid_min) / cell_extent).trunc();
								
								dynamics_compute_accessor[body_idx].collision_index
							        = uint32(cell_index.x)
									+ uint32(cell_index.y * grid_size.x);
								dynamics_compute_accessor[body_idx].collision_group
							        = (uint32(cell_group.x) & 0x01)
									+ (uint32(cell_group.y) << 1 & 0x03);

								if constexpr (num_partition_dimensions == 3)
								{
									dynamics_compute_accessor[body_idx].collision_index += uint32(cell_index.z * grid_size.y * grid_size.x);
									dynamics_compute_accessor[body_idx].collision_group += ((uint32(cell_group.z) << 2) & 0x07);
								}
							}

							dynamics_compute_accessor[body_idx].collision_state
								= collision_state_accessor[body_idx];
						}
					}
				);
			}
		);
	}

	struct kernel_bcd_solver_sort_sweep_update_overlap_count;
	struct kernel_bcd_solver_sort_sweep_update_overlap_limit;
	sycl::event bcd_solver_sort_sweep_compute_overlap_limit(sycl::queue& command_queue, uint32 num_bodies,
															sycl::buffer<uint32>& cd_overlap_count_compute_buffer,
															sycl::buffer<uint32>& overlap_limit_compute_buffer)
	{
		sycl::buffer<uint32> overlap_reduce_compute_buffer{ sycl::range<1>{ num_bodies } };

		command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto overlap_count_compute_accessor = cd_overlap_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group, sycl::range<1>{ num_bodies });
				auto overlap_reduce_compute_accessor = overlap_reduce_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				command_group.copy(overlap_count_compute_accessor, overlap_reduce_compute_accessor);
			}
		);

		parallel_reduce<struct overlap_reduce>(command_queue, num_bodies, overlap_reduce_compute_buffer, 
			[] (uint32 elem1, uint32 elem2) { return math::max(elem1, elem2); });

		return command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto overlap_reduce_compute_accessor = overlap_reduce_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group, sycl::range<1>{ 1 });
				auto overlap_limit_compute_accessor = overlap_limit_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group, sycl::range<1>{ 1 }, sycl::id<1>{ 1 });

				command_group.copy(overlap_reduce_compute_accessor, overlap_limit_compute_accessor);
			}
		);
	}

	sycl_type::sequence_event bcd_solver_sort_sweep(sycl::queue& command_queue, uint32 num_bodies, bool needs_full_sort,
													sycl::buffer<sycl_data::statics_data>& statics_buffer,
													sycl::buffer<sycl_data::position_data>& position_buffer,
													sycl::buffer<sycl_data::geometry_data>& geometry_buffer,
													sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
													sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer,
													sycl::buffer<sycl_data::bcd_body_data>& bcd_body_compute_buffer,
													sycl::buffer<sycl_data::bcd_sort_data>& bcd_sort_compute_buffer,
													sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
													sycl::buffer<uint32>& cd_overlap_count_compute_buffer, 
													sycl::buffer<uint32>& overlap_limit_compute_buffer,
													uint32 grid_side_size)
	{
		// 1. Calculate bounds for bodies and create list s1 where 'm = dot(min, main_axis)' and 'M = dot(max, main_axis)'
		auto bcd_first_event = bcd_solver_sort_sweep_compute_bounds(
			command_queue, num_bodies, needs_full_sort,
			statics_buffer,
			position_buffer,
			geometry_buffer,
			geometry_compute_buffer,
			bcd_body_compute_buffer,
			bcd_sort_compute_buffer
		);

		sycl::buffer<vector> grid_min_buffer{ sycl::range<1>{ num_bodies } };
		sycl::buffer<vector> grid_max_buffer{ sycl::range<1>{ num_bodies } };
		sycl::buffer<vector> cell_extent_buffer{ sycl::range<1>{ num_bodies } };
		
		bcd_solver_sort_sweep_compute_group(
			command_queue, num_bodies,
			statics_buffer,
			geometry_compute_buffer,
			grid_min_buffer,
			grid_max_buffer,
			cell_extent_buffer
		);

		// 2. Sort list s1 and for each thread t, register index of t in s1
		bcd_solver_sort_sweep_sort_bounds(
			command_queue, num_bodies, needs_full_sort,
			bcd_body_compute_buffer,
			bcd_sort_compute_buffer
		);

		// 3. Sweep list s1 for each thread t,
		//		and create list s2 of pairs of objects (i:t, j:s1) if 'overlap(i, j)' while 'm:j < M:i'
		bcd_solver_sort_sweep_sweep_bounds(
			command_queue, num_bodies,
			statics_buffer,
			position_buffer,
			dynamics_compute_buffer,
			geometry_compute_buffer,
			bcd_body_compute_buffer,
			bcd_sort_compute_buffer,
			bcd_sweep_compute_buffer,
			cd_overlap_count_compute_buffer,
			overlap_limit_compute_buffer,
			grid_min_buffer,
			grid_max_buffer,
			cell_extent_buffer,
			grid_side_size
		);

		// 4. Calculate overlap limit for next frame, based on results from this frame
		auto bcd_last_event = bcd_solver_sort_sweep_compute_overlap_limit(
			command_queue, num_bodies,
			cd_overlap_count_compute_buffer,
			overlap_limit_compute_buffer
		);

		return sycl_type::sequence_event{ bcd_first_event, bcd_last_event };
	}
}

#endif
