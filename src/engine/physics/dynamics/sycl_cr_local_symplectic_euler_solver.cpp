#include "engine/physics/dynamics/sycl_cr_local_symplectic_euler_solver.hpp"

#if GAEA_USING_SYCL

#if GAEA_USING_PRECISE_RSQRT
#	define GAEA_SYCL_SRSQRT(arg) 1.0f / sycl::sqrt(arg)
#	define GAEA_SYCL_DRSQRT(arg) 1.0 / sycl::sqrt(arg)
#else
#	define GAEA_SYCL_SRSQRT(arg) math::rsqrt_single_abs_2NR(arg)
#	define GAEA_SYCL_DRSQRT(arg) math::rsqrt_double_rel_1NR(arg)
#endif

namespace
{
	float32 calculate_collision_coefficient(gaea::collision_mode mode, float32 body_1_coefficient, float32 body_2_coefficient)
	{
		switch (mode)
		{
		case gaea::collision_mode::always_zero:
			return 0.f;
		case gaea::collision_mode::always_one:
			return 1.f;

		case gaea::collision_mode::minimum:
			return gaea::math::min(body_1_coefficient, body_2_coefficient);
		case gaea::collision_mode::geometric:
			return sycl::sqrt(body_1_coefficient * body_2_coefficient);
		case gaea::collision_mode::arithmetic:
			return 0.5f * (body_1_coefficient + body_2_coefficient);
		case gaea::collision_mode::maximum:
			return gaea::math::max(body_1_coefficient, body_2_coefficient);
		}
		return 0.f;
	}
}

namespace gaea::sycl_kernel
{
	void prepare_lcp_constraint(float32 step_delta,
								float32 depenetration_weight, float32 depenetration_bias, 
								float32 restitution_threshold, float32 restitution_attenuation, float32 restitution_offset,
								sycl::accessor<sycl_data::momentum_data, 1, sycl::access::mode::read_write, sycl::access::target::global_buffer> const& momentum_accessor,
								sycl::accessor<sycl_data::position_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& position_accessor,
								sycl::accessor<sycl_data::cr_tensor_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& tensor_accessor,
								sycl_data::cr_constraint_data& constraint_data)
	{
		for (std::size_t contact_index = 0; contact_index < constraint_data.num_contact_data; ++contact_index)
		{
			auto& contact = constraint_data.contact_data_array[contact_index];

			contact.contact_lever_1 = contact.contact_point - position_accessor[constraint_data.body_1_index].position;
			contact.contact_lever_2 = contact.contact_point - position_accessor[constraint_data.body_2_index].position;

			contact.normal_mass = contact.i_tangent_mass = contact.j_tangent_mass
				= tensor_accessor[constraint_data.body_1_index].mass_inverse + tensor_accessor[constraint_data.body_2_index].mass_inverse;

			auto normal_axle_1 = vector::cross(contact.contact_lever_1, constraint_data.contact_normal);
			auto normal_axle_2 = vector::cross(contact.contact_lever_2, constraint_data.contact_normal);

			contact.normal_mass
				= 1.f / (contact.normal_mass 
							+ vector::dot(normal_axle_1, tensor_accessor[constraint_data.body_1_index].inertia_inverse * normal_axle_1) 
							+ vector::dot(normal_axle_2, tensor_accessor[constraint_data.body_2_index].inertia_inverse * normal_axle_2));

			auto i_tangent_axle_1 = vector::cross(contact.contact_lever_1, constraint_data.i_contact_tangent);
			auto i_tangent_axle_2 = vector::cross(contact.contact_lever_2, constraint_data.i_contact_tangent);

			contact.i_tangent_mass
				= 1.f / (contact.i_tangent_mass 
							+ vector::dot(i_tangent_axle_1, tensor_accessor[constraint_data.body_1_index].inertia_inverse * i_tangent_axle_1) 
							+ vector::dot(i_tangent_axle_2, tensor_accessor[constraint_data.body_2_index].inertia_inverse * i_tangent_axle_2));

			auto j_tangent_axle_1 = vector::cross(contact.contact_lever_1, constraint_data.j_contact_tangent);
			auto j_tangent_axle_2 = vector::cross(contact.contact_lever_2, constraint_data.j_contact_tangent);

			contact.j_tangent_mass
				= 1.f / (contact.j_tangent_mass 
							+ vector::dot(j_tangent_axle_1, tensor_accessor[constraint_data.body_1_index].inertia_inverse * j_tangent_axle_1) 
							+ vector::dot(j_tangent_axle_2, tensor_accessor[constraint_data.body_2_index].inertia_inverse * j_tangent_axle_2));

			vector impulse
				= constraint_data.contact_normal * contact.normal_impulse
				+ constraint_data.i_contact_tangent * contact.i_tangent_impulse
				+ constraint_data.j_contact_tangent * contact.j_tangent_impulse;

			momentum_accessor[constraint_data.body_1_index].linear_momentum -= impulse;
			momentum_accessor[constraint_data.body_1_index].angular_momentum -= vector::cross(contact.contact_lever_1, impulse);

			momentum_accessor[constraint_data.body_2_index].linear_momentum += impulse;
			momentum_accessor[constraint_data.body_2_index].angular_momentum += vector::cross(contact.contact_lever_2, impulse);

			vector linear_velocity_1
				= tensor_accessor[constraint_data.body_1_index].mass_inverse
				* momentum_accessor[constraint_data.body_1_index].linear_momentum;
			vector angular_velocity_1
				= tensor_accessor[constraint_data.body_1_index].inertia_inverse
				* momentum_accessor[constraint_data.body_1_index].angular_momentum;

			vector linear_velocity_2
				= tensor_accessor[constraint_data.body_2_index].mass_inverse
				* momentum_accessor[constraint_data.body_2_index].linear_momentum;
			vector angular_velocity_2
				= tensor_accessor[constraint_data.body_2_index].inertia_inverse
				* momentum_accessor[constraint_data.body_2_index].angular_momentum;
			
			vector relative_velocity
				= linear_velocity_2 + vector::cross(angular_velocity_2, contact.contact_lever_2)
				- linear_velocity_1 - vector::cross(angular_velocity_1, contact.contact_lever_1);

			// Penetration (position) constraint
			contact.bias_velocity = -depenetration_weight * math::min(-constraint_data.penetration_depth + depenetration_bias, 0.f) / step_delta;

			// Restitution (velocity) constraint
			float32 restitution_velocity = -vector::dot(relative_velocity, constraint_data.contact_normal);
			// If velocity is too low, don't apply it, otherwise instabilities will be introduced
			if (restitution_velocity > restitution_threshold)
			{
				// Equation to attenuate velocity:
				// Zero when at threshold;
				// Asymptotically approaches velocity when velocity -> infinity
				restitution_velocity -= restitution_attenuation / (restitution_velocity - restitution_offset);
				contact.bias_velocity += constraint_data.restitution_coefficient * restitution_velocity;
			}
		}
	}

	void solve_lcp_constraint(sycl::accessor<sycl_data::momentum_data, 1, sycl::access::mode::read_write, sycl::access::target::global_buffer> const& momentum_accessor,
							  sycl::accessor<sycl_data::cr_tensor_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& tensor_accessor,
							  sycl_data::cr_constraint_data& constraint_data)
	{
		for (std::size_t contact_index = 0; contact_index < constraint_data.num_contact_data; ++contact_index)
		{
			auto& contact = constraint_data.contact_data_array[contact_index];

			float32 max_tangent_lambda = constraint_data.friction_coefficient * contact.normal_impulse;
			float32 min_tangent_lambda = -max_tangent_lambda;

			vector linear_velocity_1
				= tensor_accessor[constraint_data.body_1_index].mass_inverse
				* momentum_accessor[constraint_data.body_1_index].linear_momentum;
			vector angular_velocity_1
				= tensor_accessor[constraint_data.body_1_index].inertia_inverse
				* momentum_accessor[constraint_data.body_1_index].angular_momentum;

			vector linear_velocity_2
				= tensor_accessor[constraint_data.body_2_index].mass_inverse
				* momentum_accessor[constraint_data.body_2_index].linear_momentum;
			vector angular_velocity_2
				= tensor_accessor[constraint_data.body_2_index].inertia_inverse
				* momentum_accessor[constraint_data.body_2_index].angular_momentum;

			vector relative_velocity
				= linear_velocity_2 + vector::cross(angular_velocity_2, contact.contact_lever_2)
				- linear_velocity_1 - vector::cross(angular_velocity_1, contact.contact_lever_1);

			// Penetration and restitution, normal
			float32 normal_lambda = (-vector::dot(relative_velocity, constraint_data.contact_normal) + contact.bias_velocity) * contact.normal_mass;

			float32 old_normal_lambda = contact.normal_impulse;
			float32 new_normal_lambda = contact.normal_impulse
				= math::max(contact.normal_impulse + normal_lambda, 0.f);

			vector normal_impulse = constraint_data.contact_normal * (new_normal_lambda - old_normal_lambda);

			// Friction, i-tangent
			float32 i_tangent_lambda = -vector::dot(relative_velocity, constraint_data.i_contact_tangent) * contact.i_tangent_mass;

			float32 old_i_tangent_lambda = contact.i_tangent_impulse;
			float32 new_i_tangent_lambda = contact.i_tangent_impulse
				= math::clamp(contact.i_tangent_impulse + i_tangent_lambda, min_tangent_lambda, max_tangent_lambda);

			vector i_tangent_impulse = constraint_data.i_contact_tangent * (new_i_tangent_lambda - old_i_tangent_lambda);

			// Friction, j-tangent
			float32 j_tangent_lambda = -vector::dot(relative_velocity, constraint_data.j_contact_tangent) * contact.j_tangent_mass;

			float32 old_j_tangent_lambda = contact.j_tangent_impulse;
			float32 new_j_tangent_lambda = contact.j_tangent_impulse
				= math::clamp(contact.j_tangent_impulse + j_tangent_lambda, min_tangent_lambda, max_tangent_lambda);

			vector j_tangent_impulse = constraint_data.j_contact_tangent * (new_j_tangent_lambda - old_j_tangent_lambda);

			// Total impulse
			vector impulse = normal_impulse + i_tangent_impulse + j_tangent_impulse;

			momentum_accessor[constraint_data.body_1_index].linear_momentum -= impulse;
			momentum_accessor[constraint_data.body_1_index].angular_momentum -= vector::cross(contact.contact_lever_1, impulse);

			momentum_accessor[constraint_data.body_2_index].linear_momentum += impulse;
			momentum_accessor[constraint_data.body_2_index].angular_momentum += vector::cross(contact.contact_lever_2, impulse);
		}
	}
	
	struct kernel_cr_solver_constraint_batching_create_constraints;
	struct kernel_cr_solver_constraint_batching_separate_constraints;
	struct kernel_cr_solver_constraint_batching_batch_constraints;
	struct kernel_cr_solver_constraint_batching_sort_constraints;
	sycl_type::sequence_event cr_solver_constraint_batching(sycl::queue& command_queue, uint32 num_bodies,
															uint32 num_batches, uint8 restitution_mode, uint8 friction_mode,
															sycl::buffer<sycl_data::statics_data>& statics_buffer,
															sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
															sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
															sycl::buffer<sycl_data::ncd_overlap_data>& ncd_overlap_compute_buffer,
															sycl::buffer<sycl_data::cr_constraint_data>& cr_constraint_compute_buffer,
															sycl::buffer<uint32>& cd_overlap_count_compute_buffer,
															sycl::buffer<uint32>& cr_overlap_count_compute_buffer,
															sycl::buffer<uint32>& overlap_limit_compute_buffer,
															sycl::buffer<uint32>& constraint_count_compute_buffer,
															sycl::buffer<uint32>& constraint_index_compute_buffer,
															uint32 num_overlap_work_groups,
															uint32 num_overlap_work_items_per_group)
	{
		sycl::event first_event = command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto dynamics_compute_accessor = dynamics_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto bcd_sweep_compute_accessor = bcd_sweep_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto ncd_overlap_compute_accessor = ncd_overlap_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto cr_constraint_compute_accessor = cr_constraint_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto cd_overlap_count_compute_accessor = cd_overlap_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto cr_overlap_count_compute_accessor = cr_overlap_count_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto overlap_limit_compute_accessor = overlap_limit_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_cr_solver_constraint_batching_create_constraints>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null &&
							dynamics_compute_accessor[body_idx].collision_state == uint8(collision_state::geometry))
						{
							uint32 overlap_limit = overlap_limit_compute_accessor[0];

							uint32 cd_overlap_count = cd_overlap_count_compute_accessor[body_idx];
							uint32 cr_overlap_count = 0;

							uint32 cr_overlap_limit = math::min(cd_overlap_count, overlap_limit);

							for (uint32 index = 0; index < cr_overlap_limit; ++index)
							{
								uint32 overlap_idx = body_idx * overlap_limit + index;

								sycl_data::ncd_overlap_data overlap_data = ncd_overlap_compute_accessor[overlap_idx];
								uint32 other_body_idx = bcd_sweep_compute_accessor[overlap_idx].other_body_index;

								auto body_entity = statics_accessor[body_idx].entity_type;
								auto body_behavior = statics_accessor[body_idx].behavior_type;
								auto body_mobility = statics_accessor[body_idx].mobility;

								auto other_body_entity = statics_accessor[other_body_idx].entity_type;
								auto other_body_behavior = statics_accessor[other_body_idx].behavior_type;
								auto other_body_mobility = statics_accessor[other_body_idx].mobility;

								if (overlap_data.penetration_depth > 0.f)
								{
									switch (calculate_collision_result(body_entity, body_behavior, other_body_entity, other_body_behavior))
									{
									case collision_result::resolve:
										if (uint8(body_mobility) | uint8(other_body_mobility))
										{
											sycl_data::cr_constraint_data constraint_data;

											constraint_data.body_1_index = body_idx;
											constraint_data.body_2_index = other_body_idx;

											vector::create_orthonormal_basis(
												overlap_data.contact_normal,
												constraint_data.contact_normal,
												constraint_data.i_contact_tangent,
												constraint_data.j_contact_tangent
											);
											constraint_data.penetration_depth = overlap_data.penetration_depth;

											constraint_data.restitution_coefficient = calculate_collision_coefficient(
												collision_mode(restitution_mode),
												statics_accessor[body_idx].restitution_coefficient,
												statics_accessor[other_body_idx].restitution_coefficient
											);
											constraint_data.friction_coefficient = calculate_collision_coefficient(
												collision_mode(friction_mode),
												statics_accessor[body_idx].friction_coefficient,
												statics_accessor[other_body_idx].friction_coefficient
											);

											constraint_data.num_contact_data = overlap_data.num_contact_points;

											for (uint8 contact_index = 0; contact_index < constraint_data.num_contact_data; ++contact_index)
											{
												constraint_data.contact_data_array[contact_index].contact_point = overlap_data.contact_point_array[contact_index];
											}

											constraint_data.batch_index = uint32(-1);
											constraint_data.collision_index = dynamics_compute_accessor[body_idx].collision_index;
											constraint_data.collision_group = dynamics_compute_accessor[body_idx].collision_group;

											cr_constraint_compute_accessor[body_idx * overlap_limit + cr_overlap_count] = constraint_data;

											++cr_overlap_count;
										}
									case collision_result::generate:
										// TODO generate overlap callback
										break;
									case collision_result::ignore:
										break;
									}
								}
							}

							cr_overlap_count_compute_accessor[body_idx] = cr_overlap_count;
						}
					}
				);
			}
		);

		command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto cr_constraint_compute_accessor = cr_constraint_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto cr_overlap_count_compute_accessor = cr_overlap_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto overlap_limit_compute_accessor = overlap_limit_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto constraint_count_compute_accessor = constraint_count_compute_buffer.get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);
				auto constraint_index_compute_accessor = constraint_index_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_cr_solver_constraint_batching_separate_constraints>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 thread_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[thread_idx].entity_type != collision_entity::null)
						{
							uint32 overlap_limit = overlap_limit_compute_accessor[0];
							uint32 constraint_count = cr_overlap_count_compute_accessor[thread_idx];

							for (uint32 index = 0; index < constraint_count; ++index)
							{
								uint32 constraint_idx = thread_idx * overlap_limit + index;
								auto& constraint_data = cr_constraint_compute_accessor[constraint_idx];

								uint32 constraint_count_idx = constraint_data.collision_group * num_overlap_work_groups + constraint_data.collision_index;
								
								uint32 constraint_index_pos = constraint_count_compute_accessor[constraint_count_idx].fetch_add(1);
								if (constraint_index_pos < num_overlap_work_items_per_group)
								{
									uint32 constraint_index_off = constraint_data.collision_group * num_overlap_work_groups * num_overlap_work_items_per_group + constraint_data.collision_index * num_overlap_work_items_per_group;

									constraint_index_compute_accessor[constraint_index_off + constraint_index_pos] = constraint_idx;
								}
								else
								{
									constraint_count_compute_accessor[constraint_count_idx].fetch_sub(1);
								}
							}
						}
					}
				);
			}
		);

		sycl::buffer<uint32> body_sync_buffer{ sycl::range<1>{ num_bodies * num_constraint_partitions } };

		std::memset(body_sync_buffer.get_access<sycl::access::mode::write>().get_pointer(), 0, body_sync_buffer.get_size());

		sycl::event last_event;

	    last_event = command_queue.submit(
			[&](sycl::handler& command_group)
			{
				uint32 num_overlap_work_items = num_overlap_work_groups * num_overlap_work_items_per_group * num_constraint_partitions;

				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				
				auto cr_constraint_compute_accessor = cr_constraint_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

				auto constraint_count_compute_accessor = constraint_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto constraint_index_compute_accessor = constraint_index_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto body_sync_accessor = body_sync_buffer.get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_cr_solver_constraint_batching_batch_constraints>(
					sycl::nd_range<1>{ num_overlap_work_items, num_overlap_work_items_per_group },
					[=](sycl::nd_item<1> work_item)
					{
						uint32 g_thread_idx = uint32(work_item.get_group_linear_id());
						uint32 l_thread_idx = uint32(work_item.get_local_linear_id());

						uint32 body_offset = g_thread_idx / num_overlap_work_groups * num_bodies;

						uint32 constraint_index = constraint_index_compute_accessor[g_thread_idx * num_overlap_work_items_per_group + l_thread_idx];
						uint32 constraint_count = constraint_count_compute_accessor[g_thread_idx];
						
						uint32 body_1_index = uint32(-1);
						uint32 body_2_index = uint32(-1);

						bool has_constraint = false;
						if (l_thread_idx < constraint_count)
						{
							sycl_data::cr_constraint_data& constraint_data = cr_constraint_compute_accessor[constraint_index];
							body_1_index = constraint_data.body_1_index;
							body_2_index = constraint_data.body_2_index;
							has_constraint = true;
						}
						
						bool needs_batch = true;
						for (uint32 batch_index = 0; batch_index < num_batches; ++batch_index)
						{
							if (has_constraint && needs_batch &&
								(statics_accessor[body_1_index].mobility == 0 || body_sync_accessor[body_offset + body_1_index].exchange(1) == 0) &&
								(statics_accessor[body_2_index].mobility == 0 || body_sync_accessor[body_offset + body_2_index].exchange(1) == 0))
							{
								sycl_data::cr_constraint_data& constraint_data = cr_constraint_compute_accessor[constraint_index];
								constraint_data.batch_index = batch_index;
								needs_batch = false;
							}

							work_item.barrier(sycl::access::fence_space::global_space);
							
							if (has_constraint)
							{
								body_sync_accessor[body_offset + body_1_index].store(0);
								body_sync_accessor[body_offset + body_2_index].store(0);
							}

							work_item.barrier(sycl::access::fence_space::global_space);
						}
					}
				);
			}
		);

#if 0
		// TODO FIXME
		last_event = command_queue.submit(
			[&](sycl::handler& command_group)
			{
				uint32 num_work_items_per_group = num_overlap_work_items_per_group >> 1;
				uint32 num_work_items = num_overlap_work_groups * num_work_items_per_group * num_constraint_partitions;

				uint32 num_stages = uint32(std::log2(num_overlap_work_items_per_group));
				
				auto cr_constraint_compute_accessor = cr_constraint_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto constraint_index_compute_accessor = constraint_index_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

				auto sort_accessor = sycl::accessor<uint32, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_overlap_work_items_per_group, command_group);

				command_group.parallel_for<kernel_cr_solver_constraint_batching_sort_constraints>(
					sycl::nd_range<1>{ num_work_items, num_work_items_per_group },
					[=](sycl::nd_item<1> work_item)
					{
						uint32 g_thread_idx = uint32(work_item.get_group_linear_id());
						uint32 l_thread_idx = uint32(work_item.get_local_linear_id());

						//if (g_thread_idx * num_overlap_work_items_per_group + (l_thread_idx << 1) < num_work_items)
						{
							sort_accessor[(l_thread_idx << 1)] = constraint_index_compute_accessor[g_thread_idx * num_overlap_work_items_per_group + (l_thread_idx << 1)];
							sort_accessor[(l_thread_idx << 1) + 1] = constraint_index_compute_accessor[g_thread_idx * num_overlap_work_items_per_group + (l_thread_idx << 1) + 1];

							work_item.barrier(sycl::access::fence_space::local_space);

							for (uint32 stage = 0; stage < num_stages; ++stage)
							{
								uint32 num_substages = stage + 1;

								for (uint32 substage = 0; substage < num_substages; ++substage)
								{
									uint32 pair_distance_power = stage - substage;
									uint32 pair_distance = 1u << pair_distance_power;

									uint32 block_size = pair_distance << 1;

									// Same as (thread_idx / pair_distance) * block_size + (thread_idx % pair_distance)
									// but takes advantage of the fact that pair_distance is a power of two
									uint32 left_idx = (l_thread_idx >> pair_distance_power) * block_size + (l_thread_idx & (pair_distance - 1));
									uint32 right_idx = left_idx + pair_distance;

									uint32 left_elem = sort_accessor[left_idx];
									uint32 right_elem = sort_accessor[right_idx];

									uint32 left_order = left_elem != 0xFFFFFFFFu ? cr_constraint_compute_accessor[left_elem].batch_index : left_elem;
									uint32 right_order = right_elem != 0xFFFFFFFFu ? cr_constraint_compute_accessor[right_elem].batch_index : right_elem;

									bool is_reversed = (l_thread_idx >> stage) & 1;
									bool is_sorted = left_order < right_order;

									bool should_swap = is_reversed == is_sorted; // XNOR

									sort_accessor[left_idx] = should_swap ? right_elem : left_elem;
									sort_accessor[right_idx] = should_swap ? left_elem : right_elem;

									work_item.barrier(sycl::access::fence_space::local_space);
								}
							}

							constraint_index_compute_accessor[g_thread_idx * num_overlap_work_items_per_group + (l_thread_idx << 1)] = sort_accessor[(l_thread_idx << 1)];
							constraint_index_compute_accessor[g_thread_idx * num_overlap_work_items_per_group + (l_thread_idx << 1) + 1] = sort_accessor[(l_thread_idx << 1) + 1];
						}
					}
				);
			}
		);
#endif

		return sycl_type::sequence_event{ first_event, last_event };
	}

	struct kernel_cr_solver_local_symplectic_euler_compute_tensors_and_impulses;
	struct kernel_cr_solver_local_symplectic_euler_prepare_constraints;
	struct kernel_cr_solver_local_symplectic_euler_solve_constraints;
	struct kernel_cr_solver_local_symplectic_euler_integrate_system;
	sycl_type::sequence_event cr_solver_local_symplectic_euler(sycl::queue& command_queue, uint32 num_bodies,
															   uint32 num_batches, uint32 num_iterations, 
															   float32 substep_delta, uint32 substep_count,
															   float32 depenetration_weight, float32 depenetration_bias, 
															   float32 restitution_threshold, float32 restitution_attenuation, float32 restitution_offset,
															   sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
															   sycl::buffer<sycl_data::force_data>& force_buffer, 
															   sycl::buffer<sycl_data::mass_data>& mass_buffer,
															   sycl::buffer<sycl_data::statics_data>& statics_buffer,
															   sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer,
															   sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer,
															   sycl::buffer<sycl_data::cr_constraint_data>& cr_constraint_compute_buffer, 
															   sycl::buffer<uint32>& constraint_count_compute_buffer,
															   sycl::buffer<uint32>& constraint_index_compute_buffer,
															   uint32 num_overlap_work_groups,
															   uint32 num_overlap_work_items_per_group)
	{
		float32 half_substep_delta = 0.5f * substep_delta;
		
		sycl::buffer<sycl_data::cr_tensor_data> tensor_buffer{ sycl::range<1>{ num_bodies } };

		sycl::event first_event;
		sycl::event last_event;

		for (uint32 substep_index = 0; substep_index < substep_count; ++substep_index)
		{
			// First precalculate world tensors and calculate impulses from applied forces on bodies
			auto substep_first_event = command_queue.submit(
				[&] (sycl::handler& command_group)
				{
					auto force_accessor = force_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

					auto mass_accessor = mass_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

					auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

					auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

					auto position_accessor = position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

					auto tensor_accessor = tensor_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
					
					command_group.parallel_for<kernel_cr_solver_local_symplectic_euler_compute_tensors_and_impulses>(
						sycl::range<1>{ num_bodies },
						[=] (sycl::item<1> work_item)
						{
							uint32 body_idx = uint32(work_item.get_linear_id());

							if (statics_accessor[body_idx].entity_type != 0)
							{
								sycl_data::cr_tensor_data tensor_data;
								
								if (statics_accessor[body_idx].mobility != 0)
								{
									auto rot_mat = position_accessor[body_idx].rotation.to_matrix();
									auto tp_rot_mat = rot_mat.transpose();

									tensor_data.mass = mass_accessor[body_idx].mass;
									tensor_data.inertia = rot_mat * mass_accessor[body_idx].inertia * tp_rot_mat;
									tensor_data.mass_inverse = mass_accessor[body_idx].mass_inverse;
									tensor_data.inertia_inverse = rot_mat * mass_accessor[body_idx].inertia_inverse * tp_rot_mat;

									vector linear_impulse
										= (force_accessor[body_idx].worldspace_force
											+ rot_mat * force_accessor[body_idx].bodyspace_force // body-force to world-force
											+ world_force.linear
											+ tensor_data.mass * world_accel.linear) // world-accel to world-force
										* substep_delta;

									vector angular_impulse
										= (force_accessor[body_idx].worldspace_torque
											+ rot_mat * force_accessor[body_idx].bodyspace_torque // body-torque to world-torque
											+ world_force.angular
											+ tensor_data.inertia * world_accel.angular) // world-accel to world-torque
										* substep_delta;

									momentum_accessor[body_idx].linear_momentum += linear_impulse;
									momentum_accessor[body_idx].angular_momentum += angular_impulse;
								}

								tensor_accessor[body_idx] = tensor_data;
							}
						}
					);
				}
			);

			if (substep_index == 0)
			{
				first_event = substep_first_event;
			}

			// Then prepare the constraints for each body, synchronize on group and batch
#if 1
			for (uint32 group_index = 0; group_index < num_constraint_partitions; ++group_index)
			{
				command_queue.submit(
					[&](sycl::handler& command_group)
					{
						uint32 num_overlap_work_items = num_overlap_work_groups * num_overlap_work_items_per_group;

						auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

						auto position_accessor = position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

						auto cr_constraint_compute_accessor = cr_constraint_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

						auto constraint_count_compute_accessor = constraint_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
						auto constraint_index_compute_accessor = constraint_index_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

						auto tensor_accessor = tensor_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

						command_group.parallel_for<kernel_cr_solver_local_symplectic_euler_prepare_constraints>(
							sycl::nd_range<1>{ num_overlap_work_items, num_overlap_work_items_per_group },
							[=](sycl::nd_item<1> work_item)
							{
								uint32 g_thread_idx = uint32(work_item.get_group_linear_id());
								uint32 l_thread_idx = uint32(work_item.get_local_linear_id());

								uint32 constraint_count = constraint_count_compute_accessor[group_index * num_overlap_work_groups + g_thread_idx];
								uint32 constraint_index = constraint_index_compute_accessor[group_index * num_overlap_work_items + g_thread_idx * num_overlap_work_items_per_group + l_thread_idx];

								bool has_constraint = l_thread_idx < constraint_count;
								
								for (uint32 batch_index = 0; batch_index < num_batches; ++batch_index)
								{
									if (has_constraint)
									{
										sycl_data::cr_constraint_data& constraint_data = cr_constraint_compute_accessor[constraint_index];
										if (constraint_data.batch_index == batch_index)
										{
											prepare_lcp_constraint(
												substep_delta,
												depenetration_weight, depenetration_bias,
												restitution_threshold, restitution_attenuation, restitution_offset,
												momentum_accessor,
												position_accessor,
												tensor_accessor,
												constraint_data
											);
										}
									}
									
									work_item.barrier(sycl::access::fence_space::global_space);
								}
							}
						);
					}
				);
			}
#endif
			
			// Then run the solver for each body, synchronize on group and batch
#if 1
			for (uint32 iteration = 0; iteration < num_iterations; ++iteration)
			{
				for (uint32 group_index = 0; group_index < num_constraint_partitions; ++group_index)
				{
					command_queue.submit(
						[&](sycl::handler& command_group)
						{
							uint32 num_overlap_work_items = num_overlap_work_groups * num_overlap_work_items_per_group;

							auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

							auto cr_constraint_compute_accessor = cr_constraint_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

							auto constraint_count_compute_accessor = constraint_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
							auto constraint_index_compute_accessor = constraint_index_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

							auto tensor_accessor = tensor_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

							command_group.parallel_for<kernel_cr_solver_local_symplectic_euler_solve_constraints>(
								sycl::nd_range<1>{ num_overlap_work_items, num_overlap_work_items_per_group },
								[=](sycl::nd_item<1> work_item)
								{
									uint32 g_thread_idx = uint32(work_item.get_group_linear_id());
									uint32 l_thread_idx = uint32(work_item.get_local_linear_id());

									uint32 constraint_count = constraint_count_compute_accessor[group_index * num_overlap_work_groups + g_thread_idx];
									uint32 constraint_index = constraint_index_compute_accessor[group_index * num_overlap_work_items + g_thread_idx * num_overlap_work_items_per_group + l_thread_idx];

									bool has_constraint = l_thread_idx < constraint_count;

									for (uint32 batch_index = 0; batch_index < num_batches; ++batch_index)
									{
										if (has_constraint)
										{
											sycl_data::cr_constraint_data& constraint_data = cr_constraint_compute_accessor[constraint_index];
											if (constraint_data.batch_index == batch_index)
											{
												solve_lcp_constraint(
													momentum_accessor,
													tensor_accessor,
													constraint_data
												);
											}
										}

										work_item.barrier(sycl::access::fence_space::global_space);
									}
								}
							);
						}
					);
				}
			}
#endif

			// Finally run the integrator for each body
			last_event = command_queue.submit(
				[&](sycl::handler& command_group)
				{
					auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

					auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
					auto momentum_copy_accessor = momentum_copy_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

					auto position_accessor = position_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
					auto position_copy_accessor = position_copy_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

					auto tensor_accessor = tensor_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

					command_group.parallel_for<kernel_cr_solver_local_symplectic_euler_integrate_system>(
						sycl::range<1>{ num_bodies },
						[=](sycl::item<1> work_item)
						{
							uint32 body_idx = uint32(work_item.get_linear_id());

							if (statics_accessor[body_idx].entity_type != 0 && statics_accessor[body_idx].mobility != 0)
							{
								vector linear_velocity = tensor_accessor[body_idx].mass_inverse * momentum_accessor[body_idx].linear_momentum;
								vector angular_velocity = tensor_accessor[body_idx].inertia_inverse * momentum_accessor[body_idx].angular_momentum;

								position_accessor[body_idx].position += linear_velocity * substep_delta;
								position_accessor[body_idx].rotation += position_accessor[body_idx].rotation * quaternion{ 0.f, position_accessor[body_idx].rotation.reciprocal() * angular_velocity } * half_substep_delta;

								float32 magn_sq = position_accessor[body_idx].rotation.magnitude_square();
								position_accessor[body_idx].rotation *= GAEA_SYCL_SRSQRT(magn_sq);
							}

							momentum_copy_accessor[body_idx] = momentum_accessor[body_idx];
							position_copy_accessor[body_idx] = position_accessor[body_idx];
						}
					);
				}
			);
		}

		return sycl_type::sequence_event{ first_event, last_event };
	}
}

#endif
