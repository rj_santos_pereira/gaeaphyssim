#include "engine/physics/dynamics/bcd_parallel_sort_sweep_solver.hpp"

namespace gaea
{
	bcd_parallel_sort_sweep_solver::bcd_parallel_sort_sweep_solver()
		: _body_array{}
		, _sort_array{}
		, _grid_min{}
		, _grid_max{}
		, _grid_size{}
		, _grid_stride{}
		, _cell_extent{}
		, _group_index_array{}
#if GAEA_NATIVE_SORT_SWEEP_USING_PARTITION_MAP
		, _partition_index_map{}
#else
		, _partition_index_array{}
#endif
		, _thread_counter{ 0 }
		, _thread_barrier{ 0 }
		, _thread_mutex{}
		, _num_threads{ 0 }
	{
	}

	void bcd_parallel_sort_sweep_solver::add_body(native_rigid_body* body)
	{
		uint32 idx = uint32(_body_array.size());

		bcd_body_data body_data{ body, idx };

		if (std::find(_body_array.begin(), _body_array.end(), body_data) != _body_array.end())
		{
			GAEA_ERROR(bcd_parallel_sort_sweep_solver, "Trying to add body already in partition!");
			return;
		}

		auto const& bounds = body->get_worldspace_bounds();

		bcd_sort_data sort_data{ vector::dot(bounds.min(), vector{ sqrt_one_third_f }), idx };

		_body_array.push_back(body_data);
		_sort_array.insert(std::next(_sort_array.begin(), idx), sort_data);

		_sort_array.insert(_sort_array.end(), math::max(int64(math::ceil_power_two(_body_array.size())) - int64(_sort_array.size()), 0ll),
			bcd_sort_data{ std::numeric_limits<float32>::infinity(), uint32(-1) });
	}

	void bcd_parallel_sort_sweep_solver::remove_body(native_rigid_body* body)
	{
		auto body_it = std::find(_body_array.begin(), _body_array.end(), bcd_body_data{ body, uint32(-1) });
		if (body_it == _body_array.end())
		{
			GAEA_ERROR(bcd_parallel_sort_sweep_solver, "Trying to remove body not in partition!");
			return;
		}

		auto body_idx = std::distance(_body_array.begin(), body_it);
		auto sort_it = std::find_if(_sort_array.begin(), _sort_array.end(),
			[body_idx](bcd_sort_data const& data) { return data.body_idx == body_idx; });

		body_it = _body_array.erase(body_it);
		sort_it = _sort_array.erase(sort_it);
		_sort_array.insert(sort_it, bcd_sort_data{ std::numeric_limits<float32>::infinity(), uint32(-1) });

		while (body_it != _body_array.end())
		{
			_sort_array[body_it->sort_idx].body_idx -= 1;
			++body_it;
		}

#if 0
		int64 padding = int64(_sort_array.size()) - int64(math::ceil_power_two(_body_array.size()));
		if (padding > 0)
		{
			_sort_array.erase(std::prev(_sort_array.end(), padding), _sort_array.end());
		}
#endif
	}

    array<bcd_multi_overlap_data> bcd_parallel_sort_sweep_solver::compute_bounds_overlap()
	{
#if GAEA_DEBUG
		if (_num_threads == 0)
		{
			GAEA_FATAL(bcd_parallel_sort_sweep_solver, "Solver not configured!");
		}
#endif
		
		constexpr vector main_axis{ sqrt_one_third_f };
		constexpr vector cell_extent_factor{ 2.f };

		uint32 thread_index = _thread_counter.fetch_add(1, std::memory_order_relaxed);
		if (thread_index == 0)
		{
			_grid_min = vector{ +std::numeric_limits<float32>::max() };
			_grid_max = vector{ -std::numeric_limits<float32>::max() };

			_cell_extent = zero_vector;
		}

		_thread_barrier.busy_wait();
		
		auto grid_min = vector{ +std::numeric_limits<float32>::max() };
		auto grid_max = vector{ -std::numeric_limits<float32>::max() };
		
		auto cell_extent = zero_vector;

		for (uint32 index = thread_index; index < _sort_array.size(); index += _num_threads)
		{
			auto& sort_data = _sort_array[index];

			if (sort_data.body_idx != uint32(-1))
			{
				auto& body_data = _body_array[sort_data.body_idx];

				auto const& bounds = body_data.body_ptr->get_worldspace_bounds();

				sort_data.min_bounds = vector::dot(bounds.min(), main_axis);

				grid_min = vector::min(grid_min, bounds.min());
				grid_max = vector::max(grid_max, bounds.max());

				if (body_data.body_ptr->get_mobility() == body_mobility::movable)
				{
					cell_extent = vector::max(cell_extent, bounds.scale_by(cell_extent_factor).extent());
				}
			}
		}
		
		{
			std::scoped_lock<spin_mutex> lock{ _thread_mutex };

			_grid_min = vector::min(_grid_min, grid_min);
			_grid_max = vector::max(_grid_max, grid_max);

			_cell_extent = vector::max(_cell_extent, cell_extent);
		}

		_thread_barrier.wait();

		uint32 num_elements = uint32(math::floor_power_two(_sort_array.size()));
		uint32 num_stages = uint32(std::log2(num_elements));

		num_elements >>= 1;

		for (uint32 stage = 0; stage < num_stages; ++stage)
		{
			uint32 num_substages = stage + 1;

			for (uint32 substage = 0; substage < num_substages; ++substage)
			{
				uint32 pair_distance_power = stage - substage;
				uint32 pair_distance = 1u << pair_distance_power;

				uint32 block_size = pair_distance << 1;

				for (uint32 index = thread_index; index < num_elements; index += _num_threads)
				{
					// Same as (thread_idx / pair_distance) * block_size + (thread_idx % pair_distance)
					// but takes advantage of the fact that pair_distance is a power of two
					uint32 left_idx = (index >> pair_distance_power) * block_size + (index & (pair_distance - 1));
					uint32 right_idx = left_idx + pair_distance;

					auto left_elem = _sort_array[left_idx];
					auto right_elem = _sort_array[right_idx];

					bool is_reversed = (index >> stage) & 1;
					bool is_sorted = left_elem.min_bounds < right_elem.min_bounds;

					bool should_swap = is_reversed == is_sorted; // XNOR

					_sort_array[left_idx] = should_swap ? right_elem : left_elem;
					_sort_array[right_idx] = should_swap ? left_elem : right_elem;
				}

				_thread_barrier.busy_wait();
			}
		}

		if (thread_index == 0 && !_cell_extent.exactly_zero())
		{
			auto grid_extent = _grid_max - _grid_min;
			
			// Calculate rough size of the grid on each dimension
			// Adjust this size to the nearest integer size not greater than the estimation
			_grid_size = (grid_extent / _cell_extent).trunc();
			// Define mapping between vector grid index and scalar grid index
			_grid_stride = vector{ 1.f, _grid_size.x, _grid_size.x * _grid_size.y };

			// Recalculate cell extent based on adjusted grid size
			// This extent is always equal or greater than the initial calculation, which is fine
			_cell_extent = grid_extent / _grid_size;

#if GAEA_NATIVE_SORT_SWEEP_USING_PARTITION_MAP
			_partition_index_map.clear();
#else
			_partition_index_array = array<atomic_uint32>(uint64(_grid_size.x * _grid_size.y * _grid_size.z));
#endif
			std::for_each(_group_index_array.begin(), _group_index_array.end(), 
				[](atomic_uint32& elem)
				{
					elem.store(0, std::memory_order_relaxed);
				});
		}

		for (uint32 index = thread_index; index < _body_array.size(); index += _num_threads)
		{
			auto& sort_data = _sort_array[index];
			auto& body_data = _body_array[sort_data.body_idx];

			body_data.sort_idx = index;
		}

		_thread_barrier.wait();

		array<bcd_multi_overlap_data> overlap_array;

	    for (uint32 index = thread_index; index < _body_array.size(); index += _num_threads)
		{
			auto& body_data = _body_array[index];

			auto& overlap_data = overlap_array.emplace_back(body_data.body_ptr);

	        auto body_entity_type = body_data.body_ptr->get_collision_entity_type();
			auto body_behavior_type = body_data.body_ptr->get_collision_behavior_type();

			auto const& body_bounds = body_data.body_ptr->get_worldspace_bounds();
	    	auto const& body_position = body_data.body_ptr->get_position();

			if (!_cell_extent.exactly_zero())
			{
				// Assign cell to body, calculate cell group and index according to computed grid info
				auto cell_index = ((body_position - _grid_min) / _cell_extent).trunc();
				// Map cell_index to singular grid_index
				auto grid_index = uint32(vector::dot(cell_index, _grid_stride));

				uint8 partition_group = (uint32(cell_index.x) & 0x01) + ((uint32(cell_index.y) << 1) & 0x03) + ((uint32(cell_index.z) << 2) & 0x07);
				uint32 partition_index = 0;

#if GAEA_NATIVE_SORT_SWEEP_USING_PARTITION_MAP
				{
					std::unique_lock<spin_mutex> lock{ _thread_mutex };

					auto has_element = _partition_index_map.find(grid_index) != _partition_index_map.end();
					auto& group_index = _partition_index_map[grid_index];

					partition_index = has_element ? (group_index = _group_index_array[partition_group].fetch_add(1, std::memory_order_relaxed)) : group_index;
				}
#else
				{
					constexpr uint32 partition_unmarked_mask = 0x7FFF'FFFF;
					constexpr uint32 partition_marked_mask = 0x8000'0000;

					auto& group_index = _partition_index_array[grid_index];

					// Carefully use CAS to ensure unique assignment, CAS marks which thread should assign a new index
					if (group_index.compare_exchange_strong(partition_index, partition_unmarked_mask, std::memory_order_relaxed))
					{
						partition_index = _group_index_array[partition_group].fetch_add(1, std::memory_order_relaxed) | partition_marked_mask;
						group_index.store(partition_index, std::memory_order_relaxed);
					}
					else
					{
						while ((partition_index & partition_marked_mask) == 0)
						{
							std::this_thread::yield();
							partition_index = group_index.load(std::memory_order_relaxed);
						}
					}

					// Remove flag to avoid problems
					partition_index &= partition_unmarked_mask;
				}
#endif

				body_data.body_ptr->set_body_partition(partition_group, partition_index);
			}

			float32 max_bounds = vector::dot(body_bounds.max(), main_axis);

			uint32 sweep_idx = body_data.sort_idx + 1;
			while (sweep_idx < _body_array.size() && _sort_array[sweep_idx].min_bounds < max_bounds)
			{
				auto& other_body_data = _body_array[_sort_array[sweep_idx].body_idx];

				auto other_body_entity_type = other_body_data.body_ptr->get_collision_entity_type();
				auto other_body_behavior_type = other_body_data.body_ptr->get_collision_behavior_type();

				if (should_collide(body_entity_type, body_behavior_type, 
					other_body_entity_type, other_body_behavior_type))
				{
					auto const& other_body_bounds = other_body_data.body_ptr->get_worldspace_bounds();

					if (base_axis_aligned_bounding_box::overlap(body_bounds, other_body_bounds))
					{
						overlap_data.other_body_array.push_back(other_body_data.body_ptr);
					}
				}

				++sweep_idx;
			}
		}

		_thread_counter.store(0, std::memory_order_relaxed);

		return overlap_array;
	}

	void bcd_parallel_sort_sweep_solver::set_num_threads(uint32 num_threads)
	{
		_thread_barrier.set_initial_count(num_threads);
		_num_threads = num_threads;
	}

	uint32 bcd_parallel_sort_sweep_solver::get_num_threads() const
	{
		return _num_threads;
	}
}
