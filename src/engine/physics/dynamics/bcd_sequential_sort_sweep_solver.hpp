#ifndef ENGINE_PHYSICS_DYNAMICS_BCD_SEQUENTIAL_SORT_SWEEP_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_BCD_SEQUENTIAL_SORT_SWEEP_SOLVER_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/physics/native_rigid_body.hpp"
#include "library/containers.hpp"

// Also see https://www.math.ucsd.edu/~sbuss/ResearchWeb/EnhancedSweepPrune/SAP_paper_online.pdf

namespace gaea
{
	struct bcd_overlap_data
	{
		native_rigid_body* body_1;
		native_rigid_body* body_2;
	};
	
	class bcd_sequential_sort_sweep_solver final
	{
		struct bcd_bounds_data
		{
			uint32 body_idx : 31;
			uint32 is_min : 1;
			float32 coord;

			bool operator<(bcd_bounds_data const& other) const
			{
				return coord < other.coord;
			}
		};
		
		struct bcd_potential_overlap_data
		{
			uint32 body_1_idx;
			uint32 body_2_idx;

			bool operator<(bcd_potential_overlap_data const& other) const
			{
				return (uint64(body_1_idx) << 32 | body_2_idx) < (uint64(other.body_1_idx) << 32 | other.body_2_idx);
			}
		};
		
	public:
		explicit bcd_sequential_sort_sweep_solver();

		bcd_sequential_sort_sweep_solver(bcd_sequential_sort_sweep_solver const&) = delete;
		bcd_sequential_sort_sweep_solver(bcd_sequential_sort_sweep_solver&&) = delete;

		bcd_sequential_sort_sweep_solver& operator=(bcd_sequential_sort_sweep_solver const&) = delete;
		bcd_sequential_sort_sweep_solver& operator=(bcd_sequential_sort_sweep_solver&&) = delete;

		void add_body(native_rigid_body* body);
		void remove_body(native_rigid_body* body);

		array<bcd_overlap_data> compute_bounds_overlap();

	private:
		array<native_rigid_body*> _body_array;
		array<bcd_bounds_data> _bounds_array;

		array_set<bcd_potential_overlap_data> _potential_overlap_set;
		
	};
}

#endif