#ifndef ENGINE_PHYSICS_DYNAMICS_BCD_PARALLEL_SORT_SWEEP_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_BCD_PARALLEL_SORT_SWEEP_SOLVER_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/physics/native_rigid_body.hpp"
#include "library/atomic.hpp"
#include "library/containers.hpp"
#include "library/mutex.hpp"

#define GAEA_NATIVE_SORT_SWEEP_USING_PARTITION_MAP 1

namespace gaea
{
	struct bcd_multi_overlap_data
	{
		explicit bcd_multi_overlap_data(native_rigid_body* body)
			: body_ptr{ body }
		{
		}
		
		native_rigid_body* body_ptr;
		array<native_rigid_body*> other_body_array;
	};

	class bcd_parallel_sort_sweep_solver final
	{
		struct bcd_body_data
		{
			native_rigid_body* body_ptr;
			uint32 sort_idx;

			bool operator==(bcd_body_data const& other) const
			{
				return body_ptr == other.body_ptr;
			}
		};

		struct bcd_sort_data
		{
			float32 min_bounds;
			uint32 body_idx;

			bool operator<(bcd_sort_data const& other) const
			{
				return min_bounds < other.min_bounds;
			}
		};

	public:
		explicit bcd_parallel_sort_sweep_solver();

		bcd_parallel_sort_sweep_solver(bcd_parallel_sort_sweep_solver const&) = delete;
		bcd_parallel_sort_sweep_solver(bcd_parallel_sort_sweep_solver&&) = delete;

		bcd_parallel_sort_sweep_solver& operator=(bcd_parallel_sort_sweep_solver const&) = delete;
		bcd_parallel_sort_sweep_solver& operator=(bcd_parallel_sort_sweep_solver&&) = delete;

		void add_body(native_rigid_body* body);
		void remove_body(native_rigid_body* body);

		array<bcd_multi_overlap_data> compute_bounds_overlap();

		void set_num_threads(uint32 num_threads);

		uint32 get_num_threads() const;

	private:
		array<bcd_body_data> _body_array;
		array<bcd_sort_data> _sort_array;

		// Body partitioning grid/cell info
		vector _grid_min;
		vector _grid_max;
		vector _grid_size;
		vector _grid_stride;
		vector _cell_extent;
		
		fixed_array<atomic_uint32, 8> _group_index_array;
#if GAEA_NATIVE_SORT_SWEEP_USING_PARTITION_MAP
		map<uint32, uint32> _partition_index_map;
#else
		array<atomic_uint32> _partition_index_array;
#endif

		atomic_uint32 _thread_counter;
		barrier _thread_barrier;
		spin_mutex _thread_mutex;

		uint32 _num_threads;

	};
}

#endif