#ifndef ENGINE_PHYSICS_DYNAMICS_SYCL_CR_LOCAL_SYMPLECTIC_EULER_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_SYCL_CR_LOCAL_SYMPLECTIC_EULER_SOLVER_INCLUDE_GUARD

#include "core/core_sycl.hpp"

#include "engine/physics/sycl_physics.hpp"
#include "engine/physics/dynamics/sycl_bcd_sort_sweep_solver.hpp"
#include "engine/physics/dynamics/sycl_ncd_separating_axis_test_solver.hpp"

#if GAEA_USING_SYCL

namespace gaea::sycl_data
{
	struct cr_tensor_data
	{
		float32 mass = 0.f;
		matrix3 inertia = matrix3{ zero_matrix_tag{} };

		float32 mass_inverse = 0.f;
		matrix3 inertia_inverse = matrix3{ zero_matrix_tag{} };
	};
	
	struct cr_contact_data
	{
		vector contact_point;

		vector contact_lever_1 = zero_vector;
		vector contact_lever_2 = zero_vector;

		float32 normal_impulse = 0.f;
		float32 i_tangent_impulse = 0.f;
		float32 j_tangent_impulse = 0.f;

		float32 normal_mass = 0.f;
		float32 i_tangent_mass = 0.f;
		float32 j_tangent_mass = 0.f;

		float32 bias_velocity;
	};

	struct cr_constraint_data
	{
		uint32 body_1_index;
		uint32 body_2_index;
		
		vector contact_normal;
		vector i_contact_tangent;
		vector j_contact_tangent;

		fixed_array<cr_contact_data, 4> contact_data_array;

		float32 penetration_depth;

		float32 restitution_coefficient;
		float32 friction_coefficient;

		uint32 batch_index;
		
		uint32 collision_index;
		uint8 collision_group : 4;
		
		uint8 num_contact_data : 3;

	};
}

namespace gaea::sycl_kernel
{
	sycl_type::sequence_event cr_solver_constraint_batching(sycl::queue& command_queue, uint32 num_bodies,
															uint32 num_batches, uint8 restitution_mode, uint8 friction_mode,
															sycl::buffer<sycl_data::statics_data>& statics_buffer,
															sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
															sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer, 
															sycl::buffer<sycl_data::ncd_overlap_data>& ncd_overlap_compute_buffer,
															sycl::buffer<sycl_data::cr_constraint_data>& cr_constraint_compute_buffer, 
															sycl::buffer<uint32>& cd_overlap_count_compute_buffer,
															sycl::buffer<uint32>& cr_overlap_count_compute_buffer,
															sycl::buffer<uint32>& overlap_limit_compute_buffer,
															sycl::buffer<uint32>& constraint_count_compute_buffer,
															sycl::buffer<uint32>& constraint_index_compute_buffer,
															uint32 num_overlap_work_groups,
															uint32 num_overlap_work_items_per_group);
	
	sycl_type::sequence_event cr_solver_local_symplectic_euler(sycl::queue& command_queue, uint32 num_bodies,
															   uint32 num_batches, uint32 num_iterations, 
															   float32 substep_delta, uint32 substep_count,
															   float32 depenetration_weight, float32 depenetration_bias, 
															   float32 restitution_threshold, float32 restitution_attenuation, float32 restitution_offset,
															   sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
															   sycl::buffer<sycl_data::force_data>& force_buffer, 
															   sycl::buffer<sycl_data::mass_data>& mass_buffer,
															   sycl::buffer<sycl_data::statics_data>& statics_buffer,
															   sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer,
															   sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer,
															   sycl::buffer<sycl_data::cr_constraint_data>& cr_constraint_compute_buffer, 
															   sycl::buffer<uint32>& constraint_count_compute_buffer,
															   sycl::buffer<uint32>& constraint_index_compute_buffer,
															   uint32 num_overlap_work_groups,
															   uint32 num_overlap_work_items_per_group);
}

#endif

#endif
