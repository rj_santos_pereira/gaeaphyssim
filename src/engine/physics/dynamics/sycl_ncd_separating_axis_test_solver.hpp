#ifndef ENGINE_PHYSICS_DYNAMICS_SYCL_NCD_SEPARATING_AXIS_TEST_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_SYCL_NCD_SEPARATING_AXIS_TEST_SOLVER_INCLUDE_GUARD

#include "core/core_sycl.hpp"

#include "engine/physics/sycl_physics.hpp"
#include "engine/physics/dynamics/sycl_bcd_sort_sweep_solver.hpp"

#if GAEA_USING_SYCL

namespace gaea::sycl_data
{
	enum class ncd_penetration_type : uint8
	{
		none,
		face_1,
		face_2,
		edge,
	};

	struct ncd_body_data
	{
		uint32 body_1_index;
		uint32 body_2_index;
	};

	struct ncd_plane_data
	{
		vector point;
		vector normal;
	};

	struct ncd_overlap_data
	{
		ncd_penetration_type type;
		
		uint32 face_index;
		uint32 edge_1_index;
		uint32 edge_2_index;

		vector contact_normal;
		float32 penetration_depth;

		fixed_array<vector, 4> contact_point_array;
		uint8 num_contact_points;
	};
}

namespace gaea::sycl_kernel
{
	sycl_type::sequence_event ncd_solver_separating_axis_test(sycl::queue& command_queue, uint32 num_bodies,
															  sycl::buffer<sycl_data::statics_data>& statics_buffer,
															  sycl::buffer<sycl_data::position_data>& position_buffer,
															  sycl::buffer<sycl_data::geometry_data>& geometry_buffer,
															  sycl::buffer<sycl_data::geometry_face_data>& geometry_face_buffer,
															  sycl::buffer<sycl_data::geometry_vertex_data>& geometry_vertex_buffer,
															  sycl::buffer<sycl_data::geometry_halfedge_data>& geometry_halfedge_buffer, 
															  sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
															  sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer, 
															  sycl::buffer<sycl_data::geometry_face_data>& geometry_face_compute_buffer,
															  sycl::buffer<sycl_data::geometry_vertex_data>& geometry_vertex_compute_buffer,
															  sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
															  sycl::buffer<sycl_data::ncd_overlap_data>& ncd_overlap_compute_buffer,
															  sycl::buffer<uint32>& cd_overlap_count_compute_buffer,
															  sycl::buffer<uint32>& overlap_limit_compute_buffer);
	
	sycl_type::sequence_event ncd_solver_contact_generation(sycl::queue& command_queue, uint32 num_bodies,
															sycl::buffer<sycl_data::statics_data>& statics_buffer,
															sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
															sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer, 
															sycl::buffer<sycl_data::geometry_face_data>& geometry_face_compute_buffer,
															sycl::buffer<sycl_data::geometry_vertex_data>& geometry_vertex_compute_buffer,
															sycl::buffer<sycl_data::geometry_halfedge_data>& geometry_halfedge_buffer, 
															sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
															sycl::buffer<sycl_data::ncd_overlap_data>& ncd_overlap_compute_buffer,
															sycl::buffer<uint32>& cd_overlap_count_compute_buffer,
															sycl::buffer<uint32>& overlap_limit_compute_buffer);
}

#endif

#endif
