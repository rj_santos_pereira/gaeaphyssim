#ifndef ENGINE_PHYSICS_DYNAMICS_CR_LOCAL_SYMPLECTIC_EULER_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_CR_LOCAL_SYMPLECTIC_EULER_SOLVER_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/physics/dynamics/bcd_sequential_sort_sweep_solver.hpp"
#include "engine/physics/dynamics/ncd_separating_axis_test_solver.hpp"

namespace gaea
{
	struct cr_contact_data
	{
		vector contact_point;

		vector contact_lever_1 = zero_vector;
		vector contact_lever_2 = zero_vector;
		
		float32 normal_impulse = 0.f;
		float32 i_tangent_impulse = 0.f;
		float32 j_tangent_impulse = 0.f;

		float32 normal_mass = 0.f;
		float32 i_tangent_mass = 0.f;
		float32 j_tangent_mass = 0.f;

		float32 bias_velocity;
	};
	
	struct cr_constraint_data
	{
		vector contact_normal;
		vector i_contact_tangent;
		vector j_contact_tangent;

		fixed_array<cr_contact_data, 4> contact_data_array;

		float32 penetration_depth;

		float32 restitution_coefficient;
		float32 friction_coefficient;

		float32 cache_lifetime;
		
		uint8 num_contact_data;
	};
	
	class cr_local_symplectic_euler_solver
	{
	public:
		explicit cr_local_symplectic_euler_solver(float32 cache_lifetime = 1.f, uint32 num_iterations = 10,
												  float32 depenetration_weight = 0.2f, 
												  float32 depenetration_bias = 0.01f,
												  float32 restitution_threshold = 1.0f,
												  float32 restitution_attenuation = 0.1f,
												  collision_mode restitution_mode = collision_mode::arithmetic, 
												  collision_mode friction_mode = collision_mode::geometric);

		cr_local_symplectic_euler_solver(cr_local_symplectic_euler_solver const&) = delete;
		cr_local_symplectic_euler_solver(cr_local_symplectic_euler_solver&&) = delete;

		cr_local_symplectic_euler_solver& operator=(cr_local_symplectic_euler_solver const&) = delete;
		cr_local_symplectic_euler_solver& operator=(cr_local_symplectic_euler_solver&&) = delete;
		
		void add_body(native_rigid_body* rigid_body);
		void remove_body(native_rigid_body* rigid_body);

		void apply_world_force(vector world_force);
		void apply_world_torque(vector world_torque);
		void apply_world_linear_accel(vector world_lin_accel);
		void apply_world_angular_accel(vector world_ang_accel);

		void setup_constraint(bcd_overlap_data const& body_data, ncd_overlap_data const& contact_data);

		void compute_dynamics(float32 substep_delta, uint32 substep_count);

		void refresh_cache(float32 delta);

		void set_cache_lifetime(float32 cache_lifetime);
		
		void set_num_threads(uint32 num_threads);
		void set_num_iterations(uint32 num_iterations);

		void set_depenetration_weight(float32 depenetration_weight);
		void set_depenetration_bias(float32 depenetration_bias);

		void set_restitution_threshold(float32 restitution_threshold);
		void set_restitution_attenuation(float32 restitution_attenuation);

		void set_restitution_mode(collision_mode restitution_mode);
		void set_friction_mode(collision_mode friction_mode);

		float32 get_cache_lifetime() const;

		uint32 get_num_threads() const;
		uint32 get_num_iterations() const;

		float32 get_depenetration_weight() const;
		float32 get_depenetration_bias() const;

		float32 get_restitution_threshold() const;
		float32 get_restitution_attenuation() const;

		collision_mode get_restitution_mode() const;
		collision_mode get_friction_mode() const;

	private:
		void _prepare_lcp_constraint(float32 step_delta, native_rigid_body* body_1, native_rigid_body* body_2, cr_constraint_data& constraint);
		void _solve_lcp_constraint(native_rigid_body* body_1, native_rigid_body* body_2, cr_constraint_data& constraint_data);

		void _integrate_symplectic_euler_dynamics(float32 step_delta, native_rigid_body* body);

		void _calculate_restitution_offset();
		
		array_set<native_rigid_body*> _rigid_body_set;
		
		map<native_rigid_body*, array<native_rigid_body*>> _body_pair_map;
		map<native_body_pair_id, cr_constraint_data, native_body_pair_hash> _constraint_map;

		spin_mutex _constraint_mutex;

		atomic_uint32 _thread_counter;
		barrier _thread_barrier;

		vector _world_force;
		vector _world_torque;
		vector _world_lin_accel;
		vector _world_ang_accel;

		// Solver parameters

		// Baumgarte stabilization
		float32 _depenetration_weight;
		float32 _depenetration_bias;
		
		float32 _restitution_threshold;
		float32 _restitution_attenuation;
		float32 _restitution_offset;

		float32 _cache_lifetime;
		
		uint32 _num_threads;
		uint32 _num_iterations;

		collision_mode _restitution_mode;
		collision_mode _friction_mode;

	};
}

#endif
