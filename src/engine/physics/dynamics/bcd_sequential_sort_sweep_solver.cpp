#include "engine/physics/dynamics/bcd_sequential_sort_sweep_solver.hpp"

namespace gaea
{
	bcd_sequential_sort_sweep_solver::bcd_sequential_sort_sweep_solver()
	{
	}

	void bcd_sequential_sort_sweep_solver::add_body(native_rigid_body* body)
	{
		if (std::find(_body_array.begin(), _body_array.end(), body) != _body_array.end())
		{
			GAEA_ERROR(bcd_sequential_sort_sweep_solver, "Trying to add body already in partition!");
			return;
		}
		
		uint32 new_body_idx = uint32(_body_array.size());
		_body_array.push_back(body);

		constexpr vector main_axis{ sqrt_one_third_f };

		// Avoid reallocations during inserts
		_bounds_array.reserve(_bounds_array.size() + 2);

		auto const& body_bounds = _body_array[new_body_idx]->get_worldspace_bounds();

		auto bounds_min = bcd_bounds_data{ new_body_idx, true , vector::dot(body_bounds.min(), main_axis) };
		auto bounds_max = bcd_bounds_data{ new_body_idx, false, vector::dot(body_bounds.max(), main_axis) };

		// Since max will always be inserted ahead of min, we insert the latter first to prevent max from being invalidated

		auto min_it = std::lower_bound(_bounds_array.begin(), _bounds_array.end(), bounds_min);
		min_it = _bounds_array.insert(min_it, bounds_min);

		auto max_it = std::lower_bound(_bounds_array.begin(), _bounds_array.end(), bounds_max);
		max_it = _bounds_array.insert(max_it, bounds_max);

		auto body_collision_entity = _body_array[new_body_idx]->get_collision_entity_type();
		auto body_collision_behavior = _body_array[new_body_idx]->get_collision_behavior_type();

		while (max_it != ++min_it)
		{
			auto pair_collision_entity = _body_array[min_it->body_idx]->get_collision_entity_type();
			auto pair_collision_behavior = _body_array[min_it->body_idx]->get_collision_behavior_type();

			if (should_collide(body_collision_entity, body_collision_behavior, pair_collision_entity, pair_collision_behavior))
			{
				auto [body_1_idx, body_2_idx] = math::calc_min_max(new_body_idx, min_it->body_idx);

				bcd_potential_overlap_data overlap{ body_1_idx, body_2_idx };

				_potential_overlap_set.insert(overlap);
			}
		}
	}

	void bcd_sequential_sort_sweep_solver::remove_body(native_rigid_body* body)
	{
		auto it = std::find(_body_array.begin(), _body_array.end(), body);
		if (it == _body_array.end())
		{
			GAEA_ERROR(bcd_sequential_sort_sweep_solver, "Trying to remove body not in partition!");
			return;
		}
		std::ptrdiff_t body_idx = std::distance(_body_array.begin(), it);

		auto rem_bounds_lambda = 
			[body_idx](bcd_bounds_data const& bounds)
			{
				return bounds.body_idx == body_idx;
			};
		auto rem_overlap_lambda = 
			[body_idx](bcd_potential_overlap_data const& overlap)
			{
				return overlap.body_1_idx == body_idx || overlap.body_2_idx == body_idx;
			};

		auto bounds_it = std::remove_if(_bounds_array.begin(), _bounds_array.end(), rem_bounds_lambda);
		_bounds_array.erase(bounds_it, _bounds_array.end());
		
		auto overlap_it = std::remove_if(_potential_overlap_set.begin(), _potential_overlap_set.end(), rem_overlap_lambda);
		_potential_overlap_set.erase(overlap_it, _potential_overlap_set.end());
	}

	array<bcd_overlap_data> bcd_sequential_sort_sweep_solver::compute_bounds_overlap()
	{
		constexpr vector main_axis{ sqrt_one_third_f };
		constexpr vector cell_extent_factor{ 2.f };

		vector grid_min{ +std::numeric_limits<float32>::max() };
		vector grid_max{ -std::numeric_limits<float32>::max() };

		vector cell_extent = zero_vector;

		std::for_each(_bounds_array.begin(), _bounds_array.end(),
			[&, this](bcd_bounds_data& bounds_data)
			{
				auto const& bounds = _body_array[bounds_data.body_idx]->get_worldspace_bounds();
				bounds_data.coord = vector::dot(bounds_data.is_min ? bounds.min() : bounds.max(), main_axis);

				if (bounds_data.is_min)
				{
					grid_min = vector::min(grid_min, bounds.min());
					grid_max = vector::max(grid_max, bounds.max());

					if (_body_array[bounds_data.body_idx]->get_mobility() == body_mobility::movable)
					{
						cell_extent = vector::max(cell_extent, bounds.scale_by(cell_extent_factor).extent());
					}
				}
			});

		vector grid_size = zero_vector;
		vector grid_stride = zero_vector;

		fixed_array<uint32, 8> group_size_array{};
		map<uint32, uint32> cell_size_map;

		if (!cell_extent.exactly_zero())
		{
			auto grid_extent = grid_max - grid_min;

			// Calculate rough size of the grid on each dimension
			// Adjust this size to the nearest integer size not greater than the estimation
			grid_size = (grid_extent / cell_extent).trunc();
			// Define mapping between vector grid index and scalar grid index
			grid_stride = vector{ 1.f, grid_size.x, grid_size.x * grid_size.y };

			// Recalculate cell extent based on adjusted grid size
			// This extent is always equal or greater than the initial calculation, which is fine
			cell_extent = grid_extent / grid_size;
		}

		// Simple insertion sort
		for (std::size_t body_idx = 1; body_idx < _bounds_array.size(); ++body_idx)
		{
			float32 body_coord = _bounds_array[body_idx].coord;

			auto body_ptr = _body_array[_bounds_array[body_idx].body_idx];

			auto const& body_position = body_ptr->get_position();

			if (!cell_extent.exactly_zero())
			{
				// Assign cell to body, calculate cell group and index according to computed grid info
				auto body_index = ((body_position - grid_min) / cell_extent).trunc();
				auto grid_index = uint32(vector::dot(body_index, grid_stride));

				auto has_element = cell_size_map.find(grid_index) != cell_size_map.end();
				auto& cell_index = cell_size_map[grid_index];

				uint8 partition_group = (uint32(body_index.x) & 0x01) + ((uint32(body_index.y) << 1) & 0x03) + ((uint32(body_index.z) << 2) & 0x07);
				uint32 partition_index = has_element ? (cell_index = group_size_array[partition_group]++) : cell_index;

				body_ptr->set_body_partition(partition_group, partition_index);
			}

			std::size_t swap_idx = body_idx - 1;
			if (_bounds_array[swap_idx].coord > body_coord)
			{
				auto body_bounds = _bounds_array[body_idx];
				auto body_collision_entity = _body_array[body_bounds.body_idx]->get_collision_entity_type();
				auto body_collision_behavior = _body_array[body_bounds.body_idx]->get_collision_behavior_type();

				while (swap_idx > 0)
				{
					if (_bounds_array[swap_idx - 1].coord <= body_coord)
					{
						break;
					}

					--swap_idx;
				}

				// Recalculate overlap pairs
				for (std::size_t pair_idx = swap_idx; pair_idx < body_idx; ++pair_idx)
				{
					auto const& pair_bounds = _bounds_array[pair_idx];
					auto pair_collision_entity = _body_array[pair_bounds.body_idx]->get_collision_entity_type();
					auto pair_collision_behavior = _body_array[pair_bounds.body_idx]->get_collision_behavior_type();

					// Check if these two bodies are supposed to interact
					if (should_collide(body_collision_entity, body_collision_behavior, pair_collision_entity, pair_collision_behavior))
					{
						// If the end points of the bounds are different in nature,
						// then the bodies will either start or stop intersecting
						if (body_bounds.is_min != pair_bounds.is_min)
						{
							auto [body_1_idx, body_2_idx] = math::calc_min_max(body_bounds.body_idx, pair_bounds.body_idx);

							bcd_potential_overlap_data overlap{ body_1_idx, body_2_idx };

							if (body_bounds.is_min)
							{
								_potential_overlap_set.insert(overlap);
							}
							else
							{
								_potential_overlap_set.erase(overlap);
							}
						}
					}
				}

				// Now swap elements
				std::memmove(&_bounds_array[swap_idx + 1], &_bounds_array[swap_idx], sizeof(bcd_bounds_data) * (body_idx - swap_idx));
				_bounds_array[swap_idx] = body_bounds;
			}
		}

		// Allocate the necessary space for our results (this is a upper bound)
		array<bcd_overlap_data> overlap_array;
		overlap_array.reserve(_potential_overlap_set.size());

		// Then test each body for overlap, and add the bodies that overlap to the array
		std::for_each(_potential_overlap_set.begin(), _potential_overlap_set.end(),
			[this, &overlap_array](bcd_potential_overlap_data const& overlap)
			{
				auto body_1 = _body_array[overlap.body_1_idx];
				auto body_2 = _body_array[overlap.body_2_idx];

				if (base_axis_aligned_bounding_box::overlap(body_1->get_worldspace_bounds(), body_2->get_worldspace_bounds()))
				{
					overlap_array.push_back(bcd_overlap_data{ body_1, body_2 });
				}
			});

		return overlap_array;
	}
}
