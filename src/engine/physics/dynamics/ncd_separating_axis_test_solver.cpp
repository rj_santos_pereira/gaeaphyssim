#include "engine/physics/dynamics/ncd_separating_axis_test_solver.hpp"

#include "engine/physics/native_rigid_body.hpp"

#define USE_CROSS_PRODUCT_FOR_MINKOWSKI_CHECK 0

namespace gaea
{
	ncd_separating_axis_test_solver::ncd_separating_axis_test_solver(float32 cache_lifetime)
		: _cached_separation_map{}
		, _cache_mutex{}
		, _cache_lifetime{ cache_lifetime }
	{
	}

	ncd_overlap_data ncd_separating_axis_test_solver::compute_geometry_overlap(bcd_overlap_data const& overlap_data)
	{
		auto body_pair_id = native_body_pair_id{ overlap_data.body_1, overlap_data.body_2 };
		
		polyhedron const& poly_1 = overlap_data.body_1->get_worldspace_geometry();
		polyhedron const& poly_2 = overlap_data.body_2->get_worldspace_geometry();
		
		ncd_separation_data separation;
		ncd_overlap_data overlap;

		bool is_overlapping =
			// First check if we have cached data for this body pair
			// If so, repeat the test and if still valid, return immediately
			!_compute_cache_poly_overlap(body_pair_id, poly_1, poly_2, separation, overlap)
			&&
			// Otherwise, just run it through the gauntlet
			!_compute_poly_overlap(body_pair_id, poly_1, poly_2, separation, overlap);

		if (is_overlapping)
		{
			// If all tests failed, then the bodies are overlapping,
			// so compute contact manifold, depending on the type of contact
			switch (overlap.type)
			{
			case ncd_penetration_type::face_1:
			{
				_compute_face_contact_manifold(poly_1, poly_2, separation, overlap);
				break;
			}
			case ncd_penetration_type::face_2:
			{
				_compute_face_contact_manifold(poly_2, poly_1, separation, overlap);
				break;
			}
			case ncd_penetration_type::edge:
			{
#if 0
				auto face_1_a = poly_1.query_halfedge(separation.edge_1_index).face();
				auto face_1_b = poly_1.query_halfedge(separation.edge_1_index).pair().face();

				float32 face_1_dot_a = vector::dot(overlap.contact_normal, face_1_a.normal());
				float32 face_1_dot_b = vector::dot(overlap.contact_normal, face_1_b.normal());

				auto face_1_index = face_1_dot_a > face_1_dot_b ? face_1_a.index() : face_1_b.index();
				auto face_1_dot = face_1_dot_a > face_1_dot_b ? face_1_dot_a : face_1_dot_b;

				auto face_2_a = poly_2.query_halfedge(separation.edge_2_index).face();
				auto face_2_b = poly_2.query_halfedge(separation.edge_2_index).pair().face();

				float32 face_2_dot_a = vector::dot(overlap.contact_normal, face_2_a.normal());
				float32 face_2_dot_b = vector::dot(overlap.contact_normal, face_2_b.normal());

				auto face_2_index = face_2_dot_a > face_2_dot_b ? face_2_a.index() : face_2_b.index();
				auto face_2_dot = face_2_dot_a > face_2_dot_b ? face_2_dot_a : face_2_dot_b;

				if (face_1_dot > face_2_dot)
				{
					overlap.type = ncd_penetration_type::face_1;
					separation.face_index = face_1_index;

					_compute_face_contact_manifold(poly_1, poly_2, separation, overlap);
				}
				else
				{
					overlap.type = ncd_penetration_type::face_2;
					separation.face_index = face_2_index;

					_compute_face_contact_manifold(poly_2, poly_1, separation, overlap);
				}
#endif
				_compute_edge_contact_manifold(poly_1, poly_2, separation, overlap);
				break;
			}
			}
		}
		
		return overlap;
	}

	void ncd_separating_axis_test_solver::refresh_cache(float32 delta)
	{
		std::unique_lock<shared_spin_mutex> lock{ _cache_mutex };
		
		auto it = _cached_separation_map.begin();
		auto it_end = _cached_separation_map.end();

		while (it != it_end)
		{
			float32& lifetime = it->second.cache_lifetime;
			if (lifetime > 0.f)
			{
				// Reduce lifetime, cache will be invalidated when it is zero
				lifetime -= delta;
				++it;
			}
			else
			{
				// Invalid cache from last frame, clean it up
				it = _cached_separation_map.erase(it);
			}
		}
	}

	void ncd_separating_axis_test_solver::set_cache_lifetime(float32 cache_lifetime)
	{
		_cache_lifetime = cache_lifetime;
	}

	float32 ncd_separating_axis_test_solver::get_cache_lifetime() const
	{
		return _cache_lifetime;
	}

	bool ncd_separating_axis_test_solver::_compute_cache_poly_overlap(native_body_pair_id const& body_pair_id, 
	                                                                  polyhedron const& poly_1, polyhedron const& poly_2,
	                                                                  ncd_separation_data& separation, ncd_overlap_data& overlap)
	{
		std::shared_lock<shared_spin_mutex> lock{ _cache_mutex };

		auto it = _cached_separation_map.find(body_pair_id);
		if (it != _cached_separation_map.end())
		{
			auto& [_, cached_separation] = *it;
			
			switch (cached_separation.type)
			{
			case ncd_penetration_type::face_1: 
			{
				auto face_1 = poly_1.query_face(cached_separation.face_index);

				if (_compute_face_overlap(face_1, poly_2, cached_separation, overlap))
				{
					cached_separation.cache_lifetime = _cache_lifetime;
					return true;
				}
				
				break;
			}
			case ncd_penetration_type::face_2:
			{
				auto face_2 = poly_2.query_face(cached_separation.face_index);

				if (_compute_face_overlap(face_2, poly_1, cached_separation, overlap))
				{
					cached_separation.cache_lifetime = _cache_lifetime;
					return true;
				}

				break;
			}
			case ncd_penetration_type::edge:
			{
				vector const& center_1 = poly_1.center();

				auto edge_1 = poly_1.query_halfedge(cached_separation.edge_1_index);
				auto edge_2 = poly_2.query_halfedge(cached_separation.edge_2_index);

				vector edge_line_1 = edge_1.vertex_point() - edge_1.pair().vertex_point();
				vector edge_line_2 = edge_2.vertex_point() - edge_2.pair().vertex_point();

				vector axis = vector::cross(edge_line_1, edge_line_2);

				if (_is_minkowski_difference_face(edge_1, edge_2, axis) 
					&& _compute_edge_overlap(edge_1, edge_2, center_1, axis, cached_separation, overlap))
				{
					cached_separation.cache_lifetime = _cache_lifetime;
					return true;
				}

				break;
			}
			}
			
			separation = cached_separation;
		}
		
		return false;
	}

	bool ncd_separating_axis_test_solver::_compute_poly_overlap(native_body_pair_id const& body_pair_id, 
																polyhedron const& poly_1, polyhedron const& poly_2, 
																ncd_separation_data& separation, ncd_overlap_data& overlap)
	{
		separation.cache_lifetime = _cache_lifetime;

		separation.type = ncd_penetration_type::face_1;
		if (_compute_poly_face_overlap(poly_1, poly_2, separation, overlap))
		{
			std::unique_lock<shared_spin_mutex> lock{ _cache_mutex };

			_cached_separation_map.insert_or_assign(body_pair_id, separation);

			return true;
		}

		separation.type = ncd_penetration_type::face_2;
		if (_compute_poly_face_overlap(poly_2, poly_1, separation, overlap))
		{
			std::unique_lock<shared_spin_mutex> lock{ _cache_mutex };

			_cached_separation_map.insert_or_assign(body_pair_id, separation);

			return true;
		}

		separation.type = ncd_penetration_type::edge;
		if (_compute_poly_edge_overlap(poly_1, poly_2, separation, overlap))
		{
			std::unique_lock<shared_spin_mutex> lock{ _cache_mutex };

			_cached_separation_map.insert_or_assign(body_pair_id, separation);

			return true;
		}

		return false;
	}
	
	bool ncd_separating_axis_test_solver::_compute_poly_face_overlap(polyhedron const& poly_1, polyhedron const& poly_2, 
																	 ncd_separation_data& separation, ncd_overlap_data& overlap)
	{
		for (std::size_t index = 0; index < poly_1.face_count(); ++index) // TODO implement half-edge visiting
		{
			auto face_1 = poly_1.query_face(index);
			
			if (_compute_face_overlap(face_1, poly_2, separation, overlap))
			{
				return true;
			}
		}

		return false;
	}

	bool ncd_separating_axis_test_solver::_compute_poly_edge_overlap(polyhedron const& poly_1, polyhedron const& poly_2, 
																	 ncd_separation_data& separation, ncd_overlap_data& overlap)
	{
		vector const& center_1 = poly_1.center();

		for (std::size_t index_1 = 0; index_1 < poly_1.halfedge_count(); index_1 += 2) // TODO implement half-edge visiting
		{
			auto edge_1 = poly_1.query_halfedge(index_1);

			for (std::size_t index_2 = 0; index_2 < poly_2.halfedge_count(); index_2 += 2) // TODO implement half-edge visiting
			{
				auto edge_2 = poly_2.query_halfedge(index_2);

				vector axis;
				if (_is_minkowski_difference_face(edge_1, edge_2, axis)
					&& _compute_edge_overlap(edge_1, edge_2, center_1, axis, separation, overlap))
				{	
					return true;
				}
			}
		}
		
		return false;
	}

	bool ncd_separating_axis_test_solver::_compute_face_overlap(polyhedron_face_query const& face_1, polyhedron const& poly_2,
																ncd_separation_data& separation, ncd_overlap_data& overlap)
	{
		// Find the normal and a vertex that belongs to this face
		vector const& normal_1 = face_1.normal();
		vector const& vertex_1 = face_1.halfedge().vertex_point();

		// Find the support vertex for this poly along the opposite direction of the face normal
		vector vertex_2 = poly_2.support_vertex(-normal_1);

		// Compute distance of vertex from plane

		// Distance is 'sign_dist = ax + by + cz + d / sqrt(a^2 + b^2 + c^2) <=> sign_dist = ax + by + cz + d', when 'n = (a, b, c)' and '||n|| = 1';
		// For a plane equation 'ax + by + cz + d = 0' where 'n = (a, b, c)', we have 'd = -(n * p)' where p is a point on the plane;
		// Then for any point q, 'sign_dist = n * q - (n * p) <=> sign_dist = n * (q - p)'

		// Invert vertexes to obtain penetration instead of distance
		float32 depth = vector::dot(normal_1, vertex_1 - vertex_2);
		if (depth < overlap.penetration_depth)
		{
			overlap.type = separation.type;
			
			overlap.penetration_depth = depth;
			overlap.contact_normal = normal_1;

			separation.face_index = face_1.index();
		}

		return overlap.penetration_depth <= 0.0f;
	}

	bool ncd_separating_axis_test_solver::_compute_edge_overlap(polyhedron_halfedge_query const& edge_1, polyhedron_halfedge_query const& edge_2,
																vector const& center_1, vector const& axis, 
																ncd_separation_data& separation, ncd_overlap_data& overlap)
	{
		vector const& vertex_1 = edge_1.vertex_point();
		vector const& vertex_2 = edge_2.vertex_point();
		
		vector normal = axis.unit();

		if (vector::dot(normal, vertex_1 - center_1) < 0.f)
		{
			normal = -normal;
		}

		// Invert vertexes to obtain penetration instead of distance
		float32 depth = vector::dot(normal, vertex_1 - vertex_2);
		if (depth < overlap.penetration_depth)
		{
			overlap.type = separation.type;
			
			overlap.penetration_depth = depth;
			overlap.contact_normal = normal;
			
			separation.edge_1_index = edge_1.index();
			separation.edge_2_index = edge_2.index();
		}

		return overlap.penetration_depth <= 0.0f;
	}

	bool ncd_separating_axis_test_solver::_is_minkowski_difference_face(polyhedron_halfedge_query const& edge_1, polyhedron_halfedge_query const& edge_2, 
																		vector& axis)
	{
		// Using Gauss maps, check if arcs AB and CD intersect on the unit sphere

		vector const& a = edge_1.face_normal();
		vector const& b = edge_1.pair().face_normal();
		vector const& c = edge_2.face_normal();
		vector const& d = edge_2.pair().face_normal();

#if USE_CROSS_PRODUCT_FOR_MINKOWSKI_CHECK
		vector ba = vector::cross(b, a);
		vector dc = vector::cross(d, c);
#else
		vector ba = edge_1.vertex_point() - edge_1.pair().vertex_point();
		vector dc = edge_2.vertex_point() - edge_2.pair().vertex_point();
#endif

		// Exit early if edges are parallel, otherwise,
		// we'll use the resulting axis in further calculations,
		// as long as the Minkowski test passes
		
		axis = vector::cross(ba, dc);
		if (math::nearly_zero(axis.magnitude_square()))
		{
			return false;
		}
		
		// If the dot products between the vertexes of one arc
		// with the axis of the plane that contains the other arc
		// have differing signs, then the arcs potentially intersect;
		// need to repeat the test for each arc and check hemisphere

		// Need to negate one of the edges to compute the Minkowski difference,
		// otherwise it would test the Minkowski sum
		
		float32 cba = vector::dot(-c, ba);
		float32 dba = vector::dot(-d, ba);
		
		float32 adc = vector::dot(a, dc);
		float32 bdc = vector::dot(b, dc);

		// Hemisphere test optimization
		// Original test:
		// (a * (c x b)) * (d * (c x b)) > 0
		// Identities:
		// a * (c x b) == c * (b x a)
		// d * (c x b) == b * (d x c)
		// Optimized test:
		// (c * (b x a)) * (b * (d x c)) > 0

		// First check tests if the arcs are on the same hemisphere,
		// remaining checks test for intersection
		
		return cba * bdc > 0.f
			&& cba * dba < 0.f
			&& adc * bdc < 0.f;
	}

	void ncd_separating_axis_test_solver::_compute_face_contact_manifold(polyhedron const& poly_1, polyhedron const& poly_2,
																		 ncd_separation_data const& separation, ncd_overlap_data& overlap)
	{
		// https://gamedevelopment.tutsplus.com/tutorials/understanding-sutherland-hodgman-clipping-for-physics-engines--gamedev-11917
		
		struct plane
		{
			vector point;
			vector normal;
		};

		array<plane> reference_clip_plane_array;
		array<vector> incident_face_point_array;

		polyhedron_face_query reference_face = poly_1.query_face(separation.face_index);
		polyhedron_halfedge_query reference_edge = reference_face.halfedge();

		vector const& reference_normal = reference_face.normal();
		vector const& reference_point = reference_edge.vertex_point();

		// Compute reference face side planes for candidate contact clipping
		{
			polyhedron_halfedge_query current_edge = reference_edge;

			vector first_point = reference_point;
			current_edge = current_edge.next();

			int32 last_edge_index = current_edge.index();
			do
			{
				vector const& second_point = current_edge.vertex_point();
				vector edge_direction = (second_point - first_point).normalize();

				reference_clip_plane_array.push_back(plane{ first_point, vector::cross(reference_normal, edge_direction) });

				first_point = second_point;
				current_edge = current_edge.next();
			}
			while (current_edge.index() != last_edge_index);
		}

		polyhedron_face_query incident_face;

		// Determine maximally antiparallel incident face
		{
			std::size_t maximal_antiparallel_index = 0;
			float32 maximal_antiparallel = 2.f;

			for (std::size_t index = 0; index < poly_2.face_count(); ++index)
			{
				incident_face = poly_2.query_face(index);

				float32 antiparallel = vector::dot(reference_normal, incident_face.normal());
				if (antiparallel < maximal_antiparallel)
				{
					maximal_antiparallel = antiparallel;
					maximal_antiparallel_index = index;
				}
			}

			incident_face = poly_2.query_face(maximal_antiparallel_index);
		}

		// Compute incident edge lines for candidate contact clipping
		{
			polyhedron_halfedge_query current_edge = incident_face.halfedge();
			
			int32 last_edge_index = current_edge.index();
			do
			{
				incident_face_point_array.push_back(current_edge.vertex_point());

				current_edge = current_edge.next();
			}
			while (current_edge.index() != last_edge_index);
		}

		// Clip incident lines with reference side planes to determine candidate contacts
		{
			for (plane const& clip_plane : reference_clip_plane_array)
			{
				array<vector> clip_incident_face_point_array;
				clip_incident_face_point_array.reserve(2 * incident_face_point_array.size());
				
				vector last_point = incident_face_point_array.back();
				bool should_keep_last_point = vector::dot(clip_plane.normal, last_point - clip_plane.point) > 0.f;
				
				for (std::size_t index = 0; index < incident_face_point_array.size(); ++index)
				{
					vector const& current_point = incident_face_point_array[index];
					bool should_keep_current_point = vector::dot(clip_plane.normal, current_point - clip_plane.point) > 0.f;

					if (should_keep_current_point && should_keep_last_point)
					{
						clip_incident_face_point_array.push_back(last_point);
					}
					else if (should_keep_current_point && !should_keep_last_point)
					{
						clip_incident_face_point_array.push_back(
							vector::project_point_onto_plane(last_point,
								clip_plane.normal, clip_plane.point, 
								(current_point - last_point).normalize())
						);
					}
					else if (!should_keep_current_point && should_keep_last_point)
					{
						clip_incident_face_point_array.push_back(last_point);
						
						clip_incident_face_point_array.push_back(
							vector::project_point_onto_plane(last_point,
								clip_plane.normal, clip_plane.point, 
								(current_point - last_point).normalize())
						);
					}

					last_point = current_point;
					should_keep_last_point = should_keep_current_point;
				}

				incident_face_point_array = std::move(clip_incident_face_point_array);

				// Fail-safe to avoid crashing when contact is weird
				if (incident_face_point_array.empty())
				{
					GAEA_ERROR(ncd_separating_axis_test_solver, "Error during contact generation!");
					overlap.num_contact_points = 0;
					break;
				}
			}
		}

		array<vector> projection_face_point_array;
		projection_face_point_array.reserve(incident_face_point_array.size());

		std::size_t maximal_depth_index = 0;

		// Calculate contact points projected onto reference face, simultaneously find deepest point
		{
			float32 maximal_depth = 0.f;

			for (auto const& point : incident_face_point_array)
			{
				float32 depth = vector::dot(reference_point - point, reference_normal);

				if (depth >= 0.f)
				{
					std::size_t index = projection_face_point_array.size();

					// Point projection onto plane, but taking advantage of the depth pre-computation
					projection_face_point_array.push_back(reference_normal * depth + point);

					if (depth > maximal_depth)
					{
						maximal_depth = depth;
						maximal_depth_index = index;
					}
				}
			}
		}

		// Contact point reduction
		if (projection_face_point_array.size() > 4)
		{
			overlap.contact_point_array[0] = projection_face_point_array[maximal_depth_index];
			
			// Find most distant point
			{
				std::size_t maximal_distance_index = 0;
				float32 maximal_distance = 0.f;
				
				for (std::size_t index = 0; index < projection_face_point_array.size(); ++index)
				{
					float32 distance = (projection_face_point_array[index] - overlap.contact_point_array[0]).magnitude_square();

					if (distance > maximal_distance)
					{
						maximal_distance = distance;
						maximal_distance_index = index;
					}
				}

				overlap.contact_point_array[2] = projection_face_point_array[maximal_distance_index];
			}

			// Find points that define the largest area in opposite directions
			{
				std::size_t maximal_area_index = 0;
				float32 maximal_area = 0.f;

				std::size_t minimal_area_index = 0;
				float32 minimal_area = 0.f;

				for (std::size_t index = 0; index < projection_face_point_array.size(); ++index)
				{
					float32 area
						= vector::dot(
							vector::cross(
								overlap.contact_point_array[0] - projection_face_point_array[index], 
								overlap.contact_point_array[2] - projection_face_point_array[index]
								) * 0.5f,
							reference_normal
							);

					if (area > maximal_area)
					{
						maximal_area = area;
						maximal_area_index = index;
					}
					else if (area < minimal_area)
					{
						minimal_area = area;
						minimal_area_index = index;
					}
				}

				overlap.contact_point_array[1] = projection_face_point_array[maximal_area_index];
				overlap.contact_point_array[3] = projection_face_point_array[minimal_area_index];
			}

			overlap.num_contact_points = 4;
		}
		else
		{
			overlap.num_contact_points = 0;
			
			for (auto const& point : projection_face_point_array)
			{
				overlap.contact_point_array[overlap.num_contact_points++] = point;
			}
		}
	}

	void ncd_separating_axis_test_solver::_compute_edge_contact_manifold(polyhedron const& poly_1, polyhedron const& poly_2,
																		 ncd_separation_data const& separation, ncd_overlap_data& overlap)
	{
		// See: https://en.wikipedia.org/wiki/Skew_lines#Nearest_points
		// and: https://computergraphics.stackexchange.com/questions/8290/project-vertex-onto-plane

		auto edge_1 = poly_1.query_halfedge(separation.edge_1_index);
		auto edge_2 = poly_2.query_halfedge(separation.edge_2_index);

		vector const& point_1 = edge_1.vertex_point();
		vector const& point_2 = edge_2.vertex_point();

		vector vec_1 = (point_1 - edge_1.pair().vertex_point()).normalize();
		vector vec_2 = (point_2 - edge_2.pair().vertex_point()).normalize();

		//vector axis = vector::cross(vec_1, vec_2);
		
		vector normal_1 = vector::cross(vec_1, overlap.contact_normal);
		vector normal_2 = vector::cross(vec_2, overlap.contact_normal);

		vector contact_point_1 = vector::project_point_onto_plane(point_1, normal_2, point_2, vec_1);
		vector contact_point_2 = vector::project_point_onto_plane(point_2, normal_1, point_1, vec_2);

		overlap.contact_point_array[0] = (contact_point_1 + contact_point_2) * 0.5f;

		overlap.num_contact_points = 1;
	}
}
