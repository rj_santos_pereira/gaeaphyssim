#include "engine/physics/dynamics/cr_local_symplectic_euler_solver.hpp"

#include "library/math/matrix.hpp"
#include "library/math/vector.hpp"

#define USING_INFINITESIMAL_ROTATIONS 0

namespace
{
	float32 calculate_collision_coefficient(gaea::collision_mode mode, float32 body_1_coefficient, float32 body_2_coefficient)
	{
		switch (mode)
		{
		case gaea::collision_mode::always_zero:
			return 0.f;
		case gaea::collision_mode::always_one:
			return 1.f;

		case gaea::collision_mode::minimum:
			return gaea::math::min(body_1_coefficient, body_2_coefficient);
		case gaea::collision_mode::geometric:
			return std::sqrt(body_1_coefficient * body_2_coefficient);
		case gaea::collision_mode::arithmetic:
			return 0.5f * (body_1_coefficient + body_2_coefficient);
		case gaea::collision_mode::maximum:
			return gaea::math::max(body_1_coefficient, body_2_coefficient);
		}
		return 0.f;
	}
}

namespace gaea
{
	cr_local_symplectic_euler_solver::cr_local_symplectic_euler_solver(float32 cache_lifetime, uint32 num_iterations,
																	   float32 depenetration_weight, 
																	   float32 depenetration_bias,
																	   float32 restitution_threshold,
																	   float32 restitution_attenuation,
																	   collision_mode restitution_mode, collision_mode friction_mode)
		: _rigid_body_set{}
		, _body_pair_map{}
		, _constraint_map{}
		, _constraint_mutex{}
		, _thread_counter{ 0 }
		, _thread_barrier{ 0 }
		, _world_force{ zero_vector }
		, _world_torque{ zero_vector }
		, _world_lin_accel{ zero_vector }
		, _world_ang_accel{ zero_vector }
		, _depenetration_weight{ depenetration_weight }
		, _depenetration_bias{ depenetration_bias }
		, _restitution_threshold{ math::max(restitution_threshold, 0.f) }
		, _restitution_attenuation{ math::max(restitution_attenuation, 0.f) }
		, _restitution_offset{ 0.f }
		, _cache_lifetime{ cache_lifetime }
		, _num_threads{ 0 }
		, _num_iterations{ num_iterations }
		, _restitution_mode{ restitution_mode }
		, _friction_mode{ friction_mode }
	{
		_calculate_restitution_offset();
	}

	void cr_local_symplectic_euler_solver::add_body(native_rigid_body* rigid_body)
	{
		auto [_, inserted] = _rigid_body_set.insert(rigid_body);
		if (!inserted)
		{
			GAEA_ERROR(cr_local_symplectic_euler_solver, "Trying to re-register object!");
		}

		_body_pair_map.try_emplace(rigid_body);
	}

	void cr_local_symplectic_euler_solver::remove_body(native_rigid_body* rigid_body)
	{
		_rigid_body_set.erase(rigid_body);

		_body_pair_map.erase(rigid_body);
	}

	void cr_local_symplectic_euler_solver::apply_world_force(vector world_force)
	{
		_world_force += world_force;
	}

	void cr_local_symplectic_euler_solver::apply_world_torque(vector world_torque)
	{
		_world_torque += world_torque;
	}

	void cr_local_symplectic_euler_solver::apply_world_linear_accel(vector world_lin_accel)
	{
		_world_lin_accel += world_lin_accel;
	}

	void cr_local_symplectic_euler_solver::apply_world_angular_accel(vector world_ang_accel)
	{
		_world_ang_accel += world_ang_accel;
	}

	void cr_local_symplectic_euler_solver::setup_constraint(bcd_overlap_data const& body_data, ncd_overlap_data const& contact_data)
	{
		auto constraint_lock = std::scoped_lock<spin_mutex>{ _constraint_mutex };

		{
			auto& constraint = _constraint_map[native_body_pair_id{ body_data.body_1, body_data.body_2 }];

			vector::create_orthonormal_basis(
				contact_data.contact_normal, 
				constraint.contact_normal, 
				constraint.i_contact_tangent, 
				constraint.j_contact_tangent
			);
			constraint.penetration_depth = contact_data.penetration_depth;

			constraint.restitution_coefficient = calculate_collision_coefficient(
				_restitution_mode, 
				body_data.body_1->get_restitution_coefficient(), 
				body_data.body_2->get_restitution_coefficient()
			);
			constraint.friction_coefficient = calculate_collision_coefficient(
				_friction_mode, 
				body_data.body_1->get_friction_coefficient(), 
				body_data.body_2->get_friction_coefficient()
			);

			constraint.cache_lifetime = _cache_lifetime;

			constraint.contact_data_array = fixed_array<cr_contact_data, 4>{};
			constraint.num_contact_data = contact_data.num_contact_points;
			
			for (uint8 index = 0; index < constraint.num_contact_data; ++index)
			{
				constraint.contact_data_array[index].contact_point = contact_data.contact_point_array[index];
			}
		}

		{
			native_rigid_body* reference_body;
			native_rigid_body* incident_body;

			switch (contact_data.type)
			{
			case ncd_penetration_type::edge:
			case ncd_penetration_type::face_1:
				reference_body = body_data.body_1;
				incident_body = body_data.body_2;
				break;
			case ncd_penetration_type::face_2:
				reference_body = body_data.body_2;
				incident_body = body_data.body_1;
				break;
			}

			auto& pair_array = _body_pair_map.at(reference_body);
			pair_array.push_back(incident_body);
		}
	}

	void cr_local_symplectic_euler_solver::compute_dynamics(float32 substep_delta, uint32 substep_count)
	{
#if GAEA_DEBUG
		if (_num_threads == 0)
		{
			GAEA_FATAL(cr_local_symplectic_euler_solver, "Solver not configured!");
		}
#endif
		
		uint32 thread_index = _thread_counter.fetch_add(1, std::memory_order_relaxed);

		// First find the bodies each thread will handle
		struct rigid_body
		{
			uint64 index;
			native_rigid_body* body;
		};

		array<rigid_body> rigid_body_array;
		rigid_body_array.reserve(uint32(_rigid_body_set.size() / _num_threads));
		
		std::for_each(_rigid_body_set.begin(), _rigid_body_set.end(),
			[this, thread_index, &rigid_body_array](native_rigid_body* body_ptr)
			{
				auto body_partition = body_ptr->get_body_partition();
				if (thread_index == body_partition.index % _num_threads)
				{
					rigid_body body
					{
						(uint64(body_partition.group) << 32 | body_partition.index),
						body_ptr
					};

					auto it = std::lower_bound(rigid_body_array.begin(), rigid_body_array.end(), body,
						[](rigid_body const& body_1, rigid_body const& body_2){ return body_1.index < body_2.index; });

					rigid_body_array.insert(it, body);
				}
			});

		// Next, run the solver + integrator, for as many substeps are necessary
		for (uint32 substep_index = 0; substep_index < substep_count; ++substep_index)
		{
			// First precalculate world tensors and calculate impulses from applied forces on bodies, synchronize once done
			for (auto const& body : rigid_body_array)
			{
				if (body.body->get_mobility() == body_mobility::movable)
				{
					body.body->precalc_tensors();

					vector linear_impulse
						= (body.body->get_force() + _world_force
							+ body.body->get_mass() * _world_lin_accel)
						* substep_delta;
					vector angular_impulse
						= (body.body->get_torque() + _world_torque
							+ body.body->get_oriented_inertia_tensor() * _world_ang_accel)
						* substep_delta;

					body.body->access_linear_momentum() += linear_impulse;
					body.body->access_angular_momentum() += angular_impulse;
				}
			}

			_thread_barrier.wait();

			// Then prepare the constraints for each body, synchronize on group
			{
				auto body_it = rigid_body_array.begin();
				auto body_it_end = rigid_body_array.end();

				for (uint8 group = 0; group < 8; ++group)
				{
					while (body_it != body_it_end && body_it->body->get_body_partition().group == group)
					{
						auto& body_pair_array = _body_pair_map.at(body_it->body);
						
						auto body_1 = body_it++->body;
						for (auto body_2 : body_pair_array)
						{
							_prepare_lcp_constraint(substep_delta, body_1, body_2, _constraint_map[native_body_pair_id{ body_1, body_2 }]);
						}
					}

					// A kernel call inside these loops is too expensive, just busy wait and eat the CPU cost
					_thread_barrier.busy_wait();
				}
			}
			
			// Then run the solver for each body, synchronize on group
			for (uint32 iteration = 0; iteration < _num_iterations; ++iteration)
			{
				auto body_it = rigid_body_array.begin();
				auto body_it_end = rigid_body_array.end();

				for (uint8 group = 0; group < 8; ++group)
				{
					while (body_it != body_it_end && body_it->body->get_body_partition().group == group)
					{
						auto& body_pair_array = _body_pair_map.at(body_it->body);

						auto body_1 = body_it++->body;
						for (auto body_2 : body_pair_array)
						{
							_solve_lcp_constraint(body_1, body_2, _constraint_map[native_body_pair_id{ body_1, body_2 }]);
						}
					}

					// A kernel call inside these loops is too expensive, just busy wait and eat the CPU cost
					_thread_barrier.busy_wait();
				}
			}

			// Finally run the integrator for each body, no synchronization
			for (auto const& body : rigid_body_array)
			{
				// Avoid doing unnecessary work by simply checking whether we will change anything
				if (body.body->get_mobility() == body_mobility::movable)
				{
					_integrate_symplectic_euler_dynamics(substep_delta, body.body);
				}
			}
		}

		// Clear forces from each body and pairs from body pair map
		for (auto const& body : rigid_body_array)
		{			
			_body_pair_map.at(body.body).clear();
				
			body.body->clear_force_and_torque();
		}

		_thread_counter.store(0, std::memory_order_relaxed);
	}

	void cr_local_symplectic_euler_solver::refresh_cache(float32 delta)
	{
		std::unique_lock<spin_mutex> lock{ _constraint_mutex };

		auto it = _constraint_map.begin();
		auto it_end = _constraint_map.end();

		while (it != it_end)
		{
			float32& lifetime = it->second.cache_lifetime;
			if (lifetime > 0.f)
			{
				// Reduce lifetime, cache will be invalidated when it is zero
				lifetime -= delta;
				++it;
			}
			else
			{
				// Invalid cache from last frame, clean it up
				it = _constraint_map.erase(it);
			}
		}
	}

	void cr_local_symplectic_euler_solver::set_cache_lifetime(float32 cache_lifetime)
	{
		_cache_lifetime = cache_lifetime;
	}

	void cr_local_symplectic_euler_solver::set_num_threads(uint32 num_threads)
	{
		_thread_barrier.set_initial_count(num_threads);
		
		_num_threads = num_threads;
	}

	void cr_local_symplectic_euler_solver::set_num_iterations(uint32 num_iterations)
	{
		_num_iterations = num_iterations;
	}

	void cr_local_symplectic_euler_solver::set_depenetration_weight(float32 depenetration_weight)
	{
		_depenetration_weight = depenetration_weight;
	}

	void cr_local_symplectic_euler_solver::set_depenetration_bias(float32 depenetration_bias)
	{
		_depenetration_bias = depenetration_bias;
	}

	void cr_local_symplectic_euler_solver::set_restitution_threshold(float32 restitution_threshold)
	{
		_restitution_threshold = math::max(restitution_threshold, 0.f);

		_calculate_restitution_offset();
	}

	void cr_local_symplectic_euler_solver::set_restitution_attenuation(float32 restitution_attenuation)
	{
		_restitution_attenuation = math::max(restitution_attenuation, 0.f);

		_calculate_restitution_offset();
	}

	void cr_local_symplectic_euler_solver::set_restitution_mode(collision_mode restitution_mode)
	{
		_restitution_mode = restitution_mode;
	}

	void cr_local_symplectic_euler_solver::set_friction_mode(collision_mode friction_mode)
	{
		_friction_mode = friction_mode;
	}

	float32 cr_local_symplectic_euler_solver::get_cache_lifetime() const
	{
		return _cache_lifetime;
	}

	uint32 cr_local_symplectic_euler_solver::get_num_threads() const
	{
		return _num_threads;
	}

	uint32 cr_local_symplectic_euler_solver::get_num_iterations() const
	{
		return _num_iterations;
	}

	float32 cr_local_symplectic_euler_solver::get_depenetration_weight() const
	{
		return _depenetration_weight;
	}

	float32 cr_local_symplectic_euler_solver::get_depenetration_bias() const
	{
		return _depenetration_bias;
	}

	float32 cr_local_symplectic_euler_solver::get_restitution_threshold() const
	{
		return _restitution_threshold;
	}

	float32 cr_local_symplectic_euler_solver::get_restitution_attenuation() const
	{
		return _restitution_attenuation;
	}

	collision_mode cr_local_symplectic_euler_solver::get_restitution_mode() const
	{
		return _restitution_mode;
	}

	collision_mode cr_local_symplectic_euler_solver::get_friction_mode() const
	{
		return _friction_mode;
	}

	void cr_local_symplectic_euler_solver::_prepare_lcp_constraint(float32 step_delta, native_rigid_body* body_1, native_rigid_body* body_2, cr_constraint_data& constraint)
	{
		vector& linear_momentum_1 = body_1->access_linear_momentum();
		vector& angular_momentum_1 = body_1->access_angular_momentum();
		
		vector& linear_momentum_2 = body_2->access_linear_momentum();
		vector& angular_momentum_2 = body_2->access_angular_momentum();

		float32 inv_mass_1 = 0.f;
		matrix3 inv_inertia_1 = matrix3{ zero_matrix_tag{} }; 

		float32 inv_mass_2 = 0.f;
		matrix3 inv_inertia_2 = matrix3{ zero_matrix_tag{} };

		if (body_1->get_mobility() == body_mobility::movable)
		{
			inv_mass_1 = body_1->get_mass_inverse();
			inv_inertia_1 = body_1->get_oriented_inertia_inverse_tensor();
		}

		if (body_2->get_mobility() == body_mobility::movable)
		{
			inv_mass_2 = body_2->get_mass_inverse();
			inv_inertia_2 = body_2->get_oriented_inertia_inverse_tensor();
		}
		
		for (std::size_t contact_index = 0; contact_index < constraint.num_contact_data; ++contact_index)
		{
			auto& contact = constraint.contact_data_array[contact_index];

			contact.contact_lever_1 = contact.contact_point - body_1->get_position();
			contact.contact_lever_2 = contact.contact_point - body_2->get_position();

			contact.normal_mass = contact.i_tangent_mass = contact.j_tangent_mass = inv_mass_1 + inv_mass_2;

			auto normal_axle_1 = vector::cross(contact.contact_lever_1, constraint.contact_normal);
			auto normal_axle_2 = vector::cross(contact.contact_lever_2, constraint.contact_normal);

			contact.normal_mass = 1.f / (contact.normal_mass + vector::dot(normal_axle_1, inv_inertia_1 * normal_axle_1) + vector::dot(normal_axle_2, inv_inertia_2 * normal_axle_2));

			auto i_tangent_axle_1 = vector::cross(contact.contact_lever_1, constraint.i_contact_tangent);
			auto i_tangent_axle_2 = vector::cross(contact.contact_lever_2, constraint.i_contact_tangent);

			contact.i_tangent_mass = 1.f / (contact.i_tangent_mass + vector::dot(i_tangent_axle_1, inv_inertia_1 * i_tangent_axle_1) + vector::dot(i_tangent_axle_2, inv_inertia_2 * i_tangent_axle_2));

			auto j_tangent_axle_1 = vector::cross(contact.contact_lever_1, constraint.j_contact_tangent);
			auto j_tangent_axle_2 = vector::cross(contact.contact_lever_2, constraint.j_contact_tangent);

			contact.j_tangent_mass = 1.f / (contact.j_tangent_mass + vector::dot(j_tangent_axle_1, inv_inertia_1 * j_tangent_axle_1) + vector::dot(j_tangent_axle_2, inv_inertia_2 * j_tangent_axle_2));

			vector impulse
				= constraint.contact_normal * contact.normal_impulse
				+ constraint.i_contact_tangent * contact.i_tangent_impulse
				+ constraint.j_contact_tangent * contact.j_tangent_impulse;

			linear_momentum_1 -= impulse;
			angular_momentum_1 -= vector::cross(contact.contact_lever_1, impulse);

			linear_momentum_2 += impulse;
			angular_momentum_2 += vector::cross(contact.contact_lever_2, impulse);

			vector linear_velocity_1 = body_1->query_world_linear_velocity();
			vector angular_velocity_1 = body_1->query_world_angular_velocity();

			vector linear_velocity_2 = body_2->query_world_linear_velocity();
			vector angular_velocity_2 = body_2->query_world_angular_velocity();

			vector relative_velocity = linear_velocity_2 + vector::cross(angular_velocity_2, contact.contact_lever_2) - linear_velocity_1 - vector::cross(angular_velocity_1, contact.contact_lever_1);

			// Penetration (position) constraint
			contact.bias_velocity = -_depenetration_weight * math::min(-constraint.penetration_depth + _depenetration_bias, 0.f) / step_delta;

			// Restitution (velocity) constraint
			float32 restitution_velocity = -vector::dot(relative_velocity, constraint.contact_normal);
			// If velocity is too low, don't apply it, otherwise instabilities will be introduced
			if (restitution_velocity > _restitution_threshold)
			{
				restitution_velocity -= _restitution_attenuation / (restitution_velocity - _restitution_offset);
				contact.bias_velocity += constraint.restitution_coefficient * restitution_velocity;
			}
		}
	}

	void cr_local_symplectic_euler_solver::_solve_lcp_constraint(native_rigid_body* body_1, native_rigid_body* body_2, cr_constraint_data& constraint)
	{
		vector& linear_momentum_1 = body_1->access_linear_momentum();
		vector& angular_momentum_1 = body_1->access_angular_momentum();

		vector& linear_momentum_2 = body_2->access_linear_momentum();
		vector& angular_momentum_2 = body_2->access_angular_momentum();

		for (std::size_t contact_index = 0; contact_index < constraint.num_contact_data; ++contact_index)
		{
			auto& contact = constraint.contact_data_array[contact_index];

			float32 max_tangent_lambda = constraint.friction_coefficient * contact.normal_impulse;
			float32 min_tangent_lambda = -max_tangent_lambda;

			vector linear_velocity_1 = body_1->query_world_linear_velocity();
			vector angular_velocity_1 = body_1->query_world_angular_velocity();

			vector linear_velocity_2 = body_2->query_world_linear_velocity();
			vector angular_velocity_2 = body_2->query_world_angular_velocity();

			vector relative_velocity = linear_velocity_2 + vector::cross(angular_velocity_2, contact.contact_lever_2) - linear_velocity_1 - vector::cross(angular_velocity_1, contact.contact_lever_1);

			// Penetration and restitution, normal
			float32 normal_lambda = (-vector::dot(relative_velocity, constraint.contact_normal) + contact.bias_velocity) * contact.normal_mass;

			float32 old_normal_lambda = contact.normal_impulse;
			float32 new_normal_lambda = contact.normal_impulse = math::max(contact.normal_impulse + normal_lambda, 0.f);

			vector normal_impulse = constraint.contact_normal * (new_normal_lambda - old_normal_lambda);

			// Friction, i-tangent
			float32 i_tangent_lambda = -vector::dot(relative_velocity, constraint.i_contact_tangent) * contact.i_tangent_mass;

			float32 old_i_tangent_lambda = contact.i_tangent_impulse;
			float32 new_i_tangent_lambda = contact.i_tangent_impulse = math::clamp(contact.i_tangent_impulse + i_tangent_lambda, min_tangent_lambda, max_tangent_lambda);

			vector i_tangent_impulse = constraint.i_contact_tangent * (new_i_tangent_lambda - old_i_tangent_lambda);

			// Friction, j-tangent
			float32 j_tangent_lambda = -vector::dot(relative_velocity, constraint.j_contact_tangent) * contact.j_tangent_mass;

			float32 old_j_tangent_lambda = contact.j_tangent_impulse;
			float32 new_j_tangent_lambda = contact.j_tangent_impulse = math::clamp(contact.j_tangent_impulse + j_tangent_lambda, min_tangent_lambda, max_tangent_lambda);

			vector j_tangent_impulse = constraint.j_contact_tangent * (new_j_tangent_lambda - old_j_tangent_lambda);

			// Total impulse
			vector impulse = normal_impulse + i_tangent_impulse + j_tangent_impulse;
			
			linear_momentum_1 -= impulse;
			angular_momentum_1 -= vector::cross(contact.contact_lever_1, impulse);

			linear_momentum_2 += impulse;
			angular_momentum_2 += vector::cross(contact.contact_lever_2, impulse);
		}
	}

	void cr_local_symplectic_euler_solver::_integrate_symplectic_euler_dynamics(float32 step_delta, native_rigid_body* body)
	{
		vector linear_velocity = body->query_world_linear_velocity();
		vector angular_velocity = body->query_body_angular_velocity();

		vector& position = body->access_position();
		quaternion& rotation = body->access_rotation();

		position += linear_velocity * step_delta;

#if USING_INFINITESIMAL_ROTATIONS
		float32 ang_vel_magn = angular_velocity.magnitude();
		vector ang_vel_dir = angular_velocity.unit();

		float32 ang_vel_delta = ang_vel_magn * (step_delta * 0.5f);
		rotation *= quaternion{ std::cos(ang_vel_delta), std::sin(ang_vel_delta) * ang_vel_dir };
#else
		rotation += rotation * quaternion{ 0.0f, angular_velocity } * (step_delta * 0.5f);
		rotation.normalize_unsafe();
#endif

		body->clear_precalc();
	}

	void cr_local_symplectic_euler_solver::_calculate_restitution_offset()
	{
		_restitution_offset = ((_restitution_threshold - 1.f) * (_restitution_threshold + 1.f) - _restitution_attenuation + 1.f) / _restitution_threshold;
	}
}
