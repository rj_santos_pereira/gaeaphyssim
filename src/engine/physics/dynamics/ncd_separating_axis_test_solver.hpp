#ifndef ENGINE_PHYSICS_DYNAMICS_NCD_SEPARATING_AXIS_TEST_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_NCD_SEPARATING_AXIS_TEST_SOLVER_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/physics/native_rigid_body.hpp"
#include "engine/physics/dynamics/bcd_sequential_sort_sweep_solver.hpp"
#include "library/math/polyhedron.hpp"
#include "library/math/vector.hpp"

// Explanation:
// https://www.gdcvault.com/play/1017646/
// https://www.randygaul.net/wp-content/uploads/2013/07/SATandSupportPoints.pdf
// https://www.scss.tcd.ie/Michael.Manzke/CS7057/cs7057-1516-07-NarrowPhase-mm.pdf
// http://dai.fmph.uniba.sk/upload/2/2b/Ca15_lesson07.pdf

namespace gaea
{
	enum class ncd_penetration_type : uint8
	{
		none,
		face_1,
		face_2,
		edge,
	};

	struct ncd_overlap_data
	{
		ncd_penetration_type type = ncd_penetration_type::none;
		
		vector contact_normal = zero_vector;
		float32 penetration_depth = std::numeric_limits<float32>::max();

		fixed_array<vector, 4> contact_point_array;
		uint8 num_contact_points;
	};

	class ncd_separating_axis_test_solver
	{
		struct ncd_separation_data
		{
			ncd_penetration_type type = ncd_penetration_type::none;

			int32 face_index = -1;
			int32 edge_1_index = -1;
			int32 edge_2_index = -1;

			float32 cache_lifetime = 0.f;
		};
		
	public:
		explicit ncd_separating_axis_test_solver(float32 cache_lifetime = 1.f);

		ncd_separating_axis_test_solver(ncd_separating_axis_test_solver const&) = delete;
		ncd_separating_axis_test_solver(ncd_separating_axis_test_solver&&) = delete;

		ncd_separating_axis_test_solver& operator=(ncd_separating_axis_test_solver const&) = delete;
		ncd_separating_axis_test_solver& operator=(ncd_separating_axis_test_solver&&) = delete;

		ncd_overlap_data compute_geometry_overlap(bcd_overlap_data const& overlap_data);

		void refresh_cache(float32 delta);

		void set_cache_lifetime(float32 cache_lifetime);

		float32 get_cache_lifetime() const;
		
	private:
		bool _compute_cache_poly_overlap(native_body_pair_id const& body_data, polyhedron const& poly_1, polyhedron const& poly_2,
										 ncd_separation_data& separation, ncd_overlap_data& overlap);

		bool _compute_poly_overlap(native_body_pair_id const& body_data, polyhedron const& poly_1, polyhedron const& poly_2,
								   ncd_separation_data& separation, ncd_overlap_data& overlap);

		static bool _compute_poly_face_overlap(polyhedron const& poly_1, polyhedron const& poly_2, 
											   ncd_separation_data& separation, ncd_overlap_data& overlap);

		static bool _compute_poly_edge_overlap(polyhedron const& poly_1, polyhedron const& poly_2, 
											   ncd_separation_data& separation, ncd_overlap_data& overlap);

		static bool _compute_face_overlap(polyhedron_face_query const& face_1, polyhedron const& poly_2,
										  ncd_separation_data& separation, ncd_overlap_data& overlap);

		static bool _compute_edge_overlap(polyhedron_halfedge_query const& edge_1, polyhedron_halfedge_query const& edge_2, 
										  vector const& center_1, vector const& axis, 
										  ncd_separation_data& separation, ncd_overlap_data& overlap);

		static bool _is_minkowski_difference_face(polyhedron_halfedge_query const& edge_1, polyhedron_halfedge_query const& edge_2, 
												  vector& axis);
		
		static void _compute_face_contact_manifold(polyhedron const& poly_1, polyhedron const& poly_2, 
												   ncd_separation_data const& separation, ncd_overlap_data& overlap);
		
		static void _compute_edge_contact_manifold(polyhedron const& poly_1, polyhedron const& poly_2, 
												   ncd_separation_data const& separation, ncd_overlap_data& overlap);

		map<native_body_pair_id, ncd_separation_data, native_body_pair_hash> _cached_separation_map;
		shared_spin_mutex _cache_mutex;
		
		float32 _cache_lifetime;
		
	};
}

#endif
