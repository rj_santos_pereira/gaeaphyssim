#include "engine/physics/dynamics/sycl_ncd_separating_axis_test_solver.hpp"

#if GAEA_USING_SYCL

#define USE_CROSS_PRODUCT_FOR_MINKOWSKI_CHECK 0

namespace gaea::sycl_kernel
{	

	bool ncd_solver_separating_axis_test_test_face_overlap(sycl_data::geometry_poly_data const& body_poly_1, sycl_data::geometry_poly_data const& body_poly_2,
															  sycl::accessor<sycl_data::geometry_face_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_face_compute_accessor,
															  sycl::accessor<sycl_data::geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_compute_accessor,
															  sycl::accessor<sycl_data::geometry_halfedge_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_halfedge_compute_accessor,
															  sycl_data::ncd_penetration_type separation_type,
															  sycl_data::ncd_overlap_data& overlap_data)
	{
		bool has_separation = false;

		for (uint32 index = 0; index < body_poly_1.face_count && !has_separation; ++index)
		{
			auto face_1 = geometry_face_compute_accessor[body_poly_1.face_index + index];
			auto edge_1 = geometry_halfedge_compute_accessor[body_poly_1.halfedge_index + face_1.edge_index];

			vector normal = face_1.normal;

			vector vertex_1 = geometry_vertex_compute_accessor[body_poly_1.vertex_index + edge_1.vertex_index].point;
			vector vertex_2 = body_poly_2.support_vertex(-normal, geometry_vertex_compute_accessor);

			// Invert vertexes to obtain penetration instead of distance
			float32 depth = vector::dot(normal, vertex_1 - vertex_2);
			if (depth < overlap_data.penetration_depth)
			{
				overlap_data.type = separation_type;
				
				overlap_data.face_index = uint32(index);

				overlap_data.contact_normal = normal;
				overlap_data.penetration_depth = depth;

				has_separation = overlap_data.penetration_depth <= 0.0f;
			}
		}

		return has_separation;
	}

	bool ncd_solver_separating_axis_test_test_edge_overlap(sycl_data::geometry_poly_data const& body_poly_1, sycl_data::geometry_poly_data const& body_poly_2,
														   sycl::accessor<sycl_data::geometry_face_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_face_compute_accessor,
														   sycl::accessor<sycl_data::geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_compute_accessor,
														   sycl::accessor<sycl_data::geometry_halfedge_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_halfedge_compute_accessor,
														   sycl_data::ncd_overlap_data& overlap_data)
	{
		bool has_separation = false;

		vector center_1 = body_poly_1.center(geometry_vertex_compute_accessor);

		for (uint32 index_1 = 0; index_1 < body_poly_1.halfedge_count && !has_separation; index_1 += 2)
		{
			auto edge_1_a = geometry_halfedge_compute_accessor[body_poly_1.halfedge_index + index_1];
			auto edge_1_b = geometry_halfedge_compute_accessor[body_poly_1.halfedge_index + index_1 + 1];

			for (uint32 index_2 = 0; index_2 < body_poly_2.halfedge_count && !has_separation; index_2 += 2)
			{
				auto edge_2_a = geometry_halfedge_compute_accessor[body_poly_2.halfedge_index + index_2];
				auto edge_2_b = geometry_halfedge_compute_accessor[body_poly_2.halfedge_index + index_2 + 1];

				vector const& normal_1_a = geometry_face_compute_accessor[body_poly_1.face_index + edge_1_a.face_index].normal;
				vector const& normal_1_b = geometry_face_compute_accessor[body_poly_1.face_index + edge_1_b.face_index].normal;

				vector const& normal_2_a = geometry_face_compute_accessor[body_poly_2.face_index + edge_2_a.face_index].normal;
				vector const& normal_2_b = geometry_face_compute_accessor[body_poly_2.face_index + edge_2_b.face_index].normal;

				vector const& vertex_1_a = geometry_vertex_compute_accessor[body_poly_1.vertex_index + edge_1_a.vertex_index].point;
				vector const& vertex_1_b = geometry_vertex_compute_accessor[body_poly_1.vertex_index + edge_1_b.vertex_index].point;

				vector const& vertex_2_a = geometry_vertex_compute_accessor[body_poly_2.vertex_index + edge_2_a.vertex_index].point;
				vector const& vertex_2_b = geometry_vertex_compute_accessor[body_poly_2.vertex_index + edge_2_b.vertex_index].point;

				vector const& a = normal_1_a;
				vector const& b = normal_1_b;
				vector const& c = normal_2_a;
				vector const& d = normal_2_b;

#if USE_CROSS_PRODUCT_FOR_MINKOWSKI_CHECK
				vector ba = vector::cross(b, a);
				vector dc = vector::cross(d, c);
#else
				vector ba = vertex_1_a - vertex_1_b;
				vector dc = vertex_2_a - vertex_2_b;
#endif

				vector normal = vector::cross(ba, dc);
				if (!math::nearly_zero(normal.magnitude_square()))
				{
					float32 cba = vector::dot(-c, ba);
					float32 dba = vector::dot(-d, ba);

					float32 adc = vector::dot(a, dc);
					float32 bdc = vector::dot(b, dc);

					if (cba * bdc > 0.f && cba * dba < 0.f && adc * bdc < 0.f)
					{
						normal.normalize();

						if (vector::dot(normal, vertex_1_a - center_1) < 0.f)
						{
							normal = -normal;
						}

						// Invert vertexes to obtain penetration instead of distance
						float32 depth = vector::dot(normal, vertex_1_a - vertex_2_a);
						if (depth < overlap_data.penetration_depth)
						{
							overlap_data.type = sycl_data::ncd_penetration_type::edge;
							
							overlap_data.edge_1_index = uint32(index_1);
							overlap_data.edge_2_index = uint32(index_2);

							overlap_data.contact_normal = normal;
							overlap_data.penetration_depth = depth;

							has_separation = overlap_data.penetration_depth <= 0.f;
						}
					}
				}
			}
		}

		return has_separation;
	}
	
	void ncd_solver_separating_axis_test_compute_face_contact_manifold(sycl_data::geometry_poly_data const& body_poly_1, sycl_data::geometry_poly_data const& body_poly_2,
																	   sycl::accessor<sycl_data::geometry_face_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_face_compute_accessor,
																	   sycl::accessor<sycl_data::geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_compute_accessor,
																	   sycl::accessor<sycl_data::geometry_halfedge_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_halfedge_compute_accessor,
																	   sycl_data::ncd_overlap_data& overlap_data)
	{
		fixed_array<sycl_data::ncd_plane_data, 32> reference_clip_plane_array;
		uint32 reference_clip_plane_count = 0;

		fixed_array<vector, 32> incident_face_point_array;
		uint32 incident_face_point_count = 0;

		polyhedron_face_data reference_face = geometry_face_compute_accessor[body_poly_1.face_index + overlap_data.face_index];
		polyhedron_halfedge_data reference_halfedge = geometry_halfedge_compute_accessor[body_poly_1.halfedge_index + reference_face.edge_index];
		polyhedron_vertex_data reference_vertex = geometry_vertex_compute_accessor[body_poly_1.vertex_index + reference_halfedge.vertex_index];

		vector const& reference_normal = reference_face.normal;
		vector const& reference_point = reference_vertex.point;
		
		// Compute reference face side planes for candidate contact clipping
		{
			vector first_point = reference_point;
			
			uint32 current_edge_index = body_poly_1.halfedge_index + reference_halfedge.next_edge_index;
			uint32 last_edge_index = current_edge_index;
			
			polyhedron_halfedge_data current_edge = geometry_halfedge_compute_accessor[current_edge_index];
			
			do
			{
				vector second_point = geometry_vertex_compute_accessor[body_poly_1.vertex_index + current_edge.vertex_index].point;
				
				vector edge_direction = (second_point - first_point).normalize();

				reference_clip_plane_array[reference_clip_plane_count++] = sycl_data::ncd_plane_data{ first_point, vector::cross(reference_normal, edge_direction) };

				first_point = second_point;
				
				current_edge_index = body_poly_1.halfedge_index + current_edge.next_edge_index;
				current_edge = geometry_halfedge_compute_accessor[current_edge_index];
			}
			while (current_edge_index != last_edge_index);
		}

		polyhedron_face_data incident_face;

		// Determine maximally antiparallel incident face
		{
			float32 maximal_antiparallel = 2.f;
			uint32 maximal_antiparallel_index = 0;

			for (uint32 index = body_poly_2.face_index, count = body_poly_2.face_index + body_poly_2.face_count; index < count; ++index)
			{
				float32 antiparallel = vector::dot(reference_normal, geometry_face_compute_accessor[index].normal);
				if (antiparallel < maximal_antiparallel)
				{
					maximal_antiparallel = antiparallel;
					maximal_antiparallel_index = index;
				}
			}

			incident_face = geometry_face_compute_accessor[maximal_antiparallel_index];
		}

		// Compute incident edge lines for candidate contact clipping
		{
			uint32 current_edge_index = body_poly_2.halfedge_index + incident_face.edge_index;
			uint32 last_edge_index = current_edge_index;

			polyhedron_halfedge_data current_edge = geometry_halfedge_compute_accessor[current_edge_index];

			do
			{
				incident_face_point_array[incident_face_point_count++] = geometry_vertex_compute_accessor[body_poly_2.vertex_index + current_edge.vertex_index].point;

				current_edge_index = body_poly_2.halfedge_index + current_edge.next_edge_index;
				current_edge = geometry_halfedge_compute_accessor[current_edge_index];
			}
			while (current_edge_index != last_edge_index);
		}

		fixed_array<vector, 32> clip_incident_face_point_array;
		uint32 clip_incident_face_point_count = 0;

		// Clip incident lines with reference side planes to determine candidate contacts
		{
			for (uint32 reference_index = 0; reference_index < reference_clip_plane_count; ++reference_index)
			{
				sycl_data::ncd_plane_data clip_plane = reference_clip_plane_array[reference_index];

				vector last_point = incident_face_point_array[incident_face_point_count - 1];
				bool should_keep_last_point = vector::dot(clip_plane.normal, last_point - clip_plane.point) > 0.f;

				for (uint32 incident_index = 0; incident_index < incident_face_point_count; ++incident_index)
				{
					vector current_point = incident_face_point_array[incident_index];
					bool should_keep_current_point = vector::dot(clip_plane.normal, current_point - clip_plane.point) > 0.f;

					if (should_keep_current_point && should_keep_last_point)
					{
						clip_incident_face_point_array[clip_incident_face_point_count++]
							= last_point;
					}
					else if (should_keep_current_point && !should_keep_last_point)
					{
						clip_incident_face_point_array[clip_incident_face_point_count++]
							= vector::project_point_onto_plane(last_point, 
								clip_plane.normal, clip_plane.point, 
								(current_point - last_point).normalize());
					}
					else if (!should_keep_current_point && should_keep_last_point)
					{
						clip_incident_face_point_array[clip_incident_face_point_count++]
							= last_point;

						clip_incident_face_point_array[clip_incident_face_point_count++]
							= vector::project_point_onto_plane(last_point,
								clip_plane.normal, clip_plane.point,
								(current_point - last_point).normalize());
					}

					last_point = current_point;
					should_keep_last_point = should_keep_current_point;
				}

				incident_face_point_array = std::move(clip_incident_face_point_array);
				incident_face_point_count = std::exchange(clip_incident_face_point_count, 0);

				if (incident_face_point_count == 0)
				{
					overlap_data.num_contact_points = 0;
					break;
				}
			}
		}

		fixed_array<vector, 32> projection_face_point_array;
		uint32 projection_face_point_count = 0;

		uint32 maximal_depth_index = 0;

		// Calculate contact points projected onto reference face, simultaneously find deepest point
		{
			float32 maximal_depth = 0.f;

			for (uint32 index = 0; index < incident_face_point_count; ++index)
			{
				vector const& point = incident_face_point_array[index];
				
				float32 depth = vector::dot(reference_point - point, reference_normal);

				if (depth >= 0.f)
				{
					uint32 projection_index = projection_face_point_count++;

					// Point projection onto plane, but taking advantage of the depth pre-computation
					projection_face_point_array[projection_index] = reference_normal * depth + point;

					if (depth > maximal_depth)
					{
						maximal_depth = depth;
						maximal_depth_index = projection_index;
					}
				}
			}
		}

		//overlap_data.reference_point = reference_point;

		// Contact point reduction
		if (projection_face_point_count > 4)
		{
			overlap_data.contact_point_array[0] = projection_face_point_array[maximal_depth_index];

			// Find most distant point
			{
				uint32 maximal_distance_index = 0;
				float32 maximal_distance = 0.f;

				for (uint32 index = 0; index < projection_face_point_count; ++index)
				{
					float32 distance = (projection_face_point_array[index] - overlap_data.contact_point_array[0]).magnitude_square();

					if (distance > maximal_distance)
					{
						maximal_distance = distance;
						maximal_distance_index = index;
					}
				}

				overlap_data.contact_point_array[2] = projection_face_point_array[maximal_distance_index];
			}

			// Find points that define the largest area in opposite directions
			{
				uint32 maximal_area_index = 0;
				float32 maximal_area = 0.f;

				uint32 minimal_area_index = 0;
				float32 minimal_area = 0.f;

				for (uint32 index = 0; index < projection_face_point_count; ++index)
				{
					vector const& point = projection_face_point_array[index];
					
					float32 area
						= vector::dot(
							vector::cross(
								overlap_data.contact_point_array[0] - point,
								overlap_data.contact_point_array[2] - point
							) * 0.5f,
							reference_normal
						);

					if (area > maximal_area)
					{
						maximal_area = area;
						maximal_area_index = index;
					}
					else if (area < minimal_area)
					{
						minimal_area = area;
						minimal_area_index = index;
					}
				}

				overlap_data.contact_point_array[1] = projection_face_point_array[maximal_area_index];
				overlap_data.contact_point_array[3] = projection_face_point_array[minimal_area_index];
			}

			overlap_data.num_contact_points = 4;
		}
		else
		{
			overlap_data.num_contact_points = 0;

			for (uint32 index = 0; index < projection_face_point_count; ++index)
			{
				overlap_data.contact_point_array[overlap_data.num_contact_points++] = projection_face_point_array[index];
			}
		}
	}

	void ncd_solver_separating_axis_test_compute_edge_contact_manifold(sycl_data::geometry_poly_data const& body_poly_1, sycl_data::geometry_poly_data const& body_poly_2,
																	   sycl::accessor<sycl_data::geometry_vertex_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_vertex_compute_accessor,
																	   sycl::accessor<sycl_data::geometry_halfedge_data, 1, sycl::access::mode::read, sycl::access::target::global_buffer> const& geometry_halfedge_compute_accessor,
																	   sycl_data::ncd_overlap_data& overlap_data)
	{
		vector const& point_1_a = geometry_vertex_compute_accessor[body_poly_1.vertex_index + geometry_halfedge_compute_accessor[body_poly_1.halfedge_index + overlap_data.edge_1_index].vertex_index].point;
		vector const& point_1_b = geometry_vertex_compute_accessor[body_poly_1.vertex_index + geometry_halfedge_compute_accessor[body_poly_1.halfedge_index + overlap_data.edge_1_index + 1].vertex_index].point;
		
		vector const& point_2_a = geometry_vertex_compute_accessor[body_poly_2.vertex_index + geometry_halfedge_compute_accessor[body_poly_2.halfedge_index + overlap_data.edge_2_index].vertex_index].point;
		vector const& point_2_b = geometry_vertex_compute_accessor[body_poly_2.vertex_index + geometry_halfedge_compute_accessor[body_poly_2.halfedge_index + overlap_data.edge_2_index + 1].vertex_index].point;

		vector vec_1 = (point_1_a - point_1_b).normalize();
		vector vec_2 = (point_2_a - point_2_b).normalize();

		//vector axis = vector::cross(vec_1, vec_2);

		vector normal_1 = vector::cross(vec_1, overlap_data.contact_normal);
		vector normal_2 = vector::cross(vec_2, overlap_data.contact_normal);

		vector contact_point_1 = vector::project_point_onto_plane(point_1_a, normal_2, point_2_a, vec_1);
		vector contact_point_2 = vector::project_point_onto_plane(point_2_a, normal_1, point_1_a, vec_2);

		overlap_data.contact_point_array[overlap_data.num_contact_points++] = (contact_point_1 + contact_point_2) * 0.5f;
	}
	
	struct kernel_ncd_solver_separating_axis_test_compute_poly;
	struct kernel_ncd_solver_separating_axis_test_test_poly;
	struct kernel_ncd_solver_separating_axis_test_generate_contact;
	struct kernel_ncd_solver_separating_axis_test_update_dynamics;
	sycl_type::sequence_event ncd_solver_separating_axis_test(sycl::queue& command_queue, uint32 num_bodies,
															  sycl::buffer<sycl_data::statics_data>& statics_buffer,
															  sycl::buffer<sycl_data::position_data>& position_buffer,
															  sycl::buffer<sycl_data::geometry_data>& geometry_buffer,
															  sycl::buffer<sycl_data::geometry_face_data>& geometry_face_buffer,
															  sycl::buffer<sycl_data::geometry_vertex_data>& geometry_vertex_buffer,
															  sycl::buffer<sycl_data::geometry_halfedge_data>& geometry_halfedge_buffer, 
															  sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
															  sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer, 
															  sycl::buffer<sycl_data::geometry_face_data>& geometry_face_compute_buffer,
															  sycl::buffer<sycl_data::geometry_vertex_data>& geometry_vertex_compute_buffer,
															  sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
															  sycl::buffer<sycl_data::ncd_overlap_data>& ncd_overlap_compute_buffer,
															  sycl::buffer<uint32>& cd_overlap_count_compute_buffer,
															  sycl::buffer<uint32>& overlap_limit_compute_buffer)
	{
		// 1. regen geom according to position and rotation to use in geom tests
		auto ncd_first_event = command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto position_accessor = position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_accessor = geometry_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_face_accessor = geometry_face_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto geometry_vertex_accessor = geometry_vertex_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto dynamics_compute_accessor = dynamics_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto geometry_compute_accessor = geometry_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto geometry_face_compute_accessor = geometry_face_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto geometry_vertex_compute_accessor = geometry_vertex_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_ncd_solver_separating_axis_test_compute_poly>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null && dynamics_compute_accessor[body_idx].collision_state == uint8(collision_state::bounds))
						{
							auto position_data = position_accessor[body_idx];

							geometry_accessor[body_idx].poly.move_by(position_data.position, position_data.rotation,
								geometry_face_accessor, geometry_vertex_accessor,
								geometry_face_compute_accessor, geometry_vertex_compute_accessor);

							geometry_compute_accessor[body_idx].poly = geometry_accessor[body_idx].poly;
						}
					}
				);
			}
		);

		sycl::buffer<uint32> collision_state_buffer{ sycl::range<1>{ num_bodies } };

		std::memset(collision_state_buffer.get_access<sycl::access::mode::write>().get_pointer(), 0, collision_state_buffer.get_size());

		// 2. test geom and generate list of overlap pairs
		command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto dynamics_compute_accessor = dynamics_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_compute_accessor = geometry_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_face_compute_accessor = geometry_face_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto geometry_vertex_compute_accessor = geometry_vertex_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto geometry_halfedge_accessor = geometry_halfedge_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto bcd_sweep_compute_accessor = bcd_sweep_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto ncd_overlap_compute_accessor = ncd_overlap_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto cd_overlap_count_compute_accessor = cd_overlap_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto overlap_limit_compute_accessor = overlap_limit_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto collision_state_accessor = collision_state_buffer.get_access<sycl::access::mode::atomic, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_ncd_solver_separating_axis_test_test_poly>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null && dynamics_compute_accessor[body_idx].collision_state == uint8(collision_state::bounds))
						{
							uint32 overlap_limit = overlap_limit_compute_accessor[0];
							uint32 overlap_count = math::min(cd_overlap_count_compute_accessor[body_idx], overlap_limit);

							for (uint32 overlap_index = 0; overlap_index < overlap_count; ++overlap_index)
							{
								auto& sweep_data = bcd_sweep_compute_accessor[body_idx * overlap_limit + overlap_index];

								// HACK: we need to swap indexes for this test if we swapped them during bcd,
								// otherwise test will generate suboptimal info
								uint32 body_1_idx = sweep_data.should_swap ? sweep_data.other_body_index : body_idx;
								uint32 body_2_idx = sweep_data.should_swap ? body_idx : sweep_data.other_body_index;

								sycl_data::ncd_overlap_data overlap_data;
								
								overlap_data.type = sycl_data::ncd_penetration_type::none;
								overlap_data.penetration_depth = std::numeric_limits<float32>::max();
								overlap_data.num_contact_points = 0;

								auto body_poly_1 = geometry_compute_accessor[body_1_idx].poly;
								auto body_poly_2 = geometry_compute_accessor[body_2_idx].poly;

								bool is_overlapping =
									!ncd_solver_separating_axis_test_test_face_overlap(
										body_poly_1, body_poly_2,
										geometry_face_compute_accessor,
										geometry_vertex_compute_accessor,
										geometry_halfedge_accessor,
										sycl_data::ncd_penetration_type::face_1,
										overlap_data
									) &&
									!ncd_solver_separating_axis_test_test_face_overlap(
										body_poly_2, body_poly_1,
										geometry_face_compute_accessor,
										geometry_vertex_compute_accessor,
										geometry_halfedge_accessor,
										sycl_data::ncd_penetration_type::face_2,
										overlap_data
									) &&
									!ncd_solver_separating_axis_test_test_edge_overlap(
										body_poly_1, body_poly_2,
										geometry_face_compute_accessor,
										geometry_vertex_compute_accessor,
										geometry_halfedge_accessor,
										overlap_data
									);

								if (is_overlapping)
								{
									collision_state_accessor[body_1_idx].store(uint32(collision_state::geometry));
									collision_state_accessor[body_2_idx].store(uint32(collision_state::geometry));
								}

								// HACK: ignore index swap when storing data, we only need to swap it for the test
								ncd_overlap_compute_accessor[body_idx * overlap_limit + overlap_index] = overlap_data;
							}
						}
					}
				);
			}
		);

		// 3. update collision state if there is overlap
		auto ncd_last_event = command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto dynamics_compute_accessor = dynamics_compute_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto collision_state_accessor = collision_state_buffer.get_access<sycl::access::mode::read>(command_group);

				command_group.parallel_for<kernel_ncd_solver_separating_axis_test_update_dynamics>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (auto state = collision_state_accessor[body_idx])
						{
							dynamics_compute_accessor[body_idx].collision_state = state;
						}
					}
				);
			}
		);

		
		return sycl_type::sequence_event{ ncd_first_event, ncd_last_event };
	}

	sycl_type::sequence_event ncd_solver_contact_generation(sycl::queue& command_queue, uint32 num_bodies,
															sycl::buffer<sycl_data::statics_data>& statics_buffer, 
															sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
															sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer, 
															sycl::buffer<sycl_data::geometry_face_data>& geometry_face_compute_buffer,
															sycl::buffer<sycl_data::geometry_vertex_data>& geometry_vertex_compute_buffer,
															sycl::buffer<sycl_data::geometry_halfedge_data>& geometry_halfedge_buffer, 
															sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer, 
															sycl::buffer<sycl_data::ncd_overlap_data>& ncd_overlap_compute_buffer,
															sycl::buffer<uint32>& cd_overlap_count_compute_buffer, 
															sycl::buffer<uint32>& overlap_limit_compute_buffer)
	{
		// generate contact points for each overlapping pair
		auto ncd_event = command_queue.submit(
			[&](sycl::handler& command_group)
			{
				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto dynamics_compute_accessor = dynamics_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_compute_accessor = geometry_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto geometry_face_compute_accessor = geometry_face_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto geometry_vertex_compute_accessor = geometry_vertex_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto geometry_halfedge_accessor = geometry_halfedge_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto bcd_sweep_compute_accessor = bcd_sweep_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto ncd_overlap_compute_accessor = ncd_overlap_compute_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);

				auto cd_overlap_count_compute_accessor = cd_overlap_count_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto overlap_limit_compute_accessor = overlap_limit_compute_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_ncd_solver_separating_axis_test_generate_contact>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null && dynamics_compute_accessor[body_idx].collision_state == uint8(collision_state::geometry))
						{
							uint32 overlap_limit = overlap_limit_compute_accessor[0];
							uint32 overlap_count = math::min(cd_overlap_count_compute_accessor[body_idx], overlap_limit);

							for (uint32 overlap_index = 0; overlap_index < overlap_count; ++overlap_index)
							{
								sycl_data::ncd_overlap_data overlap_data = ncd_overlap_compute_accessor[body_idx * overlap_limit + overlap_index];

								if (overlap_data.penetration_depth > 0.f)
								{
									auto& sweep_data = bcd_sweep_compute_accessor[body_idx * overlap_limit + overlap_index];

									// HACK: see SAT function
									uint32 body_1_idx = sweep_data.should_swap ? sweep_data.other_body_index : body_idx;
									uint32 body_2_idx = sweep_data.should_swap ? body_idx : sweep_data.other_body_index;

									auto body_poly_1 = geometry_compute_accessor[body_1_idx].poly;
									auto body_poly_2 = geometry_compute_accessor[body_2_idx].poly;

									switch (overlap_data.type)
									{
									case sycl_data::ncd_penetration_type::face_1:
										ncd_solver_separating_axis_test_compute_face_contact_manifold(
											body_poly_1, body_poly_2,
											geometry_face_compute_accessor,
											geometry_vertex_compute_accessor,
											geometry_halfedge_accessor,
											overlap_data
										);
										break;
									case sycl_data::ncd_penetration_type::face_2:
										ncd_solver_separating_axis_test_compute_face_contact_manifold(
											body_poly_2, body_poly_1,
											geometry_face_compute_accessor,
											geometry_vertex_compute_accessor,
											geometry_halfedge_accessor,
											overlap_data
										);
										break;
									case sycl_data::ncd_penetration_type::edge:
										ncd_solver_separating_axis_test_compute_edge_contact_manifold(
											body_poly_1, body_poly_2,
											geometry_vertex_compute_accessor,
											geometry_halfedge_accessor,
											overlap_data
										);
										break;
									case sycl_data::ncd_penetration_type::none:
										break;
									}

									// HACK: if we swapped index for test, we need to invert this axis, otherwise CR will fail
									if (sweep_data.should_swap)
									{
										overlap_data.contact_normal = -overlap_data.contact_normal;
									}

									// HACK: see SAT function
									ncd_overlap_compute_accessor[body_idx * overlap_limit + overlap_index] = overlap_data;
								}
							}
						}
					}
				);
			}
		);

		return sycl_type::sequence_event{ ncd_event, ncd_event };
	}
}

#endif
