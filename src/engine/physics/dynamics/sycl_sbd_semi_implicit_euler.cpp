#include "engine/physics/dynamics/sycl_sbd_semi_implicit_euler.hpp"

#include "library/math/generic_math.hpp"

#if GAEA_USING_SYCL

#define USING_PRODUCTS_OF_INERTIA 0

#if GAEA_USING_PRECISE_RSQRT
#	define GAEA_SYCL_SRSQRT(arg) 1.0f / sycl::sqrt(arg)
#	define GAEA_SYCL_DRSQRT(arg) 1.0 / sycl::sqrt(arg)
#else
#	define GAEA_SYCL_SRSQRT(arg) math::rsqrt_single_abs_2NR(arg)
#	define GAEA_SYCL_DRSQRT(arg) math::rsqrt_double_rel_1NR(arg)
#endif

namespace gaea::sycl_kernel
{
	struct kernel_sbd_solver_test_symplectic_euler;
	sycl_type::sequence_event sbd_solver_test_symplectic_euler(sycl::queue& command_queue, uint32 num_bodies,
																float32 substep_delta, uint32 substep_count,
																sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
																sycl::buffer<sycl_data::force_data>& force_buffer, 
																sycl::buffer<sycl_data::mass_data>& mass_buffer,
																sycl::buffer<sycl_data::statics_data>& statics_buffer,
																sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer, 
																sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer)
	{
		auto sbd_event = command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				float32 half_substep_delta = 0.5f * substep_delta;

				auto force_accessor = force_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto mass_accessor = mass_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto momentum_copy_accessor = momentum_copy_buffer.get_access<sycl::access::mode::discard_write, sycl::access::target::global_buffer>(command_group);

				auto position_accessor = position_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto position_copy_accessor = position_copy_buffer.get_access<sycl::access::mode::discard_write, sycl::access::target::global_buffer>(command_group);

				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				
				command_group.parallel_for<kernel_sbd_solver_test_symplectic_euler>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null)
						{
							if (statics_accessor[body_idx].mobility != 0 && substep_count != 0)
							{
								for (uint32 substep_index = 0; substep_index < substep_count; ++substep_index)
								{
									vector linear_impulse
										= (force_accessor[body_idx].worldspace_force + world_force.linear
											+ position_accessor[body_idx].rotation * force_accessor[body_idx].bodyspace_force // body-force to world-force
											+ mass_accessor[body_idx].mass * world_accel.linear) // world-accel to world-force
										* substep_delta;
											
									vector angular_impulse
										= (force_accessor[body_idx].worldspace_torque + world_force.angular
											+ position_accessor[body_idx].rotation // body-torque to world-torque
												* (force_accessor[body_idx].bodyspace_torque
													+ mass_accessor[body_idx].inertia * (position_accessor[body_idx].rotation.reciprocal() * world_accel.angular))) // world-accel to body-torque
										* substep_delta;
									
									momentum_accessor[body_idx].linear_momentum += linear_impulse;
									momentum_accessor[body_idx].angular_momentum += angular_impulse;

									vector linear_velocity = mass_accessor[body_idx].mass_inverse * momentum_accessor[body_idx].linear_momentum;
									vector angular_velocity = mass_accessor[body_idx].inertia_inverse * (position_accessor[body_idx].rotation.reciprocal() * momentum_accessor[body_idx].angular_momentum);

									position_accessor[body_idx].position += linear_velocity * substep_delta;
									position_accessor[body_idx].rotation += position_accessor[body_idx].rotation * quaternion{ 0.f, angular_velocity } * half_substep_delta;

									float32 magn_sq = position_accessor[body_idx].rotation.magnitude_square();
									position_accessor[body_idx].rotation *= GAEA_SYCL_SRSQRT(magn_sq);
								}
							}

							momentum_copy_accessor[body_idx] = momentum_accessor[body_idx];
							position_copy_accessor[body_idx] = position_accessor[body_idx];
						}
					}
				);
			}
		);

		return sycl_type::sequence_event{ sbd_event, sbd_event };
	}
	
	struct kernel_sbd_solver_bp_semi_implicit_euler;
	sycl_type::sequence_event sbd_solver_bp_semi_implicit_euler(sycl::queue& command_queue, uint32 num_bodies,
																float32 substep_delta, uint32 substep_count,
																sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
																sycl::buffer<sycl_data::force_data>& force_buffer, 
																sycl::buffer<sycl_data::mass_data>& mass_buffer,
																sycl::buffer<sycl_data::statics_data>& statics_buffer,
																sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer, 
																sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer)
	{
		auto sbd_event = command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				float32 half_substep_delta = 0.5f * substep_delta;

				auto force_accessor = force_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto mass_accessor = mass_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto momentum_copy_accessor = momentum_copy_buffer.get_access<sycl::access::mode::discard_write, sycl::access::target::global_buffer>(command_group);

				auto position_accessor = position_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto position_copy_accessor = position_copy_buffer.get_access<sycl::access::mode::discard_write, sycl::access::target::global_buffer>(command_group);

				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				
				command_group.parallel_for<kernel_sbd_solver_bp_semi_implicit_euler>(
					sycl::range<1>{ num_bodies },
					[=](sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());

						if (statics_accessor[body_idx].entity_type != collision_entity::null)
						{
							if (statics_accessor[body_idx].mobility != 0 && substep_count != 0)
							{
								for (uint32 substep_index = 0; substep_index < substep_count; ++substep_index)
								{
									vector linear_impulse
										= (force_accessor[body_idx].worldspace_force + world_force.linear
											+ position_accessor[body_idx].rotation * force_accessor[body_idx].bodyspace_force // body-force to world-force
											+ mass_accessor[body_idx].mass * world_accel.linear) // world-accel to world-force
										* substep_delta;
											
									vector angular_impulse
										= (force_accessor[body_idx].worldspace_torque + world_force.angular
											+ position_accessor[body_idx].rotation // body-torque to world-torque
												* (force_accessor[body_idx].bodyspace_torque
													+ mass_accessor[body_idx].inertia * (position_accessor[body_idx].rotation.reciprocal() * world_accel.angular))) // world-accel to body-torque
										* substep_delta;
									
									momentum_accessor[body_idx].linear_momentum += linear_impulse;
									momentum_accessor[body_idx].angular_momentum += angular_impulse;

									vector linear_velocity = mass_accessor[body_idx].mass_inverse * momentum_accessor[body_idx].linear_momentum;
									vector angular_velocity = mass_accessor[body_idx].inertia_inverse * (position_accessor[body_idx].rotation.reciprocal() * momentum_accessor[body_idx].angular_momentum);

									position_accessor[body_idx].position += linear_velocity * substep_delta;
									position_accessor[body_idx].rotation += position_accessor[body_idx].rotation * quaternion{ 0.f, angular_velocity } * half_substep_delta;

									float32 magn_sq = position_accessor[body_idx].rotation.magnitude_square();
									position_accessor[body_idx].rotation *= GAEA_SYCL_SRSQRT(magn_sq);
								}
							}

							momentum_copy_accessor[body_idx] = momentum_accessor[body_idx];
							position_copy_accessor[body_idx] = position_accessor[body_idx];
						}
					}
				);
			}
		);

		return sycl_type::sequence_event{ sbd_event, sbd_event };
	}

	struct kernel_sbd_solver_dp6_semi_implicit_euler;
	sycl_type::sequence_event sbd_solver_dp6_semi_implicit_euler(sycl::queue& command_queue, uint32 num_bodies,
																 float32 substep_delta, uint32 substep_count,
																 sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
																 sycl::buffer<sycl_data::force_data>& force_buffer, 
																 sycl::buffer<sycl_data::mass_data>& mass_buffer,
																 sycl::buffer<sycl_data::statics_data>& statics_buffer,
																 sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer, 
																 sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer)
	{	
		auto sbd_event = command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				// We need to ensure that the elements of each body are contained in a single work group
				uint32 max_work_group_size = uint32(command_queue.get_device().get_info<sycl::info::device::max_work_group_size>());
				uint32 work_group_size = max_work_group_size - max_work_group_size % 6;

				uint32 num_body_work_items = num_bodies * 6;

				uint32 num_work_groups = num_body_work_items / work_group_size + uint32(bool(num_body_work_items % work_group_size));
				uint32 num_work_group_items = num_work_groups > 1 ? work_group_size : num_body_work_items;

				uint32 num_work_group_bodies = num_work_group_items / 6;

				float32 half_substep_delta = 0.5f * substep_delta;

				auto force_accessor = force_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto mass_accessor = mass_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto momentum_accessor = momentum_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto momentum_copy_accessor = momentum_copy_buffer.get_access<sycl::access::mode::discard_write, sycl::access::target::global_buffer>(command_group);
				
				auto position_accessor = position_buffer.get_access<sycl::access::mode::read_write, sycl::access::target::global_buffer>(command_group);
				auto position_copy_accessor = position_copy_buffer.get_access<sycl::access::mode::discard_write, sycl::access::target::global_buffer>(command_group);

				auto statics_accessor = statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				// We use auxiliary storage for some intermediate results, mostly since angular velocity and rotation calculations require synchronization between the threads
				// We use a quaternion for the angular velocity because we will use it as a quaternion for rotation integration with the real component set to zero
				auto aux_velocity_accessor = sycl::accessor<sycl_data::physics_vecquat_data, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_bodies, command_group);
				auto aux_rotation_accessor = sycl::accessor<quaternion, 1, sycl::access::mode::read_write, sycl::access::target::local>(num_work_group_bodies, command_group);

				command_group.parallel_for<kernel_sbd_solver_dp6_semi_implicit_euler>(
					sycl::nd_range<1>{ num_work_groups * num_work_group_items, num_work_group_items },
					[=] (sycl::nd_item<1> work_item)
					{
						uint32 gl_work_item_idx = uint32(work_item.get_global_linear_id());
						uint32 l_work_item_idx = uint32(work_item.get_local_linear_id());

						// This is used to access the buffers (in global memory) for each body
						uint32 gl_body_idx = gl_work_item_idx / 6;									// 0, 0, 0, 0, 0, 0; 1, ...
						uint32 gl_body_item_idx = gl_work_item_idx % 6;								// 0, 1, 2, 3, 4, 5; 0, ...

						// This is used to access the auxiliary buffers (in local memory) for each body
						uint32 l_body_idx = l_work_item_idx / 6;

						if (gl_work_item_idx < num_body_work_items && statics_accessor[gl_body_idx].entity_type != collision_entity::null)
						{
							// Calculate all indexes and checks we'll use ahead of time

							// Generic index to linear quantities
							uint32 lin_idx = 0;
							// Generic index to angular quantities
							uint32 ang_idx = 1;
							
							// Generic index to either the linear or the angular quantities of the body
							uint32 lin_ang_idx = gl_body_item_idx / 3;								// 0, 0, 0, 1, 1, 1; 0, ...
							// Generic index to each component of a particular three-dimensional quantity
							uint32 lin_ang_comp_idx = gl_body_item_idx % 3;							// 0, 1, 2, 0, 1, 2; 0, ...

							// Index to each component of a four-dimensional quantity (e.g. quaternions)
							// This is used to access the angular velocity and rotation quantities
							uint32 ang_comp_idx = lin_ang_idx + lin_ang_comp_idx;					// 0, 1, 2, 1, 2, 3; 0, ...
							
							// Attribute of threads which are qualified to handle linear quantities
							// TODO IDEA maybe test whether using the other 3 threads makes a difference
							bool is_lin_thread = lin_ang_idx == 0;
							// Attribute of threads which are qualified to handle angular quantities
							// This is heavily tied to how 'ang_comp_idx' is calculated, so recheck this if that index needs to change
							bool is_ang_thread = lin_ang_idx != 0 || lin_ang_comp_idx == 0;

							if (statics_accessor[gl_body_idx].mobility != 0 && substep_count != 0)
							{
								// Index to worldspace force and bodyspace torque
								uint32 for_tor_idx = 3 * lin_ang_idx;
								// Index to bodyspace force and worldspace torque (require conversion to the other space necessary)
								uint32 for_tor_aux_idx = 1 + lin_ang_idx;
								// Index to worldspace force and torque
								uint32 for_tor_world_idx = 2 * lin_ang_idx;

								// Index to mass and inertia
								uint32 mass_iner_idx = lin_ang_idx;
								// Index to mass inverse and inertia inverse
								uint32 mass_iner_inv_idx = 2 + lin_ang_idx;
#if USING_PRODUCTS_OF_INERTIA
								// TODO
								uint32 mass_iner_comp_idx = 3 * lin_ang_idx * comp_idx;
#else

								uint32 mass_iner_comp_idx = 4 * lin_ang_idx * lin_ang_comp_idx;
#endif
								// Pre-calculate signs and indexes for quat-vec product
								float32 qv_rot_y_sign = lin_ang_idx == 0 ? -1.f : +1.f;
								float32 qv_rot_z_sign = lin_ang_idx == 0 ? +1.f : -1.f;

								uint32 qv_for_x_idx = lin_ang_comp_idx;
								uint32 qv_for_y_idx = (lin_ang_comp_idx + 1) % 3;
								uint32 qv_for_z_idx = (lin_ang_comp_idx + 2) % 3;

								uint32 qv_rot_w_idx = 0;
								uint32 qv_rot_x_idx = qv_for_x_idx + 1;
								uint32 qv_rot_y_idx = qv_for_y_idx + 1;
								uint32 qv_rot_z_idx = qv_for_z_idx + 1;

								// Pre-calculate signs and indexes for quat-quat product
								float32 qq_rot_x_sign = lin_ang_idx == 0 || lin_ang_comp_idx == 1 ? -1.f : +1.f;
								float32 qq_rot_y_sign = lin_ang_idx == 0 || lin_ang_comp_idx == 2 ? -1.f : +1.f;
								float32 qq_rot_z_sign = lin_ang_idx == 0 || lin_ang_comp_idx == 0 ? -1.f : +1.f;

								uint32 qq_rot_w_idx = 0;
								uint32 qq_rot_x_idx = 1;
								uint32 qq_rot_y_idx = 2;
								uint32 qq_rot_z_idx = 3;

								uint32 qq_vel_w_idx = qq_rot_w_idx ^ ang_comp_idx;
								uint32 qq_vel_x_idx = qq_rot_x_idx ^ ang_comp_idx;
								uint32 qq_vel_y_idx = qq_rot_y_idx ^ ang_comp_idx;
								uint32 qq_vel_z_idx = qq_rot_z_idx ^ ang_comp_idx;

								// Note: the names of the last indexes do not represent the actual components that each thread accesses,
								// but instead the relative position that they appear in the respective expressions

								if (is_ang_thread)
								{
									// Initialize angular velocity with zero
									// No need to synchronize it yet, there will be sync points well before this is read again
									aux_velocity_accessor[l_body_idx][ang_idx][ang_comp_idx] = 0.0f;
								}
#if USING_PRODUCTS_OF_INERTIA
								//work_item.barrier(sycl::access::fence_space::global_space);
#								error TODO adapt for full matrix mul
								// will probably need to diverge
#else
								// Calculate world interaction (through forces and accelerations) with the body and add them to the worldspace quantities
								force_accessor[gl_body_idx][for_tor_world_idx][lin_ang_comp_idx]
									+= world_force[lin_ang_idx][lin_ang_comp_idx]
									+ mass_accessor[gl_body_idx][mass_iner_idx][mass_iner_comp_idx] * world_accel[lin_ang_idx][lin_ang_comp_idx];

								// We need to synchronize before proceeding as quat-vec product touches all components of the forces
								// (though we only need this for worldspace torque)
								work_item.barrier(sycl::access::fence_space::global_space);

								// Quaternion-vector product between rotation/reciprocal rotation and bodyspace force/worldspace torque, respectively,
								// We use the signs to differentiate between the rotation and its reciprocal in the equation
								force_accessor[gl_body_idx][for_tor_idx][lin_ang_comp_idx]
									+= force_accessor[gl_body_idx][for_tor_aux_idx][qv_for_x_idx] + 2.f
									* (force_accessor[gl_body_idx][for_tor_aux_idx][qv_for_x_idx]
										* (-math::square(position_accessor[gl_body_idx][ang_idx][qv_rot_y_idx])
											+ -math::square(position_accessor[gl_body_idx][ang_idx][qv_rot_z_idx]))
										+ force_accessor[gl_body_idx][for_tor_aux_idx][qv_for_y_idx]
										* (qv_rot_y_sign
											* position_accessor[gl_body_idx][ang_idx][qv_rot_w_idx] * position_accessor[gl_body_idx][ang_idx][qv_rot_z_idx]
											+ position_accessor[gl_body_idx][ang_idx][qv_rot_x_idx] * position_accessor[gl_body_idx][ang_idx][qv_rot_y_idx])
										+ force_accessor[gl_body_idx][for_tor_aux_idx][qv_for_z_idx]
										* (qv_rot_z_sign
											* position_accessor[gl_body_idx][ang_idx][qv_rot_w_idx] * position_accessor[gl_body_idx][ang_idx][qv_rot_y_idx]
											+ position_accessor[gl_body_idx][ang_idx][qv_rot_z_idx] * position_accessor[gl_body_idx][ang_idx][qv_rot_x_idx]));
#endif

								for (uint32 substep_index = 0; substep_index < substep_count; ++substep_index)
								{
									momentum_accessor[gl_body_idx][lin_ang_idx][lin_ang_comp_idx]
										+= force_accessor[gl_body_idx][for_tor_idx][lin_ang_comp_idx]
										* substep_delta;

#if USING_PRODUCTS_OF_INERTIA
									//work_item.barrier(sycl::access::fence_space::global_space);
#									error TODO adapt for full matrix mul
									// will probably need to diverge
#else
									// We use rot_component_idx to access the velocity because this way we keep the angular velocity w component at 0
									aux_velocity_accessor[l_body_idx][lin_ang_idx][ang_comp_idx]
										= mass_accessor[gl_body_idx][mass_iner_inv_idx][mass_iner_comp_idx]
										* momentum_accessor[gl_body_idx][lin_ang_idx][lin_ang_comp_idx];
#endif

									work_item.barrier(sycl::access::fence_space::local_space);

									// We need to diverge for the last part

									// First calculate position, we only use 3 threads for this (out of every 6)
									if (is_lin_thread)
									{
										position_accessor[gl_body_idx][lin_idx][lin_ang_comp_idx]
											+= aux_velocity_accessor[l_body_idx][lin_idx][lin_ang_comp_idx]
											* substep_delta;
									}

									// Then calculate rotation, we use 4 threads for this (out of every 6)
									if (is_ang_thread)
									{
										// Quaternion product between rotation and angular velocity
										// We use the auxiliary buffer so that threads don't trample each others' work
										aux_rotation_accessor[l_body_idx][ang_comp_idx]
											= position_accessor[gl_body_idx][ang_idx][ang_comp_idx]
											+ (position_accessor[gl_body_idx][ang_idx][qq_rot_w_idx] * aux_velocity_accessor[l_body_idx][ang_idx][qq_vel_w_idx]
												+ qq_rot_x_sign
												* position_accessor[gl_body_idx][ang_idx][qq_rot_x_idx] * aux_velocity_accessor[l_body_idx][ang_idx][qq_vel_x_idx]
												+ qq_rot_y_sign
												* position_accessor[gl_body_idx][ang_idx][qq_rot_y_idx] * aux_velocity_accessor[l_body_idx][ang_idx][qq_vel_y_idx]
												+ qq_rot_z_sign
												* position_accessor[gl_body_idx][ang_idx][qq_rot_z_idx] * aux_velocity_accessor[l_body_idx][ang_idx][qq_vel_z_idx])
											* half_substep_delta;
									}

									work_item.barrier(sycl::access::fence_space::local_space);

									// Normalize rotation
									if (is_ang_thread)
									{
										float32 magn_inv = GAEA_SYCL_SRSQRT(
											math::square(aux_rotation_accessor[l_body_idx][0])
											+ math::square(aux_rotation_accessor[l_body_idx][1])
											+ math::square(aux_rotation_accessor[l_body_idx][2])
											+ math::square(aux_rotation_accessor[l_body_idx][3])
										);

										position_accessor[gl_body_idx][ang_idx][ang_comp_idx]
											= aux_rotation_accessor[l_body_idx][ang_comp_idx] * magn_inv;
									}

									work_item.barrier(sycl::access::fence_space::global_space);
								}
							}
							
							momentum_copy_accessor[gl_body_idx][lin_ang_idx][lin_ang_comp_idx] = momentum_accessor[gl_body_idx][lin_ang_idx][lin_ang_comp_idx];

							if (is_lin_thread)
							{
								position_copy_accessor[gl_body_idx][lin_idx][lin_ang_comp_idx] = position_accessor[gl_body_idx][lin_idx][lin_ang_comp_idx];
							}
							if (is_ang_thread)
							{
								position_copy_accessor[gl_body_idx][ang_idx][ang_comp_idx] = position_accessor[gl_body_idx][ang_idx][ang_comp_idx];
							}
						}
					}
				);
			}
		);

		return sycl_type::sequence_event{ sbd_event, sbd_event };
	}
}

#endif
