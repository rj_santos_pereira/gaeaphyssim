#ifndef ENGINE_PHYSICS_DYNAMICS_SYCL_BCD_SORT_SWEEP_SOLVER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_SYCL_BCD_SORT_SWEEP_SOLVER_INCLUDE_GUARD

#include "core/core_sycl.hpp"

#include "engine/physics/sycl_physics.hpp"

#if GAEA_USING_SYCL

namespace gaea::sycl_data
{
	struct bcd_body_data
	{
		uint32 sort_index;
		float32 max_bounds;
	};

	struct bcd_sort_data
	{
		uint32 body_index;
		float32 min_bounds;
	};

	struct bcd_sweep_data
	{
		uint32 other_body_index;
		bool should_swap;
	};
}

namespace gaea::sycl_kernel
{
	sycl_type::sequence_event bcd_solver_sort_sweep(sycl::queue& command_queue, uint32 num_bodies, bool needs_full_sort,
													sycl::buffer<sycl_data::statics_data>& statics_buffer,
													sycl::buffer<sycl_data::position_data>& position_buffer,
													sycl::buffer<sycl_data::geometry_data>& geometry_buffer,
													sycl::buffer<sycl_data::dynamics_data>& dynamics_compute_buffer,
													sycl::buffer<sycl_data::geometry_data>& geometry_compute_buffer,
													sycl::buffer<sycl_data::bcd_body_data>& bcd_body_compute_buffer,
													sycl::buffer<sycl_data::bcd_sort_data>& bcd_sort_compute_buffer,
													sycl::buffer<sycl_data::bcd_sweep_data>& bcd_sweep_compute_buffer,
													sycl::buffer<uint32>& cd_overlap_count_compute_buffer, 
													sycl::buffer<uint32>& overlap_limit_compute_buffer,
													uint32 grid_side_size);
}

#endif

#endif
