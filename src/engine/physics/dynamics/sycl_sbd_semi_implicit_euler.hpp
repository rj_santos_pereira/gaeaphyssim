#ifndef ENGINE_PHYSICS_DYNAMICS_SYCL_SBD_SEMI_IMPLICIT_EULER_INCLUDE_GUARD
#define ENGINE_PHYSICS_DYNAMICS_SYCL_SBD_SEMI_IMPLICIT_EULER_INCLUDE_GUARD

#include "core/core_sycl.hpp"
#include "engine/physics/sycl_physics.hpp"

#if GAEA_USING_SYCL

namespace gaea::sycl_kernel
{
	sycl_type::sequence_event sbd_solver_bp_semi_implicit_euler(sycl::queue& command_queue, uint32 num_bodies,
																float32 substep_delta, uint32 substep_count,
																sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
																sycl::buffer<sycl_data::force_data>& force_buffer, 
																sycl::buffer<sycl_data::mass_data>& mass_buffer,
																sycl::buffer<sycl_data::statics_data>& statics_buffer,
																sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer, 
																sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer);

	sycl_type::sequence_event sbd_solver_dp6_semi_implicit_euler(sycl::queue& command_queue, uint32 num_bodies,
																 float32 substep_delta, uint32 substep_count,
																 sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
																 sycl::buffer<sycl_data::force_data>& force_buffer, 
																 sycl::buffer<sycl_data::mass_data>& mass_buffer,
																 sycl::buffer<sycl_data::statics_data>& statics_buffer,
																 sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer, 
																 sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer);

#if 0
	sycl_type::sequence_event sbd_solver_dp8_semi_implicit_euler(sycl::queue& command_queue, uint32 num_bodies,
																 float32 substep_delta, uint32 substep_count, 
																 sycl_data::physics_vec_data world_force, sycl_data::physics_vec_data world_accel,
																 sycl::buffer<sycl_data::force_data>& force_buffer, 
																 sycl::buffer<sycl_data::mass_data>& mass_buffer,
																 sycl::buffer<sycl_data::statics_data>& dynamics_buffer,
																 sycl::buffer<sycl_data::momentum_data>& momentum_buffer, sycl::buffer<sycl_data::momentum_data>& momentum_copy_buffer, 
																 sycl::buffer<sycl_data::position_data>& position_buffer, sycl::buffer<sycl_data::position_data>& position_copy_buffer);
#endif
}

#endif

#endif
