#ifndef ENGINE_PHYSICS_NATIVE_SIMULATOR_INCLUDE_GUARD
#define ENGINE_PHYSICS_NATIVE_SIMULATOR_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/physics/native_rigid_body.hpp"
#include "engine/physics/dynamics/bcd_parallel_sort_sweep_solver.hpp"
#include "engine/physics/dynamics/bcd_sequential_sort_sweep_solver.hpp"
#include "engine/physics/dynamics/cr_local_symplectic_euler_solver.hpp"
#include "engine/physics/dynamics/ncd_separating_axis_test_solver.hpp"
#include "library/containers.hpp"
#include "library/atomic.hpp"
#include "library/mutex.hpp"

namespace gaea
{
	struct native_simulator_parameters
	{
		float32 max_substep_delta = 1.0f / 120.0f;
		uint32 max_substep_count = 6;

		// If this is specified and is not zero, 'std::max(simulate_thread_count, 1)' threads will be created;
		// Otherwise, thread_ratio will be used
		uint32 thread_count = 0;
		// If this is not zero, 'std::max(std::thread::hardware_concurrency / thread_ratio, 1)' threads will be created;
		// Otherwise, 'std::max(std::thread::hardware_concurrency / 2, 1)' threads will be created
		uint32 thread_ratio = 2;
	};

	class native_simulator final : public base_simulator
	{
	public:
		explicit native_simulator();

		virtual bool initialize(void* parameters) override;
		virtual void finalize() override;

		virtual void step(float64 frame_delta) override;

		virtual void finish_step() override;

		virtual bool is_real_time() const override;

		virtual void register_body(simulation_object type, void* object) override;
		virtual void unregister_body(simulation_object type, void * object) override;
		
		virtual void apply_world_force(simulation_vector_type const& force) override;
		virtual void apply_world_torque(simulation_vector_type const& force) override;
		
		virtual void apply_world_lin_accel(simulation_vector_type const& accel) override;
		virtual void apply_world_ang_accel(simulation_vector_type const& accel) override;

		virtual float32 query_step_duration() const override;
		virtual float32 query_collision_detection_duration() const override;
		virtual float32 query_broad_phase_duration() const override;
		virtual float32 query_narrow_phase_duration() const override;
		virtual float32 query_collision_resolution_duration() const override;

		virtual uint32 num_threads() const override;

		void toggle_bounding_box_display();

		bool is_displaying_bounding_boxes() const;

	private:
		void _simulation_loop(std::size_t thread_index, std::size_t thread_count);
		
		void _sequential_sort_sweep(std::size_t thread_index, std::size_t thread_count);

		void _parallel_sort_sweep(std::size_t thread_index, std::size_t thread_count);
		
		void _parallel_separating_axis_test(std::size_t thread_index, std::size_t thread_count, float32 step_delta);

		void _parallel_local_symplectic_euler(std::size_t thread_index, std::size_t thread_count, float32 substep_delta, uint32 substep_count);

		array<native_rigid_body*> _rigid_body_array;

		array<std::thread> _simulate_thread_array;
		semaphore _begin_simulate_semaphore;
		semaphore _end_simulate_semaphore;

		barrier _bcd_barrier;
		barrier _ncd_barrier;
		barrier _cr_barrier;

		atomic_uint32 _finished_step_count;

		bcd_parallel_sort_sweep_solver _bcd_parallel_solver;
		bcd_sequential_sort_sweep_solver _bcd_sequential_solver;
		ncd_separating_axis_test_solver _ncd_solver;
		cr_local_symplectic_euler_solver _cr_solver;

		mutex _bcd_parallel_mutex;
		
		array<bcd_overlap_data> _bcd_result;
		array<ncd_overlap_data> _ncd_result;

		stdchr::steady_clock::time_point _bcd_begin_time;
		stdchr::steady_clock::time_point _ncd_begin_time;
		stdchr::steady_clock::time_point _cr_begin_time;
		stdchr::steady_clock::time_point _step_end_time;

		spin_mutex _bcd_time_mutex;
		spin_mutex _ncd_time_mutex;
		spin_mutex _cr_time_mutex;

		float32 _step_delta;
		
		float32 _max_substep_delta;
		uint32 _max_substep_count;

		uint32 _num_threads;

		uint8 _should_run : 1;
		uint8 _should_step : 1;

		uint8 _should_display_bounding_boxes : 1;
		uint8 _is_real_time : 1;

	};
}

#endif
