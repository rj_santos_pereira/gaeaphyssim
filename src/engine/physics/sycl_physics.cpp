#include "engine/physics/sycl_physics.hpp"

// ComputeCpp SYCL resources:
// https://developer.codeplay.com/products/computecpp/ce/guides/sycl-guide
// SYCL 1.2.1 spec:
// https://www.khronos.org/registry/SYCL/specs/sycl-1.2.1.pdf

// Warp divergence and conditional execution
// https://stackoverflow.com/questions/29896422/cuda-avoiding-serial-execution-on-branch-divergence
// https://stackoverflow.com/questions/5897454/what-do-work-items-execute-when-conditionals-are-used-in-gpu-programming
// https://stackoverflow.com/questions/45682318/how-does-warp-divergence-manifest-in-sass?rq=1

// Memory access and memory coalescence
// https://stackoverflow.com/questions/64780699/is-it-more-efficient-in-sycl-to-use-one-buffer-or-multiple-buffers
// https://stackoverflow.com/questions/17961331/opencl-are-work-group-axes-exchangeable
// https://stackoverflow.com/questions/63297515/why-opencl-work-group-size-has-huge-performance-impact-on-gpu

// Constant memory is quite limited:
// https://stackoverflow.com/a/44697039
// Currently some maths functions are not implemented:
// https://support.codeplay.com/t/computecpp-rt0100-with-computecpp-2-1-0-on-vs2019/366

// Instruction throughput:
// https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#arithmetic-instructions

#if GAEA_USING_SYCL

namespace gaea::sycl_type
{
	sequence_event::sequence_event()
		: _first{}
		, _last{}
	{
	}
	sequence_event::sequence_event(sycl::event const& first, sycl::event const& last)
		: _first{ first }
		, _last{ last }
	{
	}

	sequence_event::sequence_event(sequence_event const& other)
		: _first{ other._first }
		, _last{ other._last }
	{
	}

	sequence_event::sequence_event(sequence_event&& other) noexcept
		: _first{ std::move(other._first) }
		, _last{ std::move(other._last) }
	{
	}

	sequence_event& sequence_event::operator=(sequence_event const& other)
	{
		if (this != &other)
		{
			_first = other._first;
			_last = other._last;
		}
		return *this;
	}

	sequence_event& sequence_event::operator=(sequence_event&& other) noexcept
	{
		if (this != &other)
		{
			_first = std::move(other._first);
			_last = std::move(other._last);
		}
		return *this;
	}

	template <>
	sycl::info::param_traits<sycl::info::event, sycl::info::event::command_execution_status>::return_type
		sequence_event::get_info<sycl::info::event::command_execution_status>() const
	{
		auto last_status = _last.get_info<sycl::info::event::command_execution_status>();
		switch (last_status)
		{
		case sycl::info::event_command_status::complete:
		case sycl::info::event_command_status::running:
			return last_status;
		case sycl::info::event_command_status::submitted:
		default:
			auto first_status = _first.get_info<sycl::info::event::command_execution_status>();
			if (first_status == sycl::info::event_command_status::complete)
			{
				return sycl::info::event_command_status::running;
			}
			return first_status;
		}
	}
	
	template <>
	sycl::info::param_traits<sycl::info::event, sycl::info::event::reference_count>::return_type
		sequence_event::get_info<sycl::info::event::reference_count>() const
	{
		return std::max(_first.get_info<sycl::info::event::reference_count>(), _last.get_info<sycl::info::event::reference_count>());
	}

	template <>
	sycl::info::param_traits<sycl::info::event_profiling, sycl::info::event_profiling::command_submit>::return_type
		sequence_event::get_profiling_info<sycl::info::event_profiling::command_submit>() const
	{
		return _first.get_profiling_info<sycl::info::event_profiling::command_submit>();
	}

	template <>
	sycl::info::param_traits<sycl::info::event_profiling, sycl::info::event_profiling::command_start>::return_type
		sequence_event::get_profiling_info<sycl::info::event_profiling::command_start>() const
	{
		return _first.get_profiling_info<sycl::info::event_profiling::command_start>();
	}

	template <>
	sycl::info::param_traits<sycl::info::event_profiling, sycl::info::event_profiling::command_end>::return_type
		sequence_event::get_profiling_info<sycl::info::event_profiling::command_end>() const
	{
		return _last.get_profiling_info<sycl::info::event_profiling::command_end>();
	}

	void sequence_event::wait()
	{
		_last.wait();
	}

	void sequence_event::wait_and_throw()
	{
		_last.wait();
	}
}

namespace gaea::sycl_kernel
{
	struct kernel_swap_buffers;
	sycl_type::sequence_event swap_buffers(sycl::queue& command_queue, uint32 num_bodies,
										   sycl::buffer<sycl_data::mass_data>& old_mass_buffer, sycl::buffer<sycl_data::mass_data>& new_mass_buffer,
										   sycl::buffer<sycl_data::statics_data>& old_statics_buffer, sycl::buffer<sycl_data::statics_data>& new_statics_buffer,
										   sycl::buffer<sycl_data::momentum_data>& old_momentum_buffer, sycl::buffer<sycl_data::momentum_data>& new_momentum_buffer,
										   sycl::buffer<sycl_data::position_data>& old_position_buffer, sycl::buffer<sycl_data::position_data>& new_position_buffer,
										   sycl::buffer<sycl_data::geometry_data>& old_geometry_buffer, sycl::buffer<sycl_data::geometry_data>& new_geometry_buffer,
										   sycl::buffer<sycl_data::geometry_face_data>& old_geom_poly_face_buffer, sycl::buffer<sycl_data::geometry_face_data>& new_geom_poly_face_buffer,
										   sycl::buffer<sycl_data::geometry_vertex_data>& old_geom_poly_vertex_buffer, sycl::buffer<sycl_data::geometry_vertex_data>& new_geom_poly_vertex_buffer,
										   sycl::buffer<sycl_data::geometry_halfedge_data>& old_geom_poly_halfedge_buffer, sycl::buffer<sycl_data::geometry_halfedge_data>& new_geom_poly_halfedge_buffer)
	{
		auto swap_event = command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto old_mass_accessor = old_mass_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_mass_accessor = new_mass_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_statics_accessor = old_statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_statics_accessor = new_statics_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_momentum_accessor = old_momentum_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_momentum_accessor = new_momentum_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_position_accessor = old_position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_position_accessor = new_position_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_geometry_accessor = old_geometry_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_geometry_accessor = new_geometry_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_geom_poly_face_accessor = old_geom_poly_face_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_geom_poly_face_accessor = new_geom_poly_face_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_geom_poly_vertex_accessor = old_geom_poly_vertex_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_geom_poly_vertex_accessor = new_geom_poly_vertex_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				auto old_geom_poly_halfedge_accessor = old_geom_poly_halfedge_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);
				auto new_geom_poly_halfedge_accessor = new_geom_poly_halfedge_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_swap_buffers>(
					sycl::range<1>{ num_bodies },
					[=] (sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());
						
						new_mass_accessor[body_idx] = old_mass_accessor[body_idx];
						new_statics_accessor[body_idx] = old_statics_accessor[body_idx];
						new_momentum_accessor[body_idx] = old_momentum_accessor[body_idx];
						new_position_accessor[body_idx] = old_position_accessor[body_idx];
						
						new_geometry_accessor[body_idx] = old_geometry_accessor[body_idx];

						auto const& poly = new_geometry_accessor[body_idx].poly;

						for (uint32 index = poly.face_index, count = poly.face_index + poly.face_count; index < count; ++index)
						{
							new_geom_poly_face_accessor[index] = old_geom_poly_face_accessor[index];
						}

						for (uint32 index = poly.vertex_index, count = poly.vertex_index + poly.vertex_count; index < count; ++index)
						{
							new_geom_poly_vertex_accessor[index] = old_geom_poly_vertex_accessor[index];
						}

						for (uint32 index = poly.halfedge_index, count = poly.halfedge_index + poly.halfedge_count; index < count; ++index)
						{
							new_geom_poly_halfedge_accessor[index] = old_geom_poly_halfedge_accessor[index];
						}
					}
				);
			}
		);

		return sycl_type::sequence_event{ swap_event, swap_event };
	}
	
	struct kernel_update_buffers;
	sycl_type::sequence_event update_buffers(sycl::queue& command_queue, uint32 num_bodies,
											 sycl::buffer<sycl_data::mass_data>& old_mass_buffer, sycl::buffer<sycl_data::mass_data>& new_mass_buffer,
											 sycl::buffer<sycl_data::statics_data>& old_statics_buffer, sycl::buffer<sycl_data::statics_data>& new_statics_buffer,
											 sycl::buffer<sycl_data::momentum_data>& old_momentum_buffer, sycl::buffer<sycl_data::momentum_data>& new_momentum_buffer,
											 sycl::buffer<sycl_data::position_data>& old_position_buffer, sycl::buffer<sycl_data::position_data>& new_position_buffer,
											 sycl::buffer<sycl_data::geometry_data>& old_geometry_buffer, sycl::buffer<sycl_data::geometry_data>& new_geometry_buffer,
											 sycl::buffer<sycl_data::geometry_face_data>& old_geom_poly_face_buffer, sycl::buffer<sycl_data::geometry_face_data>& new_geom_poly_face_buffer,
											 sycl::buffer<sycl_data::geometry_vertex_data>& old_geom_poly_vertex_buffer, sycl::buffer<sycl_data::geometry_vertex_data>& new_geom_poly_vertex_buffer,
											 sycl::buffer<sycl_data::geometry_halfedge_data>& old_geom_poly_halfedge_buffer, sycl::buffer<sycl_data::geometry_halfedge_data>& new_geom_poly_halfedge_buffer,
											 sycl::buffer<sycl_data::flags_data>& flags_buffer)
	{
		auto update_event = command_queue.submit(
			[&] (sycl::handler& command_group)
			{
				auto old_mass_accessor = old_mass_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_mass_accessor = new_mass_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_statics_accessor = old_statics_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_statics_accessor = new_statics_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_momentum_accessor = old_momentum_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_momentum_accessor = new_momentum_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_position_accessor = old_position_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_position_accessor = new_position_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_geometry_accessor = old_geometry_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_geometry_accessor = new_geometry_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_geom_poly_face_accessor = old_geom_poly_face_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_geom_poly_face_accessor = new_geom_poly_face_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_geom_poly_vertex_accessor = old_geom_poly_vertex_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_geom_poly_vertex_accessor = new_geom_poly_vertex_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto old_geom_poly_halfedge_accessor = old_geom_poly_halfedge_buffer.get_access<sycl::access::mode::write, sycl::access::target::global_buffer>(command_group);
				auto new_geom_poly_halfedge_accessor = new_geom_poly_halfedge_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				auto flags_accessor = flags_buffer.get_access<sycl::access::mode::read, sycl::access::target::global_buffer>(command_group);

				command_group.parallel_for<kernel_update_buffers>(
					sycl::range<1>{ num_bodies },
					[=] (sycl::item<1> work_item)
					{
						uint32 body_idx = uint32(work_item.get_linear_id());
						
						if (flags_accessor[body_idx].has_update)
						{
							if (flags_accessor[body_idx].should_update_mass)
							{
								old_mass_accessor[body_idx] = new_mass_accessor[body_idx];
							}
							if (flags_accessor[body_idx].should_update_statics)
							{
								old_statics_accessor[body_idx] = new_statics_accessor[body_idx];
							}
							if (flags_accessor[body_idx].should_update_momentum)
							{
								old_momentum_accessor[body_idx] = new_momentum_accessor[body_idx];
							}
							if (flags_accessor[body_idx].should_update_position)
							{
								old_position_accessor[body_idx] = new_position_accessor[body_idx];
							}
							if (flags_accessor[body_idx].should_update_geometry)
							{
								old_geometry_accessor[body_idx] = new_geometry_accessor[body_idx];

								auto const& poly = old_geometry_accessor[body_idx].poly;
								
								for (uint32 index = poly.face_index, count = poly.face_index + poly.face_count; index < count; ++index)
								{
									old_geom_poly_face_accessor[index] = new_geom_poly_face_accessor[index];
								}
								
								for (uint32 index = poly.vertex_index, count = poly.vertex_index + poly.vertex_count; index < count; ++index)
								{
									old_geom_poly_vertex_accessor[index] = new_geom_poly_vertex_accessor[index];
								}
								
								for (uint32 index = poly.halfedge_index, count = poly.halfedge_index + poly.halfedge_count; index < count; ++index)
								{
									old_geom_poly_halfedge_accessor[index] = new_geom_poly_halfedge_accessor[index];
								}
							}
						}
					}
				);
			}
		);

		return sycl_type::sequence_event{ update_event, update_event };
	}
}

#endif
