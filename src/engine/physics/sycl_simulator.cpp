#include "engine/physics/sycl_simulator.hpp"

#include "engine/engine_config.hpp"
#include "engine/physics/dynamics/sycl_sbd_semi_implicit_euler.hpp"

#if GAEA_USING_SYCL

namespace gaea
{
	sycl_simulator::sycl_simulator()
		: _rigid_body_array{}
		, _rigid_body_data_ptr{}
		, _simulate_thread_array{}
		, _begin_simulate_semaphore{ 0 }
		, _end_simulate_semaphore{ 0 }
		, _finished_step_count{ 0 }
		, _command_device{}
		, _command_queue{}
		, _events_buffer{}
		// Host-only buffers
		, _force_host_buffer{}
		, _flags_host_buffer{}
		// Host-Device buffers
		// Infrequently changing data
		, _mass_host_buffer{}
		, _mass_device_buffer{}
		, _statics_host_buffer{}
		, _statics_device_buffer{}
		, _geometry_host_buffer{}
		, _geometry_device_buffer{}
		, _geometry_face_host_buffer{}
		, _geometry_face_device_buffer{}
		, _geometry_vertex_host_buffer{}
		, _geometry_vertex_device_buffer{}
		, _geometry_halfedge_host_buffer{}
		, _geometry_halfedge_device_buffer{}
		// Frequently changing data
		, _momentum_host_buffer{}
		, _momentum_device_buffer{}
		, _position_host_buffer{}
		, _position_device_buffer{}
		// Device-only buffers
		, _dynamics_compute_buffer{}
		, _geometry_compute_buffer{}
		, _geometry_face_compute_buffer{}
		, _geometry_vertex_compute_buffer{}
		, _bcd_body_compute_buffer{}
		, _bcd_sort_compute_buffer{}
		, _bcd_sweep_compute_buffer{}
		, _ncd_overlap_compute_buffer{}
		, _cr_constraint_compute_buffer{}
		, _cd_overlap_count_compute_buffer{}
		, _cr_overlap_count_compute_buffer{}
		, _overlap_limit_compute_buffer{}
		, _constraint_count_compute_buffer{}
		, _constraint_index_compute_buffer{}
		, _overlap_limit{ 0 }
		, _overlap_default_limit{ 0 }
		, _num_constraint_work_groups{ 0 }
		, _num_constraint_work_items_per_group{ 0 }
		, _constraint_grid_side_size{ 0 }
		, _total_body_count{ 0 }
		, _simulate_body_count{ 0 }
		, _update_body_count_buffer{}
		, _work_group_size{ 0 }
		, _world_force{ zero_vector, zero_vector }
		, _world_accel{ zero_vector, zero_vector }
		, _num_lag_frames{ 0 }
		, _num_batches{ 0 }
		, _num_iterations{ 0 }
		, _depenetration_weight{ 0.f }
		, _depenetration_bias{ 0.f }
		, _restitution_threshold{ 0.f }
		, _restitution_attenuation{ 0.f }
		, _restitution_offset{ 0.f }
		, _restitution_mode{ collision_mode::arithmetic }
		, _friction_mode{ collision_mode::geometric }
		, _max_substep_delta{ 0.f }
		, _max_substep_count{ 0 }
		, _step_delta{ 0.f }
		, _should_run{ true }
		, _should_step{ false }
		, _should_display_bounding_boxes{ false }
		, _is_real_time{ true }
		, _has_new_data{ false }
	{
	}

	bool sycl_simulator::initialize(void* parameters)
	{
		auto simulator_parameters = static_cast<sycl_simulator_parameters*>(parameters);
		if (!simulator_parameters)
		{
			GAEA_ERROR(sycl_simulator, "Simulator must be initialized with parameters of type '%s'", 
				GAEA_TOKEN_STRINGIFICATION(sycl_simulator_parameters));
			return false;
		}

#if GAEA_DEBUG_SYCL_KERNELS
		auto device_manager = engine->access_device_manager();
		
		device_manager->select_device(_command_device, sycl::info::device_type::host);
#else
		_command_device = simulator_parameters->command_device;
#endif
		
		_command_queue = sycl::queue{
			sycl::device(_command_device),
			[this](sycl::exception_list const& ex_list)
			{
				for (sycl::exception_ptr_class const& ex_ptr : ex_list)
				{
					try
					{
						std::rethrow_exception(ex_ptr);
					}
					catch (sycl::exception const& ex)
					{
						GAEA_WARN(sycl_simulator, "Asynchronous SYCL exception! OpenCL error code %d\nMessage: %s",
							ex.get_cl_code(), ex.what());
					}
				}
			},
			sycl::property_list{
#if GAEA_PROFILE_SYCL_KERNELS
				sycl::property::queue::enable_profiling{}
#endif
			}
		};

		GAEA_INFO(sycl_simulator, "Running on device '%s'", _command_device.name().c_str());

		_max_substep_delta = simulator_parameters->max_substep_delta;
		_max_substep_count = simulator_parameters->max_substep_count;

		_num_lag_frames = simulator_parameters->num_lag_frames;
		if (_num_lag_frames == 0)
		{
			// TODO FIXME if we need to test with zero lag frames,
			// need to fix 'sycl_frame_buffer::previous_buffer' and the query at end of '_simulation_loop'
			//GAEA_WARN(sycl_simulator, "Initializing simulator with no lag frames will incur a performance penalty!");
			
			GAEA_ERROR(sycl_simulator, "Initializing simulator with no lag frames is not allowed!");
			return false;
		}

		_num_batches = simulator_parameters->num_batches;
		_num_iterations = simulator_parameters->num_iterations;

		_depenetration_weight = simulator_parameters->depenetration_weight;
		_depenetration_bias = simulator_parameters->depenetration_bias;
		_restitution_threshold = simulator_parameters->restitution_threshold;
		_restitution_attenuation = simulator_parameters->restitution_attenuation;
		_restitution_offset = ((_restitution_threshold - 1.f) * (_restitution_threshold + 1.f) - _restitution_attenuation + 1.f) / _restitution_threshold;
		
		_restitution_mode = simulator_parameters->restitution_mode;
		_friction_mode = simulator_parameters->friction_mode;

		_events_buffer = sycl_type::sequence_event_buffer<_num_events>{ _num_lag_frames };
		
		GAEA_INFO(sycl_simulator, "Initializing rigid_body host storage and device buffers...");

		// We force the capacity to be square because some of the kernels require a square number of elements to work correctly
		_work_group_size = math::floor_power_two(uint32(_command_device.work_group_size()));

		_overlap_limit = _overlap_default_limit = uint32(std::log2(_work_group_size));

		// Initialize rigid_body host storage
		_rigid_body_data_ptr = std::make_shared<implementation::sycl_rigid_body_data>(_work_group_size);

		// Initialize rigid_body host and device buffers
		_force_host_buffer = sycl_type::ring_buffer<sycl_data::force_data>{ 1, _work_group_size };
		_flags_host_buffer = sycl_type::ring_buffer<sycl_data::flags_data>{ 1, _work_group_size };
		
		_mass_host_buffer = sycl_type::ring_buffer<sycl_data::mass_data>{ 1, _work_group_size };
		_mass_device_buffer = sycl_type::ring_buffer<sycl_data::mass_data>{ 1, _work_group_size };

		_statics_host_buffer = sycl_type::ring_buffer<sycl_data::statics_data>{ 1, _work_group_size };
		_statics_device_buffer = sycl_type::ring_buffer<sycl_data::statics_data>{ 1, _work_group_size };
		
		_geometry_host_buffer = sycl_type::ring_buffer<sycl_data::geometry_data>{ 1, _work_group_size };
		_geometry_device_buffer = sycl_type::ring_buffer<sycl_data::geometry_data>{ 1, _work_group_size };
		
		_geometry_face_host_buffer = sycl_type::ring_buffer<sycl_data::geometry_face_data>{ 1, _work_group_size };
		_geometry_face_device_buffer = sycl_type::ring_buffer<sycl_data::geometry_face_data>{ 1, _work_group_size };
		
		_geometry_vertex_host_buffer = sycl_type::ring_buffer<sycl_data::geometry_vertex_data>{ 1, _work_group_size };
		_geometry_vertex_device_buffer = sycl_type::ring_buffer<sycl_data::geometry_vertex_data>{ 1, _work_group_size };

		_geometry_halfedge_host_buffer = sycl_type::ring_buffer<sycl_data::geometry_halfedge_data>{ 1, _work_group_size };
		_geometry_halfedge_device_buffer = sycl_type::ring_buffer<sycl_data::geometry_halfedge_data>{ 1, _work_group_size };

		_momentum_host_buffer = sycl_type::ring_buffer<sycl_data::momentum_data>{ 1, _work_group_size };
		_momentum_device_buffer = sycl_type::ring_buffer<sycl_data::momentum_data>{ _num_lag_frames + 1, _work_group_size };
		
		_position_host_buffer = sycl_type::ring_buffer<sycl_data::position_data>{ 1, _work_group_size };
		_position_device_buffer = sycl_type::ring_buffer<sycl_data::position_data>{ _num_lag_frames + 1, _work_group_size };

		_dynamics_compute_buffer = sycl_type::ring_buffer<sycl_data::dynamics_data>{ _num_lag_frames + 1, _work_group_size };
		
		_geometry_compute_buffer = sycl_type::ring_buffer<sycl_data::geometry_data>{ _num_lag_frames + 1, _work_group_size };

		_geometry_face_compute_buffer = sycl_type::ring_buffer<sycl_data::geometry_face_data>{ 1, _work_group_size };
		_geometry_vertex_compute_buffer = sycl_type::ring_buffer<sycl_data::geometry_vertex_data>{ 1, _work_group_size };
		
		_bcd_body_compute_buffer = sycl_type::ring_buffer<sycl_data::bcd_body_data>{ 1, _work_group_size };
		_bcd_sort_compute_buffer = sycl_type::ring_buffer<sycl_data::bcd_sort_data>{ 1, _work_group_size };
		_bcd_sweep_compute_buffer = sycl_type::ring_buffer<sycl_data::bcd_sweep_data>{ _num_lag_frames + 1, _work_group_size * _overlap_limit };
		_ncd_overlap_compute_buffer = sycl_type::ring_buffer<sycl_data::ncd_overlap_data>{ _num_lag_frames + 1, _work_group_size * _overlap_limit };
		_cr_constraint_compute_buffer = sycl_type::ring_buffer<sycl_data::cr_constraint_data>{ _num_lag_frames + 1, _work_group_size * _overlap_limit };

		_cd_overlap_count_compute_buffer = sycl_type::ring_buffer<uint32>{ _num_lag_frames + 1, _work_group_size };
		_cr_overlap_count_compute_buffer = sycl_type::ring_buffer<uint32>{ _num_lag_frames + 1, _work_group_size };
		_overlap_limit_compute_buffer = sycl_type::ring_buffer<uint32>{ _num_lag_frames + 1, 2 };
		
		_constraint_count_compute_buffer = sycl_type::ring_buffer<uint32>{ _num_lag_frames + 1, 0 };
		_constraint_index_compute_buffer = sycl_type::ring_buffer<uint32>{ _num_lag_frames + 1, 0 };

		_update_body_count_buffer = implementation::ring_buffer<uint32>{ _num_lag_frames + 1, 0u };

		GAEA_INFO(sycl_simulator, "Creating simulate threads...");
		
		// Currently we only need one simulate thread on the host side
#if 0
		// NOTE: sycl accessors need to be created and destroyed by a single thread!
		std::size_t num_threads = std::max(std::thread::hardware_concurrency() / 2, 1u);
#else
		std::size_t num_threads = 1;
#endif
		_simulate_thread_array.reserve(num_threads);

		for (std::size_t index = 0; index < num_threads; ++index)
		{
			auto& thread = _simulate_thread_array.emplace_back(std::thread{ &sycl_simulator::_simulation_loop, this, index, num_threads });

			if (thread.get_id() == std::thread::id{})
			{
				GAEA_ERROR(sycl_simulator, "Error creating simulate thread!");
				return false;
			}
		}
		
		return true;
	}

	void sycl_simulator::finalize()
	{
		std::size_t num_threads = _simulate_thread_array.size();

		_should_run = false;

		_begin_simulate_semaphore.release(num_threads);
		_end_simulate_semaphore.release(num_threads);

		for (auto& thread : _simulate_thread_array)
		{
			thread.join();
		}
	}

	void sycl_simulator::step(float64 frame_delta)
	{
		int64 num_threads = _simulate_thread_array.size();
		
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Simulator is already stepping!");
			return;
		}
		
		_finished_step_count.store(0, std::memory_order_relaxed);

		_step_delta = float32(frame_delta);
		_should_step = true;
		
		_begin_simulate_semaphore.release(num_threads);
	}

	void sycl_simulator::finish_step()
	{
		int64 num_threads = _simulate_thread_array.size();
		
		if (!_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Simulator is not currently stepping!");
			return;
		}

		while (_finished_step_count.load(std::memory_order_acquire) != num_threads)
		{
			std::this_thread::yield();
		}

		_should_step = false;

		_end_simulate_semaphore.release(num_threads);
	}

	bool sycl_simulator::is_real_time() const
	{
		return _is_real_time;
	}

	void sycl_simulator::register_body(simulation_object type, void* object)
	{
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Trying to register object while stepping!");
			return;
		}
		
		switch (type)
		{
		case simulation_object::rigid_body:
		{
			auto rigid_body = static_cast<sycl_rigid_body*>(object);

			auto it = std::find(_rigid_body_array.begin(), _rigid_body_array.end(), rigid_body);
			if (it == _rigid_body_array.end())
			{
				_rigid_body_array.push_back(rigid_body);

				rigid_body->setup_body_data(_rigid_body_data_ptr);

				++_total_body_count;
				_has_new_data = uint8(sycl_update_data_flags::all_data);
				break;
			}

			GAEA_ERROR(sycl_simulator, "Trying to re-register object!");
			break;
		}
		default: // NOLINT(clang-diagnostic-covered-switch-default)
			GAEA_ERROR(sycl_simulator, "Unrecognized object type!");
			break;
		}
	}

	void sycl_simulator::unregister_body(simulation_object type, void* object)
	{
		// TODO FIXME need to fix sycl_rigid_body::setdown_body
		GAEA_UNUSED(type, object);
		// TODO force full sort when unregistering body
		//_has_new_data = uint8(sycl_update_data_flags::any_data);
		if (engine->is_running())
		{
			GAEA_FATAL(sycl_simulator, "Method is not implemented!");
		}
	}

	void sycl_simulator::apply_world_force(simulation_vector_type const& force)
	{
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Trying to register world force while stepping!");
			return;
		}

		_world_force.linear += vector{ float32(std::get<0>(force)), float32(std::get<1>(force)), float32(std::get<2>(force)) };
	}

	void sycl_simulator::apply_world_torque(simulation_vector_type const& force)
	{
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Trying to register world force while stepping!");
			return;
		}

		_world_force.angular += vector{ float32(std::get<0>(force)), float32(std::get<1>(force)), float32(std::get<2>(force)) };
	}

	void sycl_simulator::apply_world_lin_accel(simulation_vector_type const& accel)
	{
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Trying to register world acceleration while stepping!");
			return;
		}

		_world_accel.linear += vector{ float32(std::get<0>(accel)), float32(std::get<1>(accel)), float32(std::get<2>(accel)) };
	}

	void sycl_simulator::apply_world_ang_accel(simulation_vector_type const& accel)
	{
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Trying to register world acceleration while stepping!");
			return;
		}

		_world_accel.angular += vector{ float32(std::get<0>(accel)), float32(std::get<1>(accel)), float32(std::get<2>(accel)) };
	}

	void sycl_simulator::mark_has_new_data(sycl_update_data_flags data_flag)
	{
		if (_should_step)
		{
			GAEA_ERROR(sycl_simulator, "Trying to mark new data flag while stepping!");
			return;
		}

		_has_new_data |= uint8(data_flag);
	}

	float32 sycl_simulator::query_step_duration() const
	{
		return query_collision_detection_duration() + query_collision_resolution_duration();
	}

	float32 sycl_simulator::query_collision_detection_duration() const
	{
		return query_broad_phase_duration() + query_narrow_phase_duration() + query_contact_generation_duration();
	}

	float32 sycl_simulator::query_broad_phase_duration() const
	{
		auto query_frame = _num_lag_frames - 1;
		
		int64 event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_bp_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
				- _events_buffer.previous_events(query_frame)[_bp_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;

		return stdchr::duration<float32>(stdchr::nanoseconds(event_duration)).count();
	}

	float32 sycl_simulator::query_narrow_phase_duration() const
	{
		auto query_frame = _num_lag_frames - 1;
		
		int64 event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_np_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
				- _events_buffer.previous_events(query_frame)[_np_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;

		return stdchr::duration<float32>(stdchr::nanoseconds(event_duration)).count();
	}

	float32 sycl_simulator::query_collision_resolution_duration() const
	{
		return query_constraint_batching_duration() + query_constraint_solving_duration();
	}

	uint32 sycl_simulator::num_threads() const
	{
		return 1;
	}

	float32 sycl_simulator::query_swap_buffers_duration()
	{
		auto query_frame = _num_lag_frames - 1;

		int64 swap_event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_sb_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
			- _events_buffer.previous_events(query_frame)[_sb_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;

		return stdchr::duration<float32>(stdchr::nanoseconds(swap_event_duration)).count();
	}

	float32 sycl_simulator::query_update_buffers_duration()
	{
		auto query_frame = _num_lag_frames - 1;

		int64 update_event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_ub_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
			- _events_buffer.previous_events(query_frame)[_ub_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;

		return stdchr::duration<float32>(stdchr::nanoseconds(update_event_duration)).count();
	}

	float32 sycl_simulator::query_contact_generation_duration() const
	{
		auto query_frame = _num_lag_frames - 1;

		int64 event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_cg_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
			- _events_buffer.previous_events(query_frame)[_cg_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;

		return stdchr::duration<float32>(stdchr::nanoseconds(event_duration)).count();
	}

	float32 sycl_simulator::query_constraint_batching_duration() const
	{
		auto query_frame = _num_lag_frames - 1;
		
		int64 event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_cb_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
				- _events_buffer.previous_events(query_frame)[_cb_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;
	
		return stdchr::duration<float32>(stdchr::nanoseconds(event_duration)).count();
	}

	float32 sycl_simulator::query_constraint_solving_duration() const
	{
		auto query_frame = _num_lag_frames - 1;
		
		int64 event_duration = _events_buffer.has_previous_events(query_frame)
			? _events_buffer.previous_events(query_frame)[_cs_event_idx].get_profiling_info<sycl::info::event_profiling::command_end>()
				- _events_buffer.previous_events(query_frame)[_cs_event_idx].get_profiling_info<sycl::info::event_profiling::command_start>()
			: -1;
	
		return stdchr::duration<float32>(stdchr::nanoseconds(event_duration)).count();
	}

	void sycl_simulator::toggle_bounding_box_display()
	{
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_should_display_bounding_boxes = !_should_display_bounding_boxes;

		std::for_each(_rigid_body_array.begin(), _rigid_body_array.end(),
			[this](sycl_rigid_body* body)
			{
				if (body && body->is_bounds_frame_visible() != _should_display_bounding_boxes)
				{
					body->toggle_bounds_frame();
				}
			});
#endif
	}

	bool sycl_simulator::is_displaying_bounding_boxes() const
	{
		return _should_display_bounding_boxes;
	}

	void sycl_simulator::_simulation_loop(std::size_t thread_index, std::size_t thread_count)
	{
		GAEA_UNUSED(thread_index, thread_count);
		
		engine->register_thread(thread_type::simulate);

		std::size_t update_frame = _num_lag_frames - 1;

		while (_should_run)
		{
			_begin_simulate_semaphore.acquire();

			if (_should_step)
			{
				// Swap device buffers if we have new body data
				if (_simulate_body_count != _total_body_count)
				{
					// Resize buffers before proceeding
					_resize_data_buffers();
					
					// If we have body data on the device
					if (_simulate_body_count > 0)
					{
						// Submit kernel to copy existing data in the device to the resized buffers
						_events_buffer.current_events()[_sb_event_idx] =
							sycl_kernel::swap_buffers(
								_command_queue, _simulate_body_count,
								_mass_device_buffer.current_buffer(), _mass_device_buffer.next_buffer(),
								_statics_device_buffer.current_buffer(), _statics_device_buffer.next_buffer(),
								_momentum_device_buffer.current_buffer(), _momentum_device_buffer.next_buffer(),
								_position_device_buffer.current_buffer(), _position_device_buffer.next_buffer(),
								_geometry_device_buffer.current_buffer(), _geometry_device_buffer.next_buffer(),
								_geometry_face_device_buffer.current_buffer(), _geometry_face_device_buffer.next_buffer(),
								_geometry_vertex_device_buffer.current_buffer(), _geometry_vertex_device_buffer.next_buffer(),
								_geometry_halfedge_device_buffer.current_buffer(), _geometry_halfedge_device_buffer.next_buffer()
							);
					}

					// Swap old buffers with the new ones that carry the old data
					_swap_data_buffers();
					
					// Re-assign the number of bodies to simulate
					_simulate_body_count = _total_body_count;

					_update_overlap_work_sizes();
				}

				uint32 substep_count = std::min(_max_substep_count, uint32(std::ceil(_step_delta / _max_substep_delta)));
				float32 substep_delta = substep_count > 0 ? _step_delta / float32(substep_count) : 0.f;

				_is_real_time = substep_delta <= _max_substep_delta;
				if (!_is_real_time)
				{
					substep_delta = _max_substep_delta;
				}
				
				// Only proceed if we have bodies and should simulate them
				if (_simulate_body_count > 0 && substep_count > 0)
				{
					{
						std::memcpy(
							_force_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
							_rigid_body_data_ptr->force_data_ptr(),
							_rigid_body_data_ptr->force_data_size()
						);
						std::memcpy(
							_flags_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
							_rigid_body_data_ptr->flags_data_ptr(),
							_rigid_body_data_ptr->flags_data_size()
						);

						std::memset(
							_cd_overlap_count_compute_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
							0x00,
							_cd_overlap_count_compute_buffer.current_buffer().get_size()
						);
						std::memset(
							_cr_overlap_count_compute_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
							0x00,
							_cr_overlap_count_compute_buffer.current_buffer().get_size()
						);
						{
							auto overlap_limit_accessor = _overlap_limit_compute_buffer.current_buffer().get_access<sycl::access::mode::write>();

							overlap_limit_accessor[0] = _overlap_limit;
							overlap_limit_accessor[1] = 0;
						}
						{
							std::memset(
								_constraint_count_compute_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(), 
								0x00, 
								_constraint_count_compute_buffer.current_buffer().get_size()
							);
							std::memset(
								_constraint_index_compute_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(), 
								0xFF, 
								_constraint_index_compute_buffer.current_buffer().get_size()
							);
						}
					}

					// 0. Update body data if new data has been introduced
					//	  Changes to anything other than a force is considered new data
					if (_has_new_data)
					{
						{
							if (_has_new_data & uint8(sycl_update_data_flags::mass_data))
							{
								std::memcpy(
									_mass_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->mass_data_ptr(),
									_rigid_body_data_ptr->mass_data_size()
								);
							}
							if (_has_new_data & uint8(sycl_update_data_flags::statics_data))
							{
								std::memcpy(
									_statics_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->statics_data_ptr(),
									_rigid_body_data_ptr->statics_data_size()
								);
							}

							if (_has_new_data & uint8(sycl_update_data_flags::momentum_data))
							{
								std::memcpy(
									_momentum_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->momentum_data_ptr(),
									_rigid_body_data_ptr->momentum_data_size()
								);
							}
							if (_has_new_data & uint8(sycl_update_data_flags::position_data))
							{
								std::memcpy(
									_position_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->position_data_ptr(),
									_rigid_body_data_ptr->position_data_size()
								);
							}

							if (_has_new_data & uint8(sycl_update_data_flags::geometry_data))
							{
								std::memcpy(
									_geometry_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->geometry_data_ptr(),
									_rigid_body_data_ptr->geometry_data_size()
								);
								std::memcpy(
									_geometry_face_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->geometry_face_data_ptr(),
									_rigid_body_data_ptr->geometry_face_data_size()
								);
								std::memcpy(
									_geometry_vertex_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->geometry_vertex_data_ptr(),
									_rigid_body_data_ptr->geometry_vertex_data_size()
								);
								std::memcpy(
									_geometry_halfedge_host_buffer.current_buffer().get_access<sycl::access::mode::write>().get_pointer(),
									_rigid_body_data_ptr->geometry_halfedge_data_ptr(),
									_rigid_body_data_ptr->geometry_halfedge_data_size()
								);
							}
						}

						_events_buffer.current_events()[_ub_event_idx] =
							sycl_kernel::update_buffers(
								_command_queue, _simulate_body_count,
								_mass_device_buffer.current_buffer(), _mass_host_buffer.current_buffer(),
								_statics_device_buffer.current_buffer(), _statics_host_buffer.current_buffer(),
								_momentum_device_buffer.current_buffer(), _momentum_host_buffer.current_buffer(),
								_position_device_buffer.current_buffer(), _position_host_buffer.current_buffer(),
								_geometry_device_buffer.current_buffer(), _geometry_host_buffer.current_buffer(),
								_geometry_face_device_buffer.current_buffer(), _geometry_face_host_buffer.current_buffer(),
								_geometry_vertex_device_buffer.current_buffer(), _geometry_vertex_host_buffer.current_buffer(),
								_geometry_halfedge_device_buffer.current_buffer(), _geometry_halfedge_host_buffer.current_buffer(),
								_flags_host_buffer.current_buffer()
							);
					}

#if GAEA_SYCL_SORT_SWEEP_USING_PARTIAL_SORT
					// Apply full sort when we have new data (in particular, position or geometry data, as only that should alter the bounds significantly),
					// otherwise, just sort nearby elements (partition bitonic sort)
					bool needs_full_sort = bool(_has_new_data & (uint8(sycl_update_data_flags::position_data) | uint8(sycl_update_data_flags::geometry_data) | uint8(sycl_update_data_flags::any_data)));
#else
					bool needs_full_sort = true;
#endif

					// 1. Broad-phase collision detection

					_events_buffer.current_events()[_bp_event_idx] =
						sycl_kernel::bcd_solver_sort_sweep(
							_command_queue, _simulate_body_count, needs_full_sort,
							_statics_device_buffer.current_buffer(),
							_position_device_buffer.current_buffer(),
							_geometry_device_buffer.current_buffer(),
							_dynamics_compute_buffer.current_buffer(), 
							_geometry_compute_buffer.current_buffer(),
							_bcd_body_compute_buffer.current_buffer(),
							_bcd_sort_compute_buffer.current_buffer(),
							_bcd_sweep_compute_buffer.current_buffer(),
							_cd_overlap_count_compute_buffer.current_buffer(),
							_overlap_limit_compute_buffer.current_buffer(),
							_constraint_grid_side_size
						);

					// 2. Narrow-phase collision detection

					_events_buffer.current_events()[_np_event_idx] =
						sycl_kernel::ncd_solver_separating_axis_test(
							_command_queue, _simulate_body_count,
							_statics_device_buffer.current_buffer(),
							_position_device_buffer.current_buffer(),
							_geometry_device_buffer.current_buffer(),
							_geometry_face_device_buffer.current_buffer(),
							_geometry_vertex_device_buffer.current_buffer(),
							_geometry_halfedge_device_buffer.current_buffer(),
							_dynamics_compute_buffer.current_buffer(),
							_geometry_compute_buffer.current_buffer(),
							_geometry_face_compute_buffer.current_buffer(),
							_geometry_vertex_compute_buffer.current_buffer(),
							_bcd_sweep_compute_buffer.current_buffer(),
							_ncd_overlap_compute_buffer.current_buffer(),
							_cd_overlap_count_compute_buffer.current_buffer(),
							_overlap_limit_compute_buffer.current_buffer()
						);

					_events_buffer.current_events()[_cg_event_idx] =
						sycl_kernel::ncd_solver_contact_generation(
							_command_queue, _simulate_body_count,
							_statics_device_buffer.current_buffer(),
							_dynamics_compute_buffer.current_buffer(),
							_geometry_compute_buffer.current_buffer(),
							_geometry_face_compute_buffer.current_buffer(),
							_geometry_vertex_compute_buffer.current_buffer(),
							_geometry_halfedge_device_buffer.current_buffer(),
							_bcd_sweep_compute_buffer.current_buffer(),
							_ncd_overlap_compute_buffer.current_buffer(),
							_cd_overlap_count_compute_buffer.current_buffer(),
							_overlap_limit_compute_buffer.current_buffer()
						);
					
					// 3. Collision resolution

					_events_buffer.current_events()[_cb_event_idx] =
						sycl_kernel::cr_solver_constraint_batching(
							_command_queue, _simulate_body_count,
							_num_batches, uint8(_restitution_mode), uint8(_friction_mode),
							_statics_device_buffer.current_buffer(),
							_dynamics_compute_buffer.current_buffer(),
							_bcd_sweep_compute_buffer.current_buffer(),
							_ncd_overlap_compute_buffer.current_buffer(),
							_cr_constraint_compute_buffer.current_buffer(),
							_cd_overlap_count_compute_buffer.current_buffer(),
							_cr_overlap_count_compute_buffer.current_buffer(),
							_overlap_limit_compute_buffer.current_buffer(),
							_constraint_count_compute_buffer.current_buffer(),
							_constraint_index_compute_buffer.current_buffer(),
							_num_constraint_work_groups, _num_constraint_work_items_per_group
						);
					_events_buffer.current_events()[_cs_event_idx] =
						sycl_kernel::cr_solver_local_symplectic_euler(
							_command_queue, _simulate_body_count,
							_num_batches, _num_iterations,
							substep_delta, substep_count,
							_depenetration_weight, _depenetration_bias, 
							_restitution_threshold, _restitution_attenuation, _restitution_offset,
							_world_force, _world_accel,
							_force_host_buffer.current_buffer(),
							_mass_device_buffer.current_buffer(),
							_statics_device_buffer.current_buffer(),
							_momentum_device_buffer.current_buffer(), _momentum_device_buffer.next_buffer(),
							_position_device_buffer.current_buffer(), _position_device_buffer.next_buffer(),
							_cr_constraint_compute_buffer.current_buffer(),
							_constraint_count_compute_buffer.current_buffer(),
							_constraint_index_compute_buffer.current_buffer(),
							_num_constraint_work_groups, _num_constraint_work_items_per_group
						);

					uint32 update_overlap_limit = _overlap_default_limit;

					if (_update_body_count_buffer.has_previous_element(update_frame))
					{
						uint32 update_body_count = _update_body_count_buffer.previous_element(update_frame);

						// Copyback multi-body dynamics data
						auto overlap_limit_compute_accessor = _overlap_limit_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();

						// Copyback single-body dynamics data
						{
							std::memcpy(
								_rigid_body_data_ptr->momentum_data_ptr(),
								_momentum_device_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>().get_pointer(),
								update_body_count * sizeof(sycl_data::momentum_data)
							);
							std::memcpy(
								_rigid_body_data_ptr->position_data_ptr(),
								_position_device_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>().get_pointer(),
								update_body_count * sizeof(sycl_data::position_data)
							);
						}

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
						if (_should_display_bounding_boxes)
						{
							auto dynamics_data = _dynamics_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();
							auto geometry_data = _geometry_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();

							for (std::size_t body_index = 0; body_index < update_body_count; ++body_index)
							{
								auto body = _rigid_body_array[body_index];

								if (body && body->get_collision_entity_type() != collision_entity::null)
								{
									body->update_collision_state(geometry_data[body_index].bounds, collision_state(dynamics_data[body_index].collision_state));

									if (body->is_bounds_frame_visible() != _should_display_bounding_boxes)
									{
										body->toggle_bounds_frame();
									}
								}
							}
						}
#endif

#if GAEA_DEBUG_CONTACT_MANIFOLD
						auto position_accessor = _position_device_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();
						auto overlap_count_compute_accessor = _cd_overlap_count_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();

						auto bcd_sweep_accessor = _bcd_sweep_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();
						auto ncd_overlap_accessor = _ncd_overlap_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();

						for (std::size_t body_index = 0; body_index < update_body_count; ++body_index)
						{
							auto const& overlap_count = overlap_count_compute_accessor[body_index];

							for (std::size_t overlap_index = 0; overlap_index < overlap_count; ++overlap_index)
							{
								auto const& sweep_data = bcd_sweep_accessor[body_index * overlap_limit_compute_accessor[0] + overlap_index];
								auto const& overlap_data = ncd_overlap_accessor[body_index * overlap_limit_compute_accessor[0] + overlap_index];

								if (overlap_data.num_contact_points > 0)
								{
									GAEA_INFO(sycl_simulator, "contact manifold: body_1=%llu body_2=%llu", body_index, sweep_data.other_body_index);
									GAEA_INFO(sycl_simulator, "pos=%s", position_accessor[body_index].position.to_string().c_str());
									GAEA_INFO(sycl_simulator, "axis=%s", overlap_data.contact_normal.to_string().c_str());
									GAEA_INFO(sycl_simulator, "depth=%.6f", overlap_data.penetration_depth);
									GAEA_INFO(sycl_simulator, "points:");

									for (std::size_t contact_index = 0; contact_index < overlap_data.num_contact_points; ++contact_index)
									{
										GAEA_INFO(sycl_simulator, "%s", overlap_data.contact_point_array[contact_index].to_string().c_str());
									}
								}
							}
						}
#endif

#if GAEA_DEBUG_CONSTRAINT_BATCHING
						auto cr_constraint_compute_accessor = _cr_constraint_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();
						auto contact_count_compute_accessor = _cr_overlap_count_compute_buffer.previous_buffer(update_frame).get_access<sycl::access::mode::read>();

						for (std::size_t body_index = 0; body_index < update_body_count; ++body_index)
						{
							auto const& contact_count = contact_count_compute_accessor[body_index];

							for (std::size_t contact_index = 0; contact_index < contact_count; ++contact_index)
							{
								auto const& constraint_data = cr_constraint_compute_accessor[body_index * overlap_limit_compute_accessor[0] + contact_index];

								GAEA_INFO(sycl_simulator, "b1:%s b2:%s bi:%u ci:%u cg:%u", 
									_rigid_body_array[body_index]->get_position().to_string().c_str(), _rigid_body_array[constraint_data.body_2_index]->get_position().to_string().c_str(),
									constraint_data.batch_index, constraint_data.collision_index, constraint_data.collision_group);
							}
						}
#endif

						update_overlap_limit = std::max(overlap_limit_compute_accessor[1], _overlap_default_limit);
					}

					if (update_overlap_limit != _overlap_limit)
					{
						// We'll update the overlap limit buffer in the next frame, before submitting any kernels
						_overlap_limit = update_overlap_limit; // math::min(update_overlap_limit, 100u);

						_update_overlap_work_sizes();
						
						_bcd_sweep_compute_buffer.next_buffer() = sycl::buffer<sycl_data::bcd_sweep_data>{
							sycl::range<1>{ _cd_overlap_count_compute_buffer.next_buffer().get_count() * _overlap_limit }
						};
						_ncd_overlap_compute_buffer.next_buffer() = sycl::buffer<sycl_data::ncd_overlap_data>{
							sycl::range<1>{ _cd_overlap_count_compute_buffer.next_buffer().get_count() * _overlap_limit }
						};
						_cr_constraint_compute_buffer.next_buffer() = sycl::buffer<sycl_data::cr_constraint_data>{
							sycl::range<1>{ _cd_overlap_count_compute_buffer.next_buffer().get_count() * _overlap_limit }
						};
					}

					// Advance any buffers intended for updating data
					if (_has_new_data)
					{
						if (_has_new_data & uint8(sycl_update_data_flags::mass_data))
						{
							_mass_host_buffer.advance_buffer(true);
						}
						if (_has_new_data & uint8(sycl_update_data_flags::statics_data))
						{
							_statics_host_buffer.advance_buffer(true);
						}
						if (_has_new_data & uint8(sycl_update_data_flags::momentum_data))
						{
							_momentum_host_buffer.advance_buffer(true);
						}
						if (_has_new_data & uint8(sycl_update_data_flags::position_data))
						{
							_position_host_buffer.advance_buffer(true);
						}

						if (_has_new_data & uint8(sycl_update_data_flags::geometry_data))
						{
							_geometry_host_buffer.advance_buffer(true);
							_geometry_face_host_buffer.advance_buffer(true);
							_geometry_vertex_host_buffer.advance_buffer(true);
							_geometry_halfedge_host_buffer.advance_buffer(true);
						}

						_has_new_data = uint8(sycl_update_data_flags::no_data);
					}

					// Advance constantly updated buffers
					{
						_force_host_buffer.advance_buffer(true);
						_flags_host_buffer.advance_buffer(true);

						_momentum_device_buffer.advance_buffer(true);
						_position_device_buffer.advance_buffer(true);

						_dynamics_compute_buffer.advance_buffer(true);
						_geometry_compute_buffer.advance_buffer(true);
						
						_bcd_sweep_compute_buffer.advance_buffer(true);
						_ncd_overlap_compute_buffer.advance_buffer(true);
						_cr_constraint_compute_buffer.advance_buffer(true);

						_cd_overlap_count_compute_buffer.advance_buffer(true);
						_cr_overlap_count_compute_buffer.advance_buffer(true);
						_overlap_limit_compute_buffer.advance_buffer(true);

						_constraint_count_compute_buffer.advance_buffer(true);
						_constraint_index_compute_buffer.advance_buffer(true);

						_update_body_count_buffer.advance_element(true);
					}
				}

				_rigid_body_data_ptr->reset_force_data();
				_rigid_body_data_ptr->reset_flags_data();
				
				_events_buffer.advance_events();
				
				_finished_step_count.fetch_add(1, std::memory_order_release);
			}

			_end_simulate_semaphore.acquire();
		}

		try
		{
			_command_queue.wait_and_throw();
		}
		catch (sycl::exception const& ex)
		{
			GAEA_WARN(sycl_simulator, "Synchronous SYCL exception! OpenCL error code %d\nMessage: %s",
				ex.get_cl_code(), ex.what());
		}
	}

	void sycl_simulator::_resize_data_buffers()
	{
		// Let's use one of the buffers to determine if we have enough capacity for this body
		// If the buffer is at capacity, resize all buffers before copying the data
		std::size_t old_capacity = _cd_overlap_count_compute_buffer.next_buffer().get_count();
		if (old_capacity < _total_body_count)
		{
			std::size_t new_capacity = old_capacity << 1;
			while (new_capacity < _total_body_count)
			{
				new_capacity <<= 1;
			}

			// Double the default overlap limit, use the maximum between the default and the last frame
			_overlap_default_limit <<= 1;
			_overlap_limit = std::max(_overlap_limit, _overlap_default_limit);
			
			_force_host_buffer.next_buffer() = sycl::buffer<sycl_data::force_data>{ new_capacity };
			_flags_host_buffer.next_buffer() = sycl::buffer<sycl_data::flags_data>{ new_capacity };

			_mass_host_buffer.next_buffer() = sycl::buffer<sycl_data::mass_data>{ new_capacity };
			_mass_device_buffer.next_buffer() = sycl::buffer<sycl_data::mass_data>{ new_capacity };

			_statics_host_buffer.next_buffer() = sycl::buffer<sycl_data::statics_data>{ new_capacity };
			_statics_device_buffer.next_buffer() = sycl::buffer<sycl_data::statics_data>{ new_capacity };

			_momentum_host_buffer.next_buffer() = sycl::buffer<sycl_data::momentum_data>{ new_capacity };
			_momentum_device_buffer.next_buffer() = sycl::buffer<sycl_data::momentum_data>{ new_capacity };
			
			_position_host_buffer.next_buffer() = sycl::buffer<sycl_data::position_data>{ new_capacity };
			_position_device_buffer.next_buffer() = sycl::buffer<sycl_data::position_data>{ new_capacity };

			_geometry_host_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_data>{ new_capacity };
			_geometry_device_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_data>{ new_capacity };
			
			_dynamics_compute_buffer.next_buffer() = sycl::buffer<sycl_data::dynamics_data>{ new_capacity };
			
			_geometry_compute_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_data>{ new_capacity };
			
			_bcd_body_compute_buffer.next_buffer() = sycl::buffer<sycl_data::bcd_body_data>{ new_capacity };
			_bcd_sort_compute_buffer.next_buffer() = sycl::buffer<sycl_data::bcd_sort_data>{ new_capacity };
			_bcd_sweep_compute_buffer.next_buffer() = sycl::buffer<sycl_data::bcd_sweep_data>{ new_capacity * _overlap_limit };
			_ncd_overlap_compute_buffer.next_buffer() = sycl::buffer<sycl_data::ncd_overlap_data>{ new_capacity * _overlap_limit };
			_cr_constraint_compute_buffer.next_buffer() = sycl::buffer<sycl_data::cr_constraint_data>{ new_capacity * _overlap_limit };

			_cd_overlap_count_compute_buffer.next_buffer() = sycl::buffer<uint32>{ new_capacity };
			_cr_overlap_count_compute_buffer.next_buffer() = sycl::buffer<uint32>{ new_capacity };
		}

		// Always resize geometry poly buffers, as these do not allocate any extra capacity beforehand
		_geometry_face_host_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_face_data>{ _rigid_body_data_ptr->geometry_face_data_size() };
		_geometry_vertex_host_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_vertex_data>{ _rigid_body_data_ptr->geometry_vertex_data_size() };
		_geometry_halfedge_host_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_halfedge_data>{ _rigid_body_data_ptr->geometry_halfedge_data_size() };

		_geometry_face_device_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_face_data>{ _rigid_body_data_ptr->geometry_face_data_size() };
		_geometry_vertex_device_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_vertex_data>{ _rigid_body_data_ptr->geometry_vertex_data_size() };
		_geometry_halfedge_device_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_halfedge_data>{ _rigid_body_data_ptr->geometry_halfedge_data_size() };
		
		_geometry_face_compute_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_face_data>{ _rigid_body_data_ptr->geometry_face_data_size() };
		_geometry_vertex_compute_buffer.next_buffer() = sycl::buffer<sycl_data::geometry_vertex_data>{ _rigid_body_data_ptr->geometry_vertex_data_size() };

		// Always adjust _update_body_count
		_update_body_count_buffer.next_element() = _total_body_count;
	}
	
	void sycl_simulator::_swap_data_buffers()
	{
		_force_host_buffer.advance_buffer(false);
		_flags_host_buffer.advance_buffer(false);

		_mass_host_buffer.advance_buffer(false);
		_mass_device_buffer.advance_buffer(false);
		
		_statics_host_buffer.advance_buffer(false);
		_statics_device_buffer.advance_buffer(false);

		_momentum_host_buffer.advance_buffer(false);
		_momentum_device_buffer.advance_buffer(false);

		_position_host_buffer.advance_buffer(false);
		_position_device_buffer.advance_buffer(false);

		_geometry_host_buffer.advance_buffer(false);
		_geometry_device_buffer.advance_buffer(false);

		_geometry_face_host_buffer.advance_buffer(false);
		_geometry_face_device_buffer.advance_buffer(false);

		_geometry_vertex_host_buffer.advance_buffer(false);
		_geometry_vertex_device_buffer.advance_buffer(false);

		_geometry_halfedge_host_buffer.advance_buffer(false);
		_geometry_halfedge_device_buffer.advance_buffer(false);

		_dynamics_compute_buffer.advance_buffer(false);
		
		_geometry_compute_buffer.advance_buffer(false);
		
		_geometry_face_compute_buffer.advance_buffer(false);
		_geometry_vertex_compute_buffer.advance_buffer(false);

		_bcd_body_compute_buffer.advance_buffer(false);
		_bcd_sort_compute_buffer.advance_buffer(false);
		_bcd_sweep_compute_buffer.advance_buffer(false);
		_ncd_overlap_compute_buffer.advance_buffer(false);
		_cr_constraint_compute_buffer.advance_buffer(false);

		_cd_overlap_count_compute_buffer.advance_buffer(false);
		_cr_overlap_count_compute_buffer.advance_buffer(false);
		
		_update_body_count_buffer.advance_element(false);
	}

	void sycl_simulator::_update_overlap_work_sizes()
	{
		float32 total_overlap_limit = float32(_overlap_limit * _simulate_body_count);

		_num_constraint_work_groups = uint32(std::ceil(total_overlap_limit / (_work_group_size * sycl_kernel::num_constraint_partitions)));
		_constraint_grid_side_size = uint32(std::ceil(std::pow(_num_constraint_work_groups, 1.f / sycl_kernel::num_partition_dimensions)));

		_num_constraint_work_groups = uint32(std::pow(_constraint_grid_side_size, sycl_kernel::num_partition_dimensions));
		_num_constraint_work_items_per_group = math::ceil_power_two(uint32(std::ceil(total_overlap_limit / (_num_constraint_work_groups * sycl_kernel::num_constraint_partitions))));

		// Adjust overlap work-groups and work-items per work-group

		_constraint_count_compute_buffer.next_buffer() = sycl::buffer<uint32>{ sycl::range<1>{ _num_constraint_work_groups * sycl_kernel::num_constraint_partitions } };
		_constraint_index_compute_buffer.next_buffer() = sycl::buffer<uint32>{ sycl::range<1>{ _num_constraint_work_groups * _num_constraint_work_items_per_group * sycl_kernel::num_constraint_partitions } };
		
		_constraint_count_compute_buffer.advance_buffer(false);
		_constraint_index_compute_buffer.advance_buffer(false);

		if (_num_constraint_work_items_per_group > _work_group_size)
		{
			GAEA_FATAL(sycl_simulator, "Logic error: overlap work-items per work-group cannot exceed work-group size");
		}
	}

}

#endif
