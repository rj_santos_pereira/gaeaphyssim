#ifndef ENGINE_PHYSICS_NATIVE_RIGID_BODY_INCLUDE_GUARD
#define ENGINE_PHYSICS_NATIVE_RIGID_BODY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/engine_config.hpp"
#include "engine/graphics/gl_box_frame.hpp"
#include "engine/physics/physics_globals.hpp"
#include "library/mutex.hpp"
#include "library/math/bounding_box.hpp"
#include "library/math/matrix.hpp"
#include "library/math/polyhedron.hpp"
#include "library/math/quaternion.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	class native_body_pair_id
	{
	public:
		native_body_pair_id(class native_rigid_body* body_1, class native_rigid_body* body_2);

		bool operator==(native_body_pair_id const& other) const;

	private:
		uint64 _body_1_id = 0;
		uint64 _body_2_id = 0;
	};

	struct native_body_pair_hash
	{
		std::size_t operator()(native_body_pair_id const& data) const noexcept;
	};

	class native_rigid_body final : public base_rigid_body
	{
		struct body_partition
		{
			uint8 group;
			uint32 index;
		};
		
	public:
		explicit native_rigid_body(float32 mass, matrix3 inertia_tensor, vector position, quaternion rotation, 
								   vector scaling = one_vector, polyhedron geometry = polyhedron{}, 
								   float32 restitution_coefficient = 0.5f,
								   float32 friction_coefficient = 0.5f,
								   collision_entity::type entity_type = collision_entity::world_dynamic,
								   collision_behavior::type behavior_type = collision_behavior::world_dynamic, 
								   body_mobility mobility = body_mobility::movable);

		virtual void setup_body() override;

		virtual void setdown_body() override;

		// Modifiers

		virtual void apply_world_force(vector const& force) override;
		virtual void apply_body_force(vector const& force) override;

		virtual void apply_world_torque(vector const& torque) override;
		virtual void apply_body_torque(vector const& torque) override;

		// Both force and point in world-space
		void apply_world_force_at_point(vector const& force, vector const& point);
		// Both force and point in body-space
		void apply_body_force_at_point(vector const& force, vector const& point);

		virtual void clear_force_and_torque() override;

		// Computing accessors

		virtual vector query_world_linear_acceleration() const override;
		virtual vector query_body_linear_acceleration() const override;

		virtual vector query_world_angular_acceleration() const override;
		virtual vector query_body_angular_acceleration() const override;

		virtual vector query_world_linear_velocity() const override;
		virtual vector query_body_linear_velocity() const override;

		virtual vector query_world_angular_velocity() const override;
		virtual vector query_body_angular_velocity() const override;

		// Mutating accessors

		virtual vector& access_force() override;
		virtual vector& access_torque() override;

		virtual vector& access_linear_momentum() override;
		virtual vector& access_angular_momentum() override;

		virtual vector& access_position() override;
		virtual quaternion& access_rotation() override;
		
		virtual void set_force(vector const& force) override;
		virtual void set_torque(vector const& torque) override;

		virtual void set_linear_momentum(vector const& linear_momentum) override;
		virtual void set_angular_momentum(vector const& angular_momentum) override;

		virtual void set_position(vector const& position) override;
		virtual void set_rotation(quaternion const& rotation) override;

		virtual void set_mass(float32 mass) override;
		virtual void set_inertia(matrix3 const& inertia_tensor) override;

		// Non-mutating accessors

		virtual vector const& get_force() const override;
		virtual vector const& get_torque() const override;

		virtual vector const& get_linear_momentum() const override;
		virtual vector const& get_angular_momentum() const override;

		virtual vector const& get_position() const override;
		virtual quaternion const& get_rotation() const override;

		virtual float32 const& get_mass() const override;
		virtual float32 const& get_mass_inverse() const override;
		
		virtual matrix3 const& get_inertia() const override;
		virtual matrix3 const& get_inertia_inverse() const override;

		// Geometry accessors

		virtual void set_scaling(vector const& scaling) override;
		
		virtual void set_geometry(polyhedron geometry) override;

		virtual vector const& get_scaling() const override;

		virtual polyhedron get_geometry() const override;
		
		polyhedron const& get_bodyspace_geometry() const;
		polyhedron const& get_worldspace_geometry() const;

		cubic_axis_aligned_bounding_box const& get_bodyspace_bounds() const;
		cubic_axis_aligned_bounding_box const& get_worldspace_bounds() const;

		matrix3 const& get_oriented_inertia_tensor() const;
		matrix3 const& get_oriented_inertia_inverse_tensor() const;

		// Caching methods
		
		void precalc_geometry();
		void precalc_bounds();
		void precalc_tensors();

		void clear_precalc();

		// Constants setters/getters

		virtual void set_restitution_coefficient(float32 restitution_coefficient) override;
		virtual void set_friction_coefficient(float32 friction_coefficient) override;

		virtual void set_collision_entity_type(collision_entity::type entity_type) override;
		virtual void set_collision_behavior_type(collision_behavior::type behavior_type) override;
		
		virtual void set_mobility(body_mobility mobility) override;

		void set_body_partition(uint8 group, uint32 index);

		virtual float32 get_restitution_coefficient() const override;
		virtual float32 get_friction_coefficient() const override;
		
		virtual collision_entity::type get_collision_entity_type() const override;
		virtual collision_behavior::type get_collision_behavior_type() const override;

		virtual body_mobility get_mobility() const override;

		body_partition get_body_partition() const;

		// Synchronization

		std::unique_lock<spin_mutex> lock_body();
		std::unique_lock<spin_mutex> try_lock_body();
		std::unique_lock<spin_mutex> make_lock_body();
		
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		void update_collision_state(collision_state state);

		void toggle_bounds_frame();

		bool is_bounds_frame_visible() const;
#endif
		
	private:
		void _recalc_geometry_and_bounds(vector const& ratio_scaling);

		// Variables
		
		vector _force;
		vector _torque;

		vector _linear_momentum;
		vector _angular_momentum;

		vector _position;
		quaternion _rotation;

		vector _scaling;
		
		float32 _mass;
		float32 _mass_inverse;
		
		matrix3 _inertia;
		matrix3 _inertia_inverse;
		
		matrix3 _cached_oriented_inertia;
		matrix3 _cached_oriented_inertia_inverse;

		// Geometry

		polyhedron _bodyspace_geometry;
		polyhedron _cached_worldspace_geometry;

		cubic_axis_aligned_bounding_box _bodyspace_bounds;
		cubic_axis_aligned_bounding_box _cached_worldspace_bounds;

		// Synchronization

		spin_mutex _body_mutex;

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		gl_box_frame _debug_bounds_frame;
#endif
		
		// Constants

		float32 _restitution_coefficient;
		float32 _friction_coefficient;

		collision_behavior::type _behavior_type;
		collision_entity::type _entity_type;

		uint32 _partition_index;
		uint8 _partition_group : 3;

		// Flags
		
		uint8 _mobility : 1;
		uint8 _precalc_cache : 3;

	};
}

#endif
