#include "engine/physics/native_rigid_body.hpp"

#include "engine/physics/native_simulator.hpp"
#include "library/math/generic_math.hpp"

namespace
{
	constexpr uint8 invalid_cache_flag = 0x07;
	constexpr uint8 invalid_geometry_cache_flag = 0x01;
	constexpr uint8 invalid_bounds_cache_flag = 0x02;
	constexpr uint8 invalid_tensors_cache_flag = 0x04;
}

namespace gaea
{
	native_body_pair_id::native_body_pair_id(native_rigid_body* body_1, native_rigid_body* body_2)
	{
		auto [body_1_id, body_2_id]
			= math::calc_min_max(reinterpret_cast<uint64>(body_1), reinterpret_cast<uint64>(body_2));

		_body_1_id = body_1_id;
		_body_2_id = body_2_id;
	}

	bool native_body_pair_id::operator==(native_body_pair_id const& other) const
	{
		return _body_1_id == other._body_1_id && _body_2_id == other._body_2_id;
	}

	std::size_t native_body_pair_hash::operator()(native_body_pair_id const& data) const noexcept
	{
		return math::hash_bytes(&data, sizeof data);
	}

	native_rigid_body::native_rigid_body(float32 mass, matrix3 inertia_tensor, 
	                                     vector position, quaternion rotation,
	                                     vector scaling, polyhedron geometry, 
	                                     float32 restitution_coefficient,
	                                     float32 friction_coefficient,
	                                     collision_entity::type entity_type,
	                                     collision_behavior::type behavior_type, 
	                                     body_mobility mobility)
		: _force{ zero_vector }
		, _torque{ zero_vector }
		, _linear_momentum{ zero_vector }
		, _angular_momentum{ zero_vector }
		, _position{ position }
		, _rotation{ rotation }
		, _scaling{ scaling }
		, _mass{ mass }
		, _mass_inverse{ 1.f / mass }
		, _inertia{ inertia_tensor }
		, _inertia_inverse{ inertia_tensor.inverse() }
		, _bodyspace_geometry{ geometry }
		, _cached_worldspace_geometry{}
		, _bodyspace_bounds{}
		, _cached_worldspace_bounds{}
		, _body_mutex{}
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		, _debug_bounds_frame{}
#endif
		, _restitution_coefficient{ restitution_coefficient }
		, _friction_coefficient{ friction_coefficient }
		, _behavior_type{ behavior_type }
		, _entity_type{ entity_type }
		, _partition_index{ uint32(-1) }
		, _partition_group{ 0 }
		, _mobility{ uint8(mobility) }
		, _precalc_cache{ invalid_cache_flag }
	{
		_recalc_geometry_and_bounds(_scaling);
	}

	void native_rigid_body::setup_body()
	{
		if (!simulator)
		{
			GAEA_FATAL(native_rigid_body, "Trying to setup body but global simulator accessor is not initialized!");
		}

		precalc_geometry();
		precalc_bounds();
		precalc_tensors();
		
		simulator->register_body(simulation_object::rigid_body, this);

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_debug_bounds_frame.setup_commands();
#endif
	}

	void native_rigid_body::setdown_body()
	{
		if (!simulator)
		{
			GAEA_FATAL(native_rigid_body, "Trying to setdown body but global simulator accessor is not initialized!");
		}

		simulator->unregister_body(simulation_object::rigid_body, this);

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_debug_bounds_frame.setdown_commands();
#endif
	}

	void native_rigid_body::apply_world_force(vector const& force)
	{
		_force += force;
	}

	void native_rigid_body::apply_body_force(vector const& force)
	{
		_force += _rotation * force;
	}

	void native_rigid_body::apply_world_torque(vector const& torque)
	{
		_torque += torque;
	}

	void native_rigid_body::apply_body_torque(vector const& torque)
	{
		_torque += _rotation * torque;
	}

	void native_rigid_body::apply_world_force_at_point(vector const& force, vector const& point)
	{
		_force += force;
		_torque += vector::cross(point - _position, force);
	}

	void native_rigid_body::apply_body_force_at_point(vector const& force, vector const& point)
	{
		auto world_force = _rotation * force;
		_force += world_force;
		_torque += vector::cross(point, world_force);
	}

	void native_rigid_body::clear_force_and_torque()
	{
		_force = zero_vector;
		_torque = zero_vector;
	}

	vector native_rigid_body::query_world_linear_acceleration() const
	{
		return _mobility ? _mass_inverse * _force : zero_vector;
	}

	vector native_rigid_body::query_body_linear_acceleration() const
	{
		return _mobility ? _mass_inverse * (_rotation.reciprocal() * _force) : zero_vector;
	}

	vector native_rigid_body::query_world_angular_acceleration() const
	{
		return _mobility ? _cached_oriented_inertia_inverse * _torque : zero_vector;
	}

	vector native_rigid_body::query_body_angular_acceleration() const
	{
		return _mobility ? _inertia_inverse * (_rotation.reciprocal() * _torque) : zero_vector;
	}

	vector native_rigid_body::query_world_linear_velocity() const
	{
		return _mobility ? _mass_inverse * _linear_momentum : zero_vector;
	}

	vector native_rigid_body::query_body_linear_velocity() const
	{
		return _mobility ? _mass_inverse * (_rotation.reciprocal() * _linear_momentum) : zero_vector;
	}

	vector native_rigid_body::query_world_angular_velocity() const
	{
		return _mobility ? _cached_oriented_inertia_inverse * _angular_momentum : zero_vector;
	}

	vector native_rigid_body::query_body_angular_velocity() const
	{
		return _mobility ? _inertia_inverse * (_rotation.reciprocal() * _angular_momentum) : zero_vector;
	}

    vector& native_rigid_body::access_force()
	{
		return _force;
	}

	vector& native_rigid_body::access_torque()
	{
		return _torque;
	}

	vector& native_rigid_body::access_linear_momentum()
	{
		return _linear_momentum;
	}

	vector& native_rigid_body::access_angular_momentum()
	{
		return _angular_momentum;
	}

	vector& native_rigid_body::access_position()
	{
		//_precalc_cache |= invalid_geometry_cache_flag | invalid_bounds_cache_flag;
		return _position;
	}

	quaternion& native_rigid_body::access_rotation()
	{
		//_precalc_cache |= invalid_geometry_cache_flag | invalid_tensors_cache_flag;
		return _rotation;
	}

	void native_rigid_body::set_force(vector const& force)
	{
		_force = force;
	}

	void native_rigid_body::set_torque(vector const& torque)
	{
		_torque = torque;
	}

	void native_rigid_body::set_linear_momentum(vector const& linear_momentum)
	{
		_linear_momentum = linear_momentum;
	}

	void native_rigid_body::set_angular_momentum(vector const& angular_momentum)
	{
		_angular_momentum = angular_momentum;
	}

	void native_rigid_body::set_position(vector const& position)
	{
		_precalc_cache |= invalid_geometry_cache_flag | invalid_bounds_cache_flag;
		_position = position;
	}

	void native_rigid_body::set_rotation(quaternion const& rotation)
	{
		_precalc_cache |= invalid_geometry_cache_flag | invalid_tensors_cache_flag;
		_rotation = rotation;
	}

	void native_rigid_body::set_mass(float32 mass)
	{
		_precalc_cache |= invalid_tensors_cache_flag;
		_mass = mass;
		_mass_inverse = 1.f / mass;
	}

	void native_rigid_body::set_inertia(matrix3 const& inertia_tensor)
	{
		_precalc_cache |= invalid_tensors_cache_flag;
		_inertia = inertia_tensor;
		_inertia_inverse = inertia_tensor.inverse();
	}

	vector const& native_rigid_body::get_force() const
	{
		return _force;
	}

	vector const& native_rigid_body::get_torque() const
	{
		return _torque;
	}

	vector const& native_rigid_body::get_linear_momentum() const
	{
		return _linear_momentum;
	}

	vector const& native_rigid_body::get_angular_momentum() const
	{
		return _angular_momentum;
	}

	vector const& native_rigid_body::get_position() const
	{
		return _position;
	}

	quaternion const& native_rigid_body::get_rotation() const
	{
		return _rotation;
	}

	float32 const& native_rigid_body::get_mass() const
	{
		return _mass;
	}

	float32 const& native_rigid_body::get_mass_inverse() const
	{
		return _mass_inverse;
	}

	matrix3 const& native_rigid_body::get_inertia() const
	{
		return _inertia;
	}

	matrix3 const& native_rigid_body::get_inertia_inverse() const
	{
		return _inertia_inverse;
	}

	void native_rigid_body::set_scaling(vector const& scaling)
	{
		vector ratio_scaling = scaling / _scaling;
		
		_scaling = scaling;

		_recalc_geometry_and_bounds(ratio_scaling);
	}

	void native_rigid_body::set_geometry(polyhedron geometry)
	{
		_bodyspace_geometry = std::move(geometry);

		_recalc_geometry_and_bounds(_scaling);
	}

	vector const& native_rigid_body::get_scaling() const
	{
		return _scaling;
	}

	polyhedron native_rigid_body::get_geometry() const
	{
		return get_bodyspace_geometry();
	}

	polyhedron const& native_rigid_body::get_bodyspace_geometry() const
	{
		return _bodyspace_geometry;
	}

	polyhedron const& native_rigid_body::get_worldspace_geometry() const
	{
		return _cached_worldspace_geometry;
	}

	cubic_axis_aligned_bounding_box const& native_rigid_body::get_bodyspace_bounds() const
	{
		return _bodyspace_bounds;
	}

	cubic_axis_aligned_bounding_box const& native_rigid_body::get_worldspace_bounds() const
	{
		return _cached_worldspace_bounds;
	}

	matrix3 const& native_rigid_body::get_oriented_inertia_tensor() const
	{
		return _cached_oriented_inertia;
	}

	matrix3 const& native_rigid_body::get_oriented_inertia_inverse_tensor() const
	{
		return _cached_oriented_inertia_inverse;
	}

	void native_rigid_body::precalc_geometry()
	{
		if (_precalc_cache & invalid_geometry_cache_flag)
		{
			_cached_worldspace_geometry = _bodyspace_geometry.move_by(_position, _rotation);
			
			_precalc_cache &= ~invalid_geometry_cache_flag;
		}
	}

	void native_rigid_body::precalc_bounds()
	{
		if (_precalc_cache & invalid_bounds_cache_flag)
		{
			_cached_worldspace_bounds = _bodyspace_bounds.move_by(_position, _rotation);
			
			_precalc_cache &= ~invalid_bounds_cache_flag;
		}

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
		_debug_bounds_frame.set_transform(transform{ _position, identity_quaternion, _cached_worldspace_bounds.extent() });
#endif
	}

	void native_rigid_body::precalc_tensors()
	{
		if (_precalc_cache & invalid_tensors_cache_flag)
		{
			auto rot_mat = _rotation.to_matrix();
			auto tp_rot_mat = rot_mat.transpose();

			_cached_oriented_inertia = rot_mat * _inertia * tp_rot_mat;
			_cached_oriented_inertia_inverse = rot_mat * _inertia_inverse * tp_rot_mat;

			_precalc_cache &= ~invalid_tensors_cache_flag;
		}
	}

	void native_rigid_body::clear_precalc()
	{
		_precalc_cache = invalid_cache_flag;
	}

	void native_rigid_body::set_restitution_coefficient(float32 restitution_coefficient)
	{
		_restitution_coefficient = restitution_coefficient;
	}

	void native_rigid_body::set_friction_coefficient(float32 friction_coefficient)
	{
		_friction_coefficient = friction_coefficient;
	}

	void native_rigid_body::set_collision_entity_type(collision_entity::type entity_type)
	{
		_entity_type = entity_type;
	}

	void native_rigid_body::set_collision_behavior_type(collision_behavior::type behavior_type)
	{
		_behavior_type = behavior_type;
	}

	void native_rigid_body::set_mobility(body_mobility mobility)
	{
		_mobility = uint8(mobility);
	}

	void native_rigid_body::set_body_partition(uint8 group, uint32 index)
	{
		_partition_index = index;
		_partition_group = group;
	}

	float32 native_rigid_body::get_restitution_coefficient() const
	{
		return _restitution_coefficient;
	}

	float32 native_rigid_body::get_friction_coefficient() const
	{
		return _friction_coefficient;
	}

	collision_entity::type native_rigid_body::get_collision_entity_type() const
	{
		return _entity_type;
	}

	collision_behavior::type native_rigid_body::get_collision_behavior_type() const
	{
		return _behavior_type;
	}

	body_mobility native_rigid_body::get_mobility() const
	{
		return body_mobility(_mobility);
	}

	native_rigid_body::body_partition native_rigid_body::get_body_partition() const
	{
		return { _partition_group, _partition_index };
	}

	std::unique_lock<spin_mutex> native_rigid_body::lock_body()
	{
		return std::unique_lock<spin_mutex>{ _body_mutex };
	}

	std::unique_lock<spin_mutex> native_rigid_body::try_lock_body()
	{
		return std::unique_lock<spin_mutex>{ _body_mutex, std::try_to_lock };
	}

	std::unique_lock<spin_mutex> native_rigid_body::make_lock_body()
	{
		return std::unique_lock<spin_mutex>{ _body_mutex, std::defer_lock };
	}

#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
	void native_rigid_body::update_collision_state(collision_state state)
	{
		basic_color color = basic_color::white;
		switch (state)
		{
		case collision_state::none:
			color = basic_color::green;
			break;
		case collision_state::bounds:
			color = basic_color::yellow;
			break;
		case collision_state::geometry:
			color = basic_color::red;
			break;
		}

		_debug_bounds_frame.set_color(color);
	}

	void native_rigid_body::toggle_bounds_frame()
	{
		auto visibility = uint8(_debug_bounds_frame.get_visibility());
		_debug_bounds_frame.set_visibility(body_visibility(!visibility));
	}

	bool native_rigid_body::is_bounds_frame_visible() const
	{
		return _debug_bounds_frame.get_visibility() == body_visibility::visible;
	}
#endif

	void native_rigid_body::_recalc_geometry_and_bounds(vector const& ratio_scaling)
	{
		auto inverse_scaling = 1.f / ratio_scaling;
		
		for (std::size_t index = 0; index < _bodyspace_geometry.face_count(); ++index)
		{
			auto& normal = _bodyspace_geometry.query_face(index).mutable_normal();
			(normal *= inverse_scaling).normalize();
		}

		cubic_axis_aligned_bounding_box bounds;
		
		for (std::size_t index = 0; index < _bodyspace_geometry.vertex_count(); ++index)
		{
			auto& point = _bodyspace_geometry.query_vertex(index).mutable_point();
			point *= ratio_scaling;
			
			bounds.add_point(point);
		}
		
		_bodyspace_bounds = bounds;
		
		_precalc_cache |= invalid_geometry_cache_flag | invalid_bounds_cache_flag;
	}
}
