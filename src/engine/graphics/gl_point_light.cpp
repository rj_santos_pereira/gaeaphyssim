#include "engine/graphics/gl_point_light.hpp"

#include "core/core_opengl.hpp"

#include "engine/graphics/glfw_renderer.hpp"
#include "library/containers.hpp"
#include "library/utility.hpp"
#include "library/math/transform.hpp"

namespace gaea
{
	gl_point_light::gl_point_light(svector const& position, basic_color color, body_visibility visibility)
		: _position{ position }
		, _ambient_factor{ 0.1f }
		, _diffuse_factor{ 1.0f }
		, _specular_factor{ 0.5f }
		, _shader_path{}
		, _shader_program{ nullptr }
		, _draw_command_uid{ 0 }
		, _vao{ 0 }
		, _vbo{ 0, 0 }
		, _color{ uint8(color) }
		, _visibility{ uint8(visibility) }
		, _is_setup{ false }
	{
	}

	void gl_point_light::setup_shader_path(std::string const& shader_path)
	{
		_shader_path = shader_path;
	}

	void gl_point_light::setup_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_static_mesh_body_data, "Trying to reset body but global renderer accessor is not initialized!");
		}

		if (!_is_setup)
		{
			renderer->enqueue_render_command(
				[this](void*)
				{
					_shader_program = new gl_shader_program;
					_shader_program->load_shaders(_shader_path);

					if (!_shader_program->compile_shaders())
					{
						GAEA_WARN(gl_point_light, "Error loading shader program [ID: %u]", _shader_program->get_id());
					}

					glGenVertexArrays(1, &_vao);
					glGenBuffers(2, _vbo);

					glBindVertexArray(_vao);

					fixed_array<float32, 18> vertex_array = {
						+0.1f, 0.0f, 0.0f,
						0.0f, +0.1f, 0.0f,
						-0.1f, 0.0f, 0.0f,
						0.0f, -0.1f, 0.0f,
						0.0f, 0.0f, +0.1f,
						0.0f, 0.0f, -0.1f,
					};

					// Setup vertex array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * vertex_array.size(), vertex_array.data(), GL_STATIC_DRAW);

					glEnableVertexAttribArray(0);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float32), reinterpret_cast<void*>(0 * sizeof(float32)));

					fixed_array<uint32, 24> index_array = {
						0, 1, 4,
						1, 0, 5,
						1, 2, 4,
						2, 1, 5,
						2, 3, 4,
						3, 2, 5,
						3, 0, 4,
						0, 3, 5,
					};
					
					// Setup index array in OpenGL buffer
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32) * index_array.size(), index_array.data(), GL_STATIC_DRAW);

					glBindVertexArray(0);
				},
				render_priority::high,
				false
			);

			_draw_command_uid = renderer->enqueue_render_command(
				[this](void* parameters)
				{
					if (_visibility == uint8(body_visibility::invisible))
					{
						return;
					}
					
					auto command_params = static_cast<glfw_command_parameters*>(parameters);

					_shader_program->activate();

					transform model_view_proj_mat = command_params->view_proj_mat * transform{ _position };
					
					auto model_view_proj_mat_uniform = glGetUniformLocation(_shader_program->get_id(), "u_model_view_proj_mat");
					glUniformMatrix4fv(model_view_proj_mat_uniform, 1, GL_TRUE, model_view_proj_mat.data());

					auto color_tuple = util::color_to_tuple(basic_color(_color));
					vector light_color{ std::get<0>(color_tuple), std::get<1>(color_tuple), std::get<2>(color_tuple) };

					command_params->light_pos = _position;
					command_params->ambient_color = _ambient_factor * light_color;
					command_params->diffuse_color = _diffuse_factor * light_color;
					command_params->specular_color = _specular_factor * light_color;
					
					auto light_color_uniform = glGetUniformLocation(_shader_program->get_id(), "u_light_color");
					glUniform3fv(light_color_uniform, 1, light_color.data());

					glBindVertexArray(_vao);
					glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, reinterpret_cast<void*>(0));
					glBindVertexArray(0);

					_shader_program->deactivate();
				},
				render_priority::above_normal,
				true
			);

			_is_setup = true;
		}
	}

	void gl_point_light::teardown_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_static_mesh_body_data, "Trying to reset body but global renderer accessor is not initialized!");
		}

		if (_is_setup)
		{
			renderer->dequeue_render_command(_draw_command_uid);

			renderer->enqueue_render_command(
				[this](void*)
				{
					glDeleteBuffers(2, _vbo);
					glDeleteVertexArrays(1, &_vao);

					delete _shader_program;
				},
				render_priority::below_normal,
				false
			);

			_is_setup = false;
		}
	}

	void gl_point_light::set_ambient_factor(float32 ambient_factor)
	{
		_ambient_factor = ambient_factor;
	}

	void gl_point_light::set_diffuse_factor(float32 diffuse_factor)
	{
		_diffuse_factor = diffuse_factor;
	}

	void gl_point_light::set_specular_factor(float32 specular_factor)
	{
		_specular_factor = specular_factor;
	}

	float32 gl_point_light::get_ambient_factor() const
	{
		return _ambient_factor;
	}

	float32 gl_point_light::get_diffuse_factor() const
	{
		return _diffuse_factor;
	}

	float32 gl_point_light::get_specular_factor() const
	{
		return _specular_factor;
	}
	
	void gl_point_light::set_position(svector const& new_position)
	{
		_position = new_position;
	}

	void gl_point_light::set_color(basic_color new_color)
	{
		_color = uint8(new_color);
	}

	void gl_point_light::set_visibility(body_visibility new_visibility)
	{
		_visibility = uint8(new_visibility);
	}

	svector const& gl_point_light::get_position() const
	{
		return _position;
	}

	basic_color gl_point_light::get_color() const
	{
		return basic_color(_color);
	}

	body_visibility gl_point_light::get_visibility() const
	{
		return body_visibility(_visibility);
	}
}
