#ifndef ENGINE_GRAPHICS_GL_STATIC_MESH_BODY_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_STATIC_MESH_BODY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/graphics/gl_shader_program.hpp"
#include "engine/graphics/graphics_globals.hpp"
#include "engine/graphics/texture.hpp"
#include "library/containers.hpp"
#include "library/math/transform.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	class gl_static_mesh_body final : public base_static_mesh_body
	{
	public:
		explicit gl_static_mesh_body(array<svector> const& vertex_coord_array, array<svector> const& vertex_normal_array, 
									 array<texcoord> const& vertex_uv_array, array<uint32> const& index_array, 
									 transform const& model_matrix,
									 basic_color color, body_visibility visibility = body_visibility::visible);

		virtual void setup_shader_path(std::string const& shader_path) override;
		
		virtual void setup_commands() override;

		virtual void setdown_commands() override;

		virtual void bind_update_transform_callback(update_transform_callback_type&& callback) override;
		
		virtual void set_vertex_point_array(array<svector> const& new_vertex_point_array) override;

		virtual void set_vertex_normal_array(array<svector> const& new_vertex_normal_array) override;

		virtual void set_vertex_uv_array(array<texcoord> const& new_vertex_uv_array) override;

		virtual void set_index_array(array<uint32> const& new_index_array) override;

		virtual void set_texture_image(std::shared_ptr<texture> const& new_texture_image) override;

		virtual void set_transform(transform const& new_transform) override;

		virtual void set_color(basic_color new_color) override;

		virtual void set_visibility(body_visibility new_visibility) override;

		virtual array<float32> const& get_vertex_array() const override;

		virtual array<uint32> const& get_index_array() const override;

		virtual std::shared_ptr<texture> const& get_texture_image() const override;
		
		virtual transform get_transform() const override;

		virtual basic_color get_color() const override;

		virtual body_visibility get_visibility() const override;

	private:
		array<float32> _vertex_array;
		array<uint32> _index_array;

		texcolor _color;
		transform _transform; // local to world space
		transform _normal_transform;

		std::shared_ptr<texture> _texture_image;

		std::string _shader_path;
		gl_shader_program* _shader_program;

		uint64 _draw_command_uid;
		uint64 _update_transform_command_uid;
		uint64 _update_texture_command_uid;

		uint32 _vao;
		uint32 _vbo[5]; // 0 - vertex (position, normal, uv) array
						// 1 - index array
						// 2 - color vector array
						// 3 - transform matrix array
						// 4 - normal transform matrix array
		uint32 _tid;

		uint8 _body_color : 4;
		uint8 _body_visibility : 1;
		
		uint8 _is_setup : 1;

	};
}

#endif
