#ifndef ENGINE_GRAPHICS_GL_CAMERA_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_CAMERA_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "library/math/transform.hpp"

namespace gaea
{
	class gl_camera
	{
	public:
		explicit gl_camera(float32 frustum_field_of_view = 75.f, float32 frustum_near_plane = 0.001f, float32 frustum_far_plane = 1000.f, 
						   bool is_fov_horizontal = true);

		gl_camera(gl_camera const&) = delete;
		gl_camera& operator=(gl_camera const&) = delete;

		gl_camera(gl_camera&&) = delete;
		gl_camera& operator=(gl_camera&&) = delete;

		void setup_commands();

		void teardown_commands();

		void translate_camera(svector const& translation);

		void rotate_camera(squaternion const& rotation);

		void set_camera_position(svector const& position);

		void set_camera_rotation(squaternion const& rotation);

		svector const& get_camera_position() const;
		
		squaternion const& get_camera_rotation() const;

		void adjust_frustum_field_of_view(float32 field_of_view);
		
		void adjust_frustum_near_plane(float32 near_plane);
		
		void adjust_frustum_far_plane(float32 far_plane);

		float32 frustum_field_of_view() const;
		
		float32 frustum_near_plane() const;
		
		float32 frustum_far_plane() const;

		transform view_proj_matrix() const;

	private:
		transform _calculate_camera() const;
		transform _calculate_frustum() const;

		svector _position;
		squaternion _rotation;

		transform _view_mat; // world to view space
		transform _proj_mat; // view to clip space
		
		transform _view_proj_mat;

		float32 _frustum_field_of_view;
		float32 _frustum_near_plane;
		float32 _frustum_far_plane;

		bool _is_fov_horizontal;
		
		uint64 _update_view_proj_command_uid;
		
		uint8 _needs_recalc_view : 1;
		uint8 _needs_recalc_proj : 1;
		
		uint8 _is_setup : 1;

	};
}

#endif
