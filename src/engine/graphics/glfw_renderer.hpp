#ifndef ENGINE_GRAPHICS_GLFW_RENDERER_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GLFW_RENDERER_INCLUDE_GUARD

#include "core/core_minimal.hpp"
#include "core/core_opengl.hpp"

#include "engine/graphics/gl_shader_program.hpp"
#include "engine/graphics/texture.hpp"
#include "library/containers.hpp"
#include "library/math/transform.hpp"

namespace gaea::implementation
{
	struct render_command
	{
		uint32 uid;
		base_renderer::render_function_type function;
		uint8 priority : 3;
		uint8 is_persistent : 1;
	};

	struct debug_message_data
	{
		std::string message;

		uint32 vao = 0;
		uint32 vbo[2] = { 0, 0 };
		uint32 tbo = 0;
	};
}

namespace gaea
{	
	struct glfw_command_parameters
	{
		transform view_proj_mat{};

		svector view_pos{};
		svector light_pos{};

		svector ambient_color{};
		svector diffuse_color{};
		svector specular_color{};
	};

	class glfw_renderer final : public base_renderer
	{
	public:
		explicit glfw_renderer();

		virtual bool initialize(void* parameters) override;
		virtual void finalize() override;

		virtual void draw(uint64 frame_number) override;

		virtual void flush() override;

		virtual uint64 enqueue_render_command(render_function_type&& function, render_priority priority, bool is_persistent) override;
		virtual void dequeue_render_command(uint64 uid) override;

		void draw_debug_string(std::string&& message, basic_color color = basic_color::white);

		void query_window_size(int32& width, int32& height) const;

		void handle_events();

		void toggle_mouse_capture();

		void toggle_fullscreen();

		bool is_transitioning() const;

		bool is_capturing_mouse() const;

		bool is_fullscreen() const;
		
		virtual void register_window_minimize_callback(window_minimize_callback_type&& callback) override;
		virtual void register_window_maximize_callback(window_maximize_callback_type&& callback) override;

		virtual void register_window_focus_callback(window_focus_callback_type&& callback) override;
		virtual void register_window_close_callback(window_close_callback_type&& callback) override;

		virtual void register_window_move_callback(window_move_callback_type&& callback) override;
		virtual void register_window_resize_callback(window_resize_callback_type&& callback) override;

		virtual void register_event_key_button_callback(event_key_button_callback_type&& callback) override;
		virtual void register_event_mouse_button_callback(event_mouse_button_callback_type&& callback) override;
		virtual void register_event_mouse_drag_callback(event_mouse_drag_callback_type&& callback) override;
		virtual void register_event_mouse_scroll_callback(event_mouse_scroll_callback_type&& callback) override;

	private:
		void _rendering_loop();

		void _init_debug_messages();
		void _render_debug_messages();
		void _term_debug_messages();
		
		void _on_window_minimize(GLFWwindow* window, int minimized);
		void _on_window_maximize(GLFWwindow* window, int maximized);
		
		void _on_window_focus(GLFWwindow* window, int focused);
		void _on_window_close(GLFWwindow* window);
		
		void _on_window_move(GLFWwindow* window, int xpos, int ypos);
		void _on_window_resize(GLFWwindow* window, int width, int height);

		void _on_key_button(GLFWwindow* window, int key, int scancode, int action, int mods);
		void _on_mouse_button(GLFWwindow* window, int button, int action, int mods);
		void _on_mouse_drag(GLFWwindow* window, double xpos, double ypos);
		void _on_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset);

		static void _redirect_on_window_minimize(GLFWwindow* window, int minimized);
		static void _redirect_on_window_maximize(GLFWwindow* window, int maximized);

		static void _redirect_on_window_focus(GLFWwindow* window, int focused);
		static void _redirect_on_window_close(GLFWwindow* window);

		static void _redirect_on_window_move(GLFWwindow* window, int xpos, int ypos);
		static void _redirect_on_window_resize(GLFWwindow* window, int width, int height);

		static void _redirect_on_key_button(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void _redirect_on_mouse_button(GLFWwindow* window, int button, int action, int mods);
		static void _redirect_on_mouse_drag(GLFWwindow* window, double xpos, double ypos);
		static void _redirect_on_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset);

		std::thread _render_thread;
		std::condition_variable _render_condvar;
		std::mutex _render_lock;

		array<window_minimize_callback_type> _window_minimize_callback_array;
		array<window_maximize_callback_type> _window_maximize_callback_array;
		
		array<window_focus_callback_type> _window_focus_callback_array;
		array<window_close_callback_type> _window_close_callback_array;
		
		array<window_move_callback_type> _window_move_callback_array;
		array<window_resize_callback_type> _window_resize_callback_array;

		array<event_key_button_callback_type> _event_key_button_callback_array;
		array<event_mouse_button_callback_type> _event_mouse_button_callback_array;
		array<event_mouse_drag_callback_type> _event_mouse_drag_callback_array;
		array<event_mouse_scroll_callback_type> _event_mouse_scroll_callback_array;

		array<implementation::render_command> _engine_render_command_array;
		array<implementation::render_command> _render_command_array;

		implementation::debug_message_data _debug_string;

		GLFWwindow* _window_ptr;
		
		gl_shader_program* _basic_font_shader_program;
		std::shared_ptr<texture> _basic_font;

		int32 _window_pos_x;
		int32 _window_pos_y;
		uint32 _window_width;
		uint32 _window_height;

		uint64 _frame_number;
		uint8 _transition_frame_count;

		float32 _last_mouse_pos_x;
		float32 _last_mouse_pos_y;

		input_modifier _last_input_modifier_state;

		int32 _last_window_pos_x;
		int32 _last_window_pos_y;
		int32 _last_window_width;
		int32 _last_window_height;

		uint8 _has_init_glfw : 1;
		uint8 _has_init_glad : 1;
		uint8 _has_created_window : 1;
		uint8 _has_created_thread : 1;
		
		uint8 _should_run : 1;
		uint8 _should_draw : 1;
		uint8 _should_flush : 1;
		
		uint8 _is_capturing_mouse : 1;
		uint8 _needs_refresh_mouse_pos : 1;

		uint8 _is_fullscreen_window : 1;

		static constexpr uint32 _engine_reserved_command_id				= 0x8000'0000;

		static constexpr uint32 _engine_window_minimize_command_id		= _engine_reserved_command_id | 0x1;
		static constexpr uint32 _engine_window_maximize_command_id		= _engine_reserved_command_id | 0x2;

		static constexpr uint32 _engine_window_focus_command_id			= _engine_reserved_command_id | 0x3;
		static constexpr uint32 _engine_window_close_command_id			= _engine_reserved_command_id | 0x4;

		static constexpr uint32 _engine_window_move_command_id			= _engine_reserved_command_id | 0x5;
		static constexpr uint32 _engine_window_resize_command_id		= _engine_reserved_command_id | 0x6;

		static constexpr uint32 _engine_event_key_button_command_id		= _engine_reserved_command_id | 0x7;
		static constexpr uint32 _engine_event_mouse_button_command_id	= _engine_reserved_command_id | 0x8;
		static constexpr uint32 _engine_event_mouse_drag_command_id		= _engine_reserved_command_id | 0x9;

	};
}
#endif
