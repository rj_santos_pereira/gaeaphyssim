#ifndef ENGINE_GRAPHICS_GL_POINT_LIGHT_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_POINT_LIGHT_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/graphics/gl_shader_program.hpp"
#include "engine/graphics/texture.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	class gl_point_light
	{
	public:
		explicit gl_point_light(svector const& position, 
								basic_color color = basic_color::white, 
								body_visibility visibility = body_visibility::visible);

		gl_point_light(gl_point_light const&) = delete;
		gl_point_light& operator=(gl_point_light const&) = delete;

		gl_point_light(gl_point_light&&) = delete;
		gl_point_light& operator=(gl_point_light&&) = delete;

		void setup_shader_path(std::string const& shader_path);

		void setup_commands();

		void teardown_commands();

		void set_ambient_factor(float32 ambient_factor);
		
		void set_diffuse_factor(float32 diffuse_factor);
		
		void set_specular_factor(float32 specular_factor);

		float32 get_ambient_factor() const;
		
		float32 get_diffuse_factor() const;
		
		float32 get_specular_factor() const;

		void set_position(svector const& new_position);

		void set_color(basic_color new_color);

		void set_visibility(body_visibility new_visibility);

		svector const& get_position() const;

		basic_color get_color() const;

		body_visibility get_visibility() const;

	private:
		svector _position;

		float32 _ambient_factor;
		float32 _diffuse_factor;
		float32 _specular_factor; 

		std::string _shader_path;
		gl_shader_program* _shader_program;
		
		uint64 _draw_command_uid;

		uint32 _vao;
		uint32 _vbo[2];

		uint8 _color : 4;
		uint8 _visibility : 1;

		uint8 _is_setup : 1;

	};
}

#endif
