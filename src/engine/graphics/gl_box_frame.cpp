#include "engine/graphics/gl_box_frame.hpp"

#include "core/core_opengl.hpp"

#include "engine/engine_config.hpp"
#include "engine/graphics/glfw_renderer.hpp"
#include "engine/graphics/graphics_globals.hpp"
#include "library/utility.hpp"

namespace gaea::implementation
{
	gl_box_frame_data::gl_box_frame_data()
		: _color_array{}
		, _transform_array{}
		, _shader_path{ config::path::box_frame_shaders }
		, _shader_program{ nullptr }
		, _draw_command_uid{ 0 }
		, _vao{ 0 }
		, _vbo{ 0, 0, 0 }
		, _instance_count{ 0 }
		, _setup_count{ 0 }
		, _should_recreate_instance_buffers{ true }
	{
	}

	std::size_t gl_box_frame_data::add_instance(transform const& model_mat, basic_color color, body_visibility visibility)
	{
		bool has_unused_instance = (_instance_count < _instance_flags_array.size());

		std::size_t index;
		if (has_unused_instance)
		{
			auto it = std::find_if(_instance_flags_array.begin(), _instance_flags_array.end(),
				[](instance_flags const& flags) { return !flags.valid; });

			index = std::distance(_instance_flags_array.begin(), it);

			++_instance_count;
		}
		else
		{
			index = _instance_count++;

			_color_array.emplace_back();
			_transform_array.emplace_back();
			_instance_flags_array.emplace_back();

			_should_recreate_instance_buffers = true;
		}

		_instance_flags_array[index].valid = true;

		set_transform(model_mat, index);
		set_color(color, index);
		set_visibility(visibility, index);

		return index;
	}

	void gl_box_frame_data::remove_instance(std::size_t index)
	{
		_instance_flags_array[index].valid = false;

		// This will make the frag shader discard the body entirely
		set_visibility(body_visibility::invisible, index);

		--_instance_count;
	}

	void gl_box_frame_data::setup_shader_path(std::string const& shader_path)
	{
		_shader_path = shader_path;
	}

	void gl_box_frame_data::setup_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_box_frame_data, "Trying to setup body but global renderer accessor is not initialized!");
		}

		if (_setup_count == 0)
		{
			renderer->enqueue_render_command(
				[this](void*)
				{
					_shader_program = new gl_shader_program;
					_shader_program->load_shaders(_shader_path);

					if (!_shader_program->compile_shaders())
					{
						GAEA_WARN(gl_box_frame_data, "Error loading shader program [ID: %u]", _shader_program->get_id());
					}

					glGenVertexArrays(1, &_vao);
					glGenBuffers(4, _vbo);

					glBindVertexArray(_vao);

					// Setup vertex array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(svector) * rendering_primitives::cube::vertex_array.size(), rendering_primitives::cube::vertex_array.data(), GL_STATIC_DRAW);

					glEnableVertexAttribArray(0);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float32), reinterpret_cast<void*>(0 * sizeof(float32)));

					// Setup index array in OpenGL buffer
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32) * rendering_primitives::cube::index_array.size(), rendering_primitives::cube::index_array.data(), GL_STATIC_DRAW);

					glBindVertexArray(0);
				},
				render_priority::high,
				false
			);

			_draw_command_uid = renderer->enqueue_render_command(
				[this](void* parameters)
				{
					auto command_params = static_cast<glfw_command_parameters*>(parameters);

					_shader_program->activate();

					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					
					auto view_proj_mat_uniform = glGetUniformLocation(_shader_program->get_id(), "u_view_proj_mat");
					glUniformMatrix4fv(view_proj_mat_uniform, 1, GL_TRUE, command_params->view_proj_mat.data());

					glBindVertexArray(_vao);

					glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
					glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(texcolor) * _color_array.size(), _color_array.data());

					glBindBuffer(GL_ARRAY_BUFFER, _vbo[3]);
					glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(transform) * _transform_array.size(), _transform_array.data());

					glDrawElementsInstanced(GL_TRIANGLES, uint32(rendering_primitives::cube::index_array.size()), GL_UNSIGNED_INT, reinterpret_cast<void*>(0), uint32(_instance_flags_array.size()));

					glBindVertexArray(0);

					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

					_shader_program->deactivate();
				},
				render_priority::normal,
				true
			);
		}

		++_setup_count;

		if (_should_recreate_instance_buffers)
		{
			renderer->enqueue_render_command(
				[this](void*)
				{
					glBindVertexArray(_vao);

					// Setup color array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(texcolor) * _color_array.size(), _color_array.data(), GL_DYNAMIC_DRAW);

					glEnableVertexAttribArray(1);
					glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(texcolor), reinterpret_cast<void*>(0 * sizeof(float32)));
					glVertexAttribDivisor(1, 1);

					// Setup transform array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[3]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(transform) * _transform_array.size(), _transform_array.data(), GL_DYNAMIC_DRAW);

					glEnableVertexAttribArray(2);
					glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(0 * sizeof(float32)));
					glVertexAttribDivisor(2, 1);

					glEnableVertexAttribArray(3);
					glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(4 * sizeof(float32)));
					glVertexAttribDivisor(3, 1);

					glEnableVertexAttribArray(4);
					glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(8 * sizeof(float32)));
					glVertexAttribDivisor(4, 1);

					glEnableVertexAttribArray(5);
					glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(12 * sizeof(float32)));
					glVertexAttribDivisor(5, 1);

					glBindVertexArray(0);
				},
				render_priority::above_normal,
				false
			);

			_should_recreate_instance_buffers = false;
		}
	}

	void gl_box_frame_data::setdown_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_box_frame_data, "Trying to setdown body but global renderer accessor is not initialized!");
		}

		if (_setup_count > 0)
		{
			--_setup_count;
		}

		if (_setup_count == 0)
		{
			renderer->dequeue_render_command(_draw_command_uid);

			renderer->enqueue_render_command(
				[this](void*)
				{
					glDeleteBuffers(4, _vbo);
					glDeleteVertexArrays(1, &_vao);

					delete _shader_program;
				},
				render_priority::below_normal,
				false
			);
		}
	}

	void gl_box_frame_data::set_transform(transform const& new_transform, std::size_t index)
	{
		_transform_array[index] = new_transform.transpose();
	}

	void gl_box_frame_data::set_color(basic_color new_color, std::size_t index)
	{
		_instance_flags_array[index].color = uint8(new_color);

		auto body_color = util::color_to_tuple(new_color);

		_color_array[index].r = std::get<0>(body_color);
		_color_array[index].g = std::get<1>(body_color);
		_color_array[index].b = std::get<2>(body_color);
	}

	void gl_box_frame_data::set_visibility(body_visibility new_visibility, std::size_t index)
	{
		_instance_flags_array[index].visibility = uint8(new_visibility);

		_color_array[index].a = float32(new_visibility);
	}

	transform gl_box_frame_data::get_transform(std::size_t index) const
	{
		return _transform_array[index].transpose();
	}

	basic_color gl_box_frame_data::get_color(std::size_t index) const
	{
		return basic_color(_instance_flags_array[index].color);
	}

	body_visibility gl_box_frame_data::get_visibility(std::size_t index) const
	{
		return body_visibility(_instance_flags_array[index].visibility);
	}
}

namespace gaea
{
	std::weak_ptr<implementation::gl_box_frame_data> gl_box_frame::_global_body_data;
	
	gl_box_frame::gl_box_frame()
		: _body_data{ _init_with_global_data() }
		, _instance_index{ _body_data->add_instance(transform{}, basic_color::white, body_visibility::invisible) }
		, _update_transform_command_uid{ 0 }
	{
	}

	gl_box_frame::~gl_box_frame()
	{
		if (_body_data)
		{
			_body_data->remove_instance(_instance_index);
		}
	}

	void gl_box_frame::setup_shader_path(std::string const& shader_path)
	{
		_body_data->setup_shader_path(shader_path);
	}

	void gl_box_frame::setup_commands()
	{
		_body_data->setup_commands();
	}

	void gl_box_frame::setdown_commands()
	{
		_body_data->setdown_commands();
	}

	void gl_box_frame::bind_update_transform_callback(update_transform_callback_type&& callback)
	{
		if (callback)
		{
			_update_transform_command_uid = renderer->enqueue_render_command(
				[this, callback = std::move(callback)](void*)
				{
					_body_data->set_transform(callback(), _instance_index);
				},
				render_priority::above_normal,
				true
			);
		}
		else
		{
			renderer->dequeue_render_command(_update_transform_command_uid);
		}
	}

	void gl_box_frame::set_transform(transform const& new_transform)
	{
		_body_data->set_transform(new_transform, _instance_index);
	}

	void gl_box_frame::set_color(basic_color new_color)
	{
		_body_data->set_color(new_color, _instance_index);
	}

	void gl_box_frame::set_visibility(body_visibility new_visibility)
	{
		_body_data->set_visibility(new_visibility, _instance_index);
	}

	transform gl_box_frame::get_transform() const
	{
		return _body_data->get_transform(_instance_index);
	}

	basic_color gl_box_frame::get_color() const
	{
		return _body_data->get_color(_instance_index);
	}

	body_visibility gl_box_frame::get_visibility() const
	{
		return _body_data->get_visibility(_instance_index);
	}

	std::shared_ptr<implementation::gl_box_frame_data> gl_box_frame::_init_with_global_data()
	{
		auto body_data = _global_body_data.lock();

		if (!body_data)
		{
			body_data = std::make_shared<implementation::gl_box_frame_data>();
			
			_global_body_data = body_data;
		}
		
		return std::move(body_data);
	}
}
