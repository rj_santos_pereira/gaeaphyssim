#ifndef ENGINE_GRAPHICS_GRAPHICS_GLOBALS_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GRAPHICS_GLOBALS_INCLUDE_GUARD

#include "core/core_common.hpp"
#include "engine/graphics/texture.hpp"
#include "library/containers.hpp"
#include "library/math/transform.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	class base_static_mesh_body
	{
	protected:
		base_static_mesh_body() = default;
		~base_static_mesh_body() = default;
		
	public:
		using update_transform_callback_type = std::function<transform()>;

		base_static_mesh_body(base_static_mesh_body const&) = delete;
		base_static_mesh_body(base_static_mesh_body&&) = delete;

		base_static_mesh_body& operator=(base_static_mesh_body const&) = delete;
		base_static_mesh_body& operator=(base_static_mesh_body&&) = delete;

		virtual void setup_shader_path(std::string const& shader_path) = 0;

		virtual void setup_commands() = 0;

		virtual void setdown_commands() = 0;

		virtual void bind_update_transform_callback(update_transform_callback_type&& callback) = 0;

		virtual void set_vertex_point_array(array<svector> const& new_vertex_point_array) = 0;

		virtual void set_vertex_normal_array(array<svector> const& new_vertex_normal_array) = 0;

		virtual void set_vertex_uv_array(array<texcoord> const& new_vertex_uv_array) = 0;

		virtual void set_index_array(array<uint32> const& new_index_array) = 0;

		virtual void set_texture_image(std::shared_ptr<texture> const& new_texture_image) = 0;

		virtual void set_transform(transform const& new_transform) = 0;

		virtual void set_color(basic_color new_color) = 0;

		virtual void set_visibility(body_visibility new_visibility) = 0;

		virtual array<float32> const& get_vertex_array() const = 0;

		virtual array<uint32> const& get_index_array() const = 0;

		virtual std::shared_ptr<texture> const& get_texture_image() const = 0;

		virtual transform get_transform() const = 0;

		virtual basic_color get_color() const = 0;

		virtual body_visibility get_visibility() const = 0;
	};
	
	struct rendering_primitives
	{
		struct cube
		{
			static inline array<svector> vertex_array
			{
				/* Front */
				svector{ +0.5f, +0.5f, +0.5f }, /* 0 */
				svector{ +0.5f, -0.5f, +0.5f }, /* 1 */
				svector{ +0.5f, +0.5f, -0.5f }, /* 2 */
				svector{ +0.5f, -0.5f, -0.5f }, /* 3 */

				/* Back */
				svector{ -0.5f, +0.5f, -0.5f }, /* 4 */
				svector{ -0.5f, -0.5f, -0.5f }, /* 5 */
				svector{ -0.5f, +0.5f, +0.5f }, /* 6 */
				svector{ -0.5f, -0.5f, +0.5f }, /* 7 */

				/* Left */
				svector{ -0.5f, +0.5f, +0.5f }, /* 8 */
				svector{ +0.5f, +0.5f, +0.5f }, /* 9 */
				svector{ -0.5f, +0.5f, -0.5f }, /* 10 */
				svector{ +0.5f, +0.5f, -0.5f }, /* 11 */

				/* Right */
				svector{ -0.5f, -0.5f, -0.5f }, /* 12 */
				svector{ +0.5f, -0.5f, -0.5f }, /* 13 */
				svector{ -0.5f, -0.5f, +0.5f }, /* 14 */
				svector{ +0.5f, -0.5f, +0.5f }, /* 15 */

				/* Top */
				svector{ -0.5f, +0.5f, +0.5f }, /* 16 */
				svector{ -0.5f, -0.5f, +0.5f }, /* 17 */
				svector{ +0.5f, +0.5f, +0.5f }, /* 18 */
				svector{ +0.5f, -0.5f, +0.5f }, /* 19 */

				/* Bottom */
				svector{ +0.5f, +0.5f, -0.5f }, /* 20 */
				svector{ +0.5f, -0.5f, -0.5f }, /* 21 */
				svector{ -0.5f, +0.5f, -0.5f }, /* 22 */
				svector{ -0.5f, -0.5f, -0.5f }, /* 23 */
			};

			static inline array<svector> normal_array
			{
				/* Front */
				svector{ +1.0f, 0.0f, 0.0f }, /* 0 */
				svector{ +1.0f, 0.0f, 0.0f }, /* 1 */
				svector{ +1.0f, 0.0f, 0.0f }, /* 2 */
				svector{ +1.0f, 0.0f, 0.0f }, /* 3 */

				/* Back */
				svector{ -1.0f, 0.0f, 0.0f }, /* 4 */
				svector{ -1.0f, 0.0f, 0.0f }, /* 5 */
				svector{ -1.0f, 0.0f, 0.0f }, /* 6 */
				svector{ -1.0f, 0.0f, 0.0f }, /* 7 */

				/* Left */
				svector{ 0.0f, +1.0f, 0.0f }, /* 8 */
				svector{ 0.0f, +1.0f, 0.0f }, /* 9 */
				svector{ 0.0f, +1.0f, 0.0f }, /* 10 */
				svector{ 0.0f, +1.0f, 0.0f }, /* 11 */

				/* Right */
				svector{ 0.0f, -1.0f, 0.0f }, /* 12 */
				svector{ 0.0f, -1.0f, 0.0f }, /* 13 */
				svector{ 0.0f, -1.0f, 0.0f }, /* 14 */
				svector{ 0.0f, -1.0f, 0.0f }, /* 15 */

				/* Top */
				svector{ 0.0f, 0.0f, +1.0f }, /* 16 */
				svector{ 0.0f, 0.0f, +1.0f }, /* 17 */
				svector{ 0.0f, 0.0f, +1.0f }, /* 18 */
				svector{ 0.0f, 0.0f, +1.0f }, /* 19 */

				/* Bottom */
				svector{ 0.0f, 0.0f, -1.0f }, /* 20 */
				svector{ 0.0f, 0.0f, -1.0f }, /* 21 */
				svector{ 0.0f, 0.0f, -1.0f }, /* 22 */
				svector{ 0.0f, 0.0f, -1.0f }, /* 23 */
			};

			static inline array<texcoord> uv_array
			{
				/* Front */
				texcoord{ 1.0f, 1.0f }, /* 0 */
				texcoord{ 0.0f, 1.0f }, /* 1 */
				texcoord{ 1.0f, 0.0f }, /* 2 */
				texcoord{ 0.0f, 0.0f }, /* 3 */

				/* Back */
				texcoord{ 1.0f, 1.0f }, /* 4 */
				texcoord{ 0.0f, 1.0f }, /* 5 */
				texcoord{ 1.0f, 0.0f }, /* 6 */
				texcoord{ 0.0f, 0.0f }, /* 7 */

				/* Left */
				texcoord{ 1.0f, 1.0f }, /* 8 */
				texcoord{ 0.0f, 1.0f }, /* 9 */
				texcoord{ 1.0f, 0.0f }, /* 10 */
				texcoord{ 0.0f, 0.0f }, /* 11 */

				/* Right */
				texcoord{ 1.0f, 1.0f }, /* 12 */
				texcoord{ 0.0f, 1.0f }, /* 13 */
				texcoord{ 1.0f, 0.0f }, /* 14 */
				texcoord{ 0.0f, 0.0f }, /* 15 */

				/* Top */
				texcoord{ 1.0f, 1.0f }, /* 16 */
				texcoord{ 0.0f, 1.0f }, /* 17 */
				texcoord{ 1.0f, 0.0f }, /* 18 */
				texcoord{ 0.0f, 0.0f }, /* 19 */

				/* Bottom */
				texcoord{ 1.0f, 1.0f }, /* 20 */
				texcoord{ 0.0f, 1.0f }, /* 21 */
				texcoord{ 1.0f, 0.0f }, /* 22 */
				texcoord{ 0.0f, 0.0f }, /* 23 */
			};

			static inline array<uint32> index_array
			{
				/* Front */
				0, 1, 2,
				3, 2, 1,

				/* Back */
				4, 5, 6,
				7, 6, 5,

				/* Left */
				8, 9, 10,
				11, 10, 9,

				/* Right */
				12, 13, 14,
				15, 14, 13,

				/* Top */
				16, 17, 18,
				19, 18, 17,

				/* Bottom */
				20, 21, 22,
				23, 22, 21,
			};
		};
	};
}

#endif
