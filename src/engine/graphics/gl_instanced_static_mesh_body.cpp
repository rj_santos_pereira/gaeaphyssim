#include "engine/graphics/gl_instanced_static_mesh_body.hpp"

#include "core/core_opengl.hpp"

#include "engine/engine_config.hpp"
#include "engine/graphics/glfw_renderer.hpp"
#include "library/utility.hpp"

// On the subtleties of updating transform buffer each frame:
// https://stackoverflow.com/questions/25919618/whats-the-best-way-to-keep-adding-new-points-to-glbufferdata?rq=1

namespace gaea::implementation
{
	gl_instanced_static_mesh_body_data::gl_instanced_static_mesh_body_data()
		: _vertex_array{}
		, _index_array{}
		, _color_array{}
		, _transform_array{}
		, _instance_flags_array{}
		, _texture_image{}
		, _shader_path{}
		, _shader_program{ nullptr }
		, _draw_command_uid{ 0 }
		, _update_texture_command_uid{ 0 }
		, _vao{ 0 }
		, _vbo{ 0, 0, 0, 0, 0 }
		, _tid{ 0 }
		, _instance_count{ 0 }
		, _setup_count{ 0 }
		, _should_recreate_instance_buffers{ true }
	{
	}

	std::size_t gl_instanced_static_mesh_body_data::add_instance(transform const& model_mat, basic_color color, body_visibility visibility)
	{
		bool has_unused_instance = (_instance_count < _instance_flags_array.size());
		
		std::size_t index;
		if (has_unused_instance)
		{
			auto it = std::find_if(_instance_flags_array.begin(), _instance_flags_array.end(), 
				[](instance_flags const& flags) { return !flags.valid; });
			
			index = std::distance(_instance_flags_array.begin(), it);

			++_instance_count;
		}
		else
		{
			index = _instance_count++;
			
			_color_array.emplace_back();
			_transform_array.emplace_back();
			_normal_transform_array.emplace_back();
			_instance_flags_array.emplace_back();
			
			_should_recreate_instance_buffers = true;
		}

		_instance_flags_array[index].valid = true;

		set_transform(model_mat, index);
		set_color(color, index);
		set_visibility(visibility, index);

		return index;
	}

	void gl_instanced_static_mesh_body_data::remove_instance(std::size_t index)
	{
		_instance_flags_array[index].valid = false;

		// This will make the frag shader discard the body entirely
		set_visibility(body_visibility::invisible, index);
		
		--_instance_count;
	}

	void gl_instanced_static_mesh_body_data::setup_shader_path(std::string const& shader_path)
	{
		_shader_path = shader_path;
	}

	void gl_instanced_static_mesh_body_data::setup_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_instanced_static_mesh_body_data, "Trying to setup body but global renderer accessor is not initialized!");
		}
		
		if (_setup_count == 0)
		{
			renderer->enqueue_render_command(
				[this](void*)
				{
					_shader_program = new gl_shader_program;
					_shader_program->load_shaders(_shader_path);

					if (!_shader_program->compile_shaders())
					{
						GAEA_WARN(gl_instanced_static_mesh_body_data, "Error loading shader program [ID: %u]", _shader_program->get_id());
					}
					
					glGenVertexArrays(1, &_vao);
					glGenBuffers(5, _vbo);
					glGenTextures(1, &_tid);

					glBindVertexArray(_vao);

					// Setup vertex array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * _vertex_array.size(), _vertex_array.data(), GL_STATIC_DRAW);

					glEnableVertexAttribArray(0);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float32), reinterpret_cast<void*>(0 * sizeof(float32)));

					glEnableVertexAttribArray(1);
					glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float32), reinterpret_cast<void*>(3 * sizeof(float32)));

					glEnableVertexAttribArray(2);
					glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float32), reinterpret_cast<void*>(6 * sizeof(float32)));
					
					// Setup index array in OpenGL buffer
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32) * _index_array.size(), _index_array.data(), GL_STATIC_DRAW);

					glBindTexture(GL_TEXTURE_2D, _tid);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

					// HACK: we need to define some texture, otherwise the sampler will override our color
					static constexpr uint8 data[4] = { 0xFF, 0xFF, 0xFF, 0x00 };

					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

					// Notes:
					// The function glVertexAttribPointer specifies how OpenGL should interpret the vertex buffer data whenever a drawing call is made.
					// The parameters of glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid * pointer) are as follows:
					//
					//    index: Specifies the index of the vertex attribute.
					//    size: Specifies the number of components per vertex attribute. Must be 1, 2, 3, 4.
					//    type: Specifies the data type of each component in the array.
					//    normalized: Specifies whether data should be normalized (clamped to the range -1 to 1 for signed values and 0 to 1 for unsigned values).
					//    stride: Specifies the byte offset between consecutive vertex attributes. If stride is 0, the generic vertex attributes are understood to be tightly packed in the array
					//    pointer: Specifies an offset of the first component of the first vertex attribute in the array.
					//
					// Example usage:
					//
					//  GLfloat data[] = {
					//    // Position   // Color            // TexCoords
					//    1.0f, 0.0f,   0.5f, 0.5f, 0.5f,   0.0f, 0.5f,
					//    0.0f, 1.0f,   0.2f, 0.8f, 0.0f,   0.0f, 1.0f
					//  };
					//
					//  glEnableVertexAttribArray(0);
					//  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 
					//    	7 * sizeof(GLfloat), (GLvoid*)0);
					//
					//  glEnableVertexAttribArray(1);
					//  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 
					//    	7 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
					//
					//  glEnableVertexAttribArray(2);
					//  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 
					//    	7 * sizeof(GLfloat), (GLvoid*)(5 * sizeof(GLfloat)));

					glBindVertexArray(0);
				},
				render_priority::high,
				false
			);

			_draw_command_uid = renderer->enqueue_render_command(
				[this](void* parameters)
				{
					auto command_params = static_cast<glfw_command_parameters*>(parameters);

					// TODO test precalculating vertex and normal position

					_shader_program->activate();

					auto view_proj_mat_uniform = glGetUniformLocation(_shader_program->get_id(), "u_view_proj_mat");
					glUniformMatrix4fv(view_proj_mat_uniform, 1, GL_TRUE, command_params->view_proj_mat.data());

#if GAEA_USING_LIGHTING_MODEL
					auto view_pos_uniform = glGetUniformLocation(_shader_program->get_id(), "u_view_pos");
					glUniform3fv(view_pos_uniform, 1, command_params->view_pos.data());
					
					auto light_pos_uniform = glGetUniformLocation(_shader_program->get_id(), "u_light_pos");
					glUniform3fv(light_pos_uniform, 1, command_params->light_pos.data());
					
					auto ambient_color_uniform = glGetUniformLocation(_shader_program->get_id(), "u_ambient_color");
					glUniform3fv(ambient_color_uniform, 1, command_params->ambient_color.data());

					auto diffuse_color_uniform = glGetUniformLocation(_shader_program->get_id(), "u_diffuse_color");
					glUniform3fv(diffuse_color_uniform, 1, command_params->diffuse_color.data());
					
					auto specular_color_uniform = glGetUniformLocation(_shader_program->get_id(), "u_specular_color");
					glUniform3fv(specular_color_uniform, 1, command_params->specular_color.data());
#endif
					
					glBindVertexArray(_vao);
					glBindTexture(GL_TEXTURE_2D, _tid);

					glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
					glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(texcolor) * _color_array.size(), _color_array.data());
					
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[3]);
					glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(transform) * _transform_array.size(), _transform_array.data());
					
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[4]);
					glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(transform) * _normal_transform_array.size(), _normal_transform_array.data());

					if (_index_array.empty())
					{
						glDrawArraysInstanced(GL_TRIANGLES, 0, uint32(_vertex_array.size()) / 8, uint32(_transform_array.size()));
					}
					else
					{
						glDrawElementsInstanced(GL_TRIANGLES, uint32(_index_array.size()), GL_UNSIGNED_INT, reinterpret_cast<void*>(0), uint32(_instance_flags_array.size()));
					}

					glBindTexture(GL_TEXTURE_2D, 0);
					glBindVertexArray(0);

					_shader_program->deactivate();
				},
				render_priority::normal,
				true
			);
		}

		++_setup_count;

		if (_should_recreate_instance_buffers)
		{
			renderer->enqueue_render_command(
				[this](void*)
				{
					glBindVertexArray(_vao);

					// Setup color array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[2]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(texcolor) * _color_array.size(), _color_array.data(), GL_DYNAMIC_DRAW);
					
					glEnableVertexAttribArray(3);
					glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(texcolor), reinterpret_cast<void*>(0 * sizeof(float32)));
					glVertexAttribDivisor(3, 1);

					// Setup transform array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[3]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(transform) * _transform_array.size(), _transform_array.data(), GL_DYNAMIC_DRAW);

					glEnableVertexAttribArray(4);
					glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(0 * sizeof(float32)));
					glVertexAttribDivisor(4, 1);

					glEnableVertexAttribArray(5);
					glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(4 * sizeof(float32)));
					glVertexAttribDivisor(5, 1);

					glEnableVertexAttribArray(6);
					glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(8 * sizeof(float32)));
					glVertexAttribDivisor(6, 1);

					glEnableVertexAttribArray(7);
					glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(12 * sizeof(float32)));
					glVertexAttribDivisor(7, 1);
					
					// Setup normal transform array in OpenGL buffer
					glBindBuffer(GL_ARRAY_BUFFER, _vbo[4]);
					glBufferData(GL_ARRAY_BUFFER, sizeof(transform) * _normal_transform_array.size(), _normal_transform_array.data(), GL_DYNAMIC_DRAW);

					glEnableVertexAttribArray(8);
					glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(0 * sizeof(float32)));
					glVertexAttribDivisor(8, 1);

					glEnableVertexAttribArray(9);
					glVertexAttribPointer(9, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(4 * sizeof(float32)));
					glVertexAttribDivisor(9, 1);

					glEnableVertexAttribArray(10);
					glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(8 * sizeof(float32)));
					glVertexAttribDivisor(10, 1);

					glEnableVertexAttribArray(11);
					glVertexAttribPointer(11, 4, GL_FLOAT, GL_FALSE, sizeof(transform), reinterpret_cast<void*>(12 * sizeof(float32)));
					glVertexAttribDivisor(11, 1);

					glBindVertexArray(0);
				},
				render_priority::above_normal,
				false
			);

			_should_recreate_instance_buffers = false;
		}
	}

	void gl_instanced_static_mesh_body_data::setdown_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_instanced_static_mesh_body_data, "Trying to setdown body but global renderer accessor is not initialized!");
		}

		if (_setup_count > 0)
		{
			--_setup_count;
		}
		
		if (_setup_count == 0)
		{
			renderer->dequeue_render_command(_draw_command_uid);

			renderer->enqueue_render_command(
				[this](void*)
				{
					glDeleteTextures(1, &_tid);
					glDeleteBuffers(5, _vbo);
					glDeleteVertexArrays(1, &_vao);

					delete _shader_program;
				},
				render_priority::below_normal,
				false
			);
		}
	}

	void gl_instanced_static_mesh_body_data::set_vertex_point_array(array<svector> const& new_vertex_point_array)
	{
		if (_setup_count > 0)
		{
			GAEA_ERROR(gl_instanced_static_mesh_body_data, "Trying to change vertex data in a setup body!");
			return;
		}
		
		constexpr std::size_t stride = 8;
		constexpr std::size_t point_offset = 0;

		_vertex_array.resize(new_vertex_point_array.size() * stride);

		std::size_t idx = 0;
		for (svector const& new_point : new_vertex_point_array)
		{
			_vertex_array[idx + point_offset + 0] = new_point.x;
			_vertex_array[idx + point_offset + 1] = new_point.y;
			_vertex_array[idx + point_offset + 2] = new_point.z;
			idx += stride;
		}
	}

	void gl_instanced_static_mesh_body_data::set_vertex_normal_array(array<svector> const& new_vertex_normal_array)
	{
		if (_setup_count > 0)
		{
			GAEA_ERROR(gl_instanced_static_mesh_body_data, "Trying to change vertex data in a setup body!");
			return;
		}
		
		constexpr std::size_t stride = 8;
		constexpr std::size_t normal_offset = 3;

		if (_vertex_array.empty())
		{
			_vertex_array.resize(new_vertex_normal_array.size() * stride);
		}

		std::size_t vert_count = std::min(_vertex_array.size() / stride, new_vertex_normal_array.size());
		for (std::size_t idx = 0; idx < vert_count; ++idx)
		{
			_vertex_array[stride * idx + normal_offset + 0] = new_vertex_normal_array[idx].x;
			_vertex_array[stride * idx + normal_offset + 1] = new_vertex_normal_array[idx].y;
			_vertex_array[stride * idx + normal_offset + 2] = new_vertex_normal_array[idx].z;
		}
	}

	void gl_instanced_static_mesh_body_data::set_vertex_uv_array(array<texcoord> const& new_vertex_uv_array)
	{
		if (_setup_count > 0)
		{
			GAEA_ERROR(gl_instanced_static_mesh_body_data, "Trying to change vertex data in a setup body!");
			return;
		}
		
		constexpr std::size_t stride = 8;
		constexpr std::size_t uv_offset = 6;

		if (_vertex_array.empty())
		{
			_vertex_array.resize(new_vertex_uv_array.size() * stride);
		}
		
		std::size_t vert_count = std::min(_vertex_array.size() / stride, new_vertex_uv_array.size());
		for (std::size_t idx = 0; idx < vert_count; ++idx)
		{
			_vertex_array[stride * idx + uv_offset + 0] = new_vertex_uv_array[idx].u;
			_vertex_array[stride * idx + uv_offset + 1] = new_vertex_uv_array[idx].v;
		}
	}

	void gl_instanced_static_mesh_body_data::set_index_array(array<uint32> const& new_index_array)
	{
		if (_setup_count > 0)
		{
			GAEA_ERROR(gl_instanced_static_mesh_body_data, "Trying to change index data in a setup body!");
			return;
		}
		
		_index_array = new_index_array;
	}

	void gl_instanced_static_mesh_body_data::set_texture_image(std::shared_ptr<texture> const& new_texture_image)
	{
		_texture_image = new_texture_image;
		
		renderer->dequeue_render_command(_update_texture_command_uid);
		
		_update_texture_command_uid = renderer->enqueue_render_command(
			[this](void*)
			{
				//glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, _tid);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

				// GL_NEAREST_MIPMAP_NEAREST: takes the nearest mipmap to match the pixel size and uses nearest neighbor interpolation for texture sampling.
				// GL_LINEAR_MIPMAP_NEAREST: takes the nearest mipmap level and samples that level using linear interpolation.
				// GL_NEAREST_MIPMAP_LINEAR: linearly interpolates between the two mipmaps that most closely match the size of a pixel and samples the interpolated level via nearest neighbor interpolation.
				// GL_LINEAR_MIPMAP_LINEAR: linearly interpolates between the two closest mipmaps and samples the interpolated level via linear interpolation.

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				if (_texture_image)
				{
					GLenum format = _texture_image->channels() == 3 ? GL_RGB : GL_RGBA;

					glTexImage2D(GL_TEXTURE_2D, 0, 
						GL_RGBA, _texture_image->width(), _texture_image->height(), 0, format, 
						GL_UNSIGNED_BYTE, _texture_image->data());
					glGenerateMipmap(GL_TEXTURE_2D);
				}
				else
				{
					// HACK: we need to define some texture, otherwise the sampler will override our color
					static constexpr uint8 data[4] = { 0xFF, 0xFF, 0xFF, 0x00 };

					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
				}
			},
			render_priority::normal,
			false
		);
	}

	void gl_instanced_static_mesh_body_data::set_transform(transform const& new_transform, std::size_t index)
	{
		_transform_array[index] = new_transform.transpose();
		_normal_transform_array[index] = new_transform.inverse();
	}

	void gl_instanced_static_mesh_body_data::set_color(basic_color new_color, std::size_t index)
	{
		_instance_flags_array[index].color = uint8(new_color);
		
		auto body_color = util::color_to_tuple(new_color);

		_color_array[index].r = std::get<0>(body_color);
		_color_array[index].g = std::get<1>(body_color);
		_color_array[index].b = std::get<2>(body_color);
	}

	void gl_instanced_static_mesh_body_data::set_visibility(body_visibility new_visibility, std::size_t index)
	{
		_instance_flags_array[index].visibility = uint8(new_visibility);
		
		_color_array[index].a = float32(new_visibility);
	}

	array<float32> const& gl_instanced_static_mesh_body_data::get_vertex_array() const
	{
		return _vertex_array;
	}

	array<uint32> const& gl_instanced_static_mesh_body_data::get_index_array() const
	{
		return _index_array;
	}

	std::shared_ptr<texture> const& gl_instanced_static_mesh_body_data::get_texture_image() const
	{
		return _texture_image;
	}

	transform gl_instanced_static_mesh_body_data::get_transform(std::size_t index) const
	{
		return _transform_array[index].transpose();
	}

	basic_color gl_instanced_static_mesh_body_data::get_color(std::size_t index) const
	{
		return basic_color(_instance_flags_array[index].color);
	}

	body_visibility gl_instanced_static_mesh_body_data::get_visibility(std::size_t index) const
	{
		return body_visibility(_instance_flags_array[index].visibility);
	}
}

namespace gaea
{
	gl_instanced_static_mesh_body::gl_instanced_static_mesh_body()
		: _body_data{}
		, _instance_index{ std::size_t(-1) }
		, _update_transform_command_uid{ 0 }
	{
	}

	gl_instanced_static_mesh_body::gl_instanced_static_mesh_body(array<svector> const& vertex_coord_array, array<svector> const& vertex_normal_array, 
																 array<texcoord> const& vertex_uv_array, array<uint32> const& index_array,
																 transform const& model_matrix, 
	                                                             basic_color color, 
																 body_visibility visibility)
		: _body_data{ std::make_shared<implementation::gl_instanced_static_mesh_body_data>() }
		, _instance_index{ _body_data->add_instance(model_matrix, color, visibility) }
		, _update_transform_command_uid{ 0 }
	{
		_body_data->set_vertex_point_array(vertex_coord_array);
		_body_data->set_vertex_normal_array(vertex_normal_array);
		_body_data->set_vertex_uv_array(vertex_uv_array);
		_body_data->set_index_array(index_array);
	}

	gl_instanced_static_mesh_body::gl_instanced_static_mesh_body(std::shared_ptr<gl_instanced_static_mesh_body_handle> const& instance_handle, 
																 transform const& model_matrix,
																 basic_color color, 
																 body_visibility visibility)
		: _body_data{
			instance_handle
				? std::reinterpret_pointer_cast<implementation::gl_instanced_static_mesh_body_data>(instance_handle)
				: std::make_shared<implementation::gl_instanced_static_mesh_body_data>()
			}
		, _instance_index{ _body_data->add_instance(model_matrix, color, visibility) }
		, _update_transform_command_uid{ 0 }
	{
	}

	gl_instanced_static_mesh_body::~gl_instanced_static_mesh_body()
	{
		if (_body_data)
		{
			_body_data->remove_instance(_instance_index);
		}
	}

	void gl_instanced_static_mesh_body::setup_shader_path(std::string const& shader_path)
	{
		_body_data->setup_shader_path(shader_path);
	}

	void gl_instanced_static_mesh_body::setup_commands()
	{
		_body_data->setup_commands();
	}

	void gl_instanced_static_mesh_body::setdown_commands()
	{
		_body_data->setdown_commands();
	}

	void gl_instanced_static_mesh_body::bind_update_transform_callback(update_transform_callback_type&& callback)
	{
		if (callback)
		{
			_update_transform_command_uid = renderer->enqueue_render_command(
				[this, callback = std::move(callback)](void*)
				{
					_body_data->set_transform(callback(), _instance_index);
				},
				render_priority::above_normal,
				true
			);
		}
		else
		{
			renderer->dequeue_render_command(_update_transform_command_uid);
		}
	}

	void gl_instanced_static_mesh_body::set_vertex_point_array(array<svector> const& new_vertex_point_array)
	{
		_body_data->set_vertex_point_array(new_vertex_point_array);
	}

	void gl_instanced_static_mesh_body::set_vertex_normal_array(array<svector> const& new_vertex_normal_array)
	{
		_body_data->set_vertex_normal_array(new_vertex_normal_array);
	}

	void gl_instanced_static_mesh_body::set_vertex_uv_array(array<texcoord> const& new_vertex_uv_array)
	{
		_body_data->set_vertex_uv_array(new_vertex_uv_array);
	}

	void gl_instanced_static_mesh_body::set_index_array(array<uint32> const& new_index_array)
	{
		_body_data->set_index_array(new_index_array);
	}

	void gl_instanced_static_mesh_body::set_texture_image(std::shared_ptr<texture> const& new_texture_image)
	{
		_body_data->set_texture_image(new_texture_image);
	}

	void gl_instanced_static_mesh_body::set_transform(transform const& new_transform)
	{
		_body_data->set_transform(new_transform, _instance_index);
	}

	void gl_instanced_static_mesh_body::set_color(basic_color new_color)
	{
		_body_data->set_color(new_color, _instance_index);
	}

	void gl_instanced_static_mesh_body::set_visibility(body_visibility new_visibility)
	{
		_body_data->set_visibility(new_visibility, _instance_index);
	}

	array<float32> const& gl_instanced_static_mesh_body::get_vertex_array() const
	{
		return _body_data->get_vertex_array();
	}

	array<uint32> const& gl_instanced_static_mesh_body::get_index_array() const
	{
		return _body_data->get_index_array();
	}

	std::shared_ptr<texture> const& gl_instanced_static_mesh_body::get_texture_image() const
	{
		return _body_data->get_texture_image();
	}

	transform gl_instanced_static_mesh_body::get_transform() const
	{
		return _body_data->get_transform(_instance_index);
	}

	basic_color gl_instanced_static_mesh_body::get_color() const
	{
		return _body_data->get_color(_instance_index);
	}

	body_visibility gl_instanced_static_mesh_body::get_visibility() const
	{
		return _body_data->get_visibility(_instance_index);
	}

	std::shared_ptr<gl_instanced_static_mesh_body_handle> gl_instanced_static_mesh_body::instance_handle() const
	{
		return std::reinterpret_pointer_cast<gl_instanced_static_mesh_body_handle>(_body_data);
	}
}
