#ifndef ENGINE_GRAPHICS_GL_SHADER_PROGRAM_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_SHADER_PROGRAM_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/graphics/gl_shader.hpp"
#include "library/containers.hpp"

namespace gaea
{
	class gl_shader_program
	{
	public:
		explicit gl_shader_program();

		gl_shader_program(gl_shader_program const&) = delete;
		gl_shader_program& operator=(gl_shader_program const&) = delete;

		gl_shader_program(gl_shader_program&&) = delete;
		gl_shader_program& operator=(gl_shader_program&&) = delete;
		
		~gl_shader_program();
		
		bool load_shaders(std::string const& shader_dir);

		bool compile_shaders();

		array<gl_shader> const& list_shaders() const;

		//void add_shader(std::string const& shader_dir);
		//void remove_shader(uint32 shader_id);

		void activate();

		void deactivate();

		uint32 get_id() const;

		//bool needs_compile() const;
		//bool is_compiled() const;

		bool is_active() const;

	private:
		void _compile_shader(std::size_t thread_index, std::size_t thread_count);
		
		void _destroy();

		array<gl_shader> _shader_array;

		uint32 _id;

		uint8 _needs_compile : 1;
		uint8 _is_compiled : 1;

		static uint32 _active_program;

	};
}

#endif
