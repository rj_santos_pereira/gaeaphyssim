#ifndef ENGINE_GRAPHICS_GL_INSTANCED_STATIC_MESH_BODY_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_INSTANCED_STATIC_MESH_BODY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/graphics/gl_shader_program.hpp"
#include "engine/graphics/graphics_globals.hpp"
#include "engine/graphics/texture.hpp"
#include "library/containers.hpp"
#include "library/math/transform.hpp"
#include "library/math/vector.hpp"

namespace gaea::implementation
{	
	class gl_instanced_static_mesh_body_data
	{
		struct instance_flags
		{
			uint8 color : 4;
			uint8 visibility : 1;
			uint8 valid : 1;
		};
		
	public:
		explicit gl_instanced_static_mesh_body_data();

		std::size_t add_instance(transform const& model_mat, basic_color color, body_visibility visibility);
		void remove_instance(std::size_t index);

		void setup_shader_path(std::string const& shader_path);

		void setup_commands();
		
		void setdown_commands();

		void set_vertex_point_array(array<svector> const& new_vertex_point_array);
		
		void set_vertex_normal_array(array<svector> const& new_vertex_normal_array);

		void set_vertex_uv_array(array<texcoord> const& new_vertex_uv_array);

		void set_index_array(array<uint32> const& new_index_array);

		void set_texture_image(std::shared_ptr<texture> const& new_texture_image);

		void set_transform(transform const& new_transform, std::size_t index);

		void set_color(basic_color new_color, std::size_t index);

		void set_visibility(body_visibility new_visibility, std::size_t index);

		array<float32> const& get_vertex_array() const;

		array<uint32> const& get_index_array() const;

		std::shared_ptr<texture> const& get_texture_image() const;

		transform get_transform(std::size_t index) const;

		basic_color get_color(std::size_t index) const;

		body_visibility get_visibility(std::size_t index) const;

	private:
		// Per-body data
		array<float32> _vertex_array;
		array<uint32> _index_array;

		// Per-instance data
		array<texcolor> _color_array;
		array<transform> _transform_array; // local to world space
		array<transform> _normal_transform_array;

		array<instance_flags> _instance_flags_array;

		std::shared_ptr<texture> _texture_image;

		std::string _shader_path;
		gl_shader_program* _shader_program;

		uint64 _draw_command_uid;
		uint64 _update_texture_command_uid;

		uint32 _vao;
		uint32 _vbo[5]; // 0 - vertex (point, normal, uv) array
						// 1 - index array
						// 2 - color vector array
						// 3 - transform matrix array
						// 4 - normal transform matrix array
		uint32 _tid;

		std::size_t _instance_count;
		std::size_t _setup_count;
		
		uint8 _should_recreate_instance_buffers : 1;

	};
}

namespace gaea
{
	struct gl_instanced_static_mesh_body_handle{};
	
	class gl_instanced_static_mesh_body final : public base_static_mesh_body
	{
	public:
		using update_transform_callback_type = std::function<transform()>;

		explicit gl_instanced_static_mesh_body();
		explicit gl_instanced_static_mesh_body(array<svector> const& vertex_coord_array, array<svector> const& vertex_normal_array,
											   array<texcoord> const& vertex_uv_array, array<uint32> const& index_array,
											   transform const& model_matrix,
											   basic_color color, 
											   body_visibility visibility = body_visibility::visible);
		explicit gl_instanced_static_mesh_body(std::shared_ptr<gl_instanced_static_mesh_body_handle> const& instance_handle, 
											   transform const& model_matrix,
											   basic_color color,
											   body_visibility visibility = body_visibility::visible);
		~gl_instanced_static_mesh_body();

		virtual void setup_shader_path(std::string const& shader_path) override;

		virtual void setup_commands() override;

		virtual void setdown_commands() override;

		virtual void bind_update_transform_callback(update_transform_callback_type&& callback) override;

		virtual void set_vertex_point_array(array<svector> const& new_vertex_point_array) override;

		virtual void set_vertex_normal_array(array<svector> const& new_vertex_normal_array) override;

		virtual void set_vertex_uv_array(array<texcoord> const& new_vertex_uv_array) override;
		
		virtual void set_index_array(array<uint32> const& new_index_array) override;

		virtual void set_texture_image(std::shared_ptr<texture> const& new_texture_image) override;

		virtual void set_transform(transform const& new_transform) override;

		virtual void set_color(basic_color new_color) override;

		virtual void set_visibility(body_visibility new_visibility) override;

		virtual array<float32> const& get_vertex_array() const override;

		virtual array<uint32> const& get_index_array() const override;

		virtual std::shared_ptr<texture> const& get_texture_image() const override;
		
		virtual transform get_transform() const override;

		virtual basic_color get_color() const override;

		virtual body_visibility get_visibility() const override;

		std::shared_ptr<gl_instanced_static_mesh_body_handle> instance_handle() const;

	private:
		mutable std::shared_ptr<implementation::gl_instanced_static_mesh_body_data> _body_data;
		std::size_t _instance_index;

		uint64 _update_transform_command_uid;

	};
}

#endif
