#include "engine/graphics/texture.hpp"

#include "core/core_minimal.hpp"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ASSERT(expr) if (expr); else GAEA_DEBUG_BREAK()
#include "stb/stb_image.h"

namespace gaea
{
	texture::texture(uint8* data, int32 width, int32 height, int32 channels)
		: _data{ data }
		, _width{ width }
		, _height{ height }
		, _channels{ channels }
	{
	}

	uint8 const* texture::data() const
	{
		return _data;
	}

	int32 texture::width() const
	{
		return _width;
	}

	int32 texture::height() const
	{
		return _height;
	}

	int32 texture::channels() const
	{
		return _channels;
	}

	std::shared_ptr<texture> texture::load(std::string const& path, bool should_flip)
	{
		stbi_set_flip_vertically_on_load(should_flip);

		uint8* data;
		int32 width, height, channels;
		data = stbi_load(path.c_str(), &width, &height, &channels, 0);

		stbi_set_flip_vertically_on_load(false);

		texture* object = data ? new texture{ data, width, height, channels } : nullptr;
		auto deleter = [](texture* image) { if (image) { stbi_image_free(const_cast<uint8*>(image->data())); delete image; } };
		
		return std::shared_ptr<texture>{ object, deleter };
	}
}
