#ifndef ENGINE_GRAPHICS_GL_SHADER_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_SHADER_INCLUDE_GUARD

#include "core/core_minimal.hpp"
#include "core/core_opengl.hpp"

namespace gaea
{
	enum class gl_shader_type	: GLenum
	{
		null					= 0,
		vertex					= GL_VERTEX_SHADER,
		geometry				= GL_GEOMETRY_SHADER,
		fragment				= GL_FRAGMENT_SHADER,
	};

	class gl_shader
	{
	public:
		explicit gl_shader();
		explicit gl_shader(stdfs::path const& source_path, gl_shader_type type = gl_shader_type::null);

		gl_shader(gl_shader const&) = delete;
		gl_shader& operator=(gl_shader const&) = delete;

		gl_shader(gl_shader&& other) noexcept;
		gl_shader& operator=(gl_shader&& other) noexcept;

		~gl_shader();

		void reinit(stdfs::path const& source_path, gl_shader_type type = gl_shader_type::null);

		bool reload();

		bool compile();

		void attach(uint32 program_id);

		void detach(uint32 program_id);

		std::string get_name() const;

		GLuint get_id() const;

		gl_shader_type get_type() const;

		bool needs_compile() const;

		bool is_compiled() const;

	private:
		void _destroy();
		
		void _reset();

		stdfs::path _source_path;

		uint32 _id;
		GLenum _type;

		uint8 _needs_compile : 1;
		uint8 _is_compiled : 1;
		uint8 _is_attached : 1;

	};
}

#endif
