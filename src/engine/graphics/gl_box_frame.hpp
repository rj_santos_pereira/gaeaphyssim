#ifndef ENGINE_GRAPHICS_GL_BOX_FRAME_INCLUDE_GUARD
#define ENGINE_GRAPHICS_GL_BOX_FRAME_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/graphics/gl_shader_program.hpp"
#include "engine/graphics/texture.hpp"
#include "library/containers.hpp"
#include "library/math/transform.hpp"

namespace gaea::implementation
{
	class gl_box_frame_data
	{
		struct instance_flags
		{
			uint8 color : 4;
			uint8 visibility : 1;
			uint8 valid : 1;
		};

	public:
		explicit gl_box_frame_data();

		std::size_t add_instance(transform const& model_mat, basic_color color, body_visibility visibility);
		
		void remove_instance(std::size_t index);

		void setup_shader_path(std::string const& shader_path);

		void setup_commands();

		void setdown_commands();

		void set_transform(transform const& new_transform, std::size_t index);

		void set_color(basic_color new_color, std::size_t index);

		void set_visibility(body_visibility new_visibility, std::size_t index);

		transform get_transform(std::size_t index) const;

		basic_color get_color(std::size_t index) const;

		body_visibility get_visibility(std::size_t index) const;

	private:
		array<texcolor> _color_array;
		array<transform> _transform_array;

		array<instance_flags> _instance_flags_array;

		std::string _shader_path;
		gl_shader_program* _shader_program;

		uint64 _draw_command_uid;

		uint32 _vao;
		uint32 _vbo[4]; // 0 - vector array
						// 1 - index array
						// 2 - color vector array
						// 3 - transform matrix array

		std::size_t _instance_count;
		std::size_t _setup_count;

		uint8 _should_recreate_instance_buffers : 1;

	};
}

namespace gaea
{
	class gl_box_frame
	{
	public:
		using update_transform_callback_type = std::function<transform()>;

		explicit gl_box_frame();
		~gl_box_frame();

		gl_box_frame(gl_box_frame const&) = delete;
		gl_box_frame& operator=(gl_box_frame const&) = delete;

		gl_box_frame(gl_box_frame&&) = delete;
		gl_box_frame& operator=(gl_box_frame&&) = delete;

//		void reinit(std::shared_ptr<gl_box_frame_handle> const& instance_handle, transform const& model_matrix,
//			basic_color color, body_visibility visibility);

		void setup_shader_path(std::string const& shader_path);

		void setup_commands();

		void setdown_commands();

		void bind_update_transform_callback(update_transform_callback_type&& callback);

		void set_transform(transform const& new_transform);

		void set_color(basic_color new_color);

		void set_visibility(body_visibility new_visibility);

		transform get_transform() const;

		basic_color get_color() const;

		body_visibility get_visibility() const;

	private:
		static std::shared_ptr<implementation::gl_box_frame_data> _init_with_global_data();

		std::shared_ptr<implementation::gl_box_frame_data> _body_data;
		std::size_t _instance_index;

		uint64 _update_transform_command_uid;

		static std::weak_ptr<implementation::gl_box_frame_data> _global_body_data;

	};
}

#endif
