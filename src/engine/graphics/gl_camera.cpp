#include "engine/graphics/gl_camera.hpp"

#include "engine/graphics/glfw_renderer.hpp"

namespace gaea
{
	gl_camera::gl_camera(float32 frustum_field_of_view, float32 frustum_near_plane, float32 frustum_far_plane, 
						 bool is_fov_horizontal)
		: _position{ zero_svector }
		, _rotation{ identity_squaternion }
		, _view_mat{}
		, _proj_mat{}
		, _view_proj_mat{}
		, _frustum_field_of_view{ frustum_field_of_view }
		, _frustum_near_plane{ frustum_near_plane }
		, _frustum_far_plane{ frustum_far_plane }
		, _is_fov_horizontal{ is_fov_horizontal }
		, _update_view_proj_command_uid{ 0 }
		, _needs_recalc_view{ true }
		, _needs_recalc_proj{ true }
		, _is_setup{ false }
	{
	}

	void gl_camera::setup_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_camera, "Trying to setup camera but global renderer accessor is not initialized!");
		}

		if (_is_setup)
		{
			GAEA_ERROR(gl_static_body, "Trying to re-setup camera!");
			return;
		}

		_update_view_proj_command_uid = renderer->enqueue_render_command(
			[this](void* parameters)
			{
				auto command_params = static_cast<glfw_command_parameters*>(parameters);

				bool needs_recalc = _needs_recalc_view || _needs_recalc_proj;

				if (_needs_recalc_view)
				{
					_view_mat = _calculate_camera();
					_needs_recalc_view = false;
				}
				
				if (_needs_recalc_proj)
				{
					_proj_mat = _calculate_frustum();
					_needs_recalc_proj = false;
				}

				if (needs_recalc)
				{
					_view_proj_mat = _proj_mat * _view_mat;
				}

				command_params->view_proj_mat = _view_proj_mat;
				command_params->view_pos = _position;
			},
			render_priority::high,
			true
		);

		_is_setup = true;
	}

	void gl_camera::teardown_commands()
	{
		if (!renderer)
		{
			GAEA_FATAL(gl_camera, "Trying to reset camera but global renderer accessor is not initialized!");
		}
		
		if (_is_setup)
		{
			renderer->dequeue_render_command(_update_view_proj_command_uid);
		}

		_is_setup = false;
	}

	void gl_camera::translate_camera(svector const& translation)
	{
		_position += translation;
		_needs_recalc_view = true;
	}

	void gl_camera::rotate_camera(squaternion const& rotation)
	{
		_rotation *= rotation;
		_needs_recalc_view = true;
	}

	void gl_camera::set_camera_position(svector const& position)
	{
		_position = position;
		_needs_recalc_view = true;
	}

	void gl_camera::set_camera_rotation(squaternion const& rotation)
	{
		_rotation = rotation;
		_needs_recalc_view = true;
	}

	svector const& gl_camera::get_camera_position() const
	{
		return _position;
	}

	squaternion const& gl_camera::get_camera_rotation() const
	{
		return _rotation;
	}

	void gl_camera::adjust_frustum_field_of_view(float32 field_of_view)
	{
		_frustum_field_of_view = field_of_view;
		_needs_recalc_proj = true;
	}

	void gl_camera::adjust_frustum_near_plane(float32 near_plane)
	{
		_frustum_near_plane = near_plane;
		_needs_recalc_proj = true;
	}

	void gl_camera::adjust_frustum_far_plane(float32 far_plane)
	{
		_frustum_far_plane = far_plane;
		_needs_recalc_proj = true;
	}

	float32 gl_camera::frustum_field_of_view() const
	{
		return _frustum_field_of_view;
	}

	float32 gl_camera::frustum_near_plane() const
	{
		return _frustum_near_plane;
	}

	float32 gl_camera::frustum_far_plane() const
	{
		return _frustum_far_plane;
	}

	transform gl_camera::view_proj_matrix() const
	{
		return _view_proj_mat;
	}

	transform gl_camera::_calculate_camera() const
	{
		// Base change through (+0.5 -0.5i -0.5j -0.5k) * (+0.0 +0.0i +0.0j +1.0k) = (+0.5 -0.5i +0.5j +0.5k)
		return transform{ zero_svector, squaternion{ +0.5f, -0.5f, +0.5f, +0.5f } }
						.rotate(_rotation.reciprocal())
						.translate(-_position);
	}

	transform gl_camera::_calculate_frustum() const
	{
		// See https://www.songho.ca/opengl/gl_projectionmatrix.html
		// and http://www.songho.ca/opengl/gl_transform.html#example2
		
		auto gl_renderer = static_cast<glfw_renderer*>(renderer);

		if (!gl_renderer)
		{
			GAEA_ERROR(gl_camera, "Trying to calculate frustum but global renderer accessor is not initialized!");
			return transform{};
		}

		int32 window_width, window_height;
		gl_renderer->query_window_size(window_width, window_height);
		
		float32 tangent = std::tan(_frustum_field_of_view * 0.5f * deg2rad_f);
		float32 height;
		float32 width;

		if (_is_fov_horizontal)
		{
			width = _frustum_near_plane * tangent;
			height = width * (float32(window_height) / window_width);
		}
		else
		{
			height = _frustum_near_plane * tangent;
			width = height * (float32(window_width) / window_height);
		}

		auto left = -width;
		auto right = +width;
		auto bottom = -height;
		auto top = +height;
		auto near_plane = _frustum_near_plane;
		auto far_plane = _frustum_far_plane;

		// General frustum matrix
		return transform{ {
			2.f * near_plane / (right - left),								 0.f,							 (right + left) / (right - left),														 0.f,
										  0.f, 2.f * near_plane / (top - bottom),							 (top + bottom) / (top - bottom),														 0.f,
										  0.f,								 0.f, -1.f * (far_plane + near_plane) / (far_plane - near_plane), -2.f * (far_plane * near_plane) / (far_plane - near_plane),
										  0.f,								 0.f,														-1.f,														 0.f,
		} };
	}
}
