#include "engine/graphics/gl_shader_program.hpp"

#include "core/core_opengl.hpp"

#define SUPPORTS_PARALLEL_SHADER_COMPILATION 0

namespace gaea
{
	uint32 gl_shader_program::_active_program = 0;

	gl_shader_program::gl_shader_program()
		: _shader_array{}
		, _id{ 0 }
		, _needs_compile{ true }
		, _is_compiled{ false }
	{
		if (!engine->is_render_thread())
		{
			GAEA_FATAL(gl_shader_program, "Trying to create a shader program in a non-render thread!");
		}
		
		GAEA_INFO(gl_shader_program, "Creating shader program...");

		_id = glCreateProgram();
		if (_id == 0)
		{
			GAEA_ERROR(gl_shader_program, "Error creating shader program!");
			return;
		}

		GAEA_INFO(gl_shader_program, "Finished creating shader program");
	}

	gl_shader_program::~gl_shader_program()
	{
		_destroy();
	}

	bool gl_shader_program::load_shaders(std::string const& shader_dir)
	{
		GAEA_INFO(gl_shader_program, "Trying to load shader files from '%s'", shader_dir.c_str());

		stdfs::path shader_path{ shader_dir };

		if (shader_path.is_relative())
		{
			shader_path = engine->compose_engine_path(shader_path);
		}

		if (!exists(shader_path) || !is_directory(shader_path))
		{
			GAEA_WARN(gl_shader_program, "Failed to find directory with path '%s'", shader_path.string().c_str());
			return false;
		}

		stdfs::directory_iterator shader_files{ shader_path };
		for (auto const& shader_file : shader_files)
		{
			if (shader_file.is_regular_file())
			{
				gl_shader new_shader{ shader_file.path() };
				if (new_shader.get_id() != 0 && new_shader.reload())
				{
					_shader_array.push_back(std::move(new_shader));
					_needs_compile = true;
				}
			}
		}

		GAEA_INFO(gl_shader_program, "Loaded %llu shader files", _shader_array.size());

		return true;
	}

	bool gl_shader_program::compile_shaders()
	{
#if SUPPORTS_PARALLEL_SHADER_COMPILATION
		array<std::thread> thread_array;
		std::size_t thread_count = std::min<std::size_t>(_shader_array.size(), std::thread::hardware_concurrency());
		thread_array.reserve(thread_count);

		for (std::size_t thread_index = 0; thread_index < thread_count; ++thread_index)
		{
			thread_array.emplace_back(&gl_shader_program::_compile_shader, this, thread_index, thread_count);
		}
		
		for (auto& thread : thread_array)
		{
			thread.join();
		}
#endif

		for (auto& shader : _shader_array)
		{
#if !SUPPORTS_PARALLEL_SHADER_COMPILATION
			_compile_shader(0, 1);
#endif
			if (shader.is_compiled())
			{
				shader.attach(_id);
			}
			else
			{
				shader.detach(_id);
			}
		}

		GAEA_INFO(gl_shader_program, "Linking program...");

		glLinkProgram(_id);

		int32 status;
		glGetProgramiv(_id, GL_LINK_STATUS, &status);

		if (!status)
		{
			int32 buffer_size;
			glGetProgramiv(_id, GL_INFO_LOG_LENGTH, &buffer_size);

			array<char> buffer(buffer_size);
			glGetProgramInfoLog(_id, buffer_size, nullptr, buffer.data());

			GAEA_WARN(gl_shader_program, "Error linking program!\nLog: %s", buffer.data());
			return false;
		}

		_needs_compile = false;
		_is_compiled = true;

		return true;
	}

	array<gl_shader> const& gl_shader_program::list_shaders() const
	{
		return _shader_array;
	}

	void gl_shader_program::activate()
	{
		glUseProgram(_id);

		_active_program = _id;
	}

	void gl_shader_program::deactivate()
	{
		glUseProgram(0);
		
		_active_program = 0;
	}

	uint32 gl_shader_program::get_id() const
	{
		return _id;
	}

	bool gl_shader_program::is_active() const
	{
		return _active_program != 0 && _active_program == _id;
	}

	void gl_shader_program::_compile_shader(std::size_t thread_index, std::size_t thread_count)
	{
		for (std::size_t index = thread_index; index < _shader_array.size(); index += thread_count)
		{
			auto& shader = _shader_array[index];
			if (shader.needs_compile())
			{
				GAEA_INFO(gl_shader_program, "Compiling shader '%s'", shader.get_name().c_str());

				shader.compile();
			}
		}
	}

	void gl_shader_program::_destroy()
	{
		if (_id != 0)
		{
			for (auto& shader : _shader_array)
			{
				shader.detach(_id);
			}

			_shader_array.clear();

			glDeleteProgram(_id);
		}
	}

}
