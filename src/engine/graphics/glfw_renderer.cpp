#include "engine/graphics/glfw_renderer.hpp"

#include "engine/engine_config.hpp"
#include "library/utility.hpp"

namespace gaea
{
	glfw_renderer::glfw_renderer()
		: _render_thread{}
		, _render_condvar{}
		, _render_lock{}
		, _window_focus_callback_array{}
		, _window_close_callback_array{}
		, _event_key_button_callback_array{}
		, _event_mouse_button_callback_array{}
		, _event_mouse_drag_callback_array{}
		, _event_mouse_scroll_callback_array{}
		, _engine_render_command_array{}
		, _render_command_array{}
		, _debug_string{}
		, _window_ptr{ nullptr }
		, _basic_font_shader_program{ nullptr }
		, _basic_font{ nullptr }
		, _window_pos_x{ 0 }
		, _window_pos_y{ 32 }
		, _window_width{ 1280 }
		, _window_height{ 720 }
		, _frame_number{ 0 }
		, _transition_frame_count{ 5 }
		, _last_mouse_pos_x{ 0 }
		, _last_mouse_pos_y{ 0 }
		, _last_input_modifier_state{ input_modifier::modifier_NONE }
		, _last_window_pos_x{ 0 }
		, _last_window_pos_y{ 0 }
		, _last_window_width{ 0 }
		, _last_window_height{ 0 }
		, _has_init_glfw{ false }
		, _has_init_glad{ false }
		, _has_created_window{ false }
		, _has_created_thread{ false }
		, _should_run{ true }
		, _should_draw{ false }
		, _should_flush{ false }
		, _is_capturing_mouse{ false }
		, _needs_refresh_mouse_pos{ false }
		, _is_fullscreen_window{ false }
	{
	}

	bool glfw_renderer::initialize(void* parameters)
	{
		GAEA_UNUSED(parameters);

		GAEA_INFO(glfw_renderer, "Initializing GLFW [OpenGL 3.3]...");

		if (!glfwInit())
		{
			GAEA_ERROR(glfw_renderer, "Error initializing GLFW!");
			return false;
		}

		GAEA_INFO(glfw_renderer, "Finished initializing GLFW");

		_has_init_glfw = true;

		GAEA_INFO(glfw_renderer, "Creating GLFW main window...");

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
		
		_window_ptr = glfwCreateWindow(_window_width, _window_height, "GaeaPhysSim", nullptr, nullptr);
		if (_window_ptr == nullptr)
		{
			GAEA_ERROR(glfw_renderer, "Error creating GLFW window!");
			return false;
		}

		if (auto icon_tex = texture::load(config::path::default_engine_icon, false))
		{
			GLFWimage icon{ icon_tex->width(), icon_tex->height(), const_cast<uint8*>(icon_tex->data()) };
			glfwSetWindowIcon(_window_ptr, 1, &icon);
		}

		glfwGetWindowPos(_window_ptr, &_window_pos_x, &_window_pos_y);

		glfwSetWindowIconifyCallback(_window_ptr, &glfw_renderer::_redirect_on_window_minimize);
		glfwSetWindowMaximizeCallback(_window_ptr, &glfw_renderer::_redirect_on_window_maximize);
		glfwSetWindowFocusCallback(_window_ptr, &glfw_renderer::_redirect_on_window_focus);
		glfwSetWindowCloseCallback(_window_ptr, &glfw_renderer::_redirect_on_window_close);
		glfwSetWindowPosCallback(_window_ptr, &glfw_renderer::_redirect_on_window_move);
		glfwSetWindowSizeCallback(_window_ptr, &glfw_renderer::_redirect_on_window_resize);

		glfwSetKeyCallback(_window_ptr, &glfw_renderer::_redirect_on_key_button);
		glfwSetMouseButtonCallback(_window_ptr, &glfw_renderer::_redirect_on_mouse_button);
		glfwSetCursorPosCallback(_window_ptr, &glfw_renderer::_redirect_on_mouse_drag);
		glfwSetScrollCallback(_window_ptr, &glfw_renderer::_redirect_on_mouse_scroll);

		glfwSetInputMode(_window_ptr, GLFW_LOCK_KEY_MODS, GLFW_TRUE);
		if (glfwRawMouseMotionSupported())
		{
			glfwSetInputMode(_window_ptr, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
		}

		GAEA_INFO(glfw_renderer, "Finished creating GLFW main window");

		_has_created_window = true;
		
		// Acquire context to enable us to use GLAD to load OpenGL functions
		glfwMakeContextCurrent(_window_ptr);

		GAEA_INFO(glfw_renderer, "Initializing GLAD...");

		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
		{
			GAEA_ERROR(glfw_renderer, "Error initializing GLAD!");
			return false;
		}

		GAEA_INFO(glfw_renderer, "Finished initializing GLAD");

		_has_init_glad = true;

		// Release context so that render thread can make it current on the main window
		glfwMakeContextCurrent(nullptr);
		
		GAEA_INFO(glfw_renderer, "Creating render thread...");

		std::unique_lock<std::mutex> lock{ _render_lock };

		_render_thread = std::thread{ &glfw_renderer::_rendering_loop, this };
		if (_render_thread.get_id() == std::thread::id{})
		{
			GAEA_ERROR(glfw_renderer, "Error creating render thread!");
			return false;
		}

		_has_created_thread = true;

		return true;
	}

	void glfw_renderer::finalize()
	{
		{
			std::unique_lock<std::mutex> lock{ _render_lock };

			_should_run = false;
		}

		if (_has_created_thread)
		{
			GAEA_INFO(glfw_renderer, "Destroying render thread...");
			
			_render_condvar.notify_one();
			_render_thread.join();
		}

		if (_has_created_window)
		{
			GAEA_INFO(glfw_renderer, "Destroying GLFW window...");

			glfwDestroyWindow(_window_ptr);

			_window_ptr = nullptr;
		}

		if (_has_init_glfw)
		{
			GAEA_INFO(glfw_renderer, "Terminating GLFW...");
			
			glfwTerminate();
		}

		_basic_font.reset();
	}

	void glfw_renderer::draw(uint64 frame_number)
	{
		GAEA_UNUSED(frame_number);
		
		std::unique_lock<std::mutex> lock{ _render_lock };
		auto pred = [this] () -> bool { return !_should_draw && !_should_flush; };

		_should_draw = true;
		_should_flush = true;
		
		_render_condvar.notify_one();
		_render_condvar.wait(lock, pred);
	}

	void glfw_renderer::flush()
	{
		std::unique_lock<std::mutex> lock{ _render_lock };
		auto pred = [this]() -> bool { return !_should_flush; };

		_should_flush = true;
		
		_render_condvar.notify_one();
		_render_condvar.wait(lock, pred);
	}

	uint64 glfw_renderer::enqueue_render_command(render_function_type&& function, render_priority priority, bool is_persistent)
	{
		auto it = _render_command_array.end();
		auto command = implementation::render_command{ 0, std::move(function), uint8(priority), is_persistent };

		uint32 uid;
		do
		{
			uid = (uint32(priority) << 28) | (uint32(engine->make_unique_id()) & (1 << 28) - 1);

			it = std::lower_bound(_render_command_array.begin(), _render_command_array.end(), uid,
				[](implementation::render_command const& command, uint32 uid) { return command.uid < uid; });
		}
		while (it != _render_command_array.end() && it->uid == uid);

		command.uid = uid;
		_render_command_array.insert(it, std::move(command));
		
		return uid;
	}

	void glfw_renderer::dequeue_render_command(uint64 uid)
	{
		if (uid)
		{
			auto it = std::lower_bound(_render_command_array.begin(), _render_command_array.end(), uint32(uid),
				[](implementation::render_command const& command, uint32 uid) { return command.uid < uid; });
			if (it != _render_command_array.end() && it->uid == uid)
			{
				_render_command_array.erase(it);
			}
		}
	}

	void glfw_renderer::draw_debug_string(std::string&& message, basic_color color)
	{
		char color_char;
		std::to_chars(&color_char, &color_char + 1, uint8(color), 16);
		_debug_string.message += std::string{ &color_char, 1 } += std::move(message) += char(127);
	}

	void glfw_renderer::query_window_size(int32& width, int32& height) const
	{
		width = _window_width;
		height = _window_height;
	}
	
	void glfw_renderer::handle_events()
	{
		glfwPollEvents();
	}

	void glfw_renderer::toggle_mouse_capture()
	{
		_is_capturing_mouse = !_is_capturing_mouse;
		glfwSetInputMode(_window_ptr, GLFW_CURSOR, _is_capturing_mouse ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
	}

	void glfw_renderer::toggle_fullscreen()
	{
		int32 monitor_count;
		GLFWmonitor** monitor_list = glfwGetMonitors(&monitor_count);

		if (!monitor_count)
		{
			GAEA_ERROR(glfw_renderer, "No monitor detected!");
			return;
		}
		
		_is_fullscreen_window = !_is_fullscreen_window;

		GLFWmonitor* new_monitor = nullptr;
		
		int32 new_pos_x = _last_window_pos_x;
		int32 new_pos_y = _last_window_pos_y;
		
		int32 new_width = _last_window_width;
		int32 new_height = _last_window_height;
		
		if (_is_fullscreen_window)
		{
			int32 window_center_x = _window_pos_x + _window_width / 2;
			int32 window_center_y = _window_pos_y + _window_height / 2;

			for (int32 idx = 0; idx < monitor_count; ++idx) {
				auto monitor = monitor_list[idx];
				auto mode = glfwGetVideoMode(monitor);

				int32 monitor_pos_x, monitor_pos_y;
				glfwGetMonitorPos(monitor, &monitor_pos_x, &monitor_pos_y);

				if (window_center_x == std::clamp(window_center_x, monitor_pos_x, monitor_pos_x + mode->width) &&
					window_center_y == std::clamp(window_center_y, monitor_pos_y, monitor_pos_y + mode->height))
				{
					_last_window_pos_x = _window_pos_x;
					_last_window_pos_y = _window_pos_y;

					_last_window_width = _window_width;
					_last_window_height = _window_height;
					
					new_monitor = monitor;

					new_pos_x = monitor_pos_x;
					new_pos_y = monitor_pos_y;
					
					new_width = mode->width;
					new_height = mode->height;

					break;
				}
			}
		}
		
		glfwSetWindowMonitor(_window_ptr, new_monitor, new_pos_x, new_pos_y, new_width, new_height, GLFW_DONT_CARE);

		_transition_frame_count = 5;
	}

	bool glfw_renderer::is_transitioning() const
	{
		return _transition_frame_count > 0;
	}

	bool glfw_renderer::is_capturing_mouse() const
	{
		return _is_capturing_mouse;
	}

	bool glfw_renderer::is_fullscreen() const
	{
		return _is_fullscreen_window;
	}

	void glfw_renderer::register_window_minimize_callback(window_minimize_callback_type&& callback)
	{
		_window_minimize_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_window_maximize_callback(window_maximize_callback_type&& callback)
	{
		_window_maximize_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_window_focus_callback(window_focus_callback_type&& callback)
	{
		_window_focus_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_window_close_callback(window_close_callback_type&& callback)
	{
		_window_close_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_window_move_callback(window_move_callback_type&& callback)
	{
		_window_move_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_window_resize_callback(window_resize_callback_type&& callback)
	{
		_window_resize_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_event_key_button_callback(event_key_button_callback_type&& callback)
	{
		_event_key_button_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_event_mouse_button_callback(event_mouse_button_callback_type&& callback)
	{
		_event_mouse_button_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_event_mouse_drag_callback(event_mouse_drag_callback_type&& callback)
	{
		_event_mouse_drag_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::register_event_mouse_scroll_callback(event_mouse_scroll_callback_type&& callback)
	{
		_event_mouse_scroll_callback_array.push_back(std::move(callback));
	}

	void glfw_renderer::_rendering_loop()
	{
		engine->register_thread(thread_type::render);
		
		std::unique_lock<std::mutex> lock{ _render_lock };
		auto pred = [this]() -> bool { return !_should_run || _should_draw || _should_flush; };
		
		glfwMakeContextCurrent(_window_ptr);
		
		// On Windows, frame rate may show occasional stutters due to forced v-sync by DWM
		// https://stackoverflow.com/questions/30293074/opengl-tearing-with-fullscreen-native-resolution
		// https://docs.microsoft.com/en-us/windows/win32/dwm/dwm-overview
		glfwSwapInterval(0);

		// See: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glViewport.xhtml
		glViewport(0, 0, _window_width, _window_height);
		
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		_init_debug_messages();

		while (_should_run)
		{
			_render_condvar.notify_one();
			_render_condvar.wait(lock, pred);

			if (_should_flush)
			{
				glfw_command_parameters command_parameters;

				if (_transition_frame_count > 0)
				{
					--_transition_frame_count;
				}
				
				if (_should_draw)
				{
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					glClearColor(0.00f, 0.00f, 0.00f, 1.0f);
				}

				// Run engine commands first
				{
					auto it = _engine_render_command_array.begin();
					auto it_end = _engine_render_command_array.end();

					while (it != it_end)
					{
						// Check if we should be drawing, otherwise execute only if it is not a persistent command
						if (_should_draw || !it->is_persistent)
						{
							it->function(&command_parameters);
						}

						if (it->is_persistent)
						{
							++it;
						}
						else
						{
							it = _engine_render_command_array.erase(it);
							it_end = _engine_render_command_array.end();
						}
					}
				}

				// Run non-engine commands next
				{
					auto it = _render_command_array.begin();
					auto it_end = _render_command_array.end();

					while (it != it_end)
					{
						// Check if we should be drawing, otherwise execute only if it is not a persistent command
						if (_should_draw || !it->is_persistent)
						{
							it->function(&command_parameters);
						}

						if (it->is_persistent)
						{
							++it;
						}
						else
						{
							it = _render_command_array.erase(it);
							it_end = _render_command_array.end();
						}
					}
				}

				// Clear command parameters before finishing
				command_parameters = glfw_command_parameters{};

				if (_should_draw)
				{
					// Draw debug messages last
					_render_debug_messages();

					// Swap buffers and present scene
					glfwSwapBuffers(_window_ptr);

					// Advance frame number only when we draw
					++_frame_number;
					_should_draw = false;
				}
				
				_should_flush = false;
			}
		}

		_term_debug_messages();

		glfwMakeContextCurrent(nullptr);
	}

	void glfw_renderer::_init_debug_messages()
	{
		GAEA_INFO(glfw_renderer, "Loading basic font shader program...");

		_basic_font_shader_program = new gl_shader_program;
		_basic_font_shader_program->load_shaders(config::path::basic_font_shaders);

		if (!_basic_font_shader_program->compile_shaders())
		{
			GAEA_WARN(glfw_renderer, "Error loading shader program [ID: %u]", _basic_font_shader_program->get_id());
			return;
		}

		GAEA_INFO(glfw_renderer, "Finished loading basic font shader programs");

		GAEA_INFO(glfw_renderer, "Loading basic font...");

		_basic_font = texture::load(engine->compose_engine_path(stdfs::path{ config::path::basic_font_texture }).string());

		if (!_basic_font)
		{
			GAEA_WARN(glfw_renderer, "Error loading basic font");
			return;
		}

		GAEA_INFO(glfw_renderer, "Finished loading basic font");
		
		glGenVertexArrays(1, &_debug_string.vao);
		glGenBuffers(2, _debug_string.vbo);
		glGenTextures(1, &_debug_string.tbo);
		
		constexpr uint32 stride = 4;

		constexpr uint32 vert_coords_offset = 0;
		constexpr uint32 tex_coords_offset = 2;

		constexpr uint32 index_array[6]
		{
			0, 1, 2,
			3, 2, 1,
		};

		// Bind the VAO, vertex and element buffer objects, along with the texture
		glBindVertexArray(_debug_string.vao);
		glBindBuffer(GL_ARRAY_BUFFER, _debug_string.vbo[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _debug_string.vbo[1]);
		glBindTexture(GL_TEXTURE_2D, _debug_string.tbo);

		// Index data is immutable, so we'll set it up here once
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof index_array, index_array, GL_STATIC_DRAW);
		
		GLenum format = _basic_font->channels() == 3 ? GL_RGB : GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _basic_font->width(), _basic_font->height(), 0, format, GL_UNSIGNED_BYTE, _basic_font->data());

		// Configure the vertex attributes
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride * sizeof(float32), reinterpret_cast<void*>(vert_coords_offset * sizeof(float32)));

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride * sizeof(float32), reinterpret_cast<void*>(tex_coords_offset * sizeof(float32)));
		
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindVertexArray(0);
	}

	void glfw_renderer::_render_debug_messages()
	{
		// Let's do an early exit if we have invalid font data or empty debug string
		if (!_basic_font || _debug_string.message.empty())
		{
			return;
		}
		
		constexpr uint32 stride = 4;

		constexpr uint32 vert_coords_offset = 0;
		constexpr uint32 tex_coords_offset = 2;

		float32 vert_x_step = 28.f / float32(_window_width);
		float32 vert_y_step = 32.f / float32(_window_height);

		float32 tex_x_step = 14.f / 224.f;
		float32 tex_y_step = 16.f / 224.f;

		float32 vertex_array[16]
		{   // Vert coords								// Tex coords
			-1.f + vert_x_step, +1.f,					0.f, 0.f,
			-1.f, +1.f,									0.f, 0.f,
			-1.f + vert_x_step, +1.f - vert_y_step,		0.f, 0.f,
			-1.f, +1.f - vert_y_step,					0.f, 0.f,
		};

		// First activate the shader program
		_basic_font_shader_program->activate();

		glDisable(GL_DEPTH_TEST);

		glBindVertexArray(_debug_string.vao);
		// We need to bind this buffer before we can change the data
		glBindBuffer(GL_ARRAY_BUFFER, _debug_string.vbo[0]);
		glBindTexture(GL_TEXTURE_2D, _debug_string.tbo);

		// We store the texture parameters before overriding them so we can restore them once we are done
		int32 tex_wrap_s, tex_wrap_t;
		glGetTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, &tex_wrap_s);
		glGetTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, &tex_wrap_t);

		int32 tex_min_filter, tex_mag_filter;
		glGetTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, &tex_min_filter);
		glGetTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, &tex_mag_filter);

		float32 old_tex_bor_color[4];
		glGetTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, old_tex_bor_color);

		// Override the parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		float32 new_tex_bor_color[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, new_tex_bor_color);

		bool new_message = true;
		uint32 character_position = 4;
		
		char const* message_ptr = _debug_string.message.c_str();
		while (char character = *message_ptr++)
		{
			if (new_message)
			{
				uint8 color_number;
				std::from_chars(&character, &character + 1, color_number, 16);
				auto color_tuple = util::color_to_tuple(basic_color(color_number));
				svector color{ std::get<0>(color_tuple), std::get<1>(color_tuple), std::get<2>(color_tuple) };

				auto color_uniform = glGetUniformLocation(_basic_font_shader_program->get_id(), "u_color");
				glUniform3fv(color_uniform, 1, color.data());
				
				new_message = false;
				continue;
			}

			new_message = character == char(127);
			
			if (character == '\n' || new_message)
			{
				vertex_array[stride * 0 + vert_coords_offset + 0] = -1.f + vert_x_step;
				vertex_array[stride * 0 + vert_coords_offset + 1] -= vert_y_step;
				vertex_array[stride * 1 + vert_coords_offset + 0] = -1.f;
				vertex_array[stride * 1 + vert_coords_offset + 1] -= vert_y_step;
				vertex_array[stride * 2 + vert_coords_offset + 0] = -1.f + vert_x_step;
				vertex_array[stride * 2 + vert_coords_offset + 1] -= vert_y_step;
				vertex_array[stride * 3 + vert_coords_offset + 0] = -1.f;
				vertex_array[stride * 3 + vert_coords_offset + 1] -= vert_y_step;

				character_position = 0;

				continue;
			}

			if (character == '\t')
			{
				uint32 character_offset = 4 - character_position % 4;
				
				vertex_array[stride * 0 + vert_coords_offset + 0] += character_offset * vert_x_step;
				vertex_array[stride * 1 + vert_coords_offset + 0] += character_offset * vert_x_step;
				vertex_array[stride * 2 + vert_coords_offset + 0] += character_offset * vert_x_step;
				vertex_array[stride * 3 + vert_coords_offset + 0] += character_offset * vert_x_step;

				character_position += character_offset;

				continue;
			}

			vertex_array[stride * 0 + vert_coords_offset + 0] += vert_x_step;
			vertex_array[stride * 1 + vert_coords_offset + 0] += vert_x_step;
			vertex_array[stride * 2 + vert_coords_offset + 0] += vert_x_step;
			vertex_array[stride * 3 + vert_coords_offset + 0] += vert_x_step;

			++character_position;

			int char_index = character - ' ';
			int row_index = char_index / 16;
			int col_index = char_index % 16;

			vertex_array[stride * 0 + tex_coords_offset + 0] = float32(col_index + 1) * tex_x_step;
			vertex_array[stride * 0 + tex_coords_offset + 1] = 1.f - float32(row_index) * tex_y_step;
			vertex_array[stride * 1 + tex_coords_offset + 0] = float32(col_index) * tex_x_step;
			vertex_array[stride * 1 + tex_coords_offset + 1] = 1.f - float32(row_index) * tex_y_step;
			vertex_array[stride * 2 + tex_coords_offset + 0] = float32(col_index + 1) * tex_x_step;
			vertex_array[stride * 2 + tex_coords_offset + 1] = 1.f - float32(row_index + 1) * tex_y_step;
			vertex_array[stride * 3 + tex_coords_offset + 0] = float32(col_index) * tex_x_step;
			vertex_array[stride * 3 + tex_coords_offset + 1] = 1.f - float32(row_index + 1) * tex_y_step;

			glBufferData(GL_ARRAY_BUFFER, sizeof vertex_array, vertex_array, GL_STREAM_DRAW);

			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, reinterpret_cast<void*>(0));
		}

		// Now restore texture parameters before finishing
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, tex_wrap_s);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tex_wrap_t);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, tex_min_filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, tex_mag_filter);

		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, old_tex_bor_color);

		// Unbind the texture and the VAO
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindVertexArray(0);

		glEnable(GL_DEPTH_TEST);

		// Now deactivate the shader program
		_basic_font_shader_program->deactivate();

		// Lastly, clear the message data
		_debug_string.message.clear();
	}

	void glfw_renderer::_term_debug_messages()
	{
		glDeleteTextures(1, &_debug_string.tbo);
		glDeleteBuffers(2, _debug_string.vbo);
		glDeleteVertexArrays(1, &_debug_string.vao);

		delete _basic_font_shader_program;
	}

	void glfw_renderer::_on_window_minimize(GLFWwindow*, int minimized)
	{
		std::for_each(_window_minimize_callback_array.begin(), _window_minimize_callback_array.end(), 
			[minimized](auto&& callback) { callback(minimized); });
	}

	void glfw_renderer::_on_window_maximize(GLFWwindow*, int maximized)
	{
		std::for_each(_window_maximize_callback_array.begin(), _window_maximize_callback_array.end(), 
			[maximized](auto&& callback) { callback(maximized); });
	}

	void glfw_renderer::_on_window_focus(GLFWwindow*, int focused)
	{
		// Changing focus when in fullscreen is much more time consuming, so let's make it a transition
		if (_is_fullscreen_window)
		{
			_transition_frame_count = 5;
		}
		
		std::for_each(_window_focus_callback_array.begin(), _window_focus_callback_array.end(), 
			[focused](auto&& callback) { callback(focused); });
	}

	void glfw_renderer::_on_window_close(GLFWwindow*)
	{
		std::for_each(_window_close_callback_array.begin(), _window_close_callback_array.end(), 
			[](auto&& callback) { callback(); });
	}

	void glfw_renderer::_on_window_move(GLFWwindow*, int xpos, int ypos)
	{
		_window_pos_x = xpos;
		_window_pos_y = ypos;

		std::for_each(_window_move_callback_array.begin(), _window_move_callback_array.end(), 
			[xpos, ypos](auto&& callback) { callback(xpos, ypos); });
	}

	void glfw_renderer::_on_window_resize(GLFWwindow*, int width, int height)
	{
		_window_width = width;
		_window_height = height;

		auto it = std::lower_bound(_engine_render_command_array.begin(), _engine_render_command_array.end(), 
			_engine_window_resize_command_id, 
			[](implementation::render_command const& command, uint32 uid) { return command.uid < uid; });
		if (it == _engine_render_command_array.end())
		{
			_engine_render_command_array.insert(it,
				implementation::render_command{
					_engine_window_resize_command_id,
					[this](void*)
					{
						glViewport(0, 0, _window_width, _window_height);
					},
					uint8(render_priority::critical),
					false
				}
			);
		}
		
		std::for_each(_window_resize_callback_array.begin(), _window_resize_callback_array.end(), 
			[width, height](auto&& callback) { callback(width, height); });
	}

	void glfw_renderer::_on_key_button(GLFWwindow*, int key, int scancode, int action, int mods)
	{
		GAEA_UNUSED(scancode);
		
		auto invoke_callback = 
			[this](input_key key, input_state state, input_modifier modifier)
			{
				std::for_each(_event_key_button_callback_array.begin(), _event_key_button_callback_array.end(),
					[key, state, modifier](auto&& callback) { callback(key, state, modifier); });
			};
		
		auto map_event = 
			[](int action) -> input_state
			{
				switch (action)
				{
				case GLFW_RELEASE:
					return input_state::state_released;
				case GLFW_PRESS:
					return input_state::state_pressed;
				case GLFW_REPEAT:
					return input_state::state_repeating;
				default:
					return input_state::state_UNKNOWN;
				}
			};
		
		auto map_modifier = 
			[](int mods) -> input_modifier
			{
				uint8 flag = 0x00;
				if (mods & GLFW_MOD_SHIFT)
				{
					flag |= uint8(input_modifier::modifier_shift);
				}
				if (mods & GLFW_MOD_CONTROL)
				{
					flag |= uint8(input_modifier::modifier_ctrl);
				}
				if (mods & GLFW_MOD_ALT)
				{
					flag |= uint8(input_modifier::modifier_alt);
				}
				if (mods & GLFW_MOD_CAPS_LOCK)
				{
					flag |= uint8(input_modifier::modifier_caps);
				}
				if (mods & GLFW_MOD_NUM_LOCK)
				{
					flag |= uint8(input_modifier::modifier_num);
				}
				if (mods & GLFW_MOD_SUPER)
				{
					flag |= uint8(input_modifier::modifier_UNKNOWN);
				}
				return input_modifier(flag);
			};

		_last_input_modifier_state = map_modifier(mods);
		
		switch (key)
		{
		case GLFW_KEY_UNKNOWN:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_SPACE:
			return invoke_callback(input_key::key_space, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_APOSTROPHE:
		case GLFW_KEY_COMMA:
		case GLFW_KEY_MINUS:
		case GLFW_KEY_PERIOD:
		case GLFW_KEY_SLASH:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_0:
			return invoke_callback(input_key::key_0, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_1:
			return invoke_callback(input_key::key_1, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_2:
			return invoke_callback(input_key::key_2, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_3:
			return invoke_callback(input_key::key_3, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_4:
			return invoke_callback(input_key::key_4, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_5:
			return invoke_callback(input_key::key_5, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_6:
			return invoke_callback(input_key::key_6, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_7:
			return invoke_callback(input_key::key_7, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_8:
			return invoke_callback(input_key::key_8, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_9:
			return invoke_callback(input_key::key_9, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_SEMICOLON:
		case GLFW_KEY_EQUAL:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_A:
			return invoke_callback(input_key::key_A, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_B:
			return invoke_callback(input_key::key_B, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_C:
			return invoke_callback(input_key::key_C, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_D:
			return invoke_callback(input_key::key_D, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_E:
			return invoke_callback(input_key::key_E, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F:
			return invoke_callback(input_key::key_F, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_G:
			return invoke_callback(input_key::key_G, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_H:
			return invoke_callback(input_key::key_H, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_I:
			return invoke_callback(input_key::key_I, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_J:
			return invoke_callback(input_key::key_J, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_K:
			return invoke_callback(input_key::key_K, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_L:
			return invoke_callback(input_key::key_L, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_M:
			return invoke_callback(input_key::key_M, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_N:
			return invoke_callback(input_key::key_N, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_O:
			return invoke_callback(input_key::key_O, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_P:
			return invoke_callback(input_key::key_P, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_Q:
			return invoke_callback(input_key::key_Q, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_R:
			return invoke_callback(input_key::key_R, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_S:
			return invoke_callback(input_key::key_S, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_T:
			return invoke_callback(input_key::key_T, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_U:
			return invoke_callback(input_key::key_U, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_V:
			return invoke_callback(input_key::key_V, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_W:
			return invoke_callback(input_key::key_W, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_X:
			return invoke_callback(input_key::key_X, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_Y:
			return invoke_callback(input_key::key_Y, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_Z:
			return invoke_callback(input_key::key_Z, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_LEFT_BRACKET:
		case GLFW_KEY_BACKSLASH:
		case GLFW_KEY_RIGHT_BRACKET:
		case GLFW_KEY_GRAVE_ACCENT:
		case GLFW_KEY_WORLD_1:
		case GLFW_KEY_WORLD_2:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_ESCAPE:
			return invoke_callback(input_key::key_escape, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_ENTER:
			return invoke_callback(input_key::key_enter, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_TAB:
			return invoke_callback(input_key::key_tab, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_BACKSPACE:
			return invoke_callback(input_key::key_backspace, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_INSERT:
			return invoke_callback(input_key::key_insert, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_DELETE:
			return invoke_callback(input_key::key_delete, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_RIGHT:
			return invoke_callback(input_key::key_right, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_LEFT:
			return invoke_callback(input_key::key_left, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_DOWN:
			return invoke_callback(input_key::key_down, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_UP:
			return invoke_callback(input_key::key_up, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_PAGE_UP:
			return invoke_callback(input_key::key_page_up, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_PAGE_DOWN:
			return invoke_callback(input_key::key_page_down, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_HOME:
			return invoke_callback(input_key::key_home, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_END:
			return invoke_callback(input_key::key_end, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_CAPS_LOCK:
			return invoke_callback(input_key::key_caps_lock, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_SCROLL_LOCK:
			return invoke_callback(input_key::key_scroll_lock, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_NUM_LOCK:
			return invoke_callback(input_key::key_num_lock, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_PRINT_SCREEN:
			return invoke_callback(input_key::key_print_screen, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_PAUSE:
			return invoke_callback(input_key::key_pause, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F1:
			return invoke_callback(input_key::key_f1, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F2:
			return invoke_callback(input_key::key_f2, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F3:
			return invoke_callback(input_key::key_f3, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F4:
			return invoke_callback(input_key::key_f4, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F5:
			return invoke_callback(input_key::key_f5, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F6:
			return invoke_callback(input_key::key_f6, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F7:
			return invoke_callback(input_key::key_f7, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F8:
			return invoke_callback(input_key::key_f8, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F9:
			return invoke_callback(input_key::key_f9, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F10:
			return invoke_callback(input_key::key_f10, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F11:
			return invoke_callback(input_key::key_f11, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F12:
			return invoke_callback(input_key::key_f12, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_F13:
		case GLFW_KEY_F14:
		case GLFW_KEY_F15:
		case GLFW_KEY_F16:
		case GLFW_KEY_F17:
		case GLFW_KEY_F18:
		case GLFW_KEY_F19:
		case GLFW_KEY_F20:
		case GLFW_KEY_F21:
		case GLFW_KEY_F22:
		case GLFW_KEY_F23:
		case GLFW_KEY_F24:
		case GLFW_KEY_F25:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_KP_0:
			return invoke_callback(input_key::key_numpad_0, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_1:
			return invoke_callback(input_key::key_numpad_1, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_2:
			return invoke_callback(input_key::key_numpad_2, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_3:
			return invoke_callback(input_key::key_numpad_3, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_4:
			return invoke_callback(input_key::key_numpad_4, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_5:
			return invoke_callback(input_key::key_numpad_5, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_6:
			return invoke_callback(input_key::key_numpad_6, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_7:
			return invoke_callback(input_key::key_numpad_7, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_8:
			return invoke_callback(input_key::key_numpad_8, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_9:
			return invoke_callback(input_key::key_numpad_9, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_DECIMAL:
			return invoke_callback(input_key::key_numpad_sep, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_DIVIDE:
			return invoke_callback(input_key::key_numpad_div, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_MULTIPLY:
			return invoke_callback(input_key::key_numpad_mul, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_SUBTRACT:
			return invoke_callback(input_key::key_numpad_sub, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_ADD:
			return invoke_callback(input_key::key_numpad_add, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_ENTER:
			return invoke_callback(input_key::key_numpad_enter, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_KP_EQUAL:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_LEFT_SHIFT:
			return invoke_callback(input_key::key_left_shift, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_LEFT_CONTROL:
			return invoke_callback(input_key::key_left_ctrl, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_LEFT_ALT:
			return invoke_callback(input_key::key_left_alt, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_LEFT_SUPER:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_RIGHT_SHIFT:
			return invoke_callback(input_key::key_right_shift, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_RIGHT_CONTROL:
			return invoke_callback(input_key::key_right_ctrl, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_RIGHT_ALT:
			return invoke_callback(input_key::key_right_alt, map_event(action), _last_input_modifier_state);
		case GLFW_KEY_RIGHT_SUPER:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_KEY_MENU:
			return invoke_callback(input_key::key_menu, map_event(action), _last_input_modifier_state);
		default:
			invoke_callback(input_key::key_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		}

		// TODO invoke specific callback for unknown event
	}

	void glfw_renderer::_on_mouse_button(GLFWwindow*, int button, int action, int)
	{
		if (!_is_capturing_mouse)
		{
			return;
		}
		
		auto invoke_callback =
			[this](input_mouse mouse, input_state state, input_modifier modifier)
			{
				std::for_each(_event_mouse_button_callback_array.begin(), _event_mouse_button_callback_array.end(),
					[mouse, state, modifier](auto&& callback) { callback(mouse, state, modifier); });
			};

		auto map_event = 
			[](int action)
			{
				switch (action)
				{
				case GLFW_RELEASE:
					return input_state::state_released;
				case GLFW_PRESS:
					return input_state::state_pressed;
				case GLFW_REPEAT:
					return input_state::state_repeating;
				default:
					return input_state::state_UNKNOWN;
				}
			};

		switch (button)
		{
//      case GLFW_MOUSE_BUTTON_1:
//      case GLFW_MOUSE_BUTTON_2:
//      case GLFW_MOUSE_BUTTON_3:
		case GLFW_MOUSE_BUTTON_4:
		case GLFW_MOUSE_BUTTON_5:
		case GLFW_MOUSE_BUTTON_6:
		case GLFW_MOUSE_BUTTON_7:
//      case GLFW_MOUSE_BUTTON_8:
		case GLFW_MOUSE_BUTTON_LAST:
			invoke_callback(input_mouse::mouse_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		case GLFW_MOUSE_BUTTON_LEFT:
			return invoke_callback(input_mouse::mouse_lmb, map_event(action), _last_input_modifier_state);
		case GLFW_MOUSE_BUTTON_RIGHT:
			return invoke_callback(input_mouse::mouse_rmb, map_event(action), _last_input_modifier_state);
		case GLFW_MOUSE_BUTTON_MIDDLE:
			return invoke_callback(input_mouse::mouse_mmb, map_event(action), _last_input_modifier_state);
		default:
			invoke_callback(input_mouse::mouse_UNKNOWN, input_state::state_UNKNOWN, input_modifier::modifier_UNKNOWN);
			break;
		}

		// TODO invoke specific callback for unknown event
	}

	void glfw_renderer::_on_mouse_drag(GLFWwindow*, double xpos, double ypos)
	{
		if (!_is_capturing_mouse)
		{
			_needs_refresh_mouse_pos = true;
			
			return;
		}
		
		float32 pos_x = float32(xpos);
		float32 pos_y = float32(ypos);
		
		if (_needs_refresh_mouse_pos)
		{
			_last_mouse_pos_x = pos_x;
			_last_mouse_pos_y = pos_y;
			
			_needs_refresh_mouse_pos = false;
		}
		
		float32 pos_delta_x = pos_x - _last_mouse_pos_x;
		float32 pos_delta_y = pos_y - _last_mouse_pos_y;
		
		_last_mouse_pos_x = pos_x;
		_last_mouse_pos_y = pos_y;
		
		std::for_each(_event_mouse_drag_callback_array.begin(), _event_mouse_drag_callback_array.end(),
			[pos_x, pos_y, pos_delta_x, pos_delta_y, modifier = _last_input_modifier_state](auto&& callback)
			{
				callback(pos_x, pos_y, pos_delta_x, pos_delta_y, modifier);
			});
	}

	void glfw_renderer::_on_mouse_scroll(GLFWwindow*, double xoffset, double yoffset)
	{
		float32 delta_x = float32(xoffset);
		float32 delta_y = float32(yoffset);
		
		std::for_each(_event_mouse_scroll_callback_array.begin(), _event_mouse_scroll_callback_array.end(),
			[delta_x, delta_y, modifier = _last_input_modifier_state](auto&& callback) { callback(delta_x, delta_y, modifier); });
	}

	void glfw_renderer::_redirect_on_window_minimize(GLFWwindow* window, int minimized)
	{
		static_cast<glfw_renderer*>(renderer)->_on_window_minimize(window, minimized);
	}

	void glfw_renderer::_redirect_on_window_maximize(GLFWwindow* window, int maximized)
	{
		static_cast<glfw_renderer*>(renderer)->_on_window_maximize(window, maximized);
	}

	void glfw_renderer::_redirect_on_window_focus(GLFWwindow* window, int focused)
	{
		static_cast<glfw_renderer*>(renderer)->_on_window_focus(window, focused);
	}

	void glfw_renderer::_redirect_on_window_close(GLFWwindow* window)
	{
		static_cast<glfw_renderer*>(renderer)->_on_window_close(window);
	}

	void glfw_renderer::_redirect_on_window_move(GLFWwindow* window, int xpos, int ypos)
	{
		static_cast<glfw_renderer*>(renderer)->_on_window_move(window, xpos, ypos);
	}

	void glfw_renderer::_redirect_on_window_resize(GLFWwindow* window, int width, int height)
	{
		static_cast<glfw_renderer*>(renderer)->_on_window_resize(window, width, height);
	}

	void glfw_renderer::_redirect_on_key_button(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		static_cast<glfw_renderer*>(renderer)->_on_key_button(window, key, scancode, action, mods);
	}

	void glfw_renderer::_redirect_on_mouse_button(GLFWwindow* window, int button, int action, int mods)
	{
		static_cast<glfw_renderer*>(renderer)->_on_mouse_button(window, button, action, mods);
	}

	void glfw_renderer::_redirect_on_mouse_drag(GLFWwindow* window, double xpos, double ypos)
	{
		static_cast<glfw_renderer*>(renderer)->_on_mouse_drag(window, xpos, ypos);
	}

	void glfw_renderer::_redirect_on_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset)
	{
		static_cast<glfw_renderer*>(renderer)->_on_mouse_scroll(window, xoffset, yoffset);
	}
}
