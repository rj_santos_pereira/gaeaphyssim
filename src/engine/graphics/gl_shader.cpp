#include "engine/graphics/gl_shader.hpp"

#include "library/containers.hpp"

namespace gaea
{
	gl_shader::gl_shader()
		: _source_path{}
		, _id{ 0 }
		, _type{ 0 }
		, _needs_compile{ false }
		, _is_compiled{ false }
		, _is_attached{ false }
	{
	}

	gl_shader::gl_shader(stdfs::path const& source_path, gl_shader_type type)
		: gl_shader{}
	{
		reinit(source_path, type);
	}

	gl_shader::gl_shader(gl_shader&& other) noexcept
		: _source_path{ std::move(other._source_path) }
		, _id{ other._id }
		, _type{ other._type }
		, _needs_compile{ other._needs_compile }
		, _is_compiled{ other._is_compiled }
		, _is_attached{ other._is_attached }
	{
		other._reset();
	}

	gl_shader& gl_shader::operator=(gl_shader&& other) noexcept
	{
		if (this != &other)
		{
			_destroy();

			_source_path = std::move(other._source_path);
			_id = other._id;
			_type = other._type;
			_needs_compile = other._needs_compile;
			_is_compiled = other._is_compiled;
			_is_attached = other._is_attached;

			other._reset();
		}

		return *this;
	}

	gl_shader::~gl_shader()
	{
		_destroy();
	}

	void gl_shader::reinit(stdfs::path const& source_path, gl_shader_type type)
	{
		if (!engine->is_render_thread())
		{
			GAEA_FATAL(gl_shader, "Trying to create a shader in a non-render thread!");
		}
		
		_destroy();
		_reset();

		if (source_path.empty() || !exists(source_path) || !is_regular_file(source_path))
		{
			GAEA_WARN(gl_shader, "File does not exist or is not a regular file!");
			return;
		}

		if (type == gl_shader_type::null)
		{
			std::string extension = source_path.extension().string();
			if (extension == ".vert")
			{
				type = gl_shader_type::vertex;
			}
			else if (extension == ".geom")
			{
				type = gl_shader_type::geometry;
			}
			else if (extension == ".frag")
			{
				type = gl_shader_type::fragment;
			}
			else
			{
				GAEA_WARN(gl_shader, "Unrecognized extension! Unable to deduce shader type!");
				return;
			}
		}

		_source_path = source_path;
		
		if (_source_path.is_relative())
		{
			_source_path = engine->compose_engine_path(_source_path);
		}

		_type = GLenum(type);
		_id = glCreateShader(_type);

		if (_id == 0)
		{
			GAEA_WARN(gl_shader, "glCreateShader failed!");
		}

		reload();
	}

	bool gl_shader::reload()
	{
		auto shader_size = file_size(_source_path);
		if (shader_size > std::numeric_limits<GLint>::max())
		{
			GAEA_WARN(gl_shader, "Shader source file size is too big! [Actual size: %.2f GB; Max size: %.2f GB]", 
				float32(shader_size) / 1e+9f, 
				float32(std::numeric_limits<GLint>::max()) / 1e+9f);
			return false;
		}

		array<char> buffer(shader_size);
		if (!std::ifstream{ _source_path.string() }.read(buffer.data(), buffer.size()).eof())
		{
			GAEA_WARN(gl_shader, "Error loading file!");
			return false;
		}

		char* buffer_data = buffer.data();
		int32 buffer_size = int32(buffer.size());

		glShaderSource(_id, 1, &buffer_data, &buffer_size);

		_needs_compile = true;

		return true;
	}

	bool gl_shader::compile()
	{
		if (_is_attached)
		{
			GAEA_WARN(gl_shader, "Error compiling shader! Shader is currently attached to a program!");
			return false;
		}

		glCompileShader(_id);

		int32 status;
		glGetShaderiv(_id, GL_COMPILE_STATUS, &status);

		_is_compiled = bool(status);

		if (!status)
		{
			int32 buffer_size;
			glGetShaderiv(_id, GL_INFO_LOG_LENGTH, &buffer_size);

			array<char> buffer(buffer_size);
			glGetShaderInfoLog(_id, buffer_size, nullptr, buffer.data());

			GAEA_WARN(gl_shader, "Error compiling shader!\nLog: %s", buffer.data());
			return false;
		}

		_needs_compile = false;

		return true;
	}

	void gl_shader::attach(uint32 program_id)
	{
		if (!_is_attached)
		{
			glAttachShader(program_id, _id);
			_is_attached = true;
		}
	}

	void gl_shader::detach(uint32 program_id)
	{
		if (_is_attached)
		{
			glDetachShader(program_id, _id);
			_is_attached = false;
		}
	}

	std::string gl_shader::get_name() const
	{
		return _source_path.filename().string();
	}

	GLuint gl_shader::get_id() const
	{
		return _id;
	}

	gl_shader_type gl_shader::get_type() const
	{
		return gl_shader_type(_type);
	}

	bool gl_shader::needs_compile() const
	{
		return _needs_compile;
	}

	bool gl_shader::is_compiled() const
	{
		return _is_compiled;
	}

	void gl_shader::_destroy()
	{
		if (_id != 0)
		{
			glDeleteShader(_id);
			_id = 0;
		}
	}

	void gl_shader::_reset()
	{
		_source_path.clear();
		_id = 0;
		_type = 0;
		_needs_compile = false;
		_is_compiled = false;
		_is_attached = false;
	}
}
