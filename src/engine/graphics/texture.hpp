#ifndef ENGINE_GRAPHICS_TEXTURE_INCLUDE_GUARD
#define ENGINE_GRAPHICS_TEXTURE_INCLUDE_GUARD

#include "core/core_common.hpp"

namespace gaea
{
	class texture
	{
	public:
		explicit texture(uint8* data, int32 width, int32 height, int32 channels);

		uint8 const* data() const;

		int32 width() const;

		int32 height() const;

		int32 channels() const;

		static std::shared_ptr<texture> load(std::string const& path, bool should_flip = true);

	private:
		uint8* _data;

		int32 _width;
		int32 _height;
		int32 _channels;

	};

	struct texcoord
	{
		explicit texcoord() = default;
		explicit constexpr texcoord(float32 nu, float32 nv)
			: u{ nu }
			, v{ nv }
		{
		}

		float32* data()
		{
			return &u;
		}
		
		float32 u;
		float32 v;
	};

	struct texcolor
	{
		explicit texcolor() = default;
		explicit constexpr texcolor(float32 nr, float32 ng, float32 nb, float32 na)
			: r{ nr }
			, g{ ng }
			, b{ nb }
			, a{ na }
		{
		}

		float32* data()
		{
			return &r;
		}
		
		float32 r;
		float32 g;
		float32 b;
		float32 a;
	};
}

#endif
