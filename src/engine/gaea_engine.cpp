#include "engine/gaea_engine.hpp"

#include "engine/engine_config.hpp"
#include "engine/physics/native_simulator.hpp"
#include "engine/physics/sycl_simulator.hpp"
#include "engine/scene/gaea_camera_entity.hpp"
#include "engine/scene/gaea_light_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"

namespace 
{
	class debugger_streambuf : public std::basic_streambuf<char, std::char_traits<char>>
	{
	public:
		debugger_streambuf(std::function<bool()> check_func, std::function<void(std::string const&)> print_func)
			: _check_func{ std::move(check_func) }
			, _print_func{ std::move(print_func) }
		{
		}

	protected:
		virtual std::streamsize xsputn(const char* ptr, std::streamsize count) override
		{
			if (_check_func())
			{
				_print_func(std::string{ ptr, std::size_t(count) });
			}
			return count;
		}

	private:
		std::function<bool()> _check_func;
		std::function<void(std::string const&)> _print_func;

	};
}

namespace gaea
{
	// Declared in 'core/core_engine.hpp'
	base_engine* engine = nullptr;
	base_renderer* renderer = nullptr;
	base_simulator* simulator = nullptr;

	gaea_engine::gaea_engine()
		: _thread_map{}
		, _renderer{ nullptr }
		, _simulator{ nullptr }
		, _world{ nullptr }
		, _random_engine{ skip_init_tag{} }
		, _config_manager{}
		, _device_manager{}
		, _input_manager{}
		, _log_manager{}
		, _executable_path{}
		, _general_log_file{}
		, _debugger_log_device{ nullptr }
		, _profiling_log_id{ uint8(log_type::null) }
	    , _profiling_num_threads{ 1 }
	    , _profiling_num_entities{ 0 }
		, _max_frame_delta{ 0.0 }
		, _time_dilation{ 1.0 }
		, _hitch_time{ 0 }
		, _last_swap_buffers_frame{ 0 }
		, _last_swap_buffers_duration{ 0.f }
		, _last_update_buffers_frame{ 0 }
		, _last_update_buffers_duration{ 0.f }
		, _world_time{ stdchr::system_clock::duration::zero() }
		, _last_frame_time{ stdchr::steady_clock::now() }
		, _frame_delta{ 0 }
		, _frame_fps{ 0 }
		, _frame_number{ 0 }
		, _is_initializing{ false }
		, _is_finalizing{ false }
		, _should_run{ false }
		, _is_paused{ false }
		, _is_tick_paused{ false }
		, _should_display_detail_messages{ false }
		, _using_mesh_instancing{ false }
		, _using_sycl_simulator{ false }
	{
	}

	bool gaea_engine::initialize(void* parameters)
	{
		auto gaea_parameters = static_cast<gaea_engine_parameters*>(parameters);

		if (!gaea_parameters)
		{
			GAEA_ERROR(gaea_engine, "Engine must be initialized with parameters of type '%s'!", GAEA_TOKEN_STRINGIFICATION(gaea::gaea_engine_parameters));
			return false;
		}

		_profiling_num_threads = gaea_parameters->profiling_num_threads;
		_profiling_num_entities = gaea_parameters->profiling_num_entities;

		_executable_path = gaea_parameters->executable_path;

		// Register streams on log manager first to enable us to log to somewhere
		{
			_log_manager.register_device_stream(std::cout, log_type::info);

			stdfs::path log_file_path = compose_engine_path(stdfs::path{ "logs" } /= gaea_parameters->log_file_name);
			if (util::create_directory_tree(log_file_path.parent_path()))
			{
				_general_log_file = std::ofstream{ log_file_path, std::ios::out | std::ios::trunc };
				_log_manager.register_file_stream(_general_log_file, log_type::info);
			}

			_debugger_log_device.rdbuf(new debugger_streambuf{ gaea_parameters->is_debugger_attached_func, gaea_parameters->print_debugger_message_func });
			_log_manager.register_device_stream(_debugger_log_device, log_type::debug);
		}

		// Then load config file: try to load custom first, if not found, try to load default
		GAEA_INFO(gaea_engine, "Trying to load configurations from custom_engine.ini...");
		if (!_config_manager.load_config(config::path::custom_engine))
		{
			GAEA_INFO(gaea_engine, "Could not load custom_engine.ini!");
			GAEA_INFO(gaea_engine, "Trying to load configurations from default_engine.ini...");

			if (!_config_manager.load_config(config::path::default_engine))
			{
				GAEA_WARN(gaea_engine, "Could not load default_engine.ini!");
				GAEA_WARN(gaea_engine, "No configurations loaded!");
			}
		}

		// Afterwards immediately configure whether mesh instancing should be used and if SYCL simulator should be used
		{
			bool using_mesh_instancing = config::property::using_mesh_instancing_v;
			_config_manager.get_bool_property(config::section::graphics, config::property::using_mesh_instancing, using_mesh_instancing);
			_using_mesh_instancing = using_mesh_instancing;

#if GAEA_USING_SYCL
#if GAEA_PROFILE_EXECUTION
			_using_sycl_simulator = !GAEA_PROFILE_CPU;
#else
			bool using_sycl_simulator = config::property::using_sycl_simulator_v;
			_config_manager.get_bool_property(config::section::physics, config::property::using_sycl_simulator, using_sycl_simulator);
			_using_sycl_simulator = using_sycl_simulator;
#endif
#endif
		}

#if GAEA_USING_SYCL
		device simulation_device;

		// Select device from config
		if (_using_sycl_simulator)
		{
			std::string device_name;
			bool has_device = false;

			if (_config_manager.get_string_property(config::section::physics, config::property::sycl_device_name, device_name))
			{
				has_device = _device_manager.select_device(simulation_device, device_name);
				if (!has_device)
				{
					GAEA_WARN(gaea_engine, "Could not find device with name '%s'. Searching for different device...", device_name.c_str());
				}
			}

			if (!has_device)
			{
				has_device = _device_manager.select_device(simulation_device, sycl::info::device_type::gpu, device_capability::performance);

				if (has_device)
				{
					_config_manager.set_string_property(config::section::physics, config::property::sycl_device_name, simulation_device.name());
				}
				else
				{
					GAEA_WARN(gaea_engine, "Could not select simulation device. Simulation performance will be affected!");
				}
			}
		}
		else
		{
			_device_manager.select_device(simulation_device, sycl::info::device_type::host);
		}

#if GAEA_DEBUG_PRINT_DEVICE_PROPERTIES
		{
			std::ostringstream stream;
			stream << "Debugging " GAEA_TOKEN_STRINGIFICATION(gaea::device_manager) "...\nListing GPU devices:\n";
			for (auto const& device : _device_manager.list_devices(sycl::info::device_type::gpu))
			{
				stream << device;
			}
			GAEA_INFO(gaea_engine, "%s", stream.str().c_str());
		}
#endif

#endif

#if GAEA_PROFILE_EXECUTION
		{
			std::string profiling_log_file_name;
			std::string profiling_log_header_message;

			std::string scene_name = config::property::default_scene_name_v;
			if (!_config_manager.get_string_property(config::section::scene, config::property::default_scene_name, scene_name))
			{
				GAEA_FATAL(gaea_engine, "Configuration error during profiling!");
			}

			std::string device_name = simulation_device.name();

		    std::replace(device_name.begin(), device_name.end(), ' ', '-');
		    std::replace(scene_name.begin(), scene_name.end(), '_', '-');

			if (_using_sycl_simulator)
			{
				profiling_log_file_name = make_formattable_string("%s_%s_c%u_b%u.csv", device_name.c_str(), scene_name.c_str(), simulation_device.num_compute_units(), _profiling_num_entities);
				profiling_log_header_message = "frame, step, cd, bp, np, cg, cr, cb, cs";
			}
			else
			{
				profiling_log_file_name = make_formattable_string("%s_%s_c%u_b%u.csv", device_name.c_str(), scene_name.c_str(), _profiling_num_threads, _profiling_num_entities);
				profiling_log_header_message = "frame, step, cd, bp, np, cr";
			}
			
			_profiling_log_id = _log_manager.create_file_stream(std::string{ config::path::profiling_log_path }.append(profiling_log_file_name));
			_log_manager.log(_profiling_log_id, std::move(profiling_log_header_message));
		}
#endif

		GAEA_INFO(gaea_engine, "Initializing engine...");

		scope_guard<bool> init_guard{ _is_initializing, true };

		// Seed random engine from config
		{
			std::string rand_seed;
			if (_config_manager.get_string_property(config::section::general, config::property::random_engine_seed, rand_seed))
			{
				_random_engine.set_seed(random_engine_seed::deserialize(rand_seed));
			}
			else
			{
				_random_engine.seed_engine();

				rand_seed = random_engine_seed::serialize(_random_engine.get_seed());

				_config_manager.set_string_property(config::section::general, config::property::random_engine_seed, rand_seed);
			}
		}

#if GAEA_DEBUG_PRINT_RANDOM_SAMPLE
		{
			std::ostringstream stream;
			stream << "Debugging " GAEA_TOKEN_STRINGIFICATION(gaea::random_engine) "...\nSampling engine:\n";
			for (int32 n = 0; n < 10; ++n)
			{
				stream << "n" << n << ": " << _random_engine.generate_int() << "\n";
			}
			GAEA_INFO(gaea_engine, "%s", stream.str().c_str());

			// Reset engine state
			_random_engine.reset_engine();
	}
#endif

		// Try to load and configure frame rate limit
		config::property::max_frame_rate_t max_frame_rate = config::property::max_frame_rate_v;
		_config_manager.get_int_property(config::section::general, config::property::max_frame_rate, max_frame_rate);
		_max_frame_delta = 1. / max_frame_rate;

		GAEA_INFO(gaea_engine, "Creating graphics renderer '%s'...", GAEA_TOKEN_STRINGIFICATION(gaea::glfw_renderer));

		renderer = _renderer = new glfw_renderer;
		if (!_renderer->initialize(nullptr))
		{
			GAEA_ERROR(gaea_engine, "Error creating renderer!");
			return false;
		}

		// Await renderer thread init by requesting flush
		_renderer->flush();

		_renderer->register_window_focus_callback(std::bind(&gaea_engine::_on_window_focus, this, std::placeholders::_1));
		_renderer->register_window_close_callback(std::bind(&gaea_engine::_on_window_close, this));

		// Init substep values from config file
		config::property::max_substep_frequency_t max_substep_frequency_p = config::property::max_substep_frequency_v;
		_config_manager.get_int_property(config::section::physics, config::property::max_substep_frequency, max_substep_frequency_p);
		float32 max_substep_frequency = 1.f / max_substep_frequency_p;
		
		config::property::max_substep_count_t max_substep_count = config::property::max_substep_count_v;
		_config_manager.get_int_property(config::section::physics, config::property::max_substep_count, max_substep_count);

		config::property::simulate_thread_count_t simulate_thread_count = config::property::simulate_thread_count_v;
#if GAEA_PROFILE_EXECUTION
		simulate_thread_count = _profiling_num_threads;
#else
		_config_manager.get_int_property(config::section::physics, config::property::simulate_thread_count, simulate_thread_count);
#endif

		config::property::simulate_thread_ratio_t simulate_thread_ratio = config::property::simulate_thread_ratio_v;
#if GAEA_PROFILE_EXECUTION
		simulate_thread_ratio = 0;
#else
		_config_manager.get_int_property(config::section::physics, config::property::simulate_thread_ratio, simulate_thread_ratio);
#endif

#if GAEA_USING_SYCL
		if (_using_sycl_simulator)
		{
			GAEA_INFO(gaea_engine, "Creating physics simulator '%s'...", GAEA_TOKEN_STRINGIFICATION(gaea::sycl_simulator));

			simulator = _simulator = new sycl_simulator;

			sycl_simulator_parameters simulator_parameters
			{
				std::move(simulation_device),
				2, 10, 10,
				0.1f, 0.005f, 20.f, 30.f,
				collision_mode::arithmetic,
				collision_mode::geometric,
				max_substep_frequency,
				max_substep_count,
			};

			if (!_simulator->initialize(&simulator_parameters))
			{
				GAEA_ERROR(gaea_engine, "Error creating simulator!");
				return false;
			}
		}
		else
#endif
		{
			GAEA_INFO(gaea_engine, "Creating physics simulator '%s'...", GAEA_TOKEN_STRINGIFICATION(gaea::native_simulator));

			simulator = _simulator = new native_simulator;

			native_simulator_parameters simulator_parameters
			{
				max_substep_frequency,
				max_substep_count,
				simulate_thread_count,
				simulate_thread_ratio
			};
			
			if (!_simulator->initialize(&simulator_parameters))
			{
				GAEA_ERROR(gaea_engine, "Error creating simulator!");
				return false;
			}

			// Await simulator thread init by requesting step
			_simulator->step(0.0);
			_simulator->finish_step();
		}

		GAEA_INFO(gaea_engine, "Creating world '%s'...", GAEA_TOKEN_STRINGIFICATION(gaea::gaea_world));

		_world = new gaea_world;

		GAEA_INFO(gaea_engine, "Setting up input event handling...");

		if (!_input_manager.setup_handlers())
		{
			GAEA_ERROR(gaea_engine, "Error setting up input event handling!");
			return false;
		}

		// TODO IDEA load from config
		_input_manager.set_repeat_delay(100);

#if !GAEA_PROFILE_EXECUTION
		_input_manager.bind_key_button(input_key::key_escape, this,
			[this](input_key, input_state, input_modifier)
			{
				_terminate_engine();
			});
#endif
		
		_input_manager.bind_key_button(input_key::key_f1, this,
			[this](input_key, input_state state, input_modifier) 
			{
				if (state == input_state::state_pressed)
				{
					_renderer->toggle_mouse_capture();
				}
			});
		_input_manager.bind_key_button(input_key::key_f2, this,
			[this](input_key, input_state state, input_modifier)
			{
				if (state == input_state::state_pressed)
				{
					_renderer->toggle_fullscreen();
					if (_renderer->is_fullscreen() != _renderer->is_capturing_mouse())
					{
						_renderer->toggle_mouse_capture();
					}
				}
			});
		
		_input_manager.bind_key_button(input_key::key_f5, this,
			[this](input_key, input_state state, input_modifier)
			{
				if (state == input_state::state_pressed)
				{
#if GAEA_USING_SYCL
					if (_using_sycl_simulator)
					{
						static_cast<sycl_simulator*>(_simulator)->toggle_bounding_box_display();
					}
					else
#endif
					{
						static_cast<native_simulator*>(_simulator)->toggle_bounding_box_display();
					}
				}
			});
		_input_manager.bind_key_button(input_key::key_f6, this,
			[this](input_key, input_state state, input_modifier)
			{
				if (state == input_state::state_pressed)
				{
					_should_display_detail_messages = !_should_display_detail_messages;
				}
			});

#if !GAEA_PROFILE_EXECUTION
		_input_manager.bind_key_button(input_key::key_f9, this,
			[this](input_key, input_state state, input_modifier)
			{
				if (state == input_state::state_pressed)
				{
					_is_paused = !_is_paused;
					_is_tick_paused = _is_paused;
				}
			});
		_input_manager.bind_key_button(input_key::key_f10, this,
			[this](input_key, input_state state, input_modifier)
			{
				if (_is_paused && state != input_state::state_released)
				{
					_is_tick_paused = false;
				}
			});

		auto time_dilation_adjust_lambda = 
			[this](input_key key, input_state state, input_modifier)
			{
				if (state != input_state::state_released)
				{
					float64 dilation_diff = 0.0;
					switch (key)
					{
					case input_key::key_numpad_add: dilation_diff = +0.01; break;
					case input_key::key_numpad_sub: dilation_diff = -0.01; break;
					case input_key::key_numpad_mul: dilation_diff = +0.10; break;
					case input_key::key_numpad_div: dilation_diff = -0.10; break;
					case input_key::key_numpad_0: dilation_diff = 0.0 - _time_dilation; break;
					case input_key::key_numpad_1: dilation_diff = 1.0 - _time_dilation; break;
					}
					
					_time_dilation += dilation_diff;
					_time_dilation = std::clamp(_time_dilation, 0.0, 10.0);
	
					_timer_manager.set_time_scale(1.0 / _time_dilation);
				}
			};

		_input_manager.bind_key_button(input_key::key_numpad_add, this, time_dilation_adjust_lambda);
		_input_manager.bind_key_button(input_key::key_numpad_sub, this, time_dilation_adjust_lambda);
		_input_manager.bind_key_button(input_key::key_numpad_mul, this, time_dilation_adjust_lambda);
		_input_manager.bind_key_button(input_key::key_numpad_div, this, time_dilation_adjust_lambda);
		_input_manager.bind_key_button(input_key::key_numpad_0, this, time_dilation_adjust_lambda);
		_input_manager.bind_key_button(input_key::key_numpad_1, this, time_dilation_adjust_lambda);
#endif

		_should_run = true;

		return true;
	}

	void gaea_engine::finalize()
	{
		GAEA_INFO(gaea_engine, "Finalizing engine...");

		scope_guard<bool> fin_guard{ _is_finalizing, true };

		_input_manager.unbind_key_button(input_key::key_escape, this);
		
		_input_manager.unbind_key_button(input_key::key_f1, this);
		_input_manager.unbind_key_button(input_key::key_f2, this);

		_input_manager.unbind_key_button(input_key::key_f5, this);
		_input_manager.unbind_key_button(input_key::key_f6, this);

		_input_manager.unbind_key_button(input_key::key_f9, this);
		_input_manager.unbind_key_button(input_key::key_f10, this);
		
		_input_manager.unbind_key_button(input_key::key_numpad_add, this);
		_input_manager.unbind_key_button(input_key::key_numpad_sub, this);
		_input_manager.unbind_key_button(input_key::key_numpad_mul, this);
		_input_manager.unbind_key_button(input_key::key_numpad_div, this);
		_input_manager.unbind_key_button(input_key::key_numpad_0, this);
		_input_manager.unbind_key_button(input_key::key_numpad_1, this);

		if (_world)
		{
			GAEA_INFO(gaea_engine, "Destroying world '%s'...", GAEA_TOKEN_STRINGIFICATION(gaea::gaea_world));

			delete _world;
		}

		if (_simulator)
		{
			GAEA_INFO(gaea_engine, "Destroying physics simulator...");
			
			_simulator->finalize();

			simulator = nullptr;
#if GAEA_USING_SYCL
			if (_using_sycl_simulator)
			{
				delete static_cast<sycl_simulator*>(_simulator);
			}
			else
#endif
			{
				delete static_cast<native_simulator*>(_simulator);
			}
		}

		if (_renderer)
		{
			GAEA_INFO(gaea_engine, "Destroying graphics renderer...");

			_renderer->finalize();

			renderer = nullptr;
			delete _renderer;
		}

#if 0
		if (_config_manager.has_unsaved_properties())
		{
			GAEA_INFO(gaea_engine, "Saving configuration to custom_engine.ini...");

			_config_manager.save_config(config::path::custom_engine);
		}
#endif

		// Finally, close and deallocate log files/devices

#if GAEA_PROFILE_EXECUTION
		_log_manager.destroy_file_stream(_profiling_log_id);
#endif
		
		if (_general_log_file.is_open())
		{
			_general_log_file.close();
		}

		if (auto streambuf = static_cast<debugger_streambuf*>(_debugger_log_device.rdbuf(nullptr)))
		{
			delete streambuf;
		}
	}

	void gaea_engine::run()
	{
		register_thread(thread_type::engine);
		
		implementation::engine_frame_duration last_frame_duration, frame_duration;

		// TODO IDEA move this logic to scene manager
		{
			// Register gravity
			_simulator->apply_world_lin_accel({ 0.0f, 0.0f, -9.8f });

			// Create camera
			gaea_camera_entity_spawn_parameters camera_params;
			camera_params.scene.position = vector{ -10.f, 0.f, 5.f };
			camera_params.scene.rotation = vector{ 0.f, 15.f, 0.f };

			_world->spawn_entity<gaea_camera_entity>(camera_params);

			// Create single point light
			gaea_light_entity_spawn_parameters light_params;
			light_params.scene.position = vector{ 0.f, 0.f, 50.f };

			_world->spawn_entity<gaea_light_entity>(light_params);
		}

		if (!source_scene_loader::access().load_scene_from_config())
		{
			GAEA_WARN(gaea_engine, "Could not load scene from config file!");
		}

		_world->begin_run();

		// We set last frame time now to avoid stupid numbers in delta/fps calculation
		_last_frame_time = stdchr::steady_clock::now();

		// Let's also hitch application so we don't have a near-zero delta when updating frame times
		std::this_thread::sleep_for(stdchr::milliseconds(3));

		constexpr int64 drift_frame_limit = 75; // ms
		constexpr uint64 num_drift_frames_tolerance = 3;

		uint64 num_drift_frames = 0;

		while (_should_run)
		{
			_is_tick_paused = _is_paused;

			_update_frame_times();

			_display_stat_messages(frame_duration);
			
			{
				GAEA_SCOPE_STOPWATCH(frame_duration.frame);

				float32 pre_physics_scene, during_physics_scene, post_physics_scene;

				float64 delta = frame_delta();

				{
					_world->handle_entity_changes(gaea_world::engine_invoke_tag{});
				}
				
				{
					GAEA_SCOPE_STOPWATCH(frame_duration.timer);

					_timer_manager.tick_timers();
				}

				{
					GAEA_SCOPE_STOPWATCH(frame_duration.event);
					
					_input_manager.handle_events();
				}
					
				{
					GAEA_SCOPE_STOPWATCH(pre_physics_scene);

					_world->tick(tick_stage::pre_physics, float32(delta));
				}

				{
					GAEA_SCOPE_STOPWATCH(frame_duration.physics);

					if (!_is_tick_paused)
					{
						_simulator->step(delta);
					}

					{
						GAEA_SCOPE_STOPWATCH(during_physics_scene);

						_world->tick(tick_stage::during_physics, float32(delta));
					}

					if (!_is_tick_paused)
					{
						_simulator->finish_step();
					}
				}

				{
					GAEA_SCOPE_STOPWATCH(post_physics_scene);

					_world->tick(tick_stage::post_physics, float32(delta));
				}

				{
					GAEA_SCOPE_STOPWATCH(frame_duration.graphics);

					_renderer->draw(_frame_number);
				}

				frame_duration.scene = pre_physics_scene + during_physics_scene + post_physics_scene;
			}

#if GAEA_PROFILE_EXECUTION
			_log_simulation_times(frame_duration);
#endif

			if (!_simulator->is_real_time())
			{
				GAEA_WARN(gaea_engine, "Drift from real time on frame %llu! "
					"Last frame times: frame=%.6fs timer=%.6fs event=%.6fs scene=%.6fs physics=%.6fs graphics=%.6fs",
					_frame_number,
					last_frame_duration.frame, last_frame_duration.timer, last_frame_duration.event,
					last_frame_duration.scene, last_frame_duration.physics, last_frame_duration.graphics);
			}

			// When transitioning, there will likely be a slowdown in the graphics phase of the frame,
			// we make that slowdown "disappear" in order to not affect the simulation (and avoid hitch)
			if (_renderer->is_transitioning())
			{
				// Feed graphics time to _last_frame_time
				_last_frame_time += stdchr::duration_cast<stdchr::steady_clock::duration>(stdchr::duration<float32>(frame_duration.graphics));

				// Remove graphics time from frame, and make it vanish
				frame_duration.frame -= frame_duration.graphics;
				frame_duration.graphics = 0.f;
			}

			// If event handling took too long (I/O or OS events may block application), hitch application
			// For example, see https://discourse.glfw.org/t/what-glfw-callback-notifies-me-when-im-just-holding-the-window-bar-without-moving-it/1456
			bool should_hitch_due_to_event = stdchr::duration<float32>(frame_duration.event) > stdchr::milliseconds(3);
			
			// HACK: something in GLFW/OpenGL/Windows causes the graphics to hitch in some occasions, (e.g. ~30 secs after start), with spikes between 50~150ms
			bool should_hitch_due_to_graphics = stdchr::duration<float32>(frame_duration.graphics) > stdchr::milliseconds(drift_frame_limit);

			// HACK: occasionally SYCL will have isolated kernel hitches, with spikes around ~100ms
			bool should_hitch_due_to_physics = _using_sycl_simulator && stdchr::duration<float32>(frame_duration.physics) > stdchr::milliseconds(drift_frame_limit);

			// This prevents the frame delta calculation in the next frame from breaking/slowing the simulation, if the hitch is isolated
			if (should_hitch_due_to_event || should_hitch_due_to_graphics || should_hitch_due_to_physics)
			{
				// If hitch is isolated, adjust times
				if (num_drift_frames < num_drift_frames_tolerance)
				{
#if 0
					GAEA_WARN(gaea_engine, "Hitch on frame %llu! "
						"Frame times: frame=%.6fs timer=%.6fs event=%.6fs scene=%.6fs physics=%.6fs graphics=%.6fs",
						_frame_number,
						frame_duration.frame, frame_duration.timer, frame_duration.event,
						frame_duration.scene, frame_duration.physics, frame_duration.graphics);
#endif

					// Register time spent in hitch in order to keep the clock consistent with the running time
					// Only consider 'event' time as hitch time
					_hitch_time += stdchr::duration_cast<stdchr::steady_clock::duration>(stdchr::duration<float32>(frame_duration.event)).count();

					// Set hitch time in timer manager as offset time
					_timer_manager.set_time_offset(-_hitch_time);

					// Restore _last_frame_time to avoid stupid numbers in frame delta calculation;
					// Offset it so that we have a reasonable delta
					_last_frame_time = stdchr::steady_clock::now() - stdchr::milliseconds(3);
				}

				// If not caused by event, the engine is drifting from real-time, so register it as a drift frame
				if (!should_hitch_due_to_event)
				{
					num_drift_frames = std::min<uint64>(num_drift_frames + 1, 5);
				}
			}
			else
			{
				if (num_drift_frames > 0)
				{
					--num_drift_frames;
				}
			}

			_world_time += stdchr::duration_cast<stdchr::system_clock::duration>(stdchr::duration<float64>(_frame_delta * _time_dilation));

#if GAEA_PROFILE_EXECUTION
			if (_world_time >= stdchr::seconds(GAEA_PROFILE_DURATION))
			{
				_should_run = false;
			}
#endif

			last_frame_duration = frame_duration;
		}

		_world->end_run();

		// Handle any remaining I/O events, including those triggered by shutdown
		_input_manager.handle_events();

		// One last opportunity for renderer to run clear commands
		_renderer->flush();
	}

	uint64 gaea_engine::make_unique_id()
	{
		return uint64(uint32(_random_engine.generate_int(0, std::numeric_limits<uint32>::max()))) << 32
					| uint32(_random_engine.generate_int(0, std::numeric_limits<uint32>::max()));
	}

	uint64 gaea_engine::make_sequential_id()
	{
		static uint64 _seq_id = 0;
		return ++_seq_id;
	}

	uint64 gaea_engine::frame_number() const
	{
		return _frame_number;
	}

	float64 gaea_engine::frame_delta() const
	{
		return _frame_delta * _time_dilation;
	}

	float64 gaea_engine::frame_actual_delta() const
	{
		return _frame_delta;
	}

	float64 gaea_engine::max_frame_rate() const
	{
		return 1.0 / _max_frame_delta;
	}

	bool gaea_engine::is_paused() const
	{
		return _is_tick_paused;
	}

    bool gaea_engine::is_running() const
    {
		return _should_run;
    }

    bool gaea_engine::is_initializing() const
    {
		return _is_initializing;
    }

    bool gaea_engine::is_finalizing() const
    {
		return _is_finalizing;
    }

    random_engine* gaea_engine::access_random_engine()
	{
		return &_random_engine;
	}

	input_manager* gaea_engine::access_input_manager()
	{
		return &_input_manager;
	}

	config_manager* gaea_engine::access_config_manager()
	{
		return &_config_manager;
	}

	device_manager* gaea_engine::access_device_manager()
	{
		return &_device_manager;
	}

	timer_manager* gaea_engine::access_timer_manager()
	{
		return &_timer_manager;
	}

	void gaea_engine::register_thread(thread_type type)
	{
		if (type == thread_type::other)
		{
			GAEA_ERROR(gaea_engine, "Trying to register unknown thread!");
			return;
		}

		auto& thread_set = _thread_map[type];
		
		if (type == thread_type::engine && !thread_set.empty() && !thread_set.count(std::this_thread::get_id()))
		{
			GAEA_ERROR(gaea_engine, "Trying to register another engine thread!");
			return;
		}

		std::size_t id = thread_set.size();
		bool is_new_thread = thread_set.insert(std::this_thread::get_id()).second;
		
		if (is_new_thread)
		{
			switch (type)
			{
			case thread_type::engine:
				util::rename_thread("engine_thread");
				break;
			case thread_type::render:
				util::rename_thread(_formattable_string{ "render_thread_%llu" }.format(id));
				break;
			case thread_type::simulate:
				util::rename_thread(_formattable_string{ "simulate_thread_%llu" }.format(id));
				break;
			}
		}
		else
		{
			GAEA_WARN(gaea_engine, "Trying to re-register a thread!");
		}
	}

	bool gaea_engine::is_engine_thread() const
	{
		auto& engine_thread_set = _thread_map[thread_type::engine];
		return engine_thread_set.count(std::this_thread::get_id()) > 0;
	}

	bool gaea_engine::is_render_thread() const
	{
		auto& render_thread_set = _thread_map[thread_type::render];
		return render_thread_set.count(std::this_thread::get_id()) > 0;
	}

	bool gaea_engine::is_simulate_thread() const
	{
		auto& simulate_thread_set = _thread_map[thread_type::simulate];
		return simulate_thread_set.count(std::this_thread::get_id()) > 0;
	}

	stdfs::path gaea_engine::compose_engine_path(stdfs::path const& path) const
	{
		if (!path.is_relative())
		{
			GAEA_ERROR(gaea_engine, "Trying to compose engine path using a non-relative path!");
			return stdfs::path{};
		}
		
		return weakly_canonical(_executable_path.parent_path() / path);
	}

	void gaea_engine::log_message(log_type type, stdchr::system_clock::time_point&& time, std::string&& message)
	{
		_log_manager.log(uint8(type), std::move(message), std::move(time));
	}

	void gaea_engine::display_message(basic_color color, std::string&& message)
	{
		_renderer->draw_debug_string(std::move(message), color);
	}

    uint32 gaea_engine::profiling_num_threads() const
    {
		return _profiling_num_threads;
    }

    uint32 gaea_engine::profiling_num_entities() const
    {
		return _profiling_num_entities;
    }

    bool gaea_engine::is_using_mesh_instancing() const
	{
		return _using_mesh_instancing;
	}

	bool gaea_engine::is_using_sycl_simulator() const
	{
		return _using_sycl_simulator;
	}

	void* gaea_engine::access_world_internal(const type_info& world_type)
    {
		if (world_type == typeid(gaea_world))
		{
			return _world;
		}
		return nullptr;
    }

    void gaea_engine::_update_frame_times()
	{
		// These were derived experimentally
		constexpr float64 wait_block = 3.0 / 1000.0;
		constexpr float64 wait_slack = 1.0 / 1000.0;

		// Query cycle counters for delta calculation for this frame
		auto frame_time = stdchr::steady_clock::now();
		_frame_delta = stdchr::duration<float64>(frame_time - _last_frame_time).count();

#if GAEA_SHOULD_LIMIT_FRAMERATE
		float64 wait_delta = std::max(_max_frame_delta - _frame_delta, 0.0);
		if (wait_delta > 0.0)
		{
			auto wait_time = frame_time + stdchr::duration<float64>(wait_delta);

			if (wait_delta > wait_block)
			{
				std::this_thread::sleep_for(stdchr::milliseconds(int64((wait_delta - wait_slack) * 1000.0)));
			}

			while (stdchr::steady_clock::now() < wait_time)
			{
				std::this_thread::yield();
			}

			// Query cycle counters again and recalculate delta considering the wait
			frame_time = stdchr::steady_clock::now();
			_frame_delta = stdchr::duration<float64>(frame_time - _last_frame_time).count();
		}
#endif

		_frame_fps = 1. / _frame_delta;
		
		_last_frame_time = frame_time;
		++_frame_number;
	}

	void gaea_engine::_display_stat_messages(implementation::engine_frame_duration const& frame_duration)
	{
		_renderer->draw_debug_string(make_formattable_string("FPS: %.2lf", _frame_fps), basic_color::chartreuse);

		if (_should_display_detail_messages)
		{
			_renderer->draw_debug_string(make_formattable_string("frame=%.6fs", frame_duration.frame));
			_renderer->draw_debug_string(make_formattable_string("timer=%.6fs", frame_duration.timer));
			_renderer->draw_debug_string(make_formattable_string("event=%.6fs", frame_duration.event));
			_renderer->draw_debug_string(make_formattable_string("scene=%.6fs", frame_duration.scene));
			_renderer->draw_debug_string(make_formattable_string("phys.=%.6fs", frame_duration.physics));
			_renderer->draw_debug_string(make_formattable_string("grap.=%.6fs", frame_duration.graphics));
			
			_renderer->draw_debug_string(make_formattable_string("frame_num.=%llu", _frame_number));
			
			_renderer->draw_debug_string(make_formattable_string("is paused: %s", 
										 _is_paused ? "yes" : "no"),
										 _is_paused ? basic_color::spring : basic_color::rose);
			if (_using_sycl_simulator)
			{
				_renderer->draw_debug_string("\nUsing SYCL simulator", basic_color::orange);
			}
			else
			{
				_renderer->draw_debug_string("\nUsing native simulator", basic_color::violet);
			}
			
#if GAEA_DEBUG_DISPLAY_BOUNDING_BOXES
			bool is_displaying_bounding_boxes;
#if GAEA_USING_SYCL
			if (_using_sycl_simulator)
			{
				is_displaying_bounding_boxes = static_cast<sycl_simulator*>(_simulator)->is_displaying_bounding_boxes();
			}
			else
#endif
			{
				is_displaying_bounding_boxes = static_cast<native_simulator*>(_simulator)->is_displaying_bounding_boxes();
			}
		
			_renderer->draw_debug_string(make_formattable_string("is displaying bounding boxes: %s", 
										 is_displaying_bounding_boxes ? "yes" : "no"),
										 is_displaying_bounding_boxes ? basic_color::spring : basic_color::rose);
#endif

#if GAEA_USING_SYCL
			if (_using_sycl_simulator)
			{
				auto sim = static_cast<sycl_simulator*>(_simulator);
				
				float32 swap_buffers_duration = sim->query_swap_buffers_duration();
				if (swap_buffers_duration > 0.f)
				{
					_last_swap_buffers_frame = _frame_number - 1;
					_last_swap_buffers_duration = swap_buffers_duration;
				}

				float32 update_buffers_duration = sim->query_update_buffers_duration();
				if (update_buffers_duration > 0.f)
				{
					_last_update_buffers_frame = _frame_number - 1;
					_last_update_buffers_duration = update_buffers_duration;
				}

				_renderer->draw_debug_string(make_formattable_string("  swap buffers=%.6fs [frame=%llu]", _last_swap_buffers_duration, _last_swap_buffers_frame));
				_renderer->draw_debug_string(make_formattable_string("  update buffers=%.6fs [frame=%llu]", _last_update_buffers_duration, _last_update_buffers_frame));
			}
			else
#endif
			{
				_renderer->draw_debug_string(make_formattable_string("num_threads: %u\nphysics_time: ", _simulator->num_threads()));
			}

			_renderer->draw_debug_string(make_formattable_string("  collision detection=%.6fs", _simulator->query_collision_detection_duration()));
			_renderer->draw_debug_string(make_formattable_string("    broad-phase=%.6fs", _simulator->query_broad_phase_duration()));
			_renderer->draw_debug_string(make_formattable_string("    narrow-phase=%.6fs", _simulator->query_narrow_phase_duration()));

#if GAEA_USING_SYCL
			if (_using_sycl_simulator)
			{
				auto sim = static_cast<sycl_simulator*>(_simulator);

				_renderer->draw_debug_string(make_formattable_string("      contact generation=%.6fs", sim->query_contact_generation_duration()));
			}
#endif
			
			_renderer->draw_debug_string(make_formattable_string("  collision resolution=%.6fs", _simulator->query_collision_resolution_duration()));

#if GAEA_USING_SYCL
			if (_using_sycl_simulator)
			{
				auto sim = static_cast<sycl_simulator*>(_simulator);

				_renderer->draw_debug_string(make_formattable_string("    constraint batching=%.6fs", sim->query_constraint_batching_duration()));
				_renderer->draw_debug_string(make_formattable_string("    constraint solving=%.6fs", sim->query_constraint_solving_duration()));
			}
#endif

			bool is_capturing_mouse = _renderer->is_capturing_mouse();
			bool is_fullscreen = _renderer->is_fullscreen();

			_renderer->draw_debug_string(make_formattable_string("\nInteraction:"), basic_color::azure);
			_renderer->draw_debug_string(make_formattable_string("is capturing mouse: %s", is_capturing_mouse ? "yes" : "no"), 
										 is_capturing_mouse ? basic_color::spring : basic_color::rose);
			_renderer->draw_debug_string(make_formattable_string("is fullscreen: %s", is_fullscreen ? "yes" : "no"),
										 is_fullscreen ? basic_color::spring : basic_color::rose);
			
			auto clock_time = util::system_time_to_string("%H:%M:%S", stdchr::system_clock::now());
			int32 window_width, window_height;
			_renderer->query_window_size(window_width, window_height);
			
			_renderer->draw_debug_string("\nSystem:", basic_color::light_gray);
			_renderer->draw_debug_string(make_formattable_string("clock: %s\nscreen: %dx%d",
										 clock_time.c_str(), window_width, window_height));
			
			auto world_time = util::system_time_to_string("%H:%M:%S", stdchr::system_clock::time_point{ _world_time });
			
			_renderer->draw_debug_string("\nEngine:", basic_color::light_gray);
			_renderer->draw_debug_string(make_formattable_string("world time: %s\ntime dilation: x%.2f",
										 world_time.c_str(), _time_dilation));
			
			auto camera_entity = _world->current_camera_entity();
			auto camera = camera_entity->access_camera();

			auto camera_position = camera->get_camera_position().to_string();
			auto camera_rotation = camera->get_camera_rotation().to_euler();

			auto camera_translation_speed = camera_entity->get_translation_speed();
			auto camera_rotation_speed = camera_entity->get_rotation_speed();
			auto camera_fov = camera->frustum_field_of_view();
			
			_renderer->draw_debug_string("\nWorld:", basic_color::light_gray);
			_renderer->draw_debug_string(make_formattable_string("num_entities: %llu", 
				                         _world->num_entities()));
			_renderer->draw_debug_string(make_formattable_string("position: %s\nlinear speed: %.1f",
										 camera_position.c_str(), camera_translation_speed));
			_renderer->draw_debug_string(make_formattable_string("rotation: pitch=%.2f yaw=%.2f\nangular speed: %.2f",
										 camera_rotation.y, camera_rotation.z, camera_rotation_speed));
			_renderer->draw_debug_string(make_formattable_string("fov: %.1f",
										 camera_fov));
		}
	}
	
	void gaea_engine::_log_simulation_times(implementation::engine_frame_duration const& frame_duration)
	{
#if GAEA_USING_SYCL
		if (_using_sycl_simulator)
		{
			auto simulator_ptr = static_cast<sycl_simulator*>(_simulator);
			
			_log_manager.log(_profiling_log_id,
				make_formattable_string("%.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f",
					frame_duration.physics,
					_simulator->query_step_duration(),
					_simulator->query_collision_detection_duration(),
					_simulator->query_broad_phase_duration(),
					_simulator->query_narrow_phase_duration(),
					simulator_ptr->query_contact_generation_duration(),
					_simulator->query_collision_resolution_duration(),
					simulator_ptr->query_constraint_batching_duration(),
					simulator_ptr->query_constraint_solving_duration()
				)
			);
		}
		else
#endif
		{
			_log_manager.log(_profiling_log_id,
				make_formattable_string("%.6f, %.6f, %.6f, %.6f, %.6f, %.6f",
					frame_duration.physics,
					_simulator->query_step_duration(),
					_simulator->query_collision_detection_duration(),
					_simulator->query_broad_phase_duration(),
					_simulator->query_narrow_phase_duration(),
					_simulator->query_collision_resolution_duration()
				)
			);
		}
	}

	void gaea_engine::_on_window_focus(bool focused)
	{
		if (_renderer->is_fullscreen())
		{
			if (focused != _renderer->is_capturing_mouse())
			{
				_renderer->toggle_mouse_capture();
			}
		}
	}

	void gaea_engine::_on_window_close()
	{
		_terminate_engine();
	}

	void gaea_engine::_terminate_engine()
	{
		if (_renderer->is_capturing_mouse())
		{
			_renderer->toggle_mouse_capture();
		}
		if (_renderer->is_fullscreen())
		{
			_renderer->toggle_fullscreen();
		}

		_should_run = false;
	}
}
