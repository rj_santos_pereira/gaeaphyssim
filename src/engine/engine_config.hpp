#ifndef ENGINE_DEFAULT_CONFIG_INCLUDE_GUARD
#define ENGINE_DEFAULT_CONFIG_INCLUDE_GUARD

#include "core/core_common.hpp"

// Engine static configuration

// Engine
#define GAEA_TEST_EXECUTION 0
#define GAEA_TEST_FUNCTION_DEFINITION test_parallel_reduce

#define GAEA_PROFILE_EXECUTION 0
#define GAEA_PROFILE_CPU 1
#define GAEA_PROFILE_DURATION 30

#define GAEA_DEBUG_EXECUTION_BREAK_AT_START 0

#define GAEA_DEBUG_PRINT_RANDOM_SAMPLE 0
#define GAEA_DEBUG_PRINT_DEVICE_PROPERTIES 0

#define GAEA_SHOULD_LIMIT_FRAMERATE 1

// Graphics
#define GAEA_USING_LIGHTING_MODEL 1

// Physics
#define GAEA_PROFILE_SYCL_KERNELS 1
#define GAEA_DEBUG_SYCL_KERNELS 0

#define GAEA_SHOULD_LOG_SIMULATION_TIMES 1

#define GAEA_NATIVE_SORT_SWEEP_USING_PARALLEL_SOLVER 1
#define GAEA_SYCL_SORT_SWEEP_USING_PARTIAL_SORT 1

#define GAEA_DEBUG_DISPLAY_BOUNDING_BOXES 1
#define GAEA_DEBUG_CONTACT_MANIFOLD 0
#define GAEA_DEBUG_CONSTRAINT_BATCHING 0

#pragma region GAEA_CONFIG_INTERNAL

// Using https://stackoverflow.com/a/11763277

#define GAEA_CONFIG_DECLARE_PATH(identifier, path_string) \
	namespace path\
	{\
		inline constexpr auto identifier = path_string;\
	}

#define GAEA_CONFIG_DECLARE_SECTION(identifier, name) \
	namespace section\
	{\
		inline constexpr auto identifier = GAEA_TOKEN_STRINGIFICATION(name);\
	}

#define GAEA_CONFIG_DECLARE_PROPERTY_internal_2(identifier, name) \
	namespace property\
	{\
		inline constexpr auto identifier = GAEA_TOKEN_STRINGIFICATION(name);\
	}

#define GAEA_CONFIG_DECLARE_PROPERTY_internal_3(identifier, name, type) \
	namespace property\
	{\
		inline constexpr auto identifier = GAEA_TOKEN_STRINGIFICATION(name);\
		/** Default type of this property */\
		using GAEA_TOKEN_CONCATENATION(identifier, _t) = type;\
		/** Default value of this property */\
		inline const auto GAEA_TOKEN_CONCATENATION(identifier, _v) = type();\
	}

#define GAEA_CONFIG_DECLARE_PROPERTY_internal_4(identifier, name, type, value) \
	namespace property\
	{\
		inline constexpr auto identifier = GAEA_TOKEN_STRINGIFICATION(name);\
		/** Default type of this property */\
		using GAEA_TOKEN_CONCATENATION(identifier, _t) = type;\
		/** Default value of this property */\
		inline const auto GAEA_TOKEN_CONCATENATION(identifier, _v) = type(value);\
	}

#define GAEA_CONFIG_DECLARE_PROPERTY_OVERLOAD_internal(_0, _1, _2, overload, ...) \
	GAEA_TOKEN_CONCATENATION(GAEA_CONFIG_DECLARE_PROPERTY_internal, overload)

#define GAEA_CONFIG_DECLARE_PROPERTY(identifier, ...) \
	GAEA_TOKEN_EXPANSION(GAEA_CONFIG_DECLARE_PROPERTY_OVERLOAD_internal(__VA_ARGS__, _4, _3, _2)(identifier, __VA_ARGS__))

#pragma endregion

namespace gaea::config
{
	GAEA_CONFIG_DECLARE_PATH(default_engine, "config/default_engine.ini")
	GAEA_CONFIG_DECLARE_PATH(custom_engine, "config/custom_engine.ini")

	GAEA_CONFIG_DECLARE_PATH(basic_font_texture, "resource/fonts/basic_font.bmp")
//	GAEA_CONFIG_DECLARE_PATH(default_entity_texture, "resource/textures/entity/")

	GAEA_CONFIG_DECLARE_PATH(basic_font_shaders, "resource/shaders/fonts/basic_font/")
	GAEA_CONFIG_DECLARE_PATH(default_entity_shaders, "resource/shaders/entity/")

	GAEA_CONFIG_DECLARE_PATH(default_light_shaders, "resource/shaders/light/")
	
	GAEA_CONFIG_DECLARE_PATH(box_frame_shaders, "resource/shaders/box_frame/")

	GAEA_CONFIG_DECLARE_PATH(default_engine_icon, "resource/icons/engine/engine.png")
	
	GAEA_CONFIG_DECLARE_PATH(profiling_log_path, "logs/profiling/")

	// General
	GAEA_CONFIG_DECLARE_SECTION(general, General)

	GAEA_CONFIG_DECLARE_PROPERTY(max_frame_rate, MaxFrameRate, uint16, 240)
	GAEA_CONFIG_DECLARE_PROPERTY(random_engine_seed, RandomEngineSeed, std::string)

	// Graphics
	GAEA_CONFIG_DECLARE_SECTION(graphics, Graphics)

	GAEA_CONFIG_DECLARE_PROPERTY(using_mesh_instancing, UsingMeshInstancing, bool, true)
//	GAEA_CONFIG_DECLARE_PROPERTY(using_lighting_model, UsingLightingModel, bool, true)

	// Physics
	GAEA_CONFIG_DECLARE_SECTION(physics, Physics)
	
	GAEA_CONFIG_DECLARE_PROPERTY(using_sycl_simulator, UsingSYCLSimulator, bool, false)

	GAEA_CONFIG_DECLARE_PROPERTY(sycl_device_name, SYCLDeviceName, std::string)
//	GAEA_CONFIG_DECLARE_PROPERTY(sycl_num_lag_frames, SYCLNumLagFrames, uint32, 1)
	
	GAEA_CONFIG_DECLARE_PROPERTY(max_substep_frequency, MaxSubstepFrequency, uint32, 120)
	GAEA_CONFIG_DECLARE_PROPERTY(max_substep_count, MaxSubstepCount, uint32, 6)

	GAEA_CONFIG_DECLARE_PROPERTY(simulate_thread_count, SimulateThreadCount, uint32, 0)
	GAEA_CONFIG_DECLARE_PROPERTY(simulate_thread_ratio, SimulateThreadRatio, uint32, 2)

	GAEA_CONFIG_DECLARE_PROPERTY(native_broadphase_using_parallel_solver, NativeBroadphaseUsingParallelSolver, bool, true)
	GAEA_CONFIG_DECLARE_PROPERTY(sycl_broadphase_using_partial_sort, SYCLBroadphaseUsingPartialSort, bool, true)

	// Scene
	GAEA_CONFIG_DECLARE_SECTION(scene, Scene)
	
	GAEA_CONFIG_DECLARE_PROPERTY(default_scene_name, DefaultSceneName, std::string)
	GAEA_CONFIG_DECLARE_PROPERTY(default_num_entities, DefaultNumEntities, uint32)
}

#endif