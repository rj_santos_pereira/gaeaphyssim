#include "engine/scene/gaea_entity.hpp"

#include "engine/gaea_engine.hpp"
#include "engine/scene/gaea_world.hpp"
#include "library/math/transform.hpp"
#include "system/config_manager.hpp"

namespace gaea
{
	gaea_entity::gaea_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters)
		: _graphics_body{}
		, _physics_body{}
		, _sid{ engine->make_sequential_id() + 1 } // Ensure it is never 0
		, _name{ parameters.name.append("_").append(std::to_string(_sid)) }
		, _tick_frequency{ parameters.tick.frequency }
		, _last_tick_time{ 0.f }
		, _tick_stage{ uint8(parameters.tick.stage) }
		, _should_setup_graphics{ parameters.should_setup_graphics }
		, _should_setup_physics{ parameters.should_setup_physics }
		, _should_tick_during_pause{ parameters.tick.should_tick_during_pause }
		, _has_executed_begin_run{ false }
		, _has_executed_end_run{ false }
		, _has_executed_tick{ false }
	{
		_init_graphics_body(parameters);
		_init_physics_body(parameters);
	}

	void gaea_entity::begin_run()
	{
		std::function<transform()> callback;
		switch(_physics_body.index())
		{
		case 1:
			callback = _setup_physics_body(std::addressof(std::get<1>(_physics_body)));
			break;
#if GAEA_USING_SYCL
		case 2:
			callback = _setup_physics_body(std::addressof(std::get<2>(_physics_body)));
			break;
#endif
		default:
			GAEA_FATAL(gaea_entity, "Invalid physics body index!");
		}

		switch(_graphics_body.index())
		{
		case 1:
			_setup_graphics_body(std::addressof(std::get<1>(_graphics_body)), std::move(callback));
			break;
		case 2:
			_setup_graphics_body(std::addressof(std::get<2>(_graphics_body)), std::move(callback));
			break;
		default:
			GAEA_FATAL(gaea_entity, "Invalid graphics body index!");
		}

		_has_executed_begin_run = true;
	}

	void gaea_entity::end_run()
	{
		switch (_graphics_body.index())
		{
		case 1:
			_setdown_graphics_body(std::addressof(std::get<1>(_graphics_body)));
			break;
		case 2:
			_setdown_graphics_body(std::addressof(std::get<2>(_graphics_body)));
			break;
		default:
			GAEA_FATAL(gaea_entity, "Invalid graphics body index!");
		}
		
		switch (_physics_body.index())
		{
		case 1:
			_setdown_physics_body(std::addressof(std::get<1>(_physics_body)));
			break;
#if GAEA_USING_SYCL
		case 2:
			_setdown_physics_body(std::addressof(std::get<2>(_physics_body)));
			break;
#endif
		default:
			GAEA_FATAL(gaea_entity, "Invalid physics body index!");
		}
		
		_has_executed_end_run = true;
	}

	void gaea_entity::tick(float32 delta)
	{
		GAEA_UNUSED(delta);

		_has_executed_tick = true;
		
		_last_tick_time = 0.f;
	}

	bool gaea_entity::should_tick() const
	{
		return _tick_stage != uint8(tick_stage::none) && (_should_tick_during_pause || !engine->is_paused());
	}

	float32 gaea_entity::get_tick_frequency() const
	{
		return _tick_frequency;
	}

	tick_stage gaea_entity::get_tick_stage() const
	{
		return tick_stage(_tick_stage);
	}

	body_visibility gaea_entity::get_body_visibility() const
	{
		switch (_graphics_body.index())
		{
		case 1:
			return std::get<1>(_graphics_body).get_visibility();
#if GAEA_USING_SYCL
		case 2:
			return std::get<2>(_graphics_body).get_visibility();
#endif
		default:
			GAEA_FATAL(gaea_entity, "Invalid graphics body index!");
		}
	}

	body_mobility gaea_entity::get_body_mobility() const
	{
		switch (_physics_body.index())
		{
		case 1:
			return std::get<1>(_physics_body).get_mobility();
#if GAEA_USING_SYCL
		case 2:
			return std::get<2>(_physics_body).get_mobility();
#endif
		default:
			GAEA_FATAL(gaea_entity, "Invalid physics body index!");
		}
	}

	void gaea_entity::set_tick_frequency(float32 frequency)
	{
		_tick_frequency = frequency;
	}

	void gaea_entity::set_tick_stage(tick_stage stage)
	{
		_tick_stage = uint8(stage);
		// Update world after updating the tick stage
		engine->access_world<gaea_world>()->update_tick_stage(gaea_world::entity_invoke_tag{}, uid());
	}

	void gaea_entity::set_body_visibility(body_visibility visibility)
	{
		switch (_graphics_body.index())
		{
		case 1:
			std::get<1>(_graphics_body).set_visibility(visibility);
		case 2:
			std::get<2>(_graphics_body).set_visibility(visibility);
		default:
			GAEA_FATAL(gaea_entity, "Invalid graphics body index!");
		}
	}

	void gaea_entity::set_body_mobility(body_mobility mobility)
	{
		switch (_physics_body.index())
		{
		case 1:
			std::get<1>(_physics_body).set_mobility(mobility);
#if GAEA_USING_SYCL
		case 2:
			std::get<2>(_physics_body).set_mobility(mobility);
#endif
		default:
			GAEA_FATAL(gaea_entity, "Invalid physics body index!");
		}
	}

	uint64 gaea_entity::uid() const
	{
		return _sid;
	}

	std::string gaea_entity::name() const
	{
		return _name;
	}

	std::string gaea_entity::serialize()
	{
		// TODO implement
		GAEA_DEBUG_BREAK();
		return "";
	}

	void gaea_entity::deserialize(std::string&& data)
	{
		// TODO implement
		GAEA_DEBUG_BREAK();
		GAEA_UNUSED(data);
	}

	float32 gaea_entity::update_last_tick_time(world_invoke_tag, float32 delta)
	{
		// World is ticking now, mark this entity as 'not ticked'
		_has_executed_tick = false;
		
		return _last_tick_time += delta;
	}

	bool gaea_entity::check_is_running(world_invoke_tag) const
	{
		return _has_executed_begin_run && !_has_executed_end_run;
	}

	bool gaea_entity::check_has_executed_begin_run(world_invoke_tag) const
	{
		return _has_executed_begin_run;
	}

	bool gaea_entity::check_has_executed_end_run(world_invoke_tag) const
	{
		return _has_executed_end_run;
	}

	bool gaea_entity::check_has_executed_tick(world_invoke_tag) const
	{
		return _has_executed_tick;
	}

	void gaea_entity::_init_graphics_body(gaea_entity_spawn_parameters const& parameters)
	{
		if (static_cast<gaea_engine*>(engine)->is_using_mesh_instancing())
		{
			auto& body = _graphics_body.emplace<gl_instanced_static_mesh_body>(
				parameters.graphics.instance_handle,
				transform{ parameters.scene.position, parameters.scene.rotation, parameters.scene.scaling },
				parameters.graphics.color,
				parameters.graphics.visibility
			);

			if (!parameters.graphics.instance_handle)
			{
				body.set_vertex_point_array(parameters.graphics.vertex_array);
				body.set_vertex_normal_array(parameters.graphics.normal_array);
				body.set_vertex_uv_array(parameters.graphics.uv_array);
				body.set_index_array(parameters.graphics.index_array);
			}
		}
		else
		{
			_graphics_body.emplace<gl_static_mesh_body>(
				parameters.graphics.vertex_array, parameters.graphics.normal_array,
				parameters.graphics.uv_array, parameters.graphics.index_array,
				transform{ parameters.scene.position, parameters.scene.rotation, parameters.scene.scaling },
				parameters.graphics.color,
				parameters.graphics.visibility
			);
		}
	}

	void gaea_entity::_init_physics_body(gaea_entity_spawn_parameters const& parameters)
	{
#if GAEA_USING_SYCL
		if (static_cast<gaea_engine*>(engine)->is_using_sycl_simulator())
		{
			_physics_body.emplace<sycl_rigid_body>(
				parameters.physics.mass, parameters.physics.inertia,
				parameters.scene.position, parameters.scene.rotation,
				parameters.scene.scaling, parameters.physics.geometry,
				parameters.physics.restitution_coefficient,
				parameters.physics.friction_coefficient,
				parameters.physics.entity_type,
				parameters.physics.behavior_type,
				parameters.physics.mobility
			);
		}
		else
#endif
		{
			_physics_body.emplace<native_rigid_body>(
				parameters.physics.mass, parameters.physics.inertia,
				parameters.scene.position, parameters.scene.rotation,
				parameters.scene.scaling, parameters.physics.geometry,
				parameters.physics.restitution_coefficient,
				parameters.physics.friction_coefficient,
				parameters.physics.entity_type,
				parameters.physics.behavior_type,
				parameters.physics.mobility
			);
		}
	}

}
