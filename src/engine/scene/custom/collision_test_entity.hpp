#ifndef ENGINE_SCENE_CUSTOM_COLLISION_TEST_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_CUSTOM_COLLISION_TEST_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/scene/gaea_entity.hpp"
#include "system/timer_manager.hpp"

namespace gaea
{
	class collision_test_entity : public gaea_entity
	{
		using base = gaea_entity;
		
	public:
		explicit collision_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters);
		
		virtual void tick(float32 delta) override;
		
	private:
		float32 _total_delta;

		timer_handle* _handle;
		
	};
}

#endif
