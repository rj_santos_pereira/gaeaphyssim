#include "engine/scene/custom/tick_test_entity.hpp"

namespace gaea
{
	tick_test_entity::tick_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, std::move(parameters) }
	{
	}

	void tick_test_entity::begin_run()
	{
		base::begin_run();
	}

	void tick_test_entity::end_run()
	{
		base::end_run();
	}

	void tick_test_entity::tick(float32 delta)
	{
		base::tick(delta);

		std::string stage_str;
		tick_stage next_stage = tick_stage::none;
		
		switch (get_tick_stage())
		{
		case tick_stage::pre_physics: 
			stage_str = "pre_physics";
			next_stage = tick_stage::during_physics;
			break;
		case tick_stage::during_physics: 
			stage_str = "during_physics";
			next_stage = tick_stage::post_physics;
			break;
		case tick_stage::post_physics: 
			stage_str = "post_physics";
			next_stage = tick_stage::pre_physics;
			break;
		}

		GAEA_INFO(tick_test_entity, "Frame: %llu;\tStage: %s", 
			engine->frame_number(), 
			stage_str.c_str());
		
		set_tick_stage(next_stage);
	}
}
