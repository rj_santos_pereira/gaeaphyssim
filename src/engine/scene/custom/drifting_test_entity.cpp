#include "engine/scene/custom/drifting_test_entity.hpp"

#include "system/random_engine.hpp"

namespace gaea
{
	drifting_test_entity::drifting_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, std::move(parameters) }
		, _force{ zero_vector }
		, _torque{ zero_vector }
		, _handle{ nullptr }
	{
	}

	void drifting_test_entity::begin_run()
	{
		base::begin_run();
		
		// Testing physics simulation

		auto random_engine = engine->access_random_engine();

		auto timer_manager = engine->access_timer_manager();

		_handle = timer_manager->add_timer(
			[this, random_engine]
			{
				_force = svector{
					float32(random_engine->generate_int(-30, +30)),
					float32(random_engine->generate_int(-30, +30)),
					float32(random_engine->generate_int(-30, +30))
				};
				_torque = svector{
					float32(random_engine->generate_int(-30, +30)),
					float32(random_engine->generate_int(-30, +30)),
					float32(random_engine->generate_int(-30, +30))
				};
			},
			true,
			stdchr::seconds(5),
			stdchr::seconds(random_engine->generate_int(0, 10))
		);
	}

	void drifting_test_entity::end_run()
	{
		auto timer_manager = engine->access_timer_manager();

		timer_manager->remove_timer(_handle);

		base::end_run();
	}

	void drifting_test_entity::tick(float32 delta)
	{
		base::tick(delta);

		auto physics_body = access_physics_body();
		
#if 0
		GAEA_INFO(drifting_test_entity, "Body '%s' data:\nposition: %s rotation: %s\nlin_vel: %s ang_vel: %s"/*"\nlin_acc: %s ang_acc: %s"*/,
			name().c_str(),
			physics_body->get_position().to_string(1).c_str(),
			physics_body->get_rotation().to_string(1).c_str(),
			physics_body->query_world_linear_velocity().to_string(3).c_str(),
			physics_body->query_body_angular_velocity().to_string(3).c_str()
			//physics_body->query_world_linear_acceleration().to_string(1).c_str(),
			//physics_body->query_world_angular_acceleration().to_string(1).c_str()
		);
#endif

		physics_body->apply_world_force(_force * delta);
		physics_body->apply_body_torque(_torque * delta);

		physics_body->apply_world_force(vector{ 0.0f, 0.0f, 9.8f });
	}
}
