#include "engine/scene/custom/interact_test_entity.hpp"

#include "system/input_manager.hpp"

namespace gaea
{
	interact_test_entity::interact_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, parameters }
		, _force{ zero_vector }
		, _torque{ zero_vector }
		, _button_state{ 0 }
		, _is_active{ false }
	{
	}

	void interact_test_entity::begin_run()
	{
		base::begin_run();

		auto input_manager_ptr = engine->access_input_manager();

		input_manager_ptr->bind_key_button(input_key::key_left_shift, this, std::bind(&interact_test_entity::_on_change_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager_ptr->bind_key_button(input_key::key_right_shift, this, std::bind(&interact_test_entity::_on_change_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		input_manager_ptr->bind_key_button(input_key::key_I, this, std::bind(&interact_test_entity::_on_apply_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager_ptr->bind_key_button(input_key::key_K, this, std::bind(&interact_test_entity::_on_apply_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager_ptr->bind_key_button(input_key::key_J, this, std::bind(&interact_test_entity::_on_apply_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager_ptr->bind_key_button(input_key::key_L, this, std::bind(&interact_test_entity::_on_apply_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager_ptr->bind_key_button(input_key::key_U, this, std::bind(&interact_test_entity::_on_apply_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager_ptr->bind_key_button(input_key::key_O, this, std::bind(&interact_test_entity::_on_apply_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	}

	void interact_test_entity::end_run()
	{
		auto input_manager_ptr = engine->access_input_manager();

        input_manager_ptr->bind_key_button(input_key::key_left_shift, this, std::bind(&interact_test_entity::_on_change_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        input_manager_ptr->bind_key_button(input_key::key_right_shift, this, std::bind(&interact_test_entity::_on_change_force_button, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

        input_manager_ptr->unbind_key_button(input_key::key_I, this);
		input_manager_ptr->unbind_key_button(input_key::key_K, this);
		input_manager_ptr->unbind_key_button(input_key::key_J, this);
		input_manager_ptr->unbind_key_button(input_key::key_L, this);
		input_manager_ptr->unbind_key_button(input_key::key_U, this);
		input_manager_ptr->unbind_key_button(input_key::key_O, this);
		
		base::end_run();
	}

	void interact_test_entity::tick(float32 delta)
	{
		base::tick(delta);

		auto physics_body = access_physics_body();

		physics_body->apply_body_force(_force);
		physics_body->apply_body_torque(_torque);
	}

	void interact_test_entity::set_active(bool is_active)
	{
		_is_active = is_active;
	}

	bool interact_test_entity::get_active() const
	{
		return _is_active;
	}

	void interact_test_entity::_on_apply_force_button(input_key key, input_state state, input_modifier modifier)
	{
		if (_is_active)
		{
			if (state != input_state::state_released)
			{
				switch (key)
				{
				case input_key::key_I:
					_button_state |= 0x01;
					break;
				case input_key::key_K:
					_button_state |= 0x02;
					break;
				case input_key::key_J:
					_button_state |= 0x04;
					break;
				case input_key::key_L:
					_button_state |= 0x10;
					break;
				case input_key::key_U:
					_button_state |= 0x20;
					break;
				case input_key::key_O:
					_button_state |= 0x40;
					break;
				}
			}
			else
			{
				switch (key)
				{
				case input_key::key_I:
					_button_state &= ~0x01;
					break;
				case input_key::key_K:
					_button_state &= ~0x02;
					break;
				case input_key::key_J:
					_button_state &= ~0x04;
					break;
				case input_key::key_L:
					_button_state &= ~0x10;
					break;
				case input_key::key_U:
					_button_state &= ~0x20;
					break;
				case input_key::key_O:
					_button_state &= ~0x40;
					break;
				}
			}

			_calculate_force(has_all_modifiers(modifier, input_modifier::modifier_shift));
		}
	}

    void interact_test_entity::_on_change_force_button(input_key key, input_state state, input_modifier modifier)
    {
        GAEA_UNUSED(key, state, modifier);

        _force = zero_vector;
        _torque = zero_vector;
    }

	void interact_test_entity::_calculate_force(bool is_angular)
	{
		vector force = zero_vector;
		if (_button_state & 0x01)
		{
			force += (is_angular ? vector{ 0.f, +2.f * pi_f, 0.f } : vector{ +20.f, 0.f, 0.f });
		}
		if (_button_state & 0x02)
		{
			force += (is_angular ? vector{ 0.f, -2.f * pi_f, 0.f } : vector{ -20.f, 0.f, 0.f });
		}
		if (_button_state & 0x04)
		{
			force += (is_angular ? vector{ -2.f * pi_f, 0.f, 0.f } : vector{ 0.f, +20.f, 0.f });
		}
		if (_button_state & 0x10)
		{
			force += (is_angular ? vector{ +2.f * pi_f, 0.f, 0.f } : vector{ 0.f, -20.f, 0.f });
		}
		if (_button_state & 0x20)
		{
			force += (is_angular ? vector{ 0.f, 0.f, +2.f * pi_f } : vector{ 0.f, 0.f, -20.f });
		}
		if (_button_state & 0x40)
		{
			force += (is_angular ? vector{ 0.f, 0.f, -2.f * pi_f } : vector{ 0.f, 0.f, +20.f });
		}

        // Scale the force by the mass (strictly not a force anymore, but meh)
        auto physics_body = access_physics_body();
        auto mass = physics_body->get_mass();
		if (is_angular)
		{
			_torque = force * mass;
		}
		else
		{
			_force = force * mass;
		}
	}
}
