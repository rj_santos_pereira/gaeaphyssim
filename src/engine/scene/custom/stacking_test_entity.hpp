#ifndef ENGINE_SCENE_CUSTOM_STACKING_TEST_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_CUSTOM_STACKING_TEST_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/scene/gaea_entity.hpp"
#include "system/timer_manager.hpp"

namespace gaea
{
	class stacking_test_entity : public gaea_entity
	{
		using base = gaea_entity;
		
	public:
		explicit stacking_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters);

		virtual void begin_run() override;
		
		virtual void tick(float32 delta) override;
		
	private:
		vector _force;
		vector _torque;

		float32 _total_delta;
		float32 _freeze_height;

	};
}

#endif
