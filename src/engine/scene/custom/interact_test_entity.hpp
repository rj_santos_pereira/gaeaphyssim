#ifndef ENGINE_SCENE_CUSTOM_INTERACT_TEST_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_CUSTOM_INTERACT_TEST_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/scene/gaea_entity.hpp"

namespace gaea
{
	class interact_test_entity final : public gaea_entity
	{
		using base = gaea_entity;
		
	public:
		explicit interact_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters);
		
		virtual void begin_run() override;
		virtual void end_run() override;
		virtual void tick(float32 delta) override;

		void set_active(bool is_active);

		bool get_active() const;

	private:
		void _on_apply_force_button(input_key key, input_state state, input_modifier modifier);
		void _on_change_force_button(input_key key, input_state state, input_modifier modifier);

		void _calculate_force(bool is_angular);

		vector _force;
		vector _torque;

		uint8 _button_state;
		bool _is_active;
		
	};
}

#endif
