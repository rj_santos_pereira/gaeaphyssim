#include "engine/scene/custom/stacking_test_entity.hpp"

#include "engine/gaea_engine.hpp"

namespace gaea
{
	stacking_test_entity::stacking_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, std::move(parameters) }
		, _force{ zero_vector }
		, _torque{ zero_vector }
		, _total_delta{ 0.f }
		, _freeze_height{ -10.f }
	{
	}

	void stacking_test_entity::begin_run()
	{
		base::begin_run();

		//_freeze_height = _physics_body.get_position().z * 0.5f - 0.25f;
	}

	void stacking_test_entity::tick(float32 delta)
	{
		base::tick(delta);
		
		_total_delta += delta;

		auto physics_body = access_physics_body();

		vector linear_momentum = physics_body->get_linear_momentum();
		float32 height = physics_body->get_position().z;

		//auto const& pos = physics_body->get_position();
		//auto [group, index] = _physics_body.get_body_partition(); // TODO add cast
		
		//GAEA_INFO(stacking_test_entity, "body_%llu pos=%s cg=%u ci=%u", uid(), pos.to_string(1).c_str(), group, index);
		
		if (static_cast<gaea_engine*>(engine)->is_using_sycl_simulator())
		{
			// Compensate for lag frames
			height += physics_body->get_mass_inverse() * linear_momentum.z * delta;
		}
		
		if (height <= _freeze_height)
		{
			physics_body->apply_world_force(vector{ 0.f, 0.f, +9.8f - linear_momentum.z });
			if (linear_momentum.z < 0.f)
			{
				linear_momentum.z = -linear_momentum.z * 0.5f;
				//linear_momentum.z = 0.f;
				physics_body->set_linear_momentum(linear_momentum);
			}
		}
	}
}
