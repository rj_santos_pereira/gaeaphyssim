#ifndef ENGINE_SCENE_CUSTOM_TICK_TEST_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_CUSTOM_TICK_TEST_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/scene/gaea_entity.hpp"

namespace gaea
{
	class tick_test_entity : public gaea_entity
	{
		using base = gaea_entity;
		
	public:
		explicit tick_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters);
		
		virtual void begin_run() override;
		virtual void end_run() override;
		virtual void tick(float32 delta) override;

	private:

	};
}

#endif
