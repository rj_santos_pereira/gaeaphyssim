#include "engine/scene/custom/collision_test_entity.hpp"

#include "system/random_engine.hpp"

namespace gaea
{
	collision_test_entity::collision_test_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, std::move(parameters) }
		, _total_delta{ 0.f }
		, _handle{ nullptr }
	{
	}

	void collision_test_entity::tick(float32 delta)
	{
		base::tick(delta);

		auto physics_body = access_physics_body();
		
#if 0
		if (get_body_mobility() == body_mobility::movable)
		{
			GAEA_INFO(collision_test_entity, "Body '%s' data:\nposition: %s rotation: %s\nlin_vel: %s ang_vel: %s",
				name().c_str(),
				physics_body->get_position().to_string().c_str(),
				physics_body->get_rotation().to_string().c_str(),
				physics_body->query_world_linear_velocity().to_string().c_str(),
				physics_body->query_body_angular_velocity().to_string().c_str()
			);
		}
#endif
		_total_delta += delta;

		auto rotation = physics_body->get_rotation();
		if (!rotation.is_unit())
		{
			GAEA_WARN(collision_test_entity, "Entity '%s' rotation quat is not unit! Quat=%s; Magn=%f", 
				name().c_str(), rotation.to_string(1).c_str(), rotation.magnitude());
		}

		if (std::remainder(_total_delta, 10) < 5.f)
		{
			float32 pos_y = physics_body->get_position().y;
			float32 force_y = pos_y != 0.f ? std::copysign(5.f, -pos_y) : 0.f;
			physics_body->apply_world_force(vector{ 0.f, force_y, 0.f });
		}
		
		physics_body->apply_world_force(vector{ 0.0f, 0.0f, 9.8f });
	}
}
