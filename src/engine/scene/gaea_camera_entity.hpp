#ifndef ENGINE_SCENE_GAEA_CAMERA_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_GAEA_CAMERA_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/graphics/gl_camera.hpp"
#include "engine/scene/gaea_entity.hpp"

namespace gaea
{
	struct gaea_camera_entity_spawn_parameters
	{
		struct
		{
			float32 field_of_view = 75.0f;
			bool is_fov_horizontal = true;

			float32 frustum_near_plane = 0.1f;
			float32 frustum_far_plane = 1000.f;
		}
		camera;
		
		struct
		{
			svector position = zero_svector;
			svector rotation = zero_svector; // This is specified as Euler angles, roll will be discarded
		}
		scene;

		std::string name = GAEA_TOKEN_STRINGIFICATION(gaea_camera_entity);

	};
	
	class gaea_camera_entity final : public gaea_entity
	{
		using base = gaea_entity;

	public:
		explicit gaea_camera_entity(entity_invoke_tag, gaea_camera_entity_spawn_parameters parameters);

		virtual void begin_run() override;
		virtual void end_run() override;
		
		virtual void tick(float32 delta) override;

		void set_translation_speed(float32 power);
		
		void set_rotation_speed(float32 power);

		float32 get_translation_speed() const;
		
		float32 get_rotation_speed() const;

		gl_camera* access_camera();

	private:
		void _on_translate_camera(input_key key, input_state state, input_modifier modifier);

	    void _on_rotate_camera(input_key key, input_state state, input_modifier modifier);
        void _on_rotate_camera(float32 pos_x, float32 pos_y, float32 pos_delta_x, float32 pos_delta_y, input_modifier modifier);

		void _on_adjust_fov(input_key key, input_state state, input_modifier modifier);
		
		void _on_adjust_velocity(input_key key, input_state state, input_modifier modifier);
		void _on_adjust_velocity(float32 delta_x, float32 delta_y, input_modifier modifier);
		
		gl_camera _camera;

		float32 _linear_power;
		svector _linear_velocity;

		float32 _angular_power;
		svector _angular_velocity;

		svector _position;
		svector _rotation;

		bool _has_update;

	};
}

#endif
