#ifndef ENGINE_SCENE_GAEA_WORLD_INCLUDE_GUARD
#define ENGINE_SCENE_GAEA_WORLD_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/scene/gaea_camera_entity.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "library/containers.hpp"

namespace gaea
{
	class gaea_world : public base_world
	{
	public:
		class engine_invoke_tag { explicit engine_invoke_tag(){} friend class gaea_engine; };
		class entity_invoke_tag { explicit entity_invoke_tag(){} friend class gaea_entity; };
		
		explicit gaea_world();
		virtual ~gaea_world();

		virtual void spawn_world(void* parameters) override;
		
		virtual uint64 spawn_entity(base_entity* entity) override;
		virtual void despawn_entity(uint64 uid) override;

		template <class EntityType, class SpawnParamsType>
		EntityType* spawn_entity(SpawnParamsType const& parameters);
		
		virtual void begin_run() override;
		virtual void end_run() override;

		virtual void tick(tick_stage stage, float32 delta) override;
		virtual bool is_ticking() const override;

		virtual tick_stage current_tick_stage() const override;
		
		virtual std::string serialize() override;
		virtual void deserialize(std::string&& data) override;

		virtual uint64 num_entities() const override;
		virtual base_entity* find_entity(uint64 uid) const override;

		virtual void set_spawn_limit_per_frame(uint64 limit) override;
		virtual void set_despawn_limit_per_frame(uint64 limit) override;

		virtual uint64 get_spawn_limit_per_frame() const override;
		virtual uint64 get_despawn_limit_per_frame() const override;
		
		gaea_camera_entity* current_camera_entity() const;

		void update_tick_stage(entity_invoke_tag, uint64 uid);

		void handle_entity_changes(engine_invoke_tag);

	private:
		void _begin_run_entity(gaea_entity* entity);
		
		void _end_run_entity(gaea_entity* entity);
		
		void _add_entity_tick(gaea_entity* entity);

		void _remove_entity_tick(gaea_entity* entity);
		
		map<uint64, gaea_entity*> _entity_uid_map;
		
		array_set<gaea_entity*> _entity_array;
		array<gaea_entity*> _tickable_entity_array;
		
		queue<gaea_entity*> _spawning_entity_queue;
		queue<gaea_entity*> _despawning_entity_queue;
		queue<gaea_entity*> _updating_tick_stage_entity_queue;

		uint64 _spawn_limit_per_frame;
		uint64 _despawn_limit_per_frame;

		array<gaea_camera_entity*> _camera_entity_array;
		std::size_t _current_camera_index;
		
		std::size_t _dur_phys_index;
		std::size_t _post_phys_index;

		cubic_axis_aligned_bounding_box _world_bounds;

		tick_stage _current_tick_stage;
		bool _is_running;

	};

	template <class EntityType, class SpawnParamsType>
	EntityType* gaea_world::spawn_entity(SpawnParamsType const& parameters)
	{
		static_assert(std::is_base_of_v<gaea_entity, EntityType>);
		
		auto new_entity = new EntityType{ gaea_entity::world_invoke_tag{}, std::move(parameters) };

		if (spawn_entity(new_entity) == 0)
		{
			GAEA_WARN(gaea_world, "Error spawning entity!");
			
			delete new_entity;
			return nullptr;
		}

		if constexpr (std::is_base_of_v<gaea_camera_entity, EntityType>)
		{
			_camera_entity_array.push_back(new_entity);
		}

		return new_entity;
	}
}

#endif
