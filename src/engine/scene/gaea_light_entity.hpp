#ifndef ENGINE_SCENE_GAEA_LIGHT_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_GAEA_LIGHT_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"
#include "engine/graphics/gl_point_light.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "library/math/vector.hpp"

namespace gaea
{
	struct gaea_light_entity_spawn_parameters
	{
		struct
		{
			float32 ambient_factor = 0.1f;
			float32 diffuse_factor = 1.0f;
			float32 specular_factor = 0.5f;
		}
		light;
		
		struct
		{
			basic_color color = basic_color::white;

			body_visibility visibility = body_visibility::visible;
		}
		graphics;
		
		struct
		{
			svector position = zero_svector;
		}
		scene;

		std::string name = GAEA_TOKEN_STRINGIFICATION(gaea_light_entity);

	};

	class gaea_light_entity final : public gaea_entity
	{
		using base = gaea_entity;

	public:
		explicit gaea_light_entity(entity_invoke_tag, gaea_light_entity_spawn_parameters parameters);

		virtual void begin_run() override;
		virtual void end_run() override;

		void set_ambient_factor(float32 ambient_factor);
		void set_diffuse_factor(float32 diffuse_factor);
		void set_specular_factor(float32 specular_factor);

		float32 get_ambient_factor() const;
		float32 get_diffuse_factor() const;
		float32 get_specular_factor() const;

		gl_point_light* access_light();

	private:
		gl_point_light _light;

	};
}

#endif
