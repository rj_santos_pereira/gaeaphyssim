#ifndef ENGINE_SCENE_GAEA_ENTITY_INCLUDE_GUARD
#define ENGINE_SCENE_GAEA_ENTITY_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "engine/engine_config.hpp"
#include "engine/graphics/gl_instanced_static_mesh_body.hpp"
#include "engine/graphics/gl_static_mesh_body.hpp"
#include "engine/physics/native_rigid_body.hpp"
#include "engine/physics/sycl_rigid_body.hpp"
#include "engine/physics/physics_globals.hpp"

namespace gaea
{
	struct gaea_entity_spawn_parameters
	{
		struct
		{
			float32 frequency = 0.f;
			tick_stage stage = tick_stage::pre_physics;
			bool should_tick_during_pause = false;
		}
		tick;

		bool should_setup_graphics = true;
		struct
		{
			array<svector> vertex_array{};
			array<svector> normal_array{};
			array<texcoord> uv_array{};
			array<uint32> index_array{};
			
			std::shared_ptr<gl_instanced_static_mesh_body_handle> instance_handle;
			
			basic_color color = basic_color::white;

			body_visibility visibility = body_visibility::visible;
		}
		graphics;

		bool should_setup_physics = true;
		struct
		{
			float32 mass = 1.f;
			matrix3 inertia = matrix3{ identity_matrix_tag{} };

			polyhedron geometry{};

			float32 restitution_coefficient = 0.5f;
			float32 friction_coefficient = 0.5f;

			collision_entity::type entity_type = collision_entity::world_dynamic;
			collision_behavior::type behavior_type = collision_behavior::world_dynamic;

			body_mobility mobility = body_mobility::movable;
		}
		physics;

		struct
		{
			vector position = zero_vector;
			quaternion rotation = identity_quaternion;
			vector scaling = one_vector;
		}
		scene;

		std::string name = GAEA_TOKEN_STRINGIFICATION(gaea_entity);

	};

	class gaea_entity : public base_entity
	{
	protected:
		struct entity_invoke_tag{ explicit entity_invoke_tag(){} };
		
	public:
		class world_invoke_tag : public entity_invoke_tag { explicit world_invoke_tag(){} friend class gaea_world; };
		
		explicit gaea_entity(entity_invoke_tag, gaea_entity_spawn_parameters parameters);
		virtual ~gaea_entity() = default;

		virtual void begin_run() override;
		virtual void end_run() override;

		virtual void tick(float32 delta) override;
		virtual bool should_tick() const override;

		virtual float32 get_tick_frequency() const override final;
		virtual tick_stage get_tick_stage() const override final;
		
		virtual body_visibility get_body_visibility() const override;
		virtual body_mobility get_body_mobility() const override;

		virtual void set_tick_frequency(float32 frequency) override final;
		virtual void set_tick_stage(tick_stage stage) override final;
		
		virtual void set_body_visibility(body_visibility visibility) override;
		virtual void set_body_mobility(body_mobility mobility) override;

		virtual uint64 uid() const override final;
		virtual std::string name() const override final;

		virtual std::string serialize() override;
		virtual void deserialize(std::string&& data) override;

		template <class GraphicsBodyType = base_static_mesh_body>
		GraphicsBodyType* access_graphics_body();
		
		template <class PhysicsBodyType = base_rigid_body>
		PhysicsBodyType* access_physics_body();

		// World access methods

		float32 update_last_tick_time(world_invoke_tag, float32 delta);

		bool check_is_running(world_invoke_tag) const;
		
		bool check_has_executed_begin_run(world_invoke_tag) const;

		bool check_has_executed_end_run(world_invoke_tag) const;
		
		bool check_has_executed_tick(world_invoke_tag) const;

	protected:
		std::variant<std::monostate, gl_static_mesh_body, gl_instanced_static_mesh_body> _graphics_body;

		std::variant<std::monostate
                    , native_rigid_body
#if GAEA_USING_SYCL
                    , sycl_rigid_body
#endif
                    > _physics_body;

	private:
		void _init_graphics_body(gaea_entity_spawn_parameters const& parameters);
		void _init_physics_body(gaea_entity_spawn_parameters const& parameters);

		template <class GraphicsBodyType>
		void _setup_graphics_body(GraphicsBodyType* body, std::function<transform()>&& update_transform_callback);

		template <class PhysicsBodyType>
		std::function<transform()> _setup_physics_body(PhysicsBodyType* body);

		template <class GraphicsBodyType>
		void _setdown_graphics_body(GraphicsBodyType* body);

		template <class PhysicsBodyType>
		void _setdown_physics_body(PhysicsBodyType* body);

		uint64 _sid;
		std::string _name;

		float32 _tick_frequency;
		float32 _last_tick_time;

		uint8 _tick_stage				: 2;
		
		uint8 _should_setup_graphics	: 1;
		uint8 _should_setup_physics		: 1;

		uint8 _should_tick_during_pause	: 1;
		
		uint8 _has_executed_begin_run	: 1;
		uint8 _has_executed_end_run		: 1;
		uint8 _has_executed_tick		: 1;

	};

	template <class GraphicsBodyType>
	GraphicsBodyType* gaea_entity::access_graphics_body()
	{
		switch (_graphics_body.index())
		{
		case 1:
			if constexpr (std::is_base_of_v<GraphicsBodyType, gl_static_mesh_body>)
			{
				return std::addressof(std::get<gl_static_mesh_body>(_graphics_body));
			}
			else 
			{
				return nullptr;
			}
		case 2:
			if constexpr (std::is_base_of_v<GraphicsBodyType, gl_instanced_static_mesh_body>)
			{
				return std::addressof(std::get<gl_instanced_static_mesh_body>(_graphics_body));
			}
			else
			{
				return nullptr;
			}
		default:
			GAEA_FATAL(gaea_entity, "Invalid graphics body index!");
		}
	}

	template <class PhysicsBodyType>
	PhysicsBodyType* gaea_entity::access_physics_body()
	{
		switch (_physics_body.index())
		{
		case 1:
			if constexpr (std::is_base_of_v<PhysicsBodyType, native_rigid_body>)
			{
				return std::addressof(std::get<native_rigid_body>(_physics_body));
			}
			else
			{
				return nullptr;
			}
#if GAEA_USING_SYCL
		case 2:
			if constexpr (std::is_base_of_v<PhysicsBodyType, sycl_rigid_body>)
			{
				return std::addressof(std::get<sycl_rigid_body>(_physics_body));
			}
			else
			{
				return nullptr;
			}
#endif
		default:
			GAEA_FATAL(gaea_entity, "Invalid physics body index!");
		}
	}

	template <class GraphicsBodyType>
	void gaea_entity::_setup_graphics_body(GraphicsBodyType* body, base_static_mesh_body::update_transform_callback_type&& update_transform_callback)
	{
		if (_should_setup_graphics)
		{
			body->setup_shader_path(config::path::default_entity_shaders);

			body->setup_commands();

			if (update_transform_callback)
			{
				body->bind_update_transform_callback(std::move(update_transform_callback));
			}
		}
	}

	template <class PhysicsBodyType>
	base_static_mesh_body::update_transform_callback_type gaea_entity::_setup_physics_body(PhysicsBodyType* body)
	{
		if (_should_setup_physics)
		{
			body->setup_body();

			return base_static_mesh_body::update_transform_callback_type{
				[body]
				{
					return transform{
						body->get_position(),
						body->get_rotation(),
						body->get_scaling()
					};
				}
			};
		}

		return nullptr;
	}

	template <class GraphicsBodyType>
	void gaea_entity::_setdown_graphics_body(GraphicsBodyType * body)
	{
		if (_should_setup_graphics)
		{
			body->setdown_commands();

			body->bind_update_transform_callback(nullptr);
		}
	}

	template <class PhysicsBodyType>
	void gaea_entity::_setdown_physics_body(PhysicsBodyType * body)
	{
		if (_should_setup_physics)
		{
			body->setdown_body();
		}
	}
}

#endif
