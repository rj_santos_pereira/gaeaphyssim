#include "engine/scene/gaea_world.hpp"

namespace
{
	constexpr float32 default_kill_distance = 1.e+6f;
}

namespace gaea
{
	gaea_world::gaea_world()
		: _entity_uid_map{}
		, _entity_array{}
		, _tickable_entity_array{}
		, _spawning_entity_queue{}
		, _despawning_entity_queue{}
		, _updating_tick_stage_entity_queue{}
	    , _spawn_limit_per_frame{ 100 }
	    , _despawn_limit_per_frame{ 100 }
		, _camera_entity_array{}
		, _current_camera_index{ 0 }
		, _dur_phys_index{ 0 }
		, _post_phys_index{ 0 }
		, _world_bounds{ vector{ -default_kill_distance }, vector{ +default_kill_distance } }
		, _current_tick_stage{ tick_stage::none }
		, _is_running{ false }
	{
	}

	gaea_world::~gaea_world()
	{
		for (auto entity : _entity_array)
		{
			delete entity;
		}
	}

	void gaea_world::spawn_world(void* parameters)
	{
		GAEA_UNUSED(parameters);
	}

	uint64 gaea_world::spawn_entity(base_entity* entity)
	{
		auto new_entity = static_cast<gaea_entity*>(entity);

		if (_entity_array.contains(new_entity))
		{
			GAEA_ERROR(gaea_world, "Trying to spawn existing entity '%s'!", new_entity->name().c_str());
			return 0;
		}

		auto[_, success] = _entity_uid_map.try_emplace(new_entity->uid(), new_entity);

		if (!success)
		{
			GAEA_ERROR(gaea_world, "Trying to spawn entity with existing uid '%llu'!", new_entity->uid());
			return 0;
		}

		_entity_array.insert(new_entity);

		if (_is_running)
		{
			_spawning_entity_queue.push(new_entity);
		}
		else
		{
			_add_entity_tick(new_entity);
		}

		return new_entity->uid();
	}

	void gaea_world::despawn_entity(uint64 uid)
	{
		auto it = _entity_uid_map.find(uid);

		if (it == _entity_uid_map.end())
		{
			GAEA_WARN(gaea_world, "Could not find entity with uid '%llu'", uid);
			return;
		}
		
		auto old_entity = static_cast<gaea_entity*>(it->second);

		_despawning_entity_queue.push(old_entity);
	}

	void gaea_world::begin_run()
	{	
		for (auto entity : _entity_array)
		{
			_begin_run_entity(entity);
		}

		_is_running = true;
	}

	void gaea_world::end_run()
	{
		_is_running = false;
		
		for (auto entity : _entity_array)
		{
			if (entity->check_is_running(gaea_entity::world_invoke_tag{}))
			{
				_end_run_entity(entity);
			}
		}
	}

	void gaea_world::tick(tick_stage stage, float32 delta)
	{
		std::size_t start_index;
		std::size_t stop_index;

		switch (stage)
		{
		case tick_stage::pre_physics:
			start_index = 0;
			stop_index = _dur_phys_index;
			break;
		case tick_stage::during_physics:
			start_index = _dur_phys_index;
			stop_index = _post_phys_index;
			break;
		case tick_stage::post_physics:
			start_index = _post_phys_index;
			stop_index = _tickable_entity_array.size();
			break;
		case tick_stage::none:
		default:
			GAEA_ERROR(gaea_world, "Invalid tick stage value!");
			return;
		}

		_current_tick_stage = stage;
		
		for (std::size_t index = start_index; index < stop_index; ++index)
		{
			auto entity = _tickable_entity_array[index];

			if (entity->should_tick() && entity->update_last_tick_time(gaea_entity::world_invoke_tag{}, delta) >= entity->get_tick_frequency())
			{
				entity->tick(delta);

				if (!entity->check_has_executed_tick(gaea_entity::world_invoke_tag{}))
				{
					GAEA_ERROR(gaea_world, "Base entity '%s' has not ticked! Please check subclass code.", 
						entity->name().c_str());
				}
			}
		}

		_current_tick_stage = tick_stage::none;
	}

	bool gaea_world::is_ticking() const
	{
		return _is_running;
	}

	tick_stage gaea_world::current_tick_stage() const
	{
		return _current_tick_stage;
	}

	std::string gaea_world::serialize()
	{
		// TODO implement
		GAEA_DEBUG_BREAK();
		return "";
	}

	void gaea_world::deserialize(std::string&& data)
	{
		// TODO implement
		GAEA_DEBUG_BREAK();
		GAEA_UNUSED(data);
	}

	uint64 gaea_world::num_entities() const
	{
		return _entity_array.size();
	}

	base_entity* gaea_world::find_entity(uint64 uid) const
	{
		auto it = _entity_uid_map.find(uid);
		return it != _entity_uid_map.end() ? it->second : nullptr;
	}

    void gaea_world::set_spawn_limit_per_frame(uint64 limit)
    {
		_spawn_limit_per_frame = math::max(limit, 1llu);
    }

    void gaea_world::set_despawn_limit_per_frame(uint64 limit)
    {
		_despawn_limit_per_frame = math::max(limit, 1llu);
    }

    uint64 gaea_world::get_spawn_limit_per_frame() const
    {
		return _spawn_limit_per_frame;
    }

    uint64 gaea_world::get_despawn_limit_per_frame() const
    {
		return _despawn_limit_per_frame;
    }

    gaea_camera_entity* gaea_world::current_camera_entity() const
	{
		return _camera_entity_array[_current_camera_index];
	}

	void gaea_world::update_tick_stage(entity_invoke_tag, uint64 uid)
	{
		auto it = _entity_uid_map.find(uid);
		if (it == _entity_uid_map.end())
		{
			GAEA_ERROR(gaea_entity, "Trying to update tick for non-registered entity!");
			return;
		}

		_updating_tick_stage_entity_queue.push(it->second);
	}

	void gaea_world::handle_entity_changes(engine_invoke_tag)
	{
		std::size_t spawn_limit = _spawn_limit_per_frame;

		while (!_spawning_entity_queue.empty() && spawn_limit--)
		{
			auto entity = _spawning_entity_queue.front();
			_spawning_entity_queue.pop();

			_add_entity_tick(entity);
			_begin_run_entity(entity);
		}

		while (!_updating_tick_stage_entity_queue.empty())
		{
			auto entity = _updating_tick_stage_entity_queue.front();
			_updating_tick_stage_entity_queue.pop();

			_remove_entity_tick(entity);
			_add_entity_tick(entity);
		}

		std::size_t despawn_limit = _despawn_limit_per_frame;

		array<gaea_entity*> despawn_entity_array;
		despawn_entity_array.reserve(std::min(despawn_limit, _despawning_entity_queue.size()));

		while (!_despawning_entity_queue.empty() && despawn_limit--)
		{
			auto entity = _despawning_entity_queue.front();
			_despawning_entity_queue.pop();

			_end_run_entity(entity);
			_remove_entity_tick(entity);

			_entity_array.erase(entity);
			despawn_entity_array.push_back(entity);
		}

		auto entity_it = _entity_array.begin();
		while (entity_it != _entity_array.end())
		{
			auto entity = *entity_it;
			auto physics_body = entity->access_physics_body();

			auto const& position = physics_body->get_position();
			auto const& rotation = physics_body->get_rotation();

			if (position.has_nan() || rotation.has_nan() || !_world_bounds.contains_point(position))
			{
				GAEA_WARN(gaea_world, "Killing entity '%s', position/rotation [%s/%s] has NaNs or has exceeded kill_distance [%.1f]",
					entity->name().c_str(), position.to_string().c_str(), rotation.to_string().c_str(), default_kill_distance);

			    _end_run_entity(entity);
				_remove_entity_tick(entity);

				despawn_entity_array.push_back(entity);

				entity_it = _entity_array.erase(entity_it);
			}
			else
			{
				++entity_it;
			}
		}

		// If we are despawning entities, we need to flush the renderer before deleting them,
		// as the entities may have pending commands with references to them
		if (!despawn_entity_array.empty())
		{
			renderer->flush();

			for (auto entity : despawn_entity_array)
			{
				// Now delete the entities
				delete entity;
			}
		}
	}
	
	void gaea_world::_begin_run_entity(gaea_entity* entity)
	{
		entity->begin_run();

		if (!entity->check_has_executed_begin_run(gaea_entity::world_invoke_tag{}))
		{
			GAEA_ERROR(gaea_world, "Base entity '%s' has not executed begin_run! Please check subclass code.", 
				entity->name().c_str());
		}
	}

	void gaea_world::_end_run_entity(gaea_entity* entity)
	{
		entity->end_run();

		if (!entity->check_has_executed_end_run(gaea_entity::world_invoke_tag{}))
		{
			GAEA_ERROR(gaea_world, "Base entity '%s' has not executed end_run! Please check subclass code.", 
				entity->name().c_str());
		}
	}

	void gaea_world::_add_entity_tick(gaea_entity* entity)
	{
		switch (entity->get_tick_stage())
		{
		case tick_stage::none:
			break;
		case tick_stage::pre_physics:
			_tickable_entity_array.insert(_tickable_entity_array.begin(), entity);
			++_dur_phys_index;
			++_post_phys_index;
			break;
		case tick_stage::during_physics:
			_tickable_entity_array.insert(_tickable_entity_array.begin() += _dur_phys_index, entity);
			++_post_phys_index;
			break;
		case tick_stage::post_physics:
			_tickable_entity_array.insert(_tickable_entity_array.begin() += _post_phys_index, entity);
			break;
		}
	}

	void gaea_world::_remove_entity_tick(gaea_entity* entity)
	{
		auto it = std::find(_tickable_entity_array.begin(), _tickable_entity_array.end(), entity);
		if (it != _tickable_entity_array.end())
		{
			auto idx = std::size_t(std::distance(_tickable_entity_array.begin(), it));
			if (idx < _dur_phys_index)
			{
				--_dur_phys_index;
				--_post_phys_index;
			}
			else if (idx < _post_phys_index)
			{
				--_post_phys_index;
			}

			_tickable_entity_array.erase(it);
		}
	}
}
