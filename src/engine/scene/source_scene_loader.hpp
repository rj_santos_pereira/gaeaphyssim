#ifndef ENGINE_SCENE_SOURCE_SCENE_LOADER_INCLUDE_GUARD
#define ENGINE_SCENE_SOURCE_SCENE_LOADER_INCLUDE_GUARD

#include "core/core_minimal.hpp"

#include "library/containers.hpp"

#define GAEA_DECLARE_SCENE_INFO(name) static ::gaea::source_scene_storage GAEA_TOKEN_CONCATENATION(name, GAEA_TOKEN_CONCATENATION(__STORAGE_, __LINE__)){ GAEA_TOKEN_STRINGIFICATION(name), name }

namespace gaea
{
	class source_scene_loader
	{
		explicit source_scene_loader();
		
	public:
		source_scene_loader(source_scene_loader const&) = delete;
		source_scene_loader(source_scene_loader &&) = delete;
		
		source_scene_loader& operator=(source_scene_loader const&) = delete;
		source_scene_loader& operator=(source_scene_loader &&) = delete;
		
		void add_scene_info(std::string const& scene_name, void(*scene_address)());
		void remove_scene_info(std::string const& scene_name);

		bool load_scene(std::string const& scene_name);
		
		bool load_scene_from_config();

		static source_scene_loader& access();

	private:
		map<std::string, void(*)()> _scene_name_address_map;
		
	};

	struct source_scene_storage
	{
		explicit source_scene_storage(std::string const& scene_name, void(*scene_address)());
	};
}

#endif
