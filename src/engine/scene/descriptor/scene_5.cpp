#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/interact_test_entity.hpp"

#include "system/random_engine.hpp"

void scene_5()
{
	auto world = gaea::engine->access_world<gaea::gaea_world>();
	
	auto random_engine_ptr = gaea::engine->access_random_engine();

	gaea::gaea_entity_spawn_parameters spawn_params;

	spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
	spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
	spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
	spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;

	spawn_params.physics.geometry = gaea::simulation_primitives::cube;
	spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);

	spawn_params.scene.position = gaea::zero_vector;
	spawn_params.scene.scaling = gaea::vector{ 10.f, 10.f, 1.f };

	spawn_params.physics.mobility = gaea::body_mobility::immovable;

	//constexpr std::size_t num_entities_0 = 3;
	//for (std::size_t idx = 0; idx < num_entities_0; ++idx)
	{
//		spawn_params.scene.position = gaea::vector{ 0.f, 2.f * idx + -2.f, 0.f };
		{
			auto surface_entity = world->spawn_entity<gaea::interact_test_entity>(spawn_params);
			if (auto graphics_body = surface_entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
			{
				spawn_params.graphics.instance_handle = graphics_body->instance_handle();
			}
		}
	}

	spawn_params.scene.scaling = gaea::one_vector;
	
	spawn_params.physics.mobility = gaea::body_mobility::movable;

	constexpr std::size_t num_entities = 1;
	for (std::size_t idx = 0; idx < num_entities; ++idx)
	{
		//spawn_params.scene.position = gaea::vector{ 0.f, 4.f * idx + -4.f, 5.f };
		spawn_params.scene.position = gaea::vector{ 0.f, 0.f, 1.5f };
		/*spawn_params.scene.position = gaea::vector{
			float32(random_engine_ptr->generate_int(-25, +25)),
			float32(random_engine_ptr->generate_int(-25, +25)),
			25.f
		};*/

		spawn_params.graphics.color = gaea::basic_color(random_engine_ptr->generate_int(10, 15));

		auto entity = world->spawn_entity<gaea::interact_test_entity>(spawn_params);

		entity->set_active(true);
	}
}

GAEA_DECLARE_SCENE_INFO(scene_5);
