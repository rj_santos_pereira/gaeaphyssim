#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/collision_test_entity.hpp"

#include "system/timer_manager.hpp"

void scene_3()
{
	auto timer_manager = gaea::engine->access_timer_manager();

    auto world = gaea::engine->access_world<gaea::gaea_world>();

	gaea::gaea_entity_spawn_parameters spawn_params;

	spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
	spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
	spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
	spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;
	
	spawn_params.physics.geometry = gaea::simulation_primitives::cube;
	spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);

	static constexpr std::size_t num_entities = 1;
	static constexpr std::size_t spawn_entities = 256;
	static constexpr std::size_t spawn_delta = 500;

	auto spawner = [spawn_params, world]() mutable {
		static std::size_t global_idx = 0;

		std::size_t limit_idx = std::min(global_idx + spawn_entities, num_entities);
		for (std::size_t idx = global_idx; idx < limit_idx; ++idx)
		{
			spawn_params.scene.position = gaea::vector{ 5.f * idx, 0.f, 0.f };
			spawn_params.physics.mobility = gaea::body_mobility::immovable;
			//spawn_params.scene.rotation = gaea::quaternion::from_euler(gaea::vector{ 0.f, 30.f, 45.f });
			//spawn_params.scene.scaling = gaea::vector{ 2.0f };

			auto entity = world->spawn_entity<gaea::collision_test_entity>(spawn_params);

			if (idx == 0)
			{
				if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
				{
					spawn_params.graphics.instance_handle = graphics_body->instance_handle();
				}
			}

			spawn_params.scene.position = gaea::vector{ 5.f * idx, -10.f /*- 5.f * idx*/, 0.0f /*+ 0.5f*/ };
			spawn_params.physics.mobility = gaea::body_mobility::movable;
			spawn_params.scene.rotation = gaea::quaternion::from_euler(gaea::vector{ 0.f, 45.f, 45.f });
			//spawn_params.scene.rotation = gaea::quaternion::from_euler(gaea::vector{ 43.4665772f, 44.9794718f, 87.8311534f });
			//spawn_params.scene.rotation = gaea::quaternion::from_euler(gaea::vector{ 0.f, 60.f, 90.f });
			//spawn_params.scene.rotation = gaea::identity_quaternion;
			//spawn_params.scene.scaling = gaea::vector{ 2.0f };

			world->spawn_entity<gaea::collision_test_entity>(spawn_params);
		}
		
		global_idx += spawn_entities;
	};

#if 1
	auto spawner_timer = timer_manager->add_timer(
		spawner,
		true,
		stdchr::milliseconds(spawn_delta)
	);
	
	timer_manager->add_timer(
		[spawner_timer, timer_manager]
		{
			timer_manager->remove_timer(spawner_timer);
		},
		false,
		stdchr::milliseconds(spawn_delta * (num_entities / spawn_entities + 1) + 1)
	);
#else
	spawner();
#endif
}

GAEA_DECLARE_SCENE_INFO(scene_3);
