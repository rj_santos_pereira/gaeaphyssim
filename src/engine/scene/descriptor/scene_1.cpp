#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/drifting_test_entity.hpp"

#include "system/random_engine.hpp"

void scene_1()
{
	auto tex_image = gaea::texture::load("res/icons/engine/engine.png");

	auto random_engine = gaea::engine->access_random_engine();

	gaea::gaea_entity_spawn_parameters spawn_params;

	spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
	spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
	spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
	spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;

	spawn_params.physics.geometry = gaea::simulation_primitives::cube;
	spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);

	auto world = gaea::engine->access_world<gaea::gaea_world>();

	constexpr std::size_t num_entities = 10000;
	for (std::size_t idx = 0; idx < num_entities; ++idx)
	{
		//spawn_params.scene.position = gaea::zero_vector;
		//spawn_params.scene.rotation = quaternion::from_euler(vector{ 45.f, 0.f, 0.f });

		spawn_params.scene.position = gaea::vector{
			float32(random_engine->generate_int(-500, +500)),
			float32(random_engine->generate_int(-500, +500)),
			float32(random_engine->generate_int(-500, +500)),
		};
		spawn_params.scene.rotation = gaea::quaternion::from_euler(gaea::vector{
			float32(random_engine->generate_int(-180, +180)),
			float32(random_engine->generate_int(-90 , +90)),
			float32(random_engine->generate_int(-180, +180)),
			});

		spawn_params.graphics.color = gaea::basic_color(random_engine->generate_int(10, 15));

		auto entity = world->spawn_entity<gaea::drifting_test_entity>(spawn_params);

		if (idx == 0)
		{
			if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
			{
				spawn_params.graphics.instance_handle = graphics_body->instance_handle();
			}
#if 0
			graphics_body->set_texture_image(tex_image);
#endif
		}
	}
}

GAEA_DECLARE_SCENE_INFO(scene_1);
