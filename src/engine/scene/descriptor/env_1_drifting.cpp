#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/gaea_engine.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/drifting_test_entity.hpp"

#include "system/timer_manager.hpp"
#include "system/random_engine.hpp"

void env_1_drifting()
{
	auto random_engine = gaea::engine->access_random_engine();
	auto timer_manager = gaea::engine->access_timer_manager();

	timer_manager->add_timer(
		[&, random_engine]()
		{
			gaea::gaea_entity_spawn_parameters spawn_params;

			spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
			spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
			spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
			spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;
			spawn_params.graphics.color = gaea::basic_color::dark_gray;

			spawn_params.physics.geometry = gaea::simulation_primitives::cube;
			spawn_params.physics.mass = 1.f;
			spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);
			spawn_params.physics.mobility = gaea::body_mobility::movable;

			spawn_params.scene.scaling = gaea::one_vector;

			auto world = gaea::engine->access_world<gaea::gaea_world>();
			
			uint32 num_entities = 1000;
			//uint32 num_entities = static_cast<gaea::gaea_engine*>(gaea::engine)->profiling_num_entities();
			int32 entity_range = int32(num_entities / 20);

			for (uint32 i = 0; i < num_entities; ++i)
			{
				spawn_params.scene.position = gaea::vector{
					float32(random_engine->generate_int(-entity_range, +entity_range)),
					float32(random_engine->generate_int(-entity_range, +entity_range)),
					float32(random_engine->generate_int(-entity_range, +entity_range)),
				};
				spawn_params.scene.rotation = gaea::quaternion::from_euler(gaea::vector{
					float32(random_engine->generate_int(-180, +180)),
					float32(random_engine->generate_int(-90 , +90 )),
					float32(random_engine->generate_int(-180, +180)),
				});

				spawn_params.graphics.color = gaea::basic_color(random_engine->generate_int(10, 15));

				auto entity = world->spawn_entity<gaea::drifting_test_entity>(spawn_params);

				if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
				{
					spawn_params.graphics.instance_handle = graphics_body->instance_handle();
				}
			}
		},
		false,
		stdchr::seconds(0)
	);
}

GAEA_DECLARE_SCENE_INFO(env_1_drifting);