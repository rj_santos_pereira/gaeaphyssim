#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/gaea_engine.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/interact_test_entity.hpp"
#include "engine/scene/custom/stacking_test_entity.hpp"

#include "system/timer_manager.hpp"
#include "system/random_engine.hpp"

void env_4_stacking()
{
	auto random_engine = gaea::engine->access_random_engine();
	auto timer_manager = gaea::engine->access_timer_manager();

	timer_manager->add_timer(
		[&, random_engine]()
		{
			gaea::gaea_entity_spawn_parameters spawn_params;

			spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
			spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
			spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
			spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;
			spawn_params.graphics.color = gaea::basic_color::dark_gray;

			spawn_params.physics.geometry = gaea::simulation_primitives::cube;
			spawn_params.physics.mass = 1.f;
			spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);
			spawn_params.physics.restitution_coefficient = 0.1f;

		    spawn_params.tick.stage = gaea::tick_stage::none;

			auto world = gaea::engine->access_world<gaea::gaea_world>();

			{
				spawn_params.physics.mobility = gaea::body_mobility::immovable;
				spawn_params.scene.scaling = gaea::vector{ 1000.f, 1000.f, 1.f };

				auto entity = world->spawn_entity<gaea::stacking_test_entity>(spawn_params);

				if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
				{
					spawn_params.graphics.instance_handle = graphics_body->instance_handle();
				}
			}

			spawn_params.physics.mobility = gaea::body_mobility::movable;
			spawn_params.scene.scaling = gaea::one_vector;

			uint32 num_layers = 6;
			//uint32 num_layers = static_cast<gaea::gaea_engine*>(gaea::engine)->profiling_num_entities();
			
			for (uint32 z = num_layers; z > 0; --z)
			{
				float32 min_entity_component = -(z / 2.f);
				for (uint32 i = 0; i < z; ++i)
				{
					for (uint32 j = 0; j < z; ++j)
					{
						spawn_params.scene.position = gaea::vector{ 20.f + min_entity_component + i, min_entity_component + j + 0.5f, (num_layers - z) + 1.f };

						spawn_params.graphics.color = gaea::basic_color(random_engine->generate_int(10, 15));

						world->spawn_entity<gaea::stacking_test_entity>(spawn_params);
					}
				}
			}

			{
				spawn_params.tick.stage = gaea::tick_stage::pre_physics;

				spawn_params.scene.position = gaea::vector{ 0.f, 0.f, 5.f };
				gaea::vector scale = spawn_params.scene.scaling = gaea::vector{ 2.f, 1.f, 1.f };

				float32 mass = spawn_params.physics.mass = 10.f;
				spawn_params.physics.friction_coefficient = 0.3f;
				spawn_params.physics.inertia = gaea::inertia_tensors::cuboid(mass, scale.x, scale.y, scale.z);
				spawn_params.graphics.color = gaea::basic_color::white;

				auto entity = world->spawn_entity<gaea::interact_test_entity>(spawn_params);
				entity->set_active(true);
			}
		},
		false,
		stdchr::seconds(0)
	);
}

GAEA_DECLARE_SCENE_INFO(env_4_stacking);