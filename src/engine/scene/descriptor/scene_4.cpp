#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/stacking_test_entity.hpp"

#include "system/timer_manager.hpp"
#include "system/random_engine.hpp"

void scene_4()
{
	auto random_engine = gaea::engine->access_random_engine();
	auto timer_manager = gaea::engine->access_timer_manager();

	timer_manager->add_timer(
		[&, random_engine]()
		{
			gaea::gaea_entity_spawn_parameters spawn_params;

			spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
			spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
			spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
			spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;
			spawn_params.graphics.color = gaea::basic_color::dark_gray;

			spawn_params.physics.geometry = gaea::simulation_primitives::cube;
			spawn_params.physics.mass = 1.f;
			spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);

			spawn_params.physics.restitution_coefficient = 0.1f;

			spawn_params.physics.mobility = gaea::body_mobility::immovable;
			spawn_params.scene.position = gaea::zero_vector;
			spawn_params.scene.scaling = gaea::vector{ 1000.f, 1000.f, 1.f };

			auto world = gaea::engine->access_world<gaea::gaea_world>();

			auto entity = world->spawn_entity<gaea::stacking_test_entity>(spawn_params);

			if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
			{
				spawn_params.graphics.instance_handle = graphics_body->instance_handle();
			}

			spawn_params.scene.position = gaea::vector{ 0.f, 0.f, 1000.f };

			world->spawn_entity<gaea::stacking_test_entity>(spawn_params);

			//spawn_params.scene.position = gaea::vector{ -500.f, 0.f, 500.f };
			//spawn_params.scene.scaling = gaea::vector{ 1.f, 1000.f, 1000.f };
			//
			//world->spawn_entity<gaea::stacking_test_entity>(spawn_params);
			//
			//spawn_params.scene.position = gaea::vector{ +500.f, 0.f, 500.f };
			//
			//world->spawn_entity<gaea::stacking_test_entity>(spawn_params);
			//
			//spawn_params.scene.position = gaea::vector{ 0.f, -500.f, 500.f };
			//spawn_params.scene.scaling = gaea::vector{ 1000.f, 1.f, 1000.f };
			//
			//world->spawn_entity<gaea::stacking_test_entity>(spawn_params);
			//
			//spawn_params.scene.position = gaea::vector{ 0.f, +500.f, 500.f };
			//
			//world->spawn_entity<gaea::stacking_test_entity>(spawn_params);

			spawn_params.physics.mobility = gaea::body_mobility::movable;
			spawn_params.scene.scaling = gaea::one_vector;
			//spawn_params.scene.scaling = gaea::vector{ 2.f, 2.f, 1.f };

			constexpr uint64 num_entities = 10000;
			constexpr uint64 num_entities_per_line = uint64(gaea::implementation::constant_sqrt(float64(num_entities)));
			constexpr int64 half_entities_per_line = num_entities_per_line / 2;
			for (std::size_t i = 0; i < num_entities_per_line; ++i)
			{
				for (std::size_t j = 0; j < num_entities_per_line; ++j)
				{
					spawn_params.scene.position = gaea::vector{ 20.f, 2.f * (-half_entities_per_line + int64(j)), 2.f * i + 2.f };

					spawn_params.graphics.color = gaea::basic_color(random_engine->generate_int(10, 15));
					
					world->spawn_entity<gaea::stacking_test_entity>(spawn_params);
				}
			}
		},
		false,
		stdchr::seconds(1)
	);
}

GAEA_DECLARE_SCENE_INFO(scene_4);
