#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/tick_test_entity.hpp"

#include "system/random_engine.hpp"
#include "system/timer_manager.hpp"

void scene_2()
{
	auto random_engine = gaea::engine->access_random_engine();
	
	auto timer_manager = gaea::engine->access_timer_manager();

	auto world = gaea::engine->access_world<gaea::gaea_world>();

	gaea::gaea_entity_spawn_parameters spawn_params;

	spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
	spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
	spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
	spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;

	//spawn_params.physics.geometry = gaea::simulation_primitives::cube;
	//spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);
	spawn_params.should_setup_physics = false;

	constexpr std::size_t num_entities = 16;
	for (std::size_t idx = 0; idx < num_entities; ++idx)
	{
		//spawn_params.scene.position = zero_vector;
		spawn_params.scene.position = gaea::vector{
			float32(random_engine->generate_int(-50, +50)),
			float32(random_engine->generate_int(-50, +50)),
			float32(random_engine->generate_int(-50, +50)),
		};

		//spawn_params.graphics.color = gaea::basic_color::orange;
		spawn_params.graphics.color = gaea::basic_color(random_engine->generate_int(10, 15));

		auto entity = world->spawn_entity<gaea::tick_test_entity>(spawn_params);

		timer_manager->add_timer(
			[entity, world]
			{
				world->despawn_entity(entity->uid());
			},
			false,
			stdchr::seconds(5)
		);

		if (idx == 0)
		{
			if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
			{
				spawn_params.graphics.instance_handle = graphics_body->instance_handle();
			}
		}
	}
}

GAEA_DECLARE_SCENE_INFO(scene_2);
