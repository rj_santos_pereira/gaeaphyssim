#include "core/core_minimal.hpp"

#include "engine/graphics/graphics_globals.hpp"
#include "engine/physics/physics_globals.hpp"
#include "engine/gaea_engine.hpp"
#include "engine/scene/gaea_entity.hpp"
#include "engine/scene/gaea_world.hpp"
#include "engine/scene/source_scene_loader.hpp"
#include "engine/scene/custom/stacking_test_entity.hpp"

#include "system/timer_manager.hpp"
#include "system/random_engine.hpp"

void env_2_pillar_fall()
{
	auto random_engine = gaea::engine->access_random_engine();
	auto timer_manager = gaea::engine->access_timer_manager();

	timer_manager->add_timer(
		[&, random_engine]()
		{
			gaea::gaea_entity_spawn_parameters spawn_params;

			spawn_params.graphics.vertex_array = gaea::rendering_primitives::cube::vertex_array;
			spawn_params.graphics.normal_array = gaea::rendering_primitives::cube::normal_array;
			spawn_params.graphics.uv_array = gaea::rendering_primitives::cube::uv_array;
			spawn_params.graphics.index_array = gaea::rendering_primitives::cube::index_array;
			spawn_params.graphics.color = gaea::basic_color::dark_gray;

			spawn_params.physics.geometry = gaea::simulation_primitives::cube;
			spawn_params.physics.mass = 1.f;
			spawn_params.physics.inertia = gaea::inertia_tensors::cube(spawn_params.physics.mass, 1.f);
			spawn_params.physics.restitution_coefficient = 0.1f;

			spawn_params.tick.stage = gaea::tick_stage::none;

			auto world = gaea::engine->access_world<gaea::gaea_world>();

			{
				spawn_params.physics.mobility = gaea::body_mobility::immovable;
				spawn_params.physics.entity_type = gaea::collision_entity::world_dynamic;
				spawn_params.physics.behavior_type = gaea::collision_behavior::world_dynamic;
				spawn_params.scene.scaling = gaea::vector{ 1000.f, 1000.f, 1.f };

				auto entity = world->spawn_entity<gaea::stacking_test_entity>(spawn_params);

				if (auto graphics_body = entity->access_graphics_body<gaea::gl_instanced_static_mesh_body>())
				{
					spawn_params.graphics.instance_handle = graphics_body->instance_handle();
				}
			}

			spawn_params.physics.mobility = gaea::body_mobility::movable;
			spawn_params.physics.entity_type = gaea::collision_entity::world_dynamic;
			spawn_params.physics.behavior_type = gaea::collision_behavior::world_dynamic;
			spawn_params.scene.scaling = gaea::one_vector;

			uint32 num_entities = 1000;
			//uint32 num_entities = static_cast<gaea::gaea_engine*>(gaea::engine)->profiling_num_entities();

			float32 entity_distance = 1.f;
			float32 min_entity_component = -4.5f * entity_distance;

			uint32 num_spawn_entities = 0;
			for (uint32 z = 0; num_spawn_entities < num_entities; ++z)
			{
				for (uint32 i = 0; i < 10 && num_spawn_entities < num_entities; ++i)
				{
					for (uint32 j = 0; j < 10 && num_spawn_entities < num_entities; ++j)
					{
						spawn_params.scene.position = gaea::vector{ min_entity_component + entity_distance * i, min_entity_component + entity_distance * j, 2.f + 2.f * z };

					    spawn_params.graphics.color = gaea::basic_color(random_engine->generate_int(10, 15));

						world->spawn_entity<gaea::stacking_test_entity>(spawn_params);
						++num_spawn_entities;
					}
				}
			}
		},
		false,
		stdchr::seconds(0)
	);
}

GAEA_DECLARE_SCENE_INFO(env_2_pillar_fall);