#include "engine/scene/gaea_camera_entity.hpp"

#include "system/input_manager.hpp"

namespace gaea
{
	gaea_camera_entity::gaea_camera_entity(entity_invoke_tag, gaea_camera_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, gaea_entity_spawn_parameters{
				{ 0.f, tick_stage::during_physics, true },
				false, {},
				false, {},
				{},
				std::move(parameters.name)
			} }
		, _camera{
				parameters.camera.field_of_view, parameters.camera.frustum_near_plane, parameters.camera.frustum_far_plane,
				parameters.camera.is_fov_horizontal
			}
		, _linear_power{ 50.f }
		, _linear_velocity{ zero_svector }
		, _angular_power{ 5.f * pi_f }
		, _angular_velocity{ zero_svector }
		, _position{ parameters.scene.position }
		, _rotation{ svector::unwind_euler(parameters.scene.rotation) * svector{ 0.f, 1.f, 1.f } }
		, _has_update{ true }
	{
	}

	void gaea_camera_entity::begin_run()
	{
		base::begin_run();

		_camera.setup_commands();

		auto input_manager = engine->access_input_manager();

		input_manager->bind_key_button(input_key::key_Q, this, std::bind(&gaea_camera_entity::_on_translate_camera, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_W, this, std::bind(&gaea_camera_entity::_on_translate_camera, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_E, this, std::bind(&gaea_camera_entity::_on_translate_camera, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_A, this, std::bind(&gaea_camera_entity::_on_translate_camera, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_S, this, std::bind(&gaea_camera_entity::_on_translate_camera, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_D, this, std::bind(&gaea_camera_entity::_on_translate_camera, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		auto on_rotate_camera_key_func = static_cast<void(gaea_camera_entity::*)(input_key, input_state, input_modifier)>(&gaea_camera_entity::_on_rotate_camera);
		auto on_rotate_camera_mouse_func = static_cast<void(gaea_camera_entity::*)(float32, float32, float32, float32, input_modifier)>(&gaea_camera_entity::_on_rotate_camera);

		input_manager->bind_key_button(input_key::key_up, this, std::bind(on_rotate_camera_key_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_down, this, std::bind(on_rotate_camera_key_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_left, this, std::bind(on_rotate_camera_key_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_right, this, std::bind(on_rotate_camera_key_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		input_manager->bind_mouse_drag(this, std::bind(on_rotate_camera_mouse_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));

		auto on_adjust_velocity_key_func = static_cast<void(gaea_camera_entity::*)(input_key, input_state, input_modifier)>(&gaea_camera_entity::_on_adjust_velocity);
		auto on_adjust_velocity_mouse_func = static_cast<void(gaea_camera_entity::*)(float32, float32, input_modifier)>(&gaea_camera_entity::_on_adjust_velocity);

		input_manager->bind_key_button(input_key::key_page_up, this, std::bind(on_adjust_velocity_key_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_page_down, this, std::bind(on_adjust_velocity_key_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		input_manager->bind_mouse_scroll(this, std::bind(on_adjust_velocity_mouse_func, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

		input_manager->bind_key_button(input_key::key_tab, this, std::bind(&gaea_camera_entity::_on_adjust_fov, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
		input_manager->bind_key_button(input_key::key_tab, this, std::bind(&gaea_camera_entity::_on_adjust_fov, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
	}

	void gaea_camera_entity::end_run()
	{
		auto input_manager = engine->access_input_manager();

		input_manager->unbind_key_button(input_key::key_Q, this);
		input_manager->unbind_key_button(input_key::key_W, this);
		input_manager->unbind_key_button(input_key::key_E, this);
		input_manager->unbind_key_button(input_key::key_A, this);
		input_manager->unbind_key_button(input_key::key_S, this);
		input_manager->unbind_key_button(input_key::key_D, this);

		input_manager->unbind_key_button(input_key::key_up, this);
		input_manager->unbind_key_button(input_key::key_down, this);
		input_manager->unbind_key_button(input_key::key_left, this);
		input_manager->unbind_key_button(input_key::key_right, this);

		input_manager->unbind_key_button(input_key::key_page_up, this);
		input_manager->unbind_key_button(input_key::key_page_down, this);
		
		input_manager->unbind_mouse_drag(this);
		input_manager->unbind_mouse_scroll(this);

		_camera.teardown_commands();
		
		base::end_run();
	}

	void gaea_camera_entity::tick(float32 delta)
	{
		base::tick(delta);
		
		if (!_has_update)
		{
			return;
		}

		// Override delta with actual delta (to be unaffected by time dilation)
		delta = float32(engine->frame_actual_delta());
		
		// Integrate orientation with respect to time
		_rotation += _angular_velocity * delta;
		
		// Clamp pitch to avoid non-intuitive orientations
		_rotation.y = std::clamp(_rotation.y, -89.97f, +89.97f);

		auto position_delta_vec = _linear_velocity * delta;
		
		// Convert rotation to quat for further calculation
		auto rotation_quat = squaternion::from_euler(_rotation);

		// Integrate position with respect to time and update it relative to the camera rotation
		// Ignore z for rotation and add half of it to the result
		_position += rotation_quat * svector{ position_delta_vec.x, position_delta_vec.y, 0.0f } + svector{ 0.0f, 0.0f, 0.5f * position_delta_vec.z };

		// Finally, set the updated values to the camera
		_camera.set_camera_position(_position);
		_camera.set_camera_rotation(rotation_quat);
	}

	void gaea_camera_entity::set_translation_speed(float32 power)
	{
		_linear_power = power;
	}

	void gaea_camera_entity::set_rotation_speed(float32 power)
	{
		_angular_power = power;
	}

	float32 gaea_camera_entity::get_translation_speed() const
	{
		return _linear_power;
	}

	float32 gaea_camera_entity::get_rotation_speed() const
	{
		return _angular_power;
	}

	gl_camera* gaea_camera_entity::access_camera()
	{
		return &_camera;
	}

	void gaea_camera_entity::_on_translate_camera(input_key key, input_state state, input_modifier)
	{
		if (state != input_state::state_released)
		{
			switch (key)
			{
			case input_key::key_W:
				_linear_velocity.x = +_linear_power;
				break;
			case input_key::key_A:
				_linear_velocity.y = +_linear_power;
				break;
			case input_key::key_E:
				_linear_velocity.z = +_linear_power;
				break;
			case input_key::key_S:
				_linear_velocity.x = -_linear_power;
				break;
			case input_key::key_D:
				_linear_velocity.y = -_linear_power;
				break;
			case input_key::key_Q:
				_linear_velocity.z = -_linear_power;
				break;
			}
		}
		else
		{
			switch (key)
			{
			case input_key::key_W:
				_linear_velocity.x = std::min(0.f, _linear_velocity.x);
				break;
			case input_key::key_A:
				_linear_velocity.y = std::min(0.f, _linear_velocity.y);
				break;
			case input_key::key_E:
				_linear_velocity.z = std::min(0.f, _linear_velocity.z);
				break;
			case input_key::key_S:
				_linear_velocity.x = std::max(0.f, _linear_velocity.x);
				break;
			case input_key::key_D:
				_linear_velocity.y = std::max(0.f, _linear_velocity.y);
				break;
			case input_key::key_Q:
				_linear_velocity.z = std::max(0.f, _linear_velocity.z);
				break;
			}
		}

		_has_update = _has_update || !_linear_velocity.exactly_zero();
	}

	void gaea_camera_entity::_on_rotate_camera(input_key key, input_state state, input_modifier modifier)
    {
		if (has_none_modifiers(modifier, input_modifier::modifier_ctrl))
		{
			if (state != input_state::state_released)
			{
				switch (key)
				{
				case input_key::key_up:
					_angular_velocity.y = -_angular_power * 2.0f;
					break;
				case input_key::key_down:
					_angular_velocity.y = +_angular_power * 2.0f;
					break;
				case input_key::key_left:
					_angular_velocity.z = +_angular_power * 2.0f;
					break;
				case input_key::key_right:
					_angular_velocity.z = -_angular_power * 2.0f;
					break;
				}
			}
			else
			{
				switch (key)
				{
				case input_key::key_up:
					_angular_velocity.y = std::max(0.f, _angular_velocity.y);
					break;
				case input_key::key_down:
					_angular_velocity.y = std::min(0.f, _angular_velocity.y);
					break;
				case input_key::key_left:
					_angular_velocity.z = std::min(0.f, _angular_velocity.z);
					break;
				case input_key::key_right:
					_angular_velocity.z = std::max(0.f, _angular_velocity.z);
					break;
				}
			}
		}

		_has_update = _has_update || !_angular_velocity.exactly_zero();
	}

	void gaea_camera_entity::_on_rotate_camera(float32, float32, float32 pos_delta_x, float32 pos_delta_y, input_modifier)
	{
		_angular_velocity = svector{ 0.f, +pos_delta_y, -pos_delta_x } * _angular_power;
		
		_has_update = _has_update || !_angular_velocity.exactly_zero();
	}

	void gaea_camera_entity::_on_adjust_fov(input_key, input_state state, input_modifier modifier)
	{
		if (state == input_state::state_pressed)
		{
			float32 fov_diff = has_all_modifiers(modifier, input_modifier::modifier_shift) ? -5.0f : +5.0f;

			float32 new_fov = std::clamp(_camera.frustum_field_of_view() + fov_diff, 45.0f, 135.0f);
			_camera.adjust_frustum_field_of_view(new_fov);
		}
	}

    void gaea_camera_entity::_on_adjust_velocity(input_key key, input_state state, input_modifier modifier)
    {
		if (state != input_state::state_released)
		{
			float32 delta_y = 0.f;

			switch (key)
			{
			case input_key::key_page_up:
				delta_y = +1.f;
				break;
			case input_key::key_page_down:
				delta_y = -1.f;
				break;
			}

			_on_adjust_velocity(0.f, delta_y, modifier);
		}
    }

    void gaea_camera_entity::_on_adjust_velocity(float32, float32 delta_y, input_modifier modifier)
	{
		if (has_all_modifiers(modifier, input_modifier::modifier_shift))
		{
			_angular_power = std::clamp(_angular_power + delta_y * pi_f * 0.5f, pi_f, 10.f * pi_f);
		}
		else
		{
			_linear_power = std::clamp(_linear_power + delta_y, 1.f, 100.f);
		}
	}
}
