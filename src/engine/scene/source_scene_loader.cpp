#include "engine/scene/source_scene_loader.hpp"

#include "engine/engine_config.hpp"
#include "system/config_manager.hpp"

namespace gaea
{
	source_scene_loader::source_scene_loader()
	{
	}

	void source_scene_loader::add_scene_info(std::string const& scene_name, void(*scene_address)())
	{
		if (!_scene_name_address_map.try_emplace(scene_name, scene_address).second)
		{
			GAEA_WARN(source_scene_loader, "Trying to add scene info with existing name [%s]!", scene_name.c_str());
		}
	}

	void source_scene_loader::remove_scene_info(std::string const& scene_name)
	{
		_scene_name_address_map.erase(scene_name);
	}

	bool source_scene_loader::load_scene(std::string const& scene_name)
	{
		auto it = _scene_name_address_map.find(scene_name);
		if (it == _scene_name_address_map.end())
		{
			GAEA_ERROR(source_scene_loader, "Trying to load scene with unknown name [%s]!", scene_name.c_str());
			return false;
		}

		auto& [_, address] = *it;
		address();

		return true;
	}

	bool source_scene_loader::load_scene_from_config()
	{
		if (!engine)
		{
			GAEA_ERROR(source_scene_loader, "Cannot access engine through global accessor pointer");
			return false;
		}

		auto config_manager = engine->access_config_manager();

		std::string scene_name;
		if (!config_manager->get_string_property(config::section::scene, config::property::default_scene_name, scene_name) || scene_name.empty())
		{
			GAEA_ERROR(source_scene_loader, "Cannot find default scene name in configuration file");
			return false;
		}

		return load_scene(scene_name);
	}

	source_scene_loader& source_scene_loader::access()
	{
		static source_scene_loader instance;
		return instance;
	}

	source_scene_storage::source_scene_storage(std::string const& scene_name, void(* scene_address)())
	{
		source_scene_loader::access().add_scene_info(scene_name, scene_address);
	}
}
