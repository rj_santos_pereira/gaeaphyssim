#include "engine/scene/gaea_light_entity.hpp"

#include "engine/engine_config.hpp"

namespace gaea
{
	gaea_light_entity::gaea_light_entity(entity_invoke_tag, gaea_light_entity_spawn_parameters parameters)
		: base{ entity_invoke_tag{}, gaea_entity_spawn_parameters{
				{ 0.f, tick_stage::none },
				false, {},
				false, {},
				{},
				std::move(parameters.name)
			} }
		, _light{
				parameters.scene.position, parameters.graphics.color, parameters.graphics.visibility
			}
	{
		set_ambient_factor(parameters.light.ambient_factor);
		set_diffuse_factor(parameters.light.diffuse_factor);
		set_specular_factor(parameters.light.specular_factor);
	}

	void gaea_light_entity::begin_run()
	{
		base::begin_run();

		_light.setup_shader_path(config::path::default_light_shaders);

		_light.setup_commands();
	}

	void gaea_light_entity::end_run()
	{
		_light.teardown_commands();
		
		base::end_run();
	}

	void gaea_light_entity::set_ambient_factor(float32 ambient_factor)
	{
		_light.set_ambient_factor(ambient_factor);
	}

	void gaea_light_entity::set_diffuse_factor(float32 diffuse_factor)
	{
		_light.set_diffuse_factor(diffuse_factor);
	}

	void gaea_light_entity::set_specular_factor(float32 specular_factor)
	{
		_light.set_specular_factor(specular_factor);
	}

	float32 gaea_light_entity::get_ambient_factor() const
	{
		return _light.get_ambient_factor();
	}

	float32 gaea_light_entity::get_diffuse_factor() const
	{
		return _light.get_diffuse_factor();
	}

	float32 gaea_light_entity::get_specular_factor() const
	{
		return _light.get_specular_factor();
	}

	gl_point_light* gaea_light_entity::access_light()
	{
		return &_light;
	}
}
